package "json_value";

#include "../json-parser/json.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

import stream from "../stream/stream.module.c";
import String from "./string.module.c";
import json from "./json.module.c";

static int type() {
	static int t;
	if (t == 0) t = json.register("value");
	return t;
}

export typedef struct {
	void       * value;
	json_value * parsed;
	json.t     * type;
} json_value_t as t;


static int marshal_json_value(json.marshal_args * args, json_value * v) {
	stream.t * s = args->dest;
	bool pretty = args->pretty;

	unsigned int i;
	unsigned int length = 0;

	switch (v->type) {
		case json_null:
			return stream.write(s, "null", 4);
		case json_boolean:
			return stream.write(s, v->u.boolean ? "true" : "false", v->u.boolean ? 4 : 5);
		case json_integer:
			return stream.printf(s, "%ld", v->u.integer);
		case json_double:
			return stream.printf(s, "%lf", v->u.dbl);
		case json_string:
			return String.encode(v->u.string.length, s, v->u.string.ptr);
		case json_object:
			length = v->u.object.length;
			stream.write(s, "{\n", pretty ? 2 : 1);

			if (pretty) args->indent += args->indent_width;
			for (i = 0; i < length; i++) {
				json_object_entry entry = v->u.object.values[i];
				if (pretty) stream_printf(s, "%*s", args->indent, " ");
				String.encode(entry.name_length, s, entry.name);
				stream.write(s, ": ", pretty ? 2 : 1); 

				marshal_json_value(args, entry.value);
				if (i < length - 1) {
					stream.write(s, ",\n", pretty ? 2 : 1);
				} else if (pretty) {
					stream.write(s, "\n", 1);
				}
			}
			if (pretty) args->indent -= args->indent_width;

			return stream.printf(s, "%*.s}", args->indent, " ");
		case json_array:
			length = v->u.array.length;
			stream.write(s, "[\n", pretty ? 2 : 1);

			if (pretty) args->indent += args->indent_width;
			for (i = 0; i < length; i++) {
				stream.printf(s, "%*.s", args->indent, " ");
				marshal_json_value(args, v->u.array.values[i]);
				if (i < length - 1) {
					stream.write(s, ",\n", pretty ? 2 : 1);
				} else if (pretty) {
					stream.write(s, "\n", 1);
				}
			}
			if (pretty) args->indent -= args->indent_width;

			return stream.printf(s, "%*.s]", args->indent, " ");
	}
}

static int value_marshal(void *ctx, json.marshal_args * args, const void * src) {
	stream.t * s = args->dest;
	bool pretty = args->pretty;

	json_value_t * val = (json_value_t *)src;
	if (val == NULL || (val->parsed == NULL && val->value == NULL)) {
		return stream.write(s, "null", strlen("null"));
	}

	if (val->type && val->value) {
		return json.marshal_ex(val->type, args, val->value);
	}

	json_value * v = val->parsed;
	return marshal_json_value(args, v);
}

static int value_unmarshal(void * ctx, void * dest, json_value * src) {
	if (dest == NULL) return -1;

	json_value_t * v = (json_value_t *) dest; 
	v->parsed = src;

	if (v->type && v->value) {
		return json.unmarshal_ex(v->type, v->value, v->parsed);
	} else {
		v->value = v->parsed;
		v->type = NULL;
	}

	return 0;
}


export json.t * j() {
	static json.t v;
	if (v.type == 0) {
		v.type       = type();
		v.ctx        = NULL;
		v.is_pointer = true;
		v.marshal    = value_marshal;
		v.unmarshal  = value_unmarshal;
	}
	return &v;
}

export json_value_t from(json.t * type, void * value) {
	json_value_t v = {
		.type = type,
		.parsed = NULL,
		.value = value,
	};
	
	return v;
}

export json_value_t null() {
	json_value_t v = {0};
	return v;
}
