build depends "../json-parser/json.c";
build append LDLIBS "-lm";

export #include "../json-parser/json.h"
export #include <stdbool.h>

import stream from "../stream/stream.module.c";

export typedef struct {
	bool pretty;
	unsigned indent_width;
	unsigned indent;
	stream.t * dest;
} marshal_args;

export typedef int (*marshal_t  )(void * ctx, marshal_args * args, const void * src);
export typedef int (*unmarshal_t)(void * ctx, void * dest, json_value * src);

static struct {
	const char ** names;
	size_t        size;
} registered = {0};

export typedef struct {
	int         type;
	void      * ctx;
	bool        is_pointer;
	marshal_t   marshal;
	unmarshal_t unmarshal;
} json_t as t;

export int register(const char * name){
	if (registered.size == 0) { // init
		registered.names    = malloc(1 * sizeof(char*));
		registered.names[0] = "error";
		registered.size     = 1;
	}
	int next = registered.size;
	registered.names = realloc(registered.names, (registered.size + 1) * sizeof(char*));

	registered.names[next] = name;
	registered.size++;

	return next;
}

export int marshal_ex(json_t *j, marshal_args * args, void * src) {
	return j->marshal(j->ctx, args, src);
}

export int marshal(json_t *j, stream.t * dest, void * src) {
	marshal_args args = {
		.dest         = dest,
		.pretty       = 0,
		.indent_width = 0,
		.indent       = 0,
	};
	return marshal_ex(j, &args, src);
}

export int marshal_pretty(json_t *j, stream.t * dest, void * src, short indent) {
	marshal_args args = {
		.dest         = dest,
		.pretty       = 1,
		.indent_width = indent,
		.indent       = 0,
	};
	return marshal_ex(j, &args, src);
}

export int unmarshal_ex(json_t * j, void * dest, json_value * value) {
	return j->unmarshal(j->ctx, dest, value);
}

export int unmarshal(json_t * j, void * dest, const char * src, size_t length) {
	json_value * value = json_parse(src, length);
	if (value == NULL) {
		return -1;
	}
	return unmarshal_ex(j, dest, value);
}

export bool is_null(json_t * j, void * src) {
	if (j->is_pointer) {
		void ** p = (void **)src;
		return p == NULL || *p == NULL;
	}
	return false;
}
