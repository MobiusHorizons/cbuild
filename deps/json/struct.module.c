package "json_struct";

build depends "../hash/hash.c";
#include "../hash/hash.h"
#include <stdio.h>
#include <string.h>

import stream from "../stream/stream.module.c";
import string from "./string.module.c";
import json from "json.module.c";

export #include "../json-parser/json.h"
export #include <stddef.h>

static int type() {
	static int t;
	if (t == 0) t = json.register("struct");
	return t;
}

typedef struct field_t {
	char   * name;
	size_t   offset;
	json.t * type;
	struct field_t * next;
} field_t;

typedef struct {
	hash_t * fields;
	field_t * first;
	field_t * last;
	size_t length;
	size_t size;
} struct_ctx_t;

static int struct_marshal(void * _ctx, json.marshal_args * args, const void * src) {
	struct_ctx_t * ctx = (struct_ctx_t*) _ctx;

	stream.write(args->dest, "{", 1);
	args->indent += args->indent_width;
	field_t * first = ctx->first;
	field_t * f = first;
	while(f != NULL) {
		if (json.is_null(f->type, ((char*)src) + f->offset)) {
			if (f == first) first = f->next;
			f = f->next;
			continue;
		} else if (f != first) {
				stream.write(args->dest, ",", 1);
		}
		if (args->indent > 0) {
			stream.printf(args->dest, "\n%*.s", args->indent, " ");
		}
		string.encode(0, args->dest, f->name);
		stream.write(args->dest, ": ", args->pretty ? 2 : 1);
		json.marshal_ex(f->type, args, ((char*)src) + f->offset);
		f = f->next;
	}
	args->indent -= args->indent_width;
	if (args->pretty) {
		stream.printf(args->dest, "\n%*.s}", args->indent, " ");
	} else {
		stream.write(args->dest, "}", 1);
	}
}

static int struct_unmarshal(void * _ctx, void * dest, json_value * src) {
	struct_ctx_t * ctx = (struct_ctx_t*) _ctx;

	if (src == NULL) {
		return -1;
	}
	if (src->type != json_object) {
		return -1;
	}

	size_t size = 0;
	int length = src->u.object.length;
	int i;
	for (i = 0; i < length; i++) {
		json_object_entry entry = src->u.object.values[i];
		field_t * field = hash_get(ctx->fields, entry.name);
		if (field != NULL) {
			json.unmarshal_ex(field->type, dest + field->offset, entry.value);
		}
	}
}

export json.t * new() {
	json_t * j = malloc(sizeof(json_t));

	struct_ctx_t * ctx = malloc(sizeof(struct_ctx_t));
	ctx->fields = hash_new();
	ctx->length = 0;
	ctx->first  = NULL;
	ctx->last   = NULL;

	j->type       = type();
	j->is_pointer = false;
	j->ctx        = ctx;
	j->marshal    = struct_marshal;
	j->unmarshal  = struct_unmarshal;

	return j;
}

export int field(json.t * j, const char * name, size_t offset, json_t * field_type) {
	if (j->type != type()) return 0;

	struct_ctx_t * ctx = (struct_ctx_t*) j->ctx;

	field_t * field = malloc(sizeof(field_t));
	field->name     = strdup(name);
	field->offset   = offset;
	field->type     = field_type;
	field->next     = NULL;

	if (ctx->first == NULL) ctx->first = field;
	if (ctx->last != NULL) ctx->last->next = field;
	ctx->last = field;

	hash_set(ctx->fields, field->name, field);
	ctx->length ++;
	return 1;
}
