package "main";

#include <string.h>

import stream from "../stream/stream.module.c";
import file from "../stream/file.module.c";
import string from "./string.module.c";
import json from "./json.module.c";

const char * test_inputs[] = {
	"\"hello world\"",
	"\\backslash",
	"\a\b\e\f\x01\x02\x03\x1a",
};

const char * test_outputs[] = {
	"\"hello world\"",
	"\"\\\\backslash\"",
	"\"\\a\\b\\e\\f\\x01\\x02\\x03\\x1a\"",
};

int main() {
	stream.t * out = file.new(1);

	int i;
	for (i = 0; i < sizeof(test_inputs) / sizeof(test_inputs[0]); i++ ) {
		stream.printf(out, "'%s' => ", test_inputs[i]);
		string.encode(0, out, test_inputs[i]);
		stream.write(out, "\n", 1);
	}

	char * s = NULL;
	json.t * String = string.new(0);
	for (i = 0; i < sizeof(test_outputs) / sizeof(test_outputs[0]); i++ ) {
		json.unmarshal(String, &s, test_outputs[i], strlen(test_outputs[i]));
		stream.printf(out, "'%s' => '%s'\n", test_outputs[i], s);
	}
}
