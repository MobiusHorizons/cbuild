package "json_string";

#define BUFFER_SIZE 4096

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

import stream from "../stream/stream.module.c";
import json from "./json.module.c";

export #include "../json-parser/json.h"

static int type() {
	static int t;
	if (t == 0) t = json.register("string");
	return t;
}

export int encode(size_t len, stream.t * dest, const char * start) {
	char buffer[BUFFER_SIZE];

	if (len == 0) len = strlen(start);

	const char * s = start;
	char * d = buffer;

	size_t output_bytes = 0;

	*(d++) = '"';
	while(s - start < len) {
		if (d - buffer > BUFFER_SIZE - 5) {
			output_bytes += stream.write(dest, buffer, d - buffer);
			d = buffer;
		}
		switch(*s) {
			case '\\':
			case '"':
				*(d++) = '\\';
				*(d++) = *s;
				break;

			case 8:
				*(d++) = '\\';
				*(d++) = 'b';
				break;

			case 9:
				*(d++) = '\\';
				*(d++) = 't';
				break;

			case 10:
				*(d++) = '\\';
				*(d++) = 'n';
				break;
				
			case 12:
				*(d++) = '\\';
				*(d++) = 'f';
				break;

			case 13:
				*(d++) = '\\';
				*(d++) = 'r';
				break;
			default:
				if (0 <= *s && *s <= 0x1F) {
					d += snprintf(d, 5, "\\x%.2X", *s);
				} else {
					*(d++) = *s;
				}
		}
		s++;
	}

	*(d++) = '"';
	*d = 0;

	return output_bytes + stream.write(dest, buffer, d - buffer);
}

static int string_marshal(void * ctx, json.marshal_args * args, const void * src) {
	size_t len = (size_t)ctx;
	char ** start = (char **) src;
	if (start == NULL || *start == NULL) return stream.write(args->dest, "null", 4);
	return encode (len, args->dest, *start);
}

static int string_unmarshal(void * ctx, void * dest, json_value * value) {
	size_t len = (size_t)ctx;
	void ** ptr = (void **) dest;
	if (value->type == json_null) {
		*ptr = NULL;
		return 0;
	}
	if (value->type != json_string) {
		return -1;
	}

	char * s = value->u.string.ptr;
	if (len == 0) {
		*ptr = strdup(s);
	} else {
		if (value->u.string.length < len) {
			len = value->u.string.length;
		}
		strncpy(*ptr, s, len);
	}
	return 0;
}

export json.t * new(size_t size) {
	json_t * j = malloc(sizeof(json_t));

	j->type       = type();
	j->is_pointer = true;
	j->ctx        = (void*) size;
	j->marshal    = string_marshal;
	j->unmarshal  = string_unmarshal;

	return j;
}

static int char_marshal(void * ctx, json.marshal_args * args, const void * src) {
	char * c = (char *) src;
	if (*c == 0) {
		const char q[2] = { '"', '"', };
		return stream.write(args->dest, q, 2);
	}

	return encode(1, args->dest, c);
}


static int char_unmarshal(void * ctx, void * dest, json_value * value) {
	if (value->type != json_string) {
		return -1;
	}

	char * c = (char *) dest;
	*c = value->u.string.ptr[0];
	return 0;
}

export json.t * char() {
	json_t * j = malloc(sizeof(json_t));

	j->type       = type();
	j->is_pointer = false;
	j->ctx        = (void*) 1;
	j->marshal    = char_marshal;
	j->unmarshal  = char_unmarshal;

	return j;
}
