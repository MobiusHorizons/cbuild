package "json_boolean";

export #include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "../json-parser/json.h"

import stream from "../stream/stream.module.c";
import json from "./json.module.c";

static int type() {
	static int t;
	if (t == 0) t = json.register("boolean");
	return t;
}

static int boolean_marshal(void * _ctx, json.marshal_args * args, const void * src) {
	bool * val = (bool*)src;
	return stream.write(args->dest, (*val) ? "true" : "false", (*val) ? 4 : 5);
}

static int boolean_unmarshal(void * _ctx, void * dest, json_value * src) {
	bool * p = (bool*)dest;
	*p = src->u.dbl;
	return 0;
}

export json.t * new() {
	static json_t * j;

	if (j == NULL) {
		j = malloc(sizeof(json_t));

		j->ctx        = NULL;
		j->type       = type();
		j->is_pointer = false;
		j->marshal    = boolean_marshal;
		j->unmarshal  = boolean_unmarshal;
	}

	return j;
}
