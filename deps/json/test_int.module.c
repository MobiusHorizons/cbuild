package "main";

#include "../json-parser/json.h"

import json from "./json.module.c";
import stream from "../stream/stream.module.c";
import file from "../stream/file.module.c";
import integer from "./integer.module.c";

int main() { 
	stream.t * out = file.new(1);

	json.t * Uint8_t = integer.new(1, 0);
	json.t * Int8_t = integer.new(1, 1);

	unsigned char a;
	char b;

	int e;
	e = json.unmarshal(Uint8_t, &a, "255", 3);
	stream.printf(out, "e = %d, a = %d\n", e, a);

	e = json.unmarshal(Int8_t, &b, "128", 3);
	stream.printf(out, "e = %d, b = %d\n", e, b);
}
