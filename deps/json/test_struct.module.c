package "main";

#include <stddef.h>
#include <string.h>

import stream from "../stream/stream.module.c";
import file from "../stream/file.module.c";

import json from "./json.module.c";
import Struct from "./struct.module.c";
import string from "./string.module.c";
import integer from "./integer.module.c";
import pointer from "./pointer.module.c";
import value from "./value.module.c";

struct date_t {
	unsigned char  day;
	unsigned char  month;
	unsigned short year;
};

struct person_t {
	char * name;
	char * email;
	struct date_t * birthdate;
	/*json_value * birthdate;*/
};

int main() {
	// Date type
	json.t * date_t = Struct.new();
	Struct.field(date_t, "day", offsetof(struct date_t, day), integer.uint8());
	Struct.field(date_t, "month", offsetof(struct date_t, month), integer.uint8());
	Struct.field(date_t, "year", offsetof(struct date_t, year),
			integer.new(sizeof(unsigned short), 0));

	 /*person type*/
	json.t * person_t = Struct.new();
	Struct.field(person_t, "name", offsetof(struct person_t, name), string.new(0));
	Struct.field(person_t, "email", offsetof(struct person_t, email), string.new(0));
	Struct.field(person_t, "birthdate", offsetof(struct person_t, birthdate), 
			/*offsetof(struct person_t, birthdate), value.new());*/
			pointer.new(sizeof(struct date_t), date_t));

	// example
	
	struct date_t birthdate = {
		.day = 21,
		.month = 2,
		.year = 1970,
	};
	struct person_t eden = {
		.name = "Eden",
		.email = "test",
		.birthdate = &birthdate,
	};

	stream.t * out = file.new(1);
	json.marshal_pretty(person_t, out, &eden, 4);
}
