package "main";

#include <stddef.h>
#include <string.h>

import stream from "../stream/stream.module.c";
import file from "../stream/file.module.c";

import json from "./json.module.c";
import string from "./string.module.c";
import pointer from "./pointer.module.c";
import integer from "./integer.module.c";
import array from "./array.module.c";

typedef	struct {
	unsigned short length;
	char ** items;
} array_t; 

int main() {
	json.t * nullTerminated = array.of_pointers(string.new(0));
	json.t * lengthy = array.with_length(
		sizeof(void*), string.new(0),
		offsetof(array_t, length) - offsetof(array_t, items),
		2, false
	);

	stream.t * out = file.new(1);

	char ** v = NULL;
	array_t a = {0};

	const char * js = "[\"test\", \"test2\"]";

	stream.printf(out, "Parsing the following json: '%s'\n", js);
	json.unmarshal(nullTerminated, &v, js, strlen(js));
	json.marshal_pretty(nullTerminated, out, &v, 4);

	json.unmarshal(lengthy, &a.items, js, strlen(js));
	json.marshal_pretty(lengthy, out, &a.items, 4);
}

