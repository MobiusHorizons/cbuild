package "main";

#include <string.h>

import stream from "../stream/stream.module.c";
import file from "../stream/file.module.c";
import json from "./json.module.c";
import value from "./value.module.c";

int main() {
	stream.t * out = file.new(1);
	const char * js = "{ \"name\": \"Paul Martin\", \"email\": \"example@gmail.com\", \"birthdate\": { \"day\": 1, \"month\": 2, \"year\": 1990 }}";

	stream.printf(out, "Parsing the following json: '%s'\n", js);
	json_value * v = NULL;
	json.unmarshal(value.new(), &v, js, strlen(js));
	json.marshal_pretty(value.new(), out, &v, 4);
}
