package "json_pointer";

#include "../json-parser/json.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

import stream from "../stream/stream.module.c";
import json from "./json.module.c";

typedef struct {
	size_t size;
	json.t * field;
} ptr_ctx_t;

static int type() {
	static int t;
	if (t == 0) t = json.register("pointer");
	return t;
}

static int pointer_marshal(void *_ctx, json.marshal_args * args, const void * src) {
	ptr_ctx_t * ctx = (ptr_ctx_t*)_ctx;

	if (src) {
		void * const* ptr = (void * const*)src;
		if (*ptr != NULL) {
			return json.marshal_ex(ctx->field, args, *ptr);
		}
	}
	return stream.write(args->dest, "null", 4);
}

static int pointer_unmarshal(void * _ctx, void * dest, json_value * src) {
	ptr_ctx_t * ctx = (ptr_ctx_t*) _ctx;
	void ** ptr = (void **)dest;

	if (dest == NULL) {
		return -1;
	}

	if (src->type == json_null) {
		*ptr = NULL;
		return 0;
	}

	*ptr = malloc(ctx->size);
	memset(*ptr, 0, ctx->size);
	return json.unmarshal_ex(ctx->field, *ptr, src);
}

export json.t * new(size_t size, json.t * field) {
	json.t * j = malloc(sizeof(json.t));

	ptr_ctx_t * ctx = malloc(sizeof(ptr_ctx_t));
	ctx->size  = size;
	ctx->field = field;

	j->type       = type();
	j->is_pointer = true;
	j->ctx        = ctx;
	j->marshal    = pointer_marshal;
	j->unmarshal  = pointer_unmarshal;

	return j;
}
