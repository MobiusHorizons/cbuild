package "json_float";

export #include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../json-parser/json.h"

import stream from "../stream/stream.module.c";
import json from "./json.module.c";

static int type() {
	static int t;
	if (t == 0) t = json.register("float");
	return t;
}

static int float_marshal(void * _ctx, json.marshal_args * args, const void * src) {
	bool is_double = (bool)_ctx;

	if (is_double) {
		double * val = (double *)src;
		return stream.printf(args->dest, "%lf", *val);
	} else {
		float * val = (float *)src;
		return stream.printf(args->dest, "%f", val);
	}
}

static int float_unmarshal(void * _ctx, void * dest, json_value * src) {
	bool is_double = (bool)_ctx;

	if (is_double) {
		double * p = (double*)dest;
		*p = src->u.dbl;
	} else {
		float * p = (float*)dest;
		float val = src->u.dbl;
		*p = val;
	}
	return 0;
}

export json.t * new(bool is_double) {
	json_t * j = malloc(sizeof(json_t));

	j->ctx        = (void*) is_double; 
	j->type       = type();
	j->is_pointer = false;
	j->marshal    = float_marshal;
	j->unmarshal  = float_unmarshal;

	return j;
}

export json.t * float32() {
	static json_t * j;
	if (j == NULL) j = new(false);
	return j;
}

export json.t * float64() {
	static json_t * j;
	if (j == NULL) j = new(true);
	return j;
}
