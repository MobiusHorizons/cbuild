package "main";

import json from "../json/json.module.c";
import Struct from "../json/struct.module.c";
import string from "../json/string.module.c";
import integer from "../json/integer.module.c";
import value from "../json/value.module.c";
import array from "../json/array.module.c";
import pointer from "../json/pointer.module.c";

import stream from "../deps/stream_module/stream.module.c";
import file from "../deps/stream_module/file.module.c";
import closure from "../deps/closure_module/closure.module.c";
import server from "server.module.c";
import Req from "request_msg.module.c";

typedef struct {
	stream.t * out;
	stream.t * err;
} state_t;

void * log_packet(void * _ctx, void * _arg) {
	state_t * ctx = (state_t*) _ctx;
	char * message = (char *) _arg;

	stream.printf(ctx->out, "%s\n", message);
	return NULL;
}

void * log_error(void * _ctx, void * _arg) {
	state_t * ctx = (state_t*) _ctx;
	char * message = (char *) _arg;

	stream.printf(ctx->err, "%s\n", message);
	return NULL;
}

typedef struct {
	long long int line;
	long long int character;
} position_t;

json.t * position_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "line", offsetof(position_t, line), integer.uint64());
		Struct.field(j, "character", offsetof(position_t, character), integer.uint64());
	}
	return j;
}

typedef struct {
	position_t * start;
	position_t * end;
} range_t;

json.t * range_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "start", offsetof(range_t, start), 
				pointer.new(sizeof(position_t), position_j()));
		Struct.field(j, "end", offsetof(range_t, end), 
				pointer.new(sizeof(position_t), position_j()));
	}
	return j;
}

typedef struct {
	range_t * range;
	long long int range_length;
	char * text;
} text_change_t;

json.t * text_change_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "range", offsetof(text_change_t, range), 
				pointer.new(sizeof(range_t), range_j()));
		Struct.field(j, "rangeLength", offsetof(text_change_t, range_length),
				integer.uint64());
		Struct.field(j, "text", offsetof(text_change_t, text),
				string.new(0));
	}
	return j;
}

typedef struct {
	char * text;
	char * uri;
	char * languageId;
	unsigned long long version;
} text_document_t;

json.t * text_document_j() {
	static json_t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "text", offsetof(text_document_t, text), string.new(0));
		Struct.field(j, "version", offsetof(text_document_t, version), integer.uint64());
		Struct.field(j, "uri", offsetof(text_document_t, uri), string.new(0));
		Struct.field(j, "languageId", offsetof(text_document_t, languageId), string.new(0));
	}
	return j;
}

typedef struct {
	text_document_t doc;
} did_open_params_t;

void * text_document_did_open(void * _ctx, void * _arg) {
	state_t * ctx = (state_t*) _ctx;
	server.handler_args_t * args = (server.handler_args_t*)_arg;

	json.t * params_j = Struct.new();
	Struct.field(params_j, "textDocument", offsetof(did_open_params_t, doc), text_document_j());


	did_open_params_t params = {0};
	json.unmarshal_ex(params_j, &params, args->req.params);
	json.marshal_pretty(text_document_j(), ctx->err, &params.doc, 4);

	return NULL;
}

typedef struct {
	text_document_t doc;
	text_change_t * changes;
	unsigned long count;
} did_change_params_t;

void * text_document_did_change(void * _ctx, void * _arg) {
	state_t * ctx = (state_t*) _ctx;
	server.handler_args_t * args = (server.handler_args_t*)_arg;

	json.t * params_j = Struct.new();
	Struct.field(params_j, "contentChanges", 
			offsetof(did_change_params_t, changes),
			array.with_length(
				sizeof(text_change_t), text_change_j(),
				offsetof(did_change_params_t, changes) -
				offsetof(did_change_params_t, count),
				sizeof(unsigned long), false
			)
		);
	Struct.field(params_j, "textDocument", offsetof(did_change_params_t, doc),text_document_j());


	did_change_params_t params = {0};
	json.unmarshal_ex(params_j, &params, args->req.params);
	json.marshal_pretty(params_j, ctx->err, &params, 4);
	/*json.marshal_pretty(value.j(), ctx->err, &args->req.params, 4);*/
	/*stream.printf(ctx->err, "Method<%s> ", m->method);*/
	/*log_json(ctx->err, m->params, 0);*/
	/*stream.write(ctx->err, "\n", 1);*/
	return NULL;
}

int main() {
	stream.t * in = file.new(0);
	stream.t * err = file.new(2);

	state_t s = {
		.err = err,
	};



	server.t * c = server.new(in);

	server.register(c, 
		"textDocument/didOpen",
		closure.new(text_document_did_open, &s)
	);

	server.register(c, 
		"textDocument/didChange",
		closure.new(text_document_did_change, &s)
	);

	return server.listen(c);
}
