package "rpc_encode";

#include <stdlib.h>

import stream from "../../deps/stream/stream.module.c";
import string from "../../utils/string-stream.module.c";

export typedef struct {
	stream.t * out;
	stream.t * buffer;
} encoder_t as t; 

export encoder_t * new(stream.t * out) {
	encoder_t * e = malloc(sizeof(encoder_t));

	e->out    = out;
	e->buffer = string.new_writer(NULL);

	return e;
}

export stream.t * stream(encoder_t * e) {
	return e->buffer;
}

export int send(encoder_t * e) {
	size_t length = string.get_buffer_length(e->buffer);
	char * buf = string.get_buffer(e->buffer);

	stream.printf(e->out, "Content-Length: %ld\r\n\r\n", length);
	stream.write(e->out, buf, length);

	stream.close(e->buffer);
	e->buffer = string.new_writer(NULL);

	return length;
}
