package "rpc_request_msg";

import json    from "../../deps/json/json.module.c";
import Struct  from "../../deps/json/struct.module.c";
import string  from "../../deps/json/string.module.c";
import integer from "../../deps/json/integer.module.c";
import value   from "../../deps/json/value.module.c";

export typedef struct {
	char       * jsonrpc;
	char       * method;
	value.t      params;
	value.t      id;
} request_t as t;

export json.t * j() {
	static json_t * r;
	if (r == NULL) {
		r = Struct.new();
		Struct.field(r, "jsonrpc", offsetof(request_t, jsonrpc), string.new(0));
		Struct.field(r, "method", offsetof(request_t, method), string.new(0));
		Struct.field(r, "params", offsetof(request_t, params), value.j());
		Struct.field(r, "id", offsetof(request_t, id), value.j());
	}
	return r;
}
