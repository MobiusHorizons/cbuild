package "rpc_response";

import json    from "../../deps/json/json.module.c";
import Struct  from "../../deps/json/struct.module.c";
import string  from "../../deps/json/string.module.c";
import integer from "../../deps/json/integer.module.c";
import value   from "../../deps/json/value.module.c";
import pointer from "../../deps/json/pointer.module.c";

export typedef struct {
	long long    code;
	char       * message;
	value.t      data;
} error_t;

export json.t * error_j() {
	static json_t * r;
	if (r == NULL) {
		r = Struct.new();
		Struct.field(r, "code", offsetof(error_t, code), integer.int64());
		Struct.field(r, "message", offsetof(error_t, message), string.new(0));
		Struct.field(r, "data", offsetof(error_t, data), value.j());
	}
	return r;
}

export typedef struct {
	char       * jsonrpc;
	value.t      result;
	error_t    * error;
	value.t      id;
} response_t as t;

export json.t * response_j() as j {
	static json_t * r;
	if (r == NULL) {
		r = Struct.new();
		Struct.field(r, "jsonrpc", offsetof(response_t, jsonrpc), string.new(0));
		Struct.field(r, "result", offsetof(response_t, result), value.j());
		Struct.field(r,
			"error",
			offsetof(response_t, error),
			pointer.new(sizeof(error_t), error_j())
		);
		Struct.field(r, "id", offsetof(response_t, id), value.j());
	}
	return r;
}

export error_t * error(long long code, const char * message, value.t data) {
	error_t * e = malloc(sizeof(error_t));

	e->code    = code;
	e->message = message;
	e->data    = data;

	return e;
}
