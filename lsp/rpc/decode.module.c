package "rpc_decode";

build append CFLAGS "-D_GNU_SOURCE";

#define BUFSIZE 4096

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdbool.h>

import stream  from "../../deps/stream/stream.module.c";
import closure from "../../deps/closure/closure.module.c";

export typedef struct {
	stream.t * input;
	char * buffer;
	char * cursor;
	size_t length;
	size_t buffer_length;
	closure.t on_message;
	closure.t on_error;
} state_t as t;

typedef struct {
	char * start;
	size_t length;
} substring_t;

int next_page(state_t * s) {
	size_t offset = s->length - (s->cursor - s->buffer);
	memmove(s->buffer, s->cursor, offset); // move bytes to the beginning.
	s->cursor = s->buffer;
	s->length = offset;
	size_t read = stream.read(s->input, s->cursor + offset, s->buffer_length - s->length);
	if (read < 0) {
		// onError;
		return 0;
	} else if (read == 0) {
		return 0; // end but not an error.
	} 

	s->length += read;
	return 1;
}

substring_t next_header(state_t * s) {
	char * end = NULL;
	do {
		end = memmem(s->cursor, s->length - (s->cursor - s->buffer), "\r\n", 2);
		if (end == NULL) {
			if (!next_page(s)) return (substring_t){0};
		}
	} while (end == NULL);
	substring_t header = {
		.start  = s->cursor,
		.length = end - s->cursor,
	};
	s->cursor = end + 2;
	return header;
}

int handle_body(state_t * s, size_t length) {
	char * message = malloc(length + 1);
	message[length] = 0;

	size_t remaining_buffer = s->length - (s->cursor - s->buffer);
	size_t from_buffer = remaining_buffer >= length ? length : remaining_buffer;

	if (memcpy(message, s->cursor, from_buffer) == NULL) {
		perror("error copying memory");
		return 1;
	}
	s->cursor += from_buffer;
	size_t got = from_buffer;

	while (got < length) {
		size_t read = stream.read(s->input, message + got, length - got);
		if (read < 0) {
			return 3;
		}
		got += read;
	}
	closure.call(s->on_message, message);
	return 0;
}


export state_t * new(stream.t * input, closure.t on_message, closure.t on_error) {
	state_t * s = malloc(sizeof(state_t) + BUFSIZE);
	s->input = input;
	s->buffer = (char *)s + sizeof(state_t);
	s->buffer_length = BUFSIZE;
	s->length = 0;
	s->cursor = s->buffer;
	s->on_message = on_message;
	s->on_error = on_error;

	return s;
}

export int next(state_t * s) {
	size_t content_length = 0;
	while(true) {
		substring_t header = next_header(s);

		if (header.length == 0){
			if (content_length == 0) {
				closure.call(s->on_error, "Expected Content-Length:");
				return 1;
			}
			return handle_body(s, content_length);
		}
		if (header.length < strlen("content-length:")) continue; // ignore headers other than content-length
		if (strncasecmp(header.start, "content-length:", strlen("content-length:")) == 0) {
			content_length = strtoul(header.start + strlen("content-length:"), NULL, 10);
		}
	}
}

export int listen(state_t * s) {
	int e = 0;

	while((e = next(s)) == 0);

	return e;
}
