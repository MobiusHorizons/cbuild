import stream  from "../../deps/stream/stream.module.c";

static int _type;

export int type() {
  if (_type == 0) {
    _type = stream.register("tap");
  }

  return _type;
}

typedef struct {
	/** The log stream. */
	stream.t * dest;

	/** The real input/output stream. */
	stream.t * source;
} context;


static ssize_t tap_read(void * _ctx, void * buf, size_t nbyte, stream.error_t * error) {
  context * ctx = (context *) _ctx;
  stream.t * s = ctx->source;
  ssize_t len = s->read(s->ctx, buf, nbyte, &s->error);

  if (len) stream.write(ctx->dest, buf, len);

  return len;
}

static ssize_t tap_write(void * _ctx, void * buf, size_t nbyte, stream.error_t * error) {
  context * ctx = (context *) _ctx;
  stream.t * s = ctx->source;

  ssize_t len = s->write(s->ctx, buf, nbyte, &s->error);
  if (len) stream.write(ctx->dest, buf, len);

  return len;
}

static ssize_t tap_close(void * _ctx, stream.error_t * error) {
	context * ctx = (context *) _ctx;
	stream.t * s = ctx->source;

	stream.close(ctx->dest);
	ssize_t e = s->close(s->ctx, &s->error);
	if (e != -1) {
		global.free(ctx);
		global.free(s);
	}
	return e;
}

export stream.t * input(stream.t * source, stream.t * dest) {
	context * ctx = (context *) malloc(sizeof(context));
	ctx->source = source;
	ctx->dest = dest;

	stream.t * s = malloc(sizeof(stream.t));

	s->ctx   = ctx;
	s->read  = tap_read;
	s->write = NULL;
	s->pipe  = NULL;
	s->close = tap_close;
	s->type  = type();

	s->error.code    = 0;
	s->error.message = NULL;

	return s;
}

export stream.t * output(stream.t * source, stream.t * dest) {
	context * ctx = (context *) malloc(sizeof(context));
	ctx->source = source;
	ctx->dest = dest;

	stream.t * s = malloc(sizeof(stream.t));

	s->ctx   = ctx;
	s->read  = NULL;
	s->write = tap_write;
	s->pipe  = NULL;
	s->close = tap_close;
	s->type  = type();

	s->error.code    = 0;
	s->error.message = NULL;

	return s;
}

export stream.t * all(stream.t * source, stream.t * dest) {
	context * ctx = (context *) malloc(sizeof(context));
	ctx->source = source;
	ctx->dest = dest;

	stream.t * s = malloc(sizeof(stream.t));

	s->ctx   = ctx;
	s->read  = tap_read;
	s->write = tap_write;
	s->pipe  = NULL;
	s->close = tap_close;
	s->type  = type();

	s->error.code    = 0;
	s->error.message = NULL;

	return s;
}
