package "json_rpc_client";

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

import json    from "../../deps/json/json.module.c";
import value   from "../../deps/json/value.module.c";
import integer from "../../deps/json/integer.module.c";
import stream  from "../../deps/stream/stream.module.c";
import file    from "../../deps/stream/file.module.c";
import closure from "../../deps/closure/closure.module.c";

build depends "../../deps/hash/hash.c";
export #include "../../deps/hash/hash.h"

import Req     from "./request_msg.module.c";
import Res     from "./response_msg.module.c";
import decoder from "./decode.module.c";
import encoder from "./encode.module.c";

export typedef struct {
	stream.t * input;
	stream.t * output;
	stream.t * log;

	encoder.t * encoder;
	decoder.t * decoder;
	closure.t on_message;
	closure.t on_error;
	Res.t  message;
	Req.t  notification;
	uint32_t index;
	long int id;
} client_t as t;

export int next(client_t * client) {
	return decoder.next(client->decoder);
}

export Res.t request(client_t * client, char * method, value.t params) {
	client->index ++;
	Req.t req = {
		.jsonrpc = "2.0",
		.method = method,
		.params = params,
		.id = value.from(integer.uint32(), &client->index),
	};

	json.marshal(Req.j(), encoder.stream(client->encoder), &req);
	encoder.send(client->encoder);
	next(client);

	return client->message;
}

static int respond(client_t * client, Res.t * resp) {
	json.marshal(Res.j(), encoder.stream(client->encoder), resp);
	return encoder.send(client->encoder);
}

static void * on_message(void * ctx, void * arg) {
	client_t * client = (client_t *) ctx;

	const char * message = (const char *) arg;
	stream.printf(client->log, "\n==> %s\n", message);

	value.t m = {0};
	json.unmarshal(value.j(), &m, message, strlen(message));

	Res.t  res = {0};
	json.unmarshal_ex(Res.j(), &res, m.parsed);

	Req.t  req = {0};
	json.unmarshal_ex(Req.j(), &req, m.parsed);

	if (req.method != NULL) { 
		client->notification = req;
	} else if (json.is_null(value.j(), &res.id)) {
		client->message = res;
	}

	global.free(arg);
	return NULL;
}

static void * on_error(void * _ctx, void * _arg) {
	fprintf(stderr, "ERROR: %s", _arg);
	return NULL;
}

export client_t * new(stream.t * input, stream.t * output) {
	client_t * client = malloc(sizeof(client_t));

	client->input      = input;
	client->output     = output;
	client->encoder    = encoder.new(output);
	client->log        = file.new(2);
	client->on_message = closure.new(on_message, client);
	client->on_error   = closure.new(on_error, client);
	client->decoder    = decoder.new(input, client->on_message, client->on_error);
	client->index      = 0;

	return client;
}
