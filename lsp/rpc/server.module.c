package "json_rpc_server";

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

import json    from "../../deps/json/json.module.c";
import value   from "../../deps/json/value.module.c";
import string  from "../../deps/json/string.module.c";
import stream  from "../../deps/stream/stream.module.c";
import file    from "../../deps/stream/file.module.c";
import closure from "../../deps/closure/closure.module.c";
import tap     from "./tap-stream.module.c";

build depends "../../deps/hash/hash.c";
export #include "../../deps/hash/hash.h"

import decoder from "./decode.module.c";
import Req     from "./request_msg.module.c";
import Res     from "./response_msg.module.c";
import encoder from "./encode.module.c";

export typedef struct {
	closure.t handler;
	char method [];
} handler_t;

export typedef struct {
	stream.t * input;
	stream.t * output;
} debug_t;

export typedef struct {
	stream.t * input;
	stream.t * output;
	encoder.t * encoder;
	debug_t debug;
	hash_t * handlers;
	handler_t * default_handler;
	closure.t on_message;
	closure.t on_error;
	long int id;
} server_t as t;

export int notify(server_t * server, char * method, value.t params) {
	Req.t req = {
		.jsonrpc = "2.0",
		.method = method,
		.params = params,
		.id = value.null(),
	};

	json.marshal(Req.j(), encoder.stream(server->encoder), &req);
	encoder.send(server->encoder);
}

int respond(server_t * server, Res.t * resp) {
	json.marshal(Res.j(), encoder.stream(server->encoder), resp);
	return encoder.send(server->encoder);
}

export typedef struct {
	Req.t req;
	Res.t res;
	server_t * server;
} handler_args_t;

void * on_message(void * ctx, void * arg) {
	server_t * server = (server_t *) ctx;

	const char * message = (const char *) arg;
	handler_args_t args = {
		.res = {
			.jsonrpc = "2.0",
			.error   = NULL,
			.result  = value.null(),
		},
		.server = server, 
	};

	int e = json.unmarshal(Req.j(), &args.req, message, strlen(message));
	if (e == -1) return NULL;
	args.res.id = args.req.id;
	
	handler_t * h = server->default_handler;
	h = hash_get(server->handlers, args.req.method ?: "");
	if (h == NULL) {
		char * m = NULL;
		asprintf(&m, "Method not found '%s'", args.req.method);
		args.res.error = Res.error(-32601, "Method not found", value.from(
					string.new(0),
					&args.req.method
		));
		respond(server, &args.res);
		global.free(arg);
		return NULL;
	} 
	closure.call(h->handler, &args);
	global.free(arg);
	return NULL;
}

void * on_error(void * _ctx, void * _arg) {
	/*printf("ERROR: %s", _arg);*/
	return NULL;
}

export server_t * new(stream.t * input, stream.t * output, debug_t debug) {
	server_t * server = malloc(sizeof(server_t));

	if (debug.input)  input  = tap.all(input, debug.input);
	if (debug.output) output = tap.all(output, debug.output);

	server->input      = input;
	server->output     = output;
	server->encoder    = encoder.new(output);
	server->handlers   = hash_new();
	server->on_message = closure.new(on_message, server);
	server->on_error   = closure.new(on_error, server);

	return server;
}

export int register(server_t * server, const char * method, closure.t handler) {
	handler_t * h = malloc(sizeof(*h) + sizeof(char[strlen(method) + 1 ]));
	h->handler = handler;
	strcpy(h->method, method);
	hash_set(server->handlers, h->method, h);

	return 1;
}

export int register_default(server_t * server, closure.t handler) {
	const char * method = "default";
	handler_t * h = malloc(sizeof(*h) + sizeof(char[strlen(method) + 1 ]));
	h->handler = handler;
	strcpy(h->method, "default");
	server->default_handler = h;

	return 1;
}

export int listen(server_t * server) {
	decoder.t * d = decoder.new(server->input, server->on_message, server->on_error);
	return decoder.listen(d);
}

export int error(handler_args_t * ctx, long long code, const char * message, value.t data) {
	ctx->res.error = Res.error(code, message, data);
	ctx->res.result = value.null();
	return respond(ctx->server, &ctx->res);
}

export int response(handler_args_t * ctx, value.t result) {
	ctx->res.result = result;
	return respond(ctx->server, &ctx->res);
}

export void * ignore(void * _ctx, void * _arg) {
	return NULL;
}
