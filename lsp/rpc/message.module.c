package "json_rpc_message";

#include <stdlib.h>
#include <string.h>

/*build depends "../deps/json-parser/json.c";*/

/*build append CFLAGS "-I../deps/json-parser";*/
/*build append LDLIBS "-lm";*/
/*build depends "../deps/json-builder/json-builder.c";*/
/*#include "../deps/json-builder/json-builder.h"*/

/*export {*/
/*#include "../deps/json-parser/json.h"*/
/*}*/

import json    from "../../deps/json/json.module.c";
import any     from "../../deps/json/value.module.c";
import Struct  from "../../deps/json/struct.module.c";
import string  from "../../deps/json/string.module.c";
import integer from "../../deps/json/integer.module.c";

export typedef struct { 
	int64_t   code;
	char    * message;
	any.t     data;
} error_t;

export json.t * error_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "code", offsetof(error_t, code), integer.int64());
		Struct.field(j, "message", offsetof(error_t, message), string.new(0));
		Struct.field(j, "data", offsetof(error_t, data), any.new());
	}

	return j;
}

export typedef struct {
	int64_t   id; 
	char    * method;
	any.t     params;
} request_t;

export json.t * request_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "id",     offsetof(request_t, id),     integer.int16());
		Struct.field(j, "method", offsetof(request_t, method), string.new(0));
		Struct.field(j, "params", offsetof(request_t, params), any.new());
	}

	return j;
}

export request_t request_decode(char * input) {
	json_settings settings = {};
	settings.value_extra = json_builder_extra;
	char err[json_error_max];

	json_value * message = json_parse_ex(&settings, input, strlen(input), err);

	request_t req = {0};

	int i;
	for(i = 0; i < message->u.object.length; i++) {
		json_object_entry entry = message->u.object.values[i];
		if (strncmp(entry.name, "id", entry.name_length) == 0) {
			req.id = entry.value->u.integer;
		} else if (strncmp(entry.name, "method", entry.name_length) == 0) {
			req.method = entry.value->u.string.ptr;
		} else if (strncmp(entry.name, "params", entry.name_length) == 0) {
			req.params = entry.value;
		}
	}

	// TODO: Add error detection / handling
	return req;
}

export char * request_encode(request_t req) {
	json_value * message = json_object_new(4);

	json_object_push(message, "jsonrpc", json_string_new("2.0"));
	json_object_push(message, "id",      json_integer_new(req.id));
	json_object_push(message, "method",  json_string_new(req.method));
	json_object_push(message, "params",  req.params);

	char * out = malloc(json_measure(message));
	json_serialize(out, message);
	json_builder_free(message);

	return out;
}

export typedef struct {
	long int     id;
	json_value * result;
	error_t      error;
} response_t;

export request_t response_decode(char * input) {
	json_settings settings = {};
	settings.value_extra = json_builder_extra;
	char err[json_error_max];

	json_value * message = json_parse_ex(&settings, input, strlen(input), err);

	response_t resp;

	int i;
	for(i = 0; i < message->u.object.length; i++) {
		json_object_entry entry = message->u.object.values[i];
		if (strncmp(entry.name, "id", entry.name_length) == 0) {
			resp.id = entry.value->u.integer;
		} else if (strncmp(entry.name, "result", entry.name_length) == 0) {
			resp.result = entry.value;
		} else if (strncmp(entry.name, "error", entry.name_length) == 0) {
			resp.error = entry.value;
		}
	}

	// TODO: Add error detection / handling
	return resp;
}

export char * response_encode(response_t resp) {
	json_value * message = json_object_new(4);

	json_object_push(message, "jsonrpc", json_string_new("2.0"));
	json_object_push(message, "id",      json_integer_new(resp.id));
	if (resp.error) {
		json_object_push(message, "params",  resp.error);
	} else {
		json_object_push(message, "result",  resp.result);
	}

	char * out = malloc(json_measure(message));
	json_serialize(out, message);
	json_builder_free(message);

	return out;
}
