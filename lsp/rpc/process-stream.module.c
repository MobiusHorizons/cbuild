package "process_stream";

//import async from "../../deps/stream/async.module.c";
import stream from "../../deps/stream/stream.module.c";

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>

static int _type;

export int type() {
  if (_type == 0) {
    _type = stream.register("process");
  }

  return _type;
}

typedef struct {
  int   in;
  int   out;
  pid_t pid;
  int   exited;
} context;

enum read_types {
  pipe_read,
  pipe_write,
};

static bool check_exit(context * ctx, stream.error_t * error) {
  int status = 0;
  pid_t pid = waitpid(ctx->pid, &status, WNOHANG);


  if (pid == 0 && status == 0) return false;

  if (pid == -1){
    error->code    = errno;
    error->message = strerror(error->code);
    return true;
  } else if (WIFEXITED(status)) {
    error->message = "exited";
    error->code    = WEXITSTATUS(status);
    return true;
  } else if (WIFSIGNALED(status)) {
    error->message = "killed by signal";
    error->code    = WTERMSIG(status);
    return true;
  } else if (WIFSTOPPED(status)) {
    error->message = "stopped by signal";
    error->code    = WSTOPSIG(status);
    return true;
  }

  return false;
}


/*
static int on_event(void * ctx, stream.t * s, stream.handler_t h, void * user_ctx) {
	context * ctx = (context *) _ctx;
	return async.on_event(ctx->out, s, h, user_ctx);
}
*/

static ssize_t process_read(void * _ctx, void * buf, size_t nbyte, stream.error_t * error) {
  context * ctx = (context *) _ctx;
  int e = global.read(ctx->out, buf, nbyte);

  if (e < 0 && error != NULL) {
    error->code    = errno;
    error->message = strerror(error->code);
    return e;
  }

  if (check_exit(ctx, error)) {
    ctx->exited = 1;
    return -1;
  }

  return e;
}

static ssize_t process_write(void * _ctx, const void * buf, size_t nbyte, stream.error_t * error) {
  context * ctx = (context *) _ctx;
  int e = global.write(ctx->in, buf, nbyte);

  if (e < 0 && error != NULL) {
    error->code    = errno;
    error->message = strerror(error->code);
    return e;
  }

  if (check_exit(ctx, error)) {
    ctx->exited = 1;
    return -1;
  }

  return e;
}

static ssize_t process_close(void *_ctx, stream.error_t * error) {
  context * ctx = (context *) _ctx;

  global.close(ctx->in);
  global.close(ctx->out);

  int status;
  if (ctx->exited == 0){
    do {
      pid_t pid = waitpid(ctx->pid, &status, 0);

      if (pid == -1){
        error->code    = errno;
        error->message = strerror(error->code);
        return -1;
      } else if (WIFEXITED(status)) {
        error->message = "exited";
        error->code    = WEXITSTATUS(status);
        return -1;
      } else if (WIFSIGNALED(status)) {
        error->message = "killed by signal";
        error->code    = WTERMSIG(status);
        return -1;
      } else if (WIFSTOPPED(status)) {
        error->message = "stopped by signal";
        error->code    = WSTOPSIG(status);
        return -1;
      }
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }
  return 0;
}

export stream.t * new(const char * executable, char * const * argv) {
    // holds the pipe to the stdin of the process
    int in[2];
    pipe(in);

    // holds the pipe from the stdout of the process
    int out[2];
    pipe(out);

    pid_t pid = fork();
    if (pid == 0){
        // we are the child;
        close(in[pipe_write]);
        close(out[pipe_read]);

        dup2(in[pipe_read] , STDIN_FILENO );
        dup2(out[pipe_write], STDOUT_FILENO);

		// spawn the process
        int err = execvp(executable, argv);
        err = errno;
        char * msg = strerror(err);

        close(in[pipe_read]);
        close(out[pipe_write]);
        errno = err;
        return stream.error(NULL, err, strerror(err));
    } else {
        close(in[pipe_read]);
        close(out[pipe_write]);

        context * ctx = malloc(sizeof(context));
        ctx->in     = in[pipe_write];
        ctx->out    = out[pipe_read];
        ctx->pid    = pid;
        ctx->exited = 0;

        stream.t * s = malloc(sizeof(stream.t));

        s->ctx      = ctx;
        s->read     = process_read;
        s->write    = process_write;
        s->pipe     = NULL;
        s->close    = process_close;
        s->type     = type();

        s->error.code    = 0;
        s->error.message = NULL;
        return s;
    }
}
