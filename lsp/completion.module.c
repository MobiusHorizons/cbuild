#include <ctype.h>

import json  from "../deps/json/json.module.c";
import value from "../deps/json/value.module.c";

import Pkg          from "../package/index.module.c";
import Import       from "../package/import.module.c";
import Export       from "../package/export.module.c";
import Package      from "../package/package.module.c";
import Errors       from "../errors/errors.module.c";
import StringStream from "../utils/string-stream.module.c";

import completion   from "types/completion.module.c";

import server  from "rpc/server.module.c";
import encoder from "rpc/encode.module.c";
import Req     from "rpc/request_msg.module.c";
import Resp    from "rpc/response_msg.module.c";

import state from "state.module.c";

export void * handler(void * _ctx, void * _arg) {
	state.t * s = (state.t*) _ctx;
	server.handler_args_t * args = (server.handler_args_t*)_arg;

	completion.params_t params = {0};
	json.unmarshal_ex(completion.params_j(), &params, args->req.params.parsed);
	const char * line = s->input_buffer;
	int i = 0;
	for (i = 0; line >= s->input_buffer && i < params.position.line; i++) {
		line = strchr(line, '\n') + 1;
	}

	const char * end = line + params.position.character;
	const char * sep = end;
	char * ident = NULL;
	const char * c = end;
	while(c > line && *c != '.') c--;

	if (c > line) {
		sep = c;
	}

	while(c >= line && c-- && (isalnum(*c) || *c == '_'));
	c++;

	if (c >= line) {
		ident = malloc(sep - c + 1);
		strncpy(ident, c, sep - c);
		ident[sep - c] = 0;
	}

	char * filter = "";
	size_t filter_len = 0;

	if (end > sep) {
		filter_len = end - sep - 1;
		filter = malloc(end - sep);
		strncpy(filter, sep + 1, filter_len);
		filter[filter_len] = 0;
	}

	/*fprintf(stderr, "(%d)> <%s>.(%s)\n", */
		/*params.context.kind, */
		/*ident,*/
		/*filter*/
	/*);*/

	completion.list_t result = {0};

	Import.t * imp = hash_get(s->pkg->deps, ident);
	if (imp && imp->pkg) {
		/*fprintf(stderr, "Package %s\n", imp->pkg->name);*/

		result.items = malloc(sizeof(completion.item_t) * imp->pkg->n_exports);
		result.is_incomplete = true;

		int i;
		for (i = 0; i < imp->pkg->n_exports; i++){
			Export.t * exp = (Export.t *) imp->pkg->ordered[i];
			if (exp->type == type_header || exp->type == type_block) continue;
			if (strncmp(filter, exp->export_name, filter_len) != 0) continue;

			result.items[result.length].label = exp->export_name;
			result.items[result.length].preselect = result.length == 0;
			result.length++;
			/*fprintf(stderr, "  -> %s\n", exp->export_name);*/
		}
		/*fprintf(stderr, "(%d)\n", result.length);*/
	}

	server.response(args, value.from(completion.list_j(), &result));
	return NULL;
}
