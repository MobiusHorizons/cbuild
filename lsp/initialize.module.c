#include <string.h>
#include <stdio.h>

import json    from "../deps/json/json.module.c";
import Struct  from "../deps/json/struct.module.c";
import array   from "../deps/json/array.module.c";
import value   from "../deps/json/value.module.c";
import stream  from "../deps/stream/stream.module.c";
import file    from "../deps/stream/file.module.c";

import client  from "rpc/client.module.c";
import process from "rpc/process-stream.module.c";
import server  from "rpc/server.module.c";
import Req     from "rpc/request_msg.module.c";
import Resp    from "rpc/response_msg.module.c";

import capabilities from "./types/server-capabilities.module.c";
import initialize_params from "./types/initialize-params.module.c";

import state from "./state.module.c";

export typedef struct {
	capabilities.t capabilities;
	capabilities.server_info_t server_info;
} results_t;

export json.t * results_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "capabilities",
			offsetof(results_t, capabilities),
			capabilities.j()
		);

		Struct.field(j, "serverInfo",
			offsetof(results_t, server_info),
			capabilities.server_info_j()
		);
	}
	return j;
}

export void * handler(void * _ctx, void * _arg) {
	state.t * ctx = (state.t*) _ctx;
	server.handler_args_t * args = (server.handler_args_t*)_arg;

	initialize_params.t params = {0};
	json.unmarshal_ex(initialize_params.j(), &params, args->req.params.parsed);


	ctx->opts.silent    = false;
	ctx->opts.force     = false;
	ctx->opts.verbosity = SILENT;

	if (params.root_uri && strncmp(params.root_uri, "file://", strlen("file://")) == 0) {
		size_t len = strlen(params.root_uri);
		int has_trailing_slash = params.root_uri[len -1] == '/';

		ctx->root_uri = malloc(len + 2);
		sprintf(ctx->root_uri, "%s%s", params.root_uri, has_trailing_slash ? "" : "/");
		ctx->opts.cwd = strdup(ctx->root_uri + strlen("file://"));
	} else if (params.root_path) {
		size_t len = strlen(params.root_uri);
		int has_trailing_slash = params.root_uri[len -1] == '/';

		ctx->root_uri = malloc(len + strlen("file://") + 2);
		sprintf(ctx->root_uri, "file://%s%s", params.root_path, has_trailing_slash ? "" : "/");
		ctx->opts.cwd = strdup(ctx->root_uri + strlen("file://"));
	}

	char * const clangd_args[] = { 
		"clangd",
		NULL,
	};

	capabilities.completion_options_t completion_opts = {
		.trigger_characters = ".",
	};

	results_t res = {
		.capabilities = {
			.completion_provider = &completion_opts,
			.text_document_sync = {
				.open_close = true,
				.change = text_document_sync_full,
			},
			.definition_provider = true,
		},
		.server_info = {
			.name = "cbuild",
		},
	};
	server.response(args, value.from(results_j(), &res));

	//stream.t * clangd = process.new("clangd", clangd_args);
	//ctx->clangd = client.new(clangd, clangd);

	//Resp.t resp = client.request(ctx->clangd, "initialize", args->req.params);
	//stream.t * log = file.open("/home/paul/clang-log.log", O_WRONLY | O_CREAT);
	//ctx->log = log;

//	server.response(args, resp.result);
}

