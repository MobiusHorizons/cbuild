import Package    from "../package/package.module.c";
import stream     from "../deps/stream/stream.module.c";

import Diagnostic from "types/diagnostic.module.c";
import client     from "rpc/client.module.c";
import server     from "rpc/server.module.c";

export typedef struct {
	server.t     * lsp;
	stream.t     * err;
	stream.t     * log;
	char         * input_buffer;
	char         * compiled;
	//client.t     * clangd;
	Diagnostic.t   diagnostics;
	char * root_uri;
	Package.options_t opts;
	Package.t    * pkg;
} state_t as t;
