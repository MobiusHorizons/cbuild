package "text_document";

#include <stdio.h>
#include <string.h>

import json    from "../deps/json/json.module.c";
import Struct  from "../deps/json/struct.module.c";
import array   from "../deps/json/array.module.c";
import any     from "../deps/json/value.module.c";

import server from "rpc/server.module.c";
import client from "rpc/client.module.c";
import Req     from "rpc/request_msg.module.c";
import Resp    from "rpc/response_msg.module.c";

import Diagnostic   from "types/diagnostic.module.c";
import TextDocument from "types/text-document.module.c";

import compile from "./compile.module.c";
import state   from "./state.module.c";

typedef struct {
	TextDocument.t doc;
} open_params_t;

json.t * open_params_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "textDocument", offsetof(open_params_t, doc), TextDocument.j());
	}

	return j;
}

export void * open(void * _ctx, void * _arg) {
	state.t * ctx = (state.t*) _ctx;
	server.handler_args_t * args = (server.handler_args_t*)_arg;

	open_params_t params = {0};
	json.unmarshal_ex(open_params_j(), &params, args->req.params.parsed);

	char * generated = compile.check(params.doc.uri, params.doc.text, ctx);

	if (generated) {
		char * uri = malloc(strlen("file://") + strlen(generated) + 1);
		sprintf(uri, "file://%s", generated);

		open_params_t p = {
			.doc = {
				.uri        = params.doc.uri,
				.languageId = "c",
				.version    = params.doc.version,
				.text       = ctx->compiled,
			},
		};

		//json.marshal_pretty(open_params_j(), ctx->log, &p, 4);
	}
	//client.request(ctx->clangd, "textDocument/didOpen", any.from(open_params_j(), &p));

#if 0
	Req.t n = ctx->clangd->notification;
	if (
		n.method && 
		strcmp(n.method , "textDocument/publishDiagnostics") == 0
	) {
		Diagnostic.publish_diagnostic_t diagnostic = {0};
		json.unmarshal_ex(Diagnostic.publish_diagnostic_j(), &diagnostic, n.params.parsed);
		/*diagnostic.uri = params.doc.uri;*/

		server.notify(ctx->lsp, "textDocument/publishDiagnostics", n.params);
		/*any.from(*/
			/*Diagnostic.publish_diagnostic_j(),*/
			/*&diagnostic*/
		/*));*/
	}
#endif
	return NULL;
}

typedef struct {
	TextDocument.t doc;
	TextDocument.change_t * changes;
	unsigned long count;
} change_params_t;

json.t * change_params_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "contentChanges", 
				offsetof(change_params_t, changes),
				array.with_length(
					sizeof(TextDocument.change_t), TextDocument.change_j(),
					offsetof(change_params_t, changes) -
					offsetof(change_params_t, count),
					sizeof(unsigned long), false
					)
				);
		Struct.field(j, "textDocument", offsetof(change_params_t, doc),TextDocument.j());
	}

	return j;
}

export void * change(void * _ctx, void * _arg) {
	state.t * ctx = (state.t*) _ctx;
	server.handler_args_t * args = (server.handler_args_t*)_arg;

	change_params_t params = {0};
	json.unmarshal_ex(change_params_j(), &params, args->req.params.parsed);

	compile.check(params.doc.uri, params.changes[0].text, ctx);

#if 0
	change_params_t p = {
		.doc = {
			.uri        = params.doc.uri,
			.languageId = "c",
			.version    = params.doc.version,
		},
		.count = 1,
	};
	p.changes = calloc(sizeof(TextDocument.change_t), 1);
	p.changes[0].text = ctx->compiled;

#
	json.marshal_pretty(change_params_j(), ctx->log, &p, 4);
	client.request(ctx->clangd, "textDocument/didChange", any.from(change_params_j(), &p));

	//Req.t n = ctx->clangd->notification;
	if (
		n.method && 
		strcmp(n.method , "textDocument/publishDiagnostics") == 0
	) {
		Diagnostic.publish_diagnostic_t diagnostic = {0};
		json.unmarshal_ex(Diagnostic.publish_diagnostic_j(), &diagnostic, n.params.parsed);
		diagnostic.uri = params.doc.uri;

		//server.notify(ctx->lsp, "textDocument/publishDiagnostics", n.params);
		server.notify(ctx->lsp, "textDocument/publishDiagnostics",
		any.from(
			Diagnostic.publish_diagnostic_j(),
			&diagnostic
		));
	}
#endif
	return NULL;
}
