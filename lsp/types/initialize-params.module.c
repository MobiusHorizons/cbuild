package "type_initialize_params";

export #include <stdint.h>
import json    from "../../deps/json/json.module.c";
import Struct  from "../../deps/json/struct.module.c";
import integer from "../../deps/json/integer.module.c";
import string  from "../../deps/json/string.module.c";
import array   from "../../deps/json/array.module.c";
import boolean from "../../deps/json/boolean.module.c";
import pointer from "../../deps/json/pointer.module.c";

/**
 * Partial implementation of InitializeParameters from 
 * https://microsoft.github.io/language-server-protocol/specification#initialize.
 */
export typedef struct {
	/**
	 * The process Id of the parent process that started
	 * the server. Is 0 if the process has not been started by another process.
	 * If the parent process is not alive then the server should exit (see exit notification) its process.
	 */
	int64_t process_id;

	/**
	 * The rootPath of the workspace. Is null
	 * if no folder is open.
	 *
	 * @deprecated in favour of root_uri.
	 */
	char * root_path;

	/**
	 * The rootUri of the workspace. Is null if no
	 * folder is open. If both `rootPath` and `rootUri` are set
	 * `rootUri` wins.
	 */
	char * root_uri;

} initialize_params_t as t;

export json.t * initialize_params_j() as j {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();

		Struct.field(j, "processId", 
			offsetof(initialize_params_t, process_id),
			integer.int64()
		);

		Struct.field(j, "rootPath", 
			offsetof(initialize_params_t, root_path),
			string.new(0)
		);

		Struct.field(j, "rootUri", 
			offsetof(initialize_params_t, root_uri),
			string.new(0)
		);
	}

	return j;
}
