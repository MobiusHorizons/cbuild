#include <stdint.h>

import json    from "../../deps/json/json.module.c";
import Struct  from "../../deps/json/struct.module.c";
import integer from "../../deps/json/integer.module.c";
import string  from "../../deps/json/string.module.c";
import array   from "../../deps/json/array.module.c";

import TextDocument from "text-document.module.c";

export enum sevarity_t {
	error = 1,
	warning,
	information,
	hint,
};

export typedef struct {
	TextDocument.location_t location;
	char * message;
} related_information_t;

export json.t * related_information_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "location", offsetof(related_information_t, location), TextDocument.location_j());
		Struct.field(j, "message", offsetof(related_information_t, message), string.new(0));
	}

	return j;
}

export typedef struct {
	TextDocument.range_t range;
	uint16_t severity;
	char * code;
	char * source;
	char * message;
	related_information_t * related_information;
	uint16_t related_count;
} diagnostic_t as t;

export json.t * diagnostic_j() as j {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "range", offsetof(diagnostic_t, range), TextDocument.range_j());
		Struct.field(j, "severity", offsetof(diagnostic_t, severity), integer.uint16());
		Struct.field(j, "code", offsetof(diagnostic_t, code), string.new(0));
		Struct.field(j, "source", offsetof(diagnostic_t, source), string.new(0));
		Struct.field(j, "message", offsetof(diagnostic_t, message), string.new(0));
		Struct.field(j, "relatedInformation", offsetof(diagnostic_t, related_information),
			array.with_length(
				sizeof(related_information_t),
				related_information_j(),
				offsetof(diagnostic_t, related_count) -
				offsetof(diagnostic_t, related_information),
				2, false
			)
		);
	}

	return j;
}

export typedef struct {
	char * uri;
	diagnostic_t * diagnostics;
	uint16_t count;
} publish_diagnostic_t;

export json.t * publish_diagnostic_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "uri", offsetof(publish_diagnostic_t, uri), string.new(0));
		Struct.field(j, "diagnostics", offsetof(publish_diagnostic_t, diagnostics),
			array.with_length(
				sizeof(diagnostic_t), diagnostic_j(),
				offsetof(publish_diagnostic_t, count) -
				offsetof(publish_diagnostic_t, diagnostics),
				2, false
			)
		);
	}
	return j;
}
