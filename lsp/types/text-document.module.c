package "lsp_text_document";

import json    from "../../deps/json/json.module.c";
import boolean from "../../deps/json/boolean.module.c";
import array   from "../../deps/json/array.module.c";
import Struct  from "../../deps/json/struct.module.c";
import string  from "../../deps/json/string.module.c";
import integer from "../../deps/json/integer.module.c";
import value   from "../../deps/json/value.module.c";
import pointer from "../../deps/json/pointer.module.c";

export typedef struct {
	long long int line;
	long long int character;
} position_t;

export json.t * position_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "line", offsetof(position_t, line), integer.uint64());
		Struct.field(j, "character", offsetof(position_t, character), integer.uint64());
	}
	return j;
}

export typedef struct {
	position_t start;
	position_t end;
} range_t;

export json.t * range_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "start", offsetof(range_t, start), position_j());
		Struct.field(j, "end", offsetof(range_t, end), position_j());
	}
	return j;
}

export typedef struct {
	range_t * range;
	long long int range_length;
	char * text;
} change_t;

export json.t * change_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "range", offsetof(change_t, range), 
				pointer.new(sizeof(range_t), range_j()));
		Struct.field(j, "rangeLength", offsetof(change_t, range_length),
				integer.uint64());
		Struct.field(j, "text", offsetof(change_t, text),
				string.new(0));
	}
	return j;
}

export typedef struct {
	char * uri;
	range_t range;
} location_t;

export json.t * location_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "uri", offsetof(location_t, uri), string.new(0));
		Struct.field(j, "range", offsetof(location_t, range), range_j());
	}
	return j;
}

export typedef struct {
	char * text;
	char * uri;
	char * languageId;
	unsigned long long version;
} text_document_t as t;

export json.t * text_document_j() as j {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "text", offsetof(text_document_t, text), string.new(0));
		Struct.field(j, "version", offsetof(text_document_t, version), integer.uint64());
		Struct.field(j, "uri", offsetof(text_document_t, uri), string.new(0));
		Struct.field(j, "languageId", offsetof(text_document_t, languageId), string.new(0));
	}
	return j;
}

export typedef struct {
	char * label;
	bool preselect;
} item_t;

export json.t * item_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "label", offsetof(item_t, label), string.new(0));
		Struct.field(j, "preselect", offsetof(item_t, preselect), boolean.new());
	}

	return j;
}

export typedef struct {
	bool is_incomplete;
	item_t * items;
	uint16_t length;
} list_t;

export json.t * list_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "isIncomplete", offsetof(list_t, is_incomplete), boolean.new());

		Struct.field(j, "items", offsetof(list_t, items),
			array.with_length(
				sizeof(item_t), item_j(),
				offsetof(list_t, length) - offsetof(list_t, items),
				2, false
			)
		);
	}

	return j;
}

export typedef struct {
	text_document_t document;
	position_t position;
} position_params_t;

export json.t * position_params_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "textDocument",
			offsetof(position_params_t, document),
			text_document_j()
		);

		Struct.field(j, "position",
			offsetof(position_params_t, position),
			position_j()
		);
	}

	return j;
}
