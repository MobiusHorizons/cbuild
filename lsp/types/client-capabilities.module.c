package "client_capabilities";

import json    from "../../deps/json/json.module.c";
import Struct  from "../../deps/json/struct.module.c";
import integer from "../../deps/json/integer.module.c";
import string  from "../../deps/json/string.module.c";
import array   from "../../deps/json/array.module.c";
import boolean from "../../deps/json/boolean.module.c";
import pointer from "../../deps/json/pointer.module.c";

typedef struct {
} workspace_capabilities_t;


typedef struct {
	bool dynamic_registration;
	bool will_save;
	bool will_save_wait_until;
	bool did_save;
} synchronization_t;

json.t * synchronization_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "dynamicRegistration", 
			offsetof(synchronization_t, dynamic_registration),
			boolean.new()
		);

		Struct.field(j, "willSave", 
			offsetof(synchronization_t, will_save),
			boolean.new()
		);

		Struct.field(j, "willSaveWaitUntil", 
			offsetof(synchronization_t, will_save_wait_until),
			boolean.new()
		);

		Struct.field(j, "didSave", 
			offsetof(synchronization_t, did_save),
			boolean.new()
		);
	}

	return j;
}

export typedef struct {
	/**
	 * The client supports snippets as insert text.
	 *
	 * A snippet can define tab stops and placeholders with `$1`, `$2`
	 * and `${3:foo}`. `$0` defines the final tab stop, it defaults to
	 * the end of the snippet. Placeholders with equal identifiers are linked,
	 * that is typing in one will update others too.
	 */
	bool snippet_support;

	/** The client supports commit characters on a completion item.  */
	bool commit_characters_support;

	/**
	 * The client supports the following content formats for the documentation
	 * property. The order describes the preferred format of the client.
	 */
	//markup_kind[] documentation_format;

	/** The client supports the deprecated property on a completion item. */
	bool deprecated_support;

	/** The client supports the preselect property on a completion item. */
	bool preselect_support;
} completion_item_t;

export json.t * completion_item_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "snippetSupport",
			offsetof(completion_item_t, snippet_support),
			boolean.new()
		);

		Struct.field(j, "commitCharactersSupport",
			offsetof(completion_item_t, commit_characters_support),
			boolean.new()
		);

		/** TODO: add support for documentationFormat */
		/*Struct.field(j, "documentationFormat",*/
			/*offsetof(completion_item_t, documentationFormat),*/
			/*Array.new*/
		/*);*/

		Struct.field(j, "deprecatedSupport",
			offsetof(completion_item_t, deprecated_support),
			boolean.new()
		);

		Struct.field(j, "preselctSupport",
			offsetof(completion_item_t, preselect_support),
			boolean.new()
		);
	}

	return j;
}

typedef struct {
	bool dynamic_registration;
	completion_item_t * completion_item;
	
	bool context_support;
} completion_t;

typedef struct {
} text_document_capabilities_t;

typedef struct {
	workspace_capabilities_t * workspace;
	text_document_capabilities_t * text_document;
} capabilities_t as t;
