package "server_capabilities";

import json    from "../../deps/json/json.module.c";
import Struct  from "../../deps/json/struct.module.c";
import integer from "../../deps/json/integer.module.c";
import string  from "../../deps/json/string.module.c";
import array   from "../../deps/json/array.module.c";
import boolean from "../../deps/json/boolean.module.c";
import pointer from "../../deps/json/pointer.module.c";

export enum text_document_sync_kind {
	text_document_sync_none = 0,
	text_document_sync_full = 1,
	text_document_sync_incremental = 2,
};

export typedef struct {
	bool open_close;
	uint8_t change;
	bool will_save;
	bool will_save_wait_until;
	bool save;
} text_document_sync_options_t;

export json.t * text_document_sync_options_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "openClose", 
			offsetof(text_document_sync_options_t, open_close),
			boolean.new()
		);

		Struct.field(j, "change", 
			offsetof(text_document_sync_options_t, change),
			integer.uint8()
		);

		Struct.field(j, "willSave", 
			offsetof(text_document_sync_options_t, will_save),
			boolean.new()
		);

		Struct.field(j, "willSaveWaitUntil", 
			offsetof(text_document_sync_options_t, will_save_wait_until),
			boolean.new()
		);

		Struct.field(j, "save",
			offsetof(text_document_sync_options_t, save),
			boolean.new()
		);
	}

	return j;
}

export typedef struct {
	bool resolve_provider;
	char * trigger_characters;
} completion_options_t;

export json.t *	completion_options_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "resolveProvider",
			offsetof(completion_options_t, resolve_provider),
			boolean.new()
		);

		Struct.field(j, "triggerCharacters",
			offsetof(completion_options_t, trigger_characters),
			array.dynamic(string.char(), 1)
		);
	}

	return j;
}

export typedef struct {
	bool hover_provider;
	bool definition_provider;
	bool references_provider;
	bool document_highlight_provider;
	bool document_symbol_provider;
	bool workspace_symbol_provider;
	text_document_sync_options_t text_document_sync;
	completion_options_t * completion_provider;
	/*signature_help_options * signature_help_provider;*/
} capabilities_t as t;

export json.t * capabilities_j() as j {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "textDocumentSync",
			offsetof(capabilities_t, text_document_sync),
			text_document_sync_options_j()
		);

		Struct.field(j, "hoverProvider",
			offsetof(capabilities_t, hover_provider),
			boolean.new()
		);

		Struct.field(j, "completionProvider",
			offsetof(capabilities_t, completion_provider),
			pointer.new(
				sizeof(completion_options_t), completion_options_j()
			)
		);

		// signatureHelpProvider

		Struct.field(j, "definitionProvider",
			offsetof(capabilities_t, definition_provider),
			boolean.new()
		);

		// typeDefinitionProvider
		// implementationProvider

		Struct.field(j, "referencesProvider",
			offsetof(capabilities_t, references_provider),
			boolean.new()
		);

		Struct.field(j, "documentHighlightProvider",
			offsetof(capabilities_t, document_highlight_provider),
			boolean.new()
		);

		Struct.field(j, "documentSymbolProvider",
			offsetof(capabilities_t, document_symbol_provider),
			boolean.new()
		);

		Struct.field(j, "workspaceSymbolProvider",
			offsetof(capabilities_t, workspace_symbol_provider),
			boolean.new()
		);

	}
	return j;
}

export typedef struct {
	/** The name of the server  as defined by the server */
	char * name;

	/** The server's version as defined by the server. */
	char * version;
} server_info_t;

export json.t * server_info_j() {
	static json.t * j;
	if (j == NULL) {
		j = Struct.new();

		Struct.field(j, "name",
			offsetof(server_info_t, name),
			string.new(0)
		);

		Struct.field(j, "version",
			offsetof(server_info_t, version),
			string.new(0)
		);

	}
	return j;
}
