package "completion_types";

import doc from "./text-document.module.c";

import json    from "../../deps/json/json.module.c";
import Struct  from "../../deps/json/struct.module.c";
import string  from "../../deps/json/string.module.c";
import integer from "../../deps/json/integer.module.c";
import boolean from "../../deps/json/boolean.module.c";
import array   from "../../deps/json/array.module.c";

export{
#include <stdint.h>
}

export enum trigger_kind {
	trigger_none = 0,
	trigger_invoked = 1,
	trigger_character = 2,
	trigger_incomplete = 3,
};

export typedef struct {
	uint8_t kind;
	char trigger;
} context_t;

export json.t * context_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "triggerKind", offsetof(context_t, kind), integer.uint8());
		Struct.field(j, "triggerCharacter", offsetof(context_t, trigger), string.char());
 	}

	return j;
}

export typedef struct {
	context_t context;
	doc.t document;
	doc.position_t position;
} params_t;

export json.t * params_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "context",
			offsetof(params_t, context),
			context_j()
		);

		Struct.field(j, "textDocument",
			offsetof(params_t, document),
			doc.j()
		);

		Struct.field(j, "position",
			offsetof(params_t, position),
			doc.position_j()
		);
	}

	return j;
}

export typedef struct {
	char * label;
	bool preselect;
} item_t;

export json.t * item_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "label", offsetof(item_t, label), string.new(0));
		Struct.field(j, "preselect", offsetof(item_t, preselect), boolean.new());
	}

	return j;
}

export typedef struct {
	bool is_incomplete;
	item_t * items;
	uint16_t length;
} list_t;

export json.t * list_j() {
	static json.t * j;

	if (j == NULL) {
		j = Struct.new();
		Struct.field(j, "isIncomplete", offsetof(list_t, is_incomplete), boolean.new());

		Struct.field(j, "items", offsetof(list_t, items),
			array.with_length(
				sizeof(item_t), item_j(),
				offsetof(list_t, length) - offsetof(list_t, items),
				2, false
			)
		);
	}

	return j;
}
