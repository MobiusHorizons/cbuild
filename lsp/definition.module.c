package "definition";
#include <ctype.h>
#include <stdio.h>

import stream from "../deps/stream/stream.module.c";

import json from "../deps/json/json.module.c";
import array from "../deps/json/array.module.c";
import value from "../deps/json/value.module.c";

import package from "../package/package.module.c";
import import from "../package/import.module.c";
import export from "../package/export.module.c";
import cache from "../lexer/cache.module.c";
import stack from "../lexer/stack.module.c";
import item from "../lexer/item.module.c";

import server from "rpc/server.module.c";

import doc from "types/text-document.module.c";

import state from "state.module.c";

#define token_range(T) { .line = T->line, .character = T->start - T->line_pos }

struct location_array_t {
	doc.location_t * values;
	uint8_t length;
};

static json.t * location_array_j() {
	static json.t * j;
	if (j == NULL) {
		j = array.with_length(
			sizeof(doc.location_t), doc.location_j(), 
			offsetof(struct location_array_t, length) -
			offsetof(struct location_array_t, values),
			1, false
		);
	}

	return j;
}

static void * no_result(server.handler_args_t * args) {
	struct location_array_t resp = {0};
	server.response(args, value.from(location_array_j(), &resp));
	return NULL;
}

static void * result(
		server.handler_args_t * args,
		state_t * s,
		char * uri,
		item.t * start,
		item.t * end
) {

		doc.location_t loc = {
			.uri = uri,
			.range = {
				.start = token_range(start),
				.end   = token_range(end),
			},
		};

		struct location_array_t resp = {
			.length = 1,
			.values = &loc,
		};

		stream.write(s->err, "I: ", 3);
		json.marshal(doc.location_j(), s->err, &resp.values[0]);
		stream.write(s->err, "\n", 1);
	
		server.response(args, value.from(location_array_j(), &resp));
	return NULL;
}

static void * exp_result(
	server.handler_args_t * args,
	state_t * s,
	package.t * pkg,
	export.t * exp
) {
	size_t i = 0;
	item.t * symbol = NULL;
	do {
		symbol = stack.peek(exp->declaration, i++);
	} while(symbol && strcmp(symbol->value, exp->symbol) != 0);
	if (symbol == NULL) symbol = stack.first(exp->declaration);

	item.t * end   = stack.last(exp->declaration);
	char * uri = malloc(strlen("file://") + strlen(pkg->source_abs) + 1);
	strcpy(uri, "file://");
	strcpy(uri + strlen("file://"), pkg->source_abs);
	result(args, s, uri, symbol, end);

	free(uri);
	return NULL;
}


export void * handler(void * _ctx, void * _arg) {
	state_t * s = (state_t*) _ctx;
	server.handler_args_t * args = (server.handler_args_t*)_arg;

	doc.position_params_t params = {0};
	json.unmarshal_ex(doc.position_params_j(), &params, args->req.params.parsed);

	package.t * p = s->pkg;
	if (!p) return no_result(args);

	cache.token_context_t tok_ctx = {0};
	item.t * tok = cache.token_at(
		p->source_cache, 
		params.position.line,
		params.position.character,
		&tok_ctx
	);

	if (tok == NULL) {
		fprintf(stderr, "I: No token found at (%lld:%lld).\n", params.position.line + 1, params.position.character);
		return no_result(args);
	}

	if (tok->replaced_by == NULL) {
		fprintf(stderr, "I: '%s'(%lld:%lld) is not defined by cbuild.\n", tok->value, params.position.line + 1, params.position.character);
		return no_result(args);
	}

	item.t * it;
	// <pkg>._<foo>_ case (cursor is somewhere in foo). 
	if (tok_ctx.token_no >= 2) {
		it = tok_ctx.line->items[tok_ctx.token_no - 1];
		//fprintf(stderr, "I: %s\n", item.to_string(it));
		if (it && it->type == item_symbol && it->value[0] == '.' && it->replaced_by == tok->replaced_by) {
			item.t * pkg = tok_ctx.line->items[tok_ctx.token_no - 2];
			if (pkg && pkg->replaced_by == tok->replaced_by) {
				/**
				 * This means that the token belongs to a renamed symbol. 
				 *
				 * If pkg is also replaced by the same symbol then 
				 */
				fprintf(stderr, "I: Attempting to find definition of package symbol %s.%s\n", pkg->value, tok->value);
				import.t * imp = hash_get(p->deps, pkg->value);
				if (imp == NULL) return no_result(args);
				export.t * exp = hash_get(imp->pkg->exports, tok->value);
				return exp_result(args, s, imp->pkg, exp);
			}
		}
	}

	if (tok->type == item_id) {
		fprintf(stderr, "I: Attempting to find definition of local symbol '%s'\n", tok->value);

		// local symbol case 
		export.t * exp = hash_get(p->exports, tok->value);
		if (exp) return exp_result(args, s, p, exp);

		// local package case
		import.t * imp = hash_get(p->deps, tok->value);
		if (imp) {
			//TODO: track import symbols in the import definition.
		}
	}

	fprintf(stderr, "I: Cannot define %s (%lld:%lld)\n", item.to_string(tok), params.position.line + 1, params.position.character);
	return no_result(args);
}
