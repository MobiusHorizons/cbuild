#include <stdio.h>

import json   from "../deps/json/json.module.c";
import value  from "../deps/json/value.module.c";
import stream from "../deps/stream/stream.module.c";

import Pkg          from "../package/index.module.c";
import Import       from "../package/import.module.c";
import Export       from "../package/export.module.c";
import Package      from "../package/package.module.c";
import Errors       from "../errors/errors.module.c";
import StringStream from "../utils/string-stream.module.c";

import server from "rpc/server.module.c";

import Diagnostic from "types/diagnostic.module.c";

import state from "./state.module.c";

Diagnostic.t convert_error(Errors.error_t e) {
	Diagnostic.t d = {
		.range = {
			.start = {
				.line      = e.context.start.line,
				.character = e.context.start.character,
			},
			.end = {
				.line      = e.context.end.line,
				.character = e.context.end.character,
			},
		},
		.severity = e.severity,
		.source   = "cbuild",
		.message  = e.message,
	};

	return d;
}

void publish_cbuild_errors(char * uri, Errors.t * errors, state.t * s) {
	Diagnostic.publish_diagnostic_t params = {
		.uri   = uri,
		.count = 0,
	};
	params.diagnostics = malloc(sizeof(Diagnostic.t) * (errors->count));

	char * filename = uri + strlen("file://");

	int i;
	for (i = 0; i < errors->count; i++) {
	/*printf("%s => %s", filename, errors->errors[i].context.filename);*/
		/*if (strcmp(filename, errors->errors[i].context.filename) == 0) {*/
			params.diagnostics[params.count++] = convert_error(errors->errors[i]);
		/*}*/
	}

	server.notify(s->lsp, "textDocument/publishDiagnostics", value.from(
		Diagnostic.publish_diagnostic_j(),
		&params
	));
}

export char * check(char * uri, char * text, state.t * s) {
	fprintf(stderr, "I: checking '%s'\n", uri);
	static int invocation;
	s->input_buffer = text;
	stream.t * in  = StringStream.new_reader(text);
	stream.t * out = StringStream.new_writer(&s->compiled);
	Errors.t errors = {0};

	char * relative = uri;
	/**
	if (strncmp(relative, "file://", 7) == 0) {
		relative += strlen("file://");
	}
	*/
	if (strncmp(relative, s->root_uri, strlen(s->root_uri)) == 0) {
		relative += strlen(s->root_uri);
	}

	fprintf(stderr, "I: Pkg.new('%s', ...)\n", relative);
	if (s->pkg) {
		Pkg.free(s->pkg);
	}
	s->pkg = Pkg.new(relative, &errors, s->opts);
	if (s->pkg) {
		s->pkg->out = out;
		Pkg.parse(s->pkg, in, relative, &errors);
		Pkg.resolve(s->pkg, &errors);
	}

	//s->compiled = StringStream.get_buffer(out);
	/*char * buf = StringStream.get_buffer(out);*/
	stream.printf(s->err, 
		  "\n===================(%s)====================\n"
		"%s\n===========================================\n",
		s->pkg->generated, s->compiled
	);

	/*Pkg.free(p);*/

	publish_cbuild_errors(uri, &errors, s);
	return s->pkg ? s->pkg->generated : NULL;
}
