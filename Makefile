PREFIX ?= /usr/local
EXEC   ?= cbuild

all : .bin/cbuild .bin/cbuild-lsp 

tests: .bin/cbuild
	rm -rf .test
	.bin/cbuild -o .test test.module.c
	.test/test

.bin/cbuild: 
	cd .bin; ./bootstrap.sh
	make -C .bin cbuild

.bin/cbuild-lsp: 
	make -C .bin cbuild-lsp

clean:
	make -C .bin clean

install: .bin/cbuild .bin/cbuild-lsp
	rm -f $(PREFIX)/bin/$(EXEC)
	cp .bin/cbuild $(PREFIX)/bin/$(EXEC)
	rm -f $(PREFIX)/bin/$(EXEC)-lsp
	cp .bin/cbuild-lsp $(PREFIX)/bin/$(EXEC)-lsp

dist: clean .bin/cbuild .bin/cbuild-lsp

.PHONY: run_tests  clean .bin/cbuild .bin/cbuild-lsp
