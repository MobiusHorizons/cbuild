#include <strings.h>
package "package_import";

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <libgen.h>
#include "../deps/hash/hash.h"
export #include <stdbool.h>

import Errors     from "../errors/errors.module.c";
import symbol     from "../parser/symbol.module.c";
import Package    from "./package.module.c";
import pkg_export from "./export.module.c";
build  depends         "../deps/hash/hash.c";

export typedef enum {
	IMPORT_QUEUED = 0,
	IMPORT_C_FILE,
	IMPORT_H_FILE,
	IMPORT_PARSED,
} import_type_t as type_t;

export typedef struct {
	char          * alias;
	char          * filename;
	import_type_t   type;
	Package.t     * pkg;
	hash_t	      * symbols;
} Import_t as t;

export Package.t * free(Import_t * imp) {
	if (imp == NULL) return NULL;
	
	Package.t * pkg = imp->pkg;
	/*fprintf(stderr, "  -> %s (%lu)\n", imp->filename, pkg ? pkg->refcount : 0);*/
	if (imp->alias == imp->filename) {
		global.free(imp->alias);
	} else {
		global.free(imp->alias);
		global.free(imp->filename);
	}

	if (imp->symbols) {
		hash_each_val(imp->symbols, {
			symbol.free(val);
		});
		hash_free(imp->symbols);
	}

	global.free(imp);
	return pkg;
}

export Import_t * add(char * alias, char * filename, Package.t * parent, Errors.t * errors) {
	Import_t * imp = malloc(sizeof(Import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_PARSED;
	imp->pkg      = Package.new(filename, errors, parent->opts);
	imp->symbols  = hash_new();
	imp->pkg      = Package.parse_file(imp->pkg, filename, errors);

	if (imp->pkg == NULL) return NULL;

	pkg_export.write_headers(imp->pkg);
	return imp;
}

export Import_t * queue(char * alias, char * filename, Package.t * parent, Errors.t * errors) {
	Import_t * imp = malloc(sizeof(Import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_QUEUED;
	imp->pkg      = Package.new(filename, errors, parent->opts);
	imp->symbols  = hash_new();

	if (imp->pkg == NULL) return NULL;

	if (imp->pkg->status >= PKG_PARSED) {
		imp->type = IMPORT_PARSED;
	}

	return imp;
}

export int reconcile(Import_t * imp, Errors.t * errors) {
	if (imp == NULL || imp->type == IMPORT_C_FILE || imp->type == IMPORT_H_FILE) return 0;

	int i;

	hash_each(imp->symbols, {	
		char * source_abs = strdup(key);
		pkg_export.t * exp = hash_get(imp->pkg->exports, source_abs);
		if (exp) {
			symbol.resolve(val, exp->symbol);
		} else {
			if (imp->pkg->opts.verbosity >= DEBUG) {
				fprintf(stderr, "package %s doesn't export symbol %s\n", imp->pkg->name, key);
				fprintf(stderr, "deps for imp->pkg->name: [");
			}
			for (i = 0; i < imp->pkg->n_exports; i++) {
				pkg_export.t * e = imp->pkg->ordered[i];
				if (e->symbol[0] == 0) continue;
				//fprintf(stderr, "%s%s", e->symbol, (i == imp->pkg->n_exports - 1) ? "": ", ");
			}
			if (imp->pkg->opts.verbosity >= DEBUG) {
				fprintf(stderr, "]\n");
			}
			symbol.resolve_error(val, imp->pkg->name, imp->pkg->source_abs, errors);
		}
		global.free(source_abs);
	});
	imp->type = IMPORT_PARSED;

	return 0;
}

export Import_t * add_c_file(Package.t * parent, char * filename, char ** error) {
	char * alias = realpath(filename, NULL);
	if (alias == NULL) {
		if (error) *error = strerror(errno);
		return NULL;
	}

	Import_t * imp = calloc(1, sizeof(Import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_C_FILE;
	imp->pkg      = Package.c_file(strdup(alias), parent->opts);

	return imp;
}


export Import_t * add_h_file(Package.t * parent, char * filename) {
	char * alias = realpath(filename, NULL);
	if (alias == NULL) {
		return NULL;
	}

	Import_t * imp = calloc(1, sizeof(Import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_H_FILE;
	imp->pkg      = Package.c_header(strdup(alias), parent->opts);

	return imp;
}

export Import_t * passthrough(Package.t * parent, char * filename, Errors.t * errors) {
	Import_t * imp = add(filename, filename, parent, errors);

	if (imp == NULL || imp->pkg == NULL) {
		Errors.error_t e = {
			.severity = ERROR,
			.is_fatal = false,
		};

		asprintf(&e.message, "Could not import '%s'", filename);
		Errors.append(errors, e);
		return NULL;
	}

	hash_each_val(imp->pkg->exports, {
		pkg_export.t * exp = (pkg_export.t *) val;
		hash_set(parent->exports, exp->export_name, exp);
	});

	pkg_export.export_headers(parent, imp->pkg);
	return imp;
}
