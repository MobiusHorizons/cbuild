package "package_export";

#include <stdlib.h>
#include <stdio.h>
#include "../deps/hash/hash.h"
#include <stdbool.h>

import Package  from "./package.module.c";
import lex_item from "../lexer/item.module.c";
import stack    from "../lexer/stack.module.c";
import stream   from "../deps/stream/stream.module.c";
import atomic   from "./atomic-stream.module.c";
import utils    from "../utils/utils.module.c";
import str      from "../utils/strings.module.c";
build  depends       "../deps/hash/hash.c";

export enum export_type {
	type_block = 0,
	type_header,
	type_type,
	type_variable,
	type_function,
	type_enum,
	type_union,
	type_struct,
} as type;

const char * type_names[] = {
	"block",
	"header",
	"type",
	"variable",
	"function",
	"enum",
	"union",
	"struct",
};

export const char * get_type_name(enum export_type t) {
	return type_names[t];
}

export typedef struct {
	char             * local_name;
	char             * export_name;
	char             * symbol;
	stack.t          * declaration;
	enum export_type   type;
} Export_t as t;

export void free(Export_t * exp) {
	if (exp == NULL) return;

	if (exp->local_name != exp->export_name) {
		global.free(exp->export_name);
	} 
	global.free(exp->local_name);
	global.free(exp->symbol);
	stack.free_items(exp->declaration);
	stack.free(exp->declaration);
	global.free(exp);
}

export char * add(
		char * local,
		char * alias,
		char * symbol,
		enum export_type type,
		stack.t * declaration,
		Package.t * parent
) {

	char * export_name = alias == NULL ? local : alias; 
	export_name = strdup(export_name);

	// Tagged types
	if (type == type_struct || type == type_enum || type == type_union) {
		// set the high bit of the character to represent a tagged type.
		export_name[0] |= 0x80;
		local[0] |= 0x80;
	}

	if (type > type_header && hash_has(parent->exports, export_name)) {
		global.free(local);
		global.free(alias);
		global.free(symbol);
		stack.free_items(declaration);
		stack.free(declaration);
		return export_name;
	}

	Export_t * exp = malloc(sizeof(Export_t));

	exp->local_name  = local;
	exp->export_name = export_name;
	exp->symbol      = symbol;
	exp->declaration = declaration;
	exp->type        = type;

	/*fprintf(stderr, "typeof(%s) = %s (%d)\n", symbol, type, exp->type);*/

	parent->ordered = realloc(
		parent->ordered,
		sizeof(void*) * (parent->n_exports + 1)
	);
	parent->ordered[parent->n_exports] = exp;
	parent->n_exports ++;

	hash_set(parent->exports, exp->export_name, exp);
	hash_set(parent->symbols, exp->local_name,  exp);

	/**
	fprintf(stderr, "symbol %s_%s\n", parent->name, export_name);
	fprintf(stderr, "{\n"
			"  local_name : '%s',\n"
			"  export_name: '%s',\n"
			"  symbol     : '%s',\n"
			"  type       : '%s',\n"
			"}\n", local, export_name, symbol, type);
			*/

	return exp->export_name;
}

export char * get_header_path(char * generated) {
	char * header = str.dup(generated);
	size_t len = str.len(header);
	header[len - 1] = 'h';
	return header;
}

#define B(a) (a ? "true" : "false")

export void write_headers(Package.t * pkg) {
	if (pkg->status <  PKG_RESOLVED) return;
	if (pkg->status >= PKG_EXPORTED) return;

	if (pkg->opts.force == false && (pkg->opts.silent || !utils.newer(pkg->source_abs, pkg->header))) return;

	stream.t * header = atomic.open(pkg->header);
	stream.printf(header, "#ifndef _package_%s_\n" "#define _package_%s_\n\n", pkg->name, pkg->name);

	bool had_newline   = true;
	enum export_type last_type = type_header;

	{ // add the headers
		hash_each_val(pkg->exported_headers, {
				had_newline = false;
				stream.write(header, val, strlen(val));
		});
	}

	int i;
	for (i = 0; i < pkg->n_exports; i++){
			Export_t * exp = (Export_t *) pkg->ordered[i];

			lex_item.t * first_token = stack.first(exp->declaration);
			lex_item.t * last_token  = stack.last(exp->declaration);
			bool multiline = first_token && last_token && first_token->line != last_token->line;

			if (had_newline == false && (last_type != exp->type || multiline)) {
				stream.write(header, "\n", 1);
			}

			size_t t;
			for (t = 0; t < exp->declaration->length; t++) {
				lex_item.t * tok = exp->declaration->items[t];
				stream.write(header, tok->value, tok->length);
			}
			stream.write(header, "\n", 1);

			if (multiline) {
				had_newline = true;
				stream.write(header, "\n", 1);
			} else {
				had_newline = false;
			}
			last_type     = exp->type;
	}
	stream.printf(header, "%s#endif\n", had_newline ? "" : "\n");
	stream.close(header);
}

export void export_headers(Package.t * pkg, Package.t * dep) {
	if (pkg == NULL || dep == NULL) return;
	if (hash_has(pkg->exported_headers, dep->header)) return;

	char * rel  = utils.relative(pkg->generated, dep->header);

	char * include = NULL;
	asprintf(&include, "#include \"%s\"\n", rel);
	hash_set(pkg->exported_headers, dep->header, include);

	global.free(rel);
}
