build depends "../deps/hash/hash.c";
#include "../deps/hash/hash.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <libgen.h>

import lex_item from "../lexer/item.module.c";
import cache    from "../lexer/cache.module.c";
import stack    from "../lexer/stack.module.c";
import stream  from "../deps/stream/stream.module.c";
import file    from "../deps/stream/file.module.c";
import grammer from "../parser/grammer.module.c";
import parser  from "../parser/parser.module.c";
import utils   from "../utils/utils.module.c";
import str     from "../utils/strings.module.c";
import fs      from "../utils/fs.module.c";
import Errors  from "../errors/errors.module.c";
import Package from "./package.module.c";
import Import  from "./import.module.c";
import Export  from "./export.module.c";
import atomic  from "./atomic-stream.module.c";

static void init_cache() {
	Package.path_cache = hash_new();
	Package.id_cache   = hash_new();
}

static char * get_header_path(char * generated) {

	char * header = strdup(generated);
	size_t len = strlen(header);
	header[len - 1] = 'h';
	return header;
}

static char * package_name(const char * rel_path) {
	char * buffer   = strdup(rel_path);
	char * filename = basename(buffer);

	filename[strlen(filename) - 2] = 0; // trim the .c extension

	char * c;
	for (c = filename; *c != 0; c++){
		switch(*c) {
			case '.':
			case '-':
			case ' ':
				*c = '_';
				break;
			default:
				break;
		}
	}

	char * name = strdup(filename);
	global.free(buffer);
	return name;
}

static bool assert_name(const char * relative_path, Errors.t * errors) {
	ssize_t len      = strlen(relative_path);
	size_t  suffix_l = strlen(".module.c");

	if (len < suffix_l || strcmp(".module.c", relative_path + len - suffix_l) != 0) {
		Errors.error_t e = {
			.severity = WARNING,
			.is_fatal = false,
		};
		asprintf(&e.message, "Unsupported input filename. Expecting '<file>.module.c'");
		Errors.append(errors, e);
		return true;
	}
	return false;
}

export char * generated_name(const char * path, Package.options_t opts) {
	char * root_src = opts.root_src ? opts.root_src : "/";
	char * root_gen = opts.root_gen ? opts.root_gen : "/";
	size_t src_l = strlen(root_src);
	size_t gen_l = strlen(root_gen);
	size_t  suffix_l = strlen(".module.c");

	if (strncmp(root_src, path, src_l) == 0) {
		size_t len = strlen(path);
		size_t buffer_l = len - src_l + gen_l - suffix_l + strlen(".c") + 1;
		char * buffer = malloc(buffer_l);
		snprintf(buffer, buffer_l, "%s%.*s.c",
				root_gen,
				(int) (len - src_l - suffix_l),
				path + src_l
		);
		return buffer;
	} else {
		ssize_t len      = strlen(path);
		size_t  suffix_l = strlen(".module.c");
		size_t buffer_l  = len - suffix_l + strlen(".c") + 1;
		char * buffer    = malloc(buffer_l);
		snprintf(buffer, buffer_l, "%.*s.c", (int) (len - suffix_l), path);
		return buffer;
	}
}

export int reconcile_deps(Package.t * p, Errors.t * errors);
export Package.t * new(const char * relative_path, Errors.t * errors, Package.options_t opts);
export Package.t * parse(Package.t * p, stream.t * input, const char * relative_path, Errors.t * errors);
export Package.t * parse_file(Package.t * p, const char * relative_path, Errors.t * errors);

static void init() {
	if (Package.path_cache == NULL) init_cache();
	if (Package.new        == NULL) Package.new        = new;
	if (Package.parse      == NULL) Package.parse      = parse;
	if (Package.parse_file == NULL) Package.parse_file = parse_file;
}

/** TODO: rename to new */
export Package.t * _new(
	char * abs,
	stream.t * out,
	Package.options_t opts
) {
	init();

	char * generated = generated_name(abs, opts);

	Package.t * p = calloc(1, sizeof(Package.t));
	p->deps             = hash_new();
	p->exports          = hash_new();
	p->symbols          = hash_new();
	p->exported_headers = hash_new();
	p->ordered          = NULL;
	p->n_exports        = 0;
	p->source_abs       = abs;
	p->generated        = generated;
	p->header           = get_header_path(generated);
	p->status           = PKG_QUEUED;
	p->name             = package_name(p->generated);
	p->out              = out;
	p->opts             = opts;

	hash_set(Package.path_cache, abs, p);
	return p;
}

/** TODO: rename to get */
Package.t * new(
	const char * rel,
	Errors.t * errors,
	Package.options_t opts
) {
	init();
	assert_name(rel, errors);
	char * key = realpath(rel, NULL);
	if (key == NULL) {
		Errors.error_t e = {
			.severity = ERROR,
			.is_fatal = true,
			.message  = strdup(strerror(errno)),
		};

		Errors.append(errors, e);
		return NULL;
	}

	Package.t * cached = hash_get(Package.path_cache, key);
	if (cached != NULL) {
		global.free(key);
		cached->refcount++;
		return cached;
	}

	return _new(key, NULL, opts);
}

// Parse a package.
Package.t * parse(Package.t * p, stream.t * input, const char * rel, Errors.t * errors) {
	if (p->status >= PKG_PARSED) return p;

#ifndef SYMBOL_DEBUG
	if (p->opts.verbosity >= DEBUG) {
#endif
		fprintf(stderr, "\nParsing '%s', [%s]\n", p->name, p->source_abs);

#ifndef SYMBOL_DEBUG
	}
#endif
	p->status = PKG_PARSING;
	p->errors = grammer.parse(input, rel, p, errors);
	p->status = PKG_PARSED;

	return p;
}

export Package.t * resolve(Package.t *p, Errors.t * errors) {
	if (p->status >= PKG_RESOLVED) return p;

	if (p->opts.verbosity >= DEBUG) {
		fprintf(stderr, "resolving %s [%s]\n", p->name, p->source_abs);
	}
	p->errors = reconcile_deps(p, errors);
	p->status = PKG_RESOLVED;

	if (p->opts.force || (!p->opts.silent && utils.newer(p->source_abs, p->generated))) {
		if (p->out == NULL) {
			char * pdir = fs.dirname(strdup(p->generated));
			fs.mkdir_recursive(pdir);
			global.free(pdir);
			p->out = atomic.open(p->generated);
			if (p->out->error.code != 0) {
				/*global.free(key);*/

				Errors.error_t e = {
					.severity = ERROR,
					.is_fatal = true,
					.message  = strdup(p->out->error.message),
				};
				Errors.append(errors, e);

				atomic.abort(p->out);
				return NULL;
			}
		}

		if (p->opts.verbosity >= DEBUG) {
			fprintf(stderr, "Writing %s [%s]\n", p->name, p->generated);
		}

		// replace first blank line with package info.
		if (p->dest_cache && p->dest_cache->num_lines > 1) {
			cache.line_t * line = &p->dest_cache->lines[0];

			int i = 0;
			while ( /* find all the whitespace or newline characters */
					i < line->num_items && (
						line->items[i]->type == item_newline ||
						line->items[i]->type == item_whitespace
						)
				  ) i++;

			bool line_empty = (i == line->num_items);

			char * tok = malloc(strlen("_package__") + strlen(p->name) + 1);
			strcpy(tok, "_package_");
			strcat(tok, p->name);
			strcat(tok, "_");

			lex_item.t ** items = line->items;
			line->items = malloc((line->num_items + 5) * sizeof(lex_item.t *));

			int offset = 0;
			line->items[offset++] = lex_item.new(str.dup("#"),      item_pp_symbol,  0, 0, 0);
			line->items[offset++] = lex_item.new(str.dup("define"), item_pp_token,   0, 0, 0);
			line->items[offset++] = lex_item.new(str.dup(" "),      item_whitespace, 0, 0, 0);
			line->items[offset++] = lex_item.new(tok,               item_pp_token,   0, 0, 0);
			if (!line_empty || line->num_items == 0) {
				line->items[offset++] = lex_item.new(strdup("\n"), item_newline, 0, 0, 0);
			}

			for (i = 0; i < line->num_items; i++) {
				line->items[i + offset] = items[i];
			}
			line->num_items += offset;

			global.free(items);
		}

		cache.write(p->dest_cache, p->out);
		stream.close(p->out);
	}

	hash_each_val(p->deps, {
			Import.t * imp = (Import.t*) val;
			if (imp->type == IMPORT_PARSED && !imp->pkg->c_file) {
			resolve(imp->pkg, errors);
			Export.write_headers(imp->pkg);
			}
			});

	return p;
}

Package.t * parse_file(Package.t * p, const char * rel, Errors.t * errors) {
	stream.t * input = file.open(rel, O_RDONLY);
	if (input->error.code != 0) {
		Errors.error_t e = {
			.severity = ERROR,
			.is_fatal = true,
			.message  = strdup(input->error.message),
		};
		Errors.append(errors, e);
		return NULL;
	}
	return parse(p, input, rel, errors);
}

export int parse_queued(Import.t * imp, Errors.t * errors) {
	char * rel = utils.relative(imp->pkg->opts.cwd, imp->pkg->source_abs);
	Package.t * parsed = parse_file(imp->pkg, rel, errors);
	global.free(rel);

	if (parsed == NULL) return 1;
	return errors->count;
}

/** reconcile_deps updates all of the symbols which were pending from all of this package's dependencies. */
int reconcile_deps(Package.t * p, Errors.t * errors) {
	hash_each_val(p->deps, {
			Import.t * imp = (Import.t*) val;
			if (imp->pkg) {
			if (imp->pkg->opts.verbosity >= DEBUG) {
			fprintf(stderr, "    - %s [%s]\n", imp->filename, Package.status_names[imp->pkg->status]);
			}
			if (imp->pkg->status <= PKG_QUEUED) parse_queued(imp, errors);
			if (imp->pkg->status >= PKG_PARSED) Import.reconcile(imp, errors);
			}
			});
	return p->errors;
}

export void free(Package.t * pkg) {
	if (pkg == NULL) return;
	if (pkg->refcount > 0) {
		pkg->refcount--;
		return;
	}
	if (Package.path_cache && hash_has(Package.path_cache, pkg->source_abs)) {
		hash_del(Package.path_cache, pkg->source_abs);
	}
	if (Package.path_cache && hash_has(Package.id_cache, pkg->name)) {
		hash_del(Package.id_cache, pkg->name);
	}

	// exports
	int i;
	for (i = 0; i < pkg->n_exports; i++) {
		Export.free((Export.t *) pkg->ordered[i]);
	}
	global.free(pkg->ordered);

	/*fprintf(stderr, "Freeing %s\n", pkg->source_abs);*/
	hash_free(pkg->exports);
	hash_free(pkg->symbols);

	if (pkg->name != pkg->source_abs) global.free(pkg->source_abs);
	if (pkg->name != pkg->generated)  global.free(pkg->generated);
	if (pkg->name != pkg->header)     global.free(pkg->header);
	global.free(pkg->name);

	// imports
	if (pkg->deps) {
		hash_each_val(pkg->deps, {
			free(Import.free((Import.t *) val));
		});
		hash_free(pkg->deps);
	}

	for (i = 0; i < pkg->n_variables; i++) {
		Package.var_t v = pkg->variables[i];
		global.free(v.name);
		global.free(v.value);
	}
	global.free(pkg->variables);

	for (i = 0; i < pkg->n_rules; i++) {
		Package.rule_t r = pkg->rules[i];
		global.free(r.target);
		global.free(r.deps);
		global.free(r.recipe);
	}
	global.free(pkg->rules);

	if (pkg->exported_headers) {
		hash_each_val(pkg->exported_headers, {
			global.free(val);
		});
	}
	hash_free(pkg->exported_headers);

	cache.free(pkg->source_cache);
	cache.free(pkg->dest_cache);

	global.free(pkg);
}
