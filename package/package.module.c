build depends "../deps/hash/hash.c";
export #include "../deps/hash/hash.h"
export #include <stdlib.h>
export #include <stdbool.h>
#include <stdio.h>
#include <libgen.h>

import stream   from "../deps/stream/stream.module.c";
import file     from "../deps/stream/file.module.c";
import Errors   from "../errors/errors.module.c";
import lex_item from "../lexer/item.module.c";
import fs       from "../utils/fs.module.c";
import atomic   from "./atomic-stream.module.c";
import cache    from "../lexer/cache.module.c";

export enum var_type {
	build_var_set = 0,
	build_var_set_default,
	build_var_append,
};

export typedef struct {
	char * name;
	char * value;
	enum var_type operation;
} var_t;

export typedef struct {
	char * target;
	char * deps;
	char * recipe;
	unsigned char is_generic;
} rule_t;

export enum status_e {
	PKG_EMPTY = 0, // uninitialized
	PKG_QUEUED,    // package is collecting references, but is not yet been looked at.
	PKG_PARSING,   // package is in the process of being parsed.
	PKG_PARSED,    // package has been parsed, but not all dependencies have been resolved.
	PKG_RESOLVED,  // package has been parsed and dependencies have been resolved.
	PKG_EXPORTED,  // package has been output to disk.
};

export const char * status_names[] = {
	"EMPTY",
	"QUEUED",
	"PARSING",
	"PARSED",
	"RESOLVED",
	"EXPORTED",
};

export enum verbosity_t {
	SILENT = 0,
	DEBUG,
};

export typedef struct {
	bool       force;
	bool       silent;
	enum       verbosity_t verbosity;
	char *     root_src;
	char *     root_gen;
	char *     cwd;
} options_t;

export typedef struct {
	hash_t   * deps;
	hash_t   * exports;
	hash_t   * symbols;
	hash_t   * exported_headers;
	void    ** ordered;
	size_t     n_exports;
	var_t    * variables;
	size_t     n_variables;
	rule_t   * rules;
	size_t     n_rules;
	size_t     refcount;
	char     * name;
	char     * source_abs;
	char     * generated;
	char     * header;
	char     * target;
	size_t     errors;
	enum status_e status;
	bool       c_file;
	options_t opts;
	cache.t * source_cache;
	cache.t * dest_cache;
	stream.t * out;
} package_t as t;

export hash_t * path_cache = NULL;
export hash_t * id_cache = NULL;

/* TODO: fix the circrular dependency issue */
export package_t * (*new)(const char * relative_path, Errors.t * errors, options_t opts) = NULL;

export package_t * (*parse)(package_t * p, stream.t * input, const char * relative_path, Errors.t * errors) = NULL;

export package_t * (*parse_file)(package_t * p, const char * relative_path, Errors.t * errors) = NULL;

export void emit(package_t * pkg, lex_item.t * token) {
	if (token) {
		if (pkg->dest_cache) {
			lex_item_cache_insert(pkg->dest_cache, token);
			/*if (token->source == token) {*/
				/*token->refcount++;*/
			/*}*/
		}
		/*if (pkg->out) {*/
			/*stream.write(pkg->out, token->value, token->length);*/
		/*}*/
	}
}

char * get_generated_path(char * path, options_t opts) {
	char * root_src = opts.root_src ? opts.root_src : "/";
	char * root_gen = opts.root_gen ? opts.root_gen : "/";
	size_t src_l = strlen(root_src);
	size_t gen_l = strlen(root_gen);

	char * filename = basename(path);
	char * suffix = index(filename, '.');
	size_t  suffix_l = strlen(suffix);

	if (strncmp(root_src, path, src_l) == 0) {
		size_t len = strlen(path);
		size_t buffer_l = len - src_l + gen_l + 1;
		char * buffer = malloc(buffer_l);
		snprintf(buffer, buffer_l, "%s%.*s%s",
				 root_gen,
				 (int)(len - src_l - suffix_l), path + src_l,
				 suffix
		);

		return buffer;
	} else {
		ssize_t len      = strlen(path);
		char * buffer    = malloc(len + 1);
		snprintf(buffer, len, "%.*s%s",
				(int)(len - suffix_l), path,
				suffix
		);
		return buffer;
	}
}

ssize_t copy_generated(char * from, char * to) {
	if (strcmp(from, to) == 0) return 0;

	stream.t * in = file.open(from, O_RDONLY);
	char * pdir = fs.dirname(strdup(to));
	fs.mkdir_recursive(pdir);
	global.free(pdir);
	stream.t * out = atomic.open(to);
	ssize_t ret = stream.pipe(in, out);
	stream.close(in);
	stream.close(out);
	return ret;
}

export package_t * c_file(char * abs_path, options_t opts) {
	package_t * cached = hash_get(path_cache, abs_path);
	if (cached != NULL) {
		cached->refcount++;
		global.free(abs_path);
		return cached;
	}

	char * generated = get_generated_path(abs_path, opts);
	if (opts.silent == false) {
		copy_generated(abs_path, generated);
	}

	package_t * pkg = calloc(1, sizeof(package_t));

	pkg->name       = abs_path;
	pkg->source_abs = abs_path;
	pkg->generated  = generated;
	pkg->c_file     = true;
	pkg->status     = PKG_PARSED;

	hash_set(path_cache, abs_path, pkg);
	return pkg;
}

export package_t * c_header(char * abs_path, options_t opts) {
	package_t * cached = hash_get(path_cache, abs_path);
	if (cached != NULL) {
		cached->refcount++;
		global.free(abs_path);
		return cached;
	}

	char * generated = get_generated_path(abs_path, opts);
	if (opts.silent == false) {
		copy_generated(abs_path, generated);
	}

	package_t * pkg = calloc(1, sizeof(package_t));

	pkg->name       = abs_path;
	pkg->source_abs = abs_path;
	pkg->header     = abs_path;
	pkg->generated  = generated;
	pkg->c_file     = true;
	pkg->status     = PKG_EXPORTED;

	hash_set(path_cache, abs_path, pkg);
	return pkg;
}
