#define VERSION "v2.0.0"

package "main";

#include <stdio.h>
#include <stdbool.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include "deps/hash/khash.h"

build append CFLAGS "-std=c99";
build append CFLAGS "-D_DEFAULT_SOURCE";
build append CFLAGS "-D_GNU_SOURCE";

import Errors     from "errors/errors.module.c";
import stream     from "deps/stream/stream.module.c";
import file       from "deps/stream/file.module.c";
import Pkg        from "package/index.module.c";
import lex_item   from "lexer/item.module.c";
import cache      from "lexer/cache.module.c";
import Package    from "package/package.module.c";
import pkg_import from "package/import.module.c";
import pkg_export from "package/export.module.c";
import utils      from "utils/utils.module.c";
import fs         from "utils/fs.module.c";
import makefile   from "makefile.module.c";
import cli        from "cli.module.c";

typedef struct {
  bool force;
  bool debug; 
  const char * output;
} options_t;

Package.t * generate(const char * filename, const char * exec_name, options_t * o, bool silent) {
  Errors.t errors = {0};

  Package.options_t opts = {
	.silent    = silent,
	.force     = o->force,
	.verbosity = o->debug ? DEBUG : SILENT,
	.cwd       = utils.getcwd(),
  };
  stream.t * err = file.new(2);

  if (o->output) {
	
	if (fs.mkdir_recursive(o->output) == -1) {
		//error(1, errno, "can't create output directory %s", o->output);
		fflush(stdout);
		fprintf(stderr, "%s: can't create output directory %s", strerror(errno), o->output);
	} else {
		opts.root_src = fs.dirname(realpath(filename, NULL));
		opts.root_gen = realpath(o->output, NULL);
	}
  }
  Package.t * pkg = Pkg.new(filename, &errors, opts);
  // find if there are any sources not in a child of root_src
  // update opts to point to the lowest common denominator.
  if (pkg) {
	  Package.parse_file(pkg, filename, &errors);
	  Pkg.resolve(pkg, &errors);
  }


  if (false && pkg && pkg->dest_cache) {
	cache.t * c = (cache.t *) pkg->source_cache;
	size_t l;
	for (l = 0; l < c->num_lines; l ++) {
		size_t i;
		cache.line_t * line = &c->lines[l];
		lex_item.t * replaced = NULL;
		for (i = 0; i < line->num_items; i++) {
			lex_item.t * item = line->items[i];
			/*stream.write(err, item->value, item->length);*/
			if (replaced != NULL) {
				if (replaced == item->replaced_by) {
					stream.write(err, item->value, item->length);
				} else {
					stream.printf(err, "' => '%s'\n", replaced->value);
					replaced = NULL;
				}
			}
			if (replaced == NULL && item->replaced_by) {
				replaced = item->replaced_by;
				stream.printf(err, "%d: '%s", item->line, item->value);
			}
		}
	}
  }

  if (errors.count) {
	char * abs = malloc(strlen(opts.cwd) + strlen(filename) + 1);
	sprintf(abs, "%s%s", opts.cwd, filename);

	int i;
	for (i = 0; i < errors.count; i++) {
		Errors.error_t e = errors.errors[i];
		if (!e.context.filename) {
			// handle loading the root module
			e.context.filename = abs;
		}
		Errors.print(e, err, opts.cwd);

	}
	stream.printf(err, "got %d errors\n", i);
	Errors.free_all(errors);
	Pkg.free(pkg);
	global.free(abs);
	global.free(opts.cwd);
	global.free(opts.root_gen);
	global.free(opts.root_src);
	file.free(err);

    return NULL;
  }

  if (strcmp(pkg->name, "main") != 0) {
	  pkg_export.write_headers(pkg);
  }

  char * cbuild_exec_name = NULL;
  if (utils.is_absolute(exec_name) || !utils.is_relative(exec_name)) {
	  // absolute or path based
	  cbuild_exec_name = strdup(exec_name);
  } else {
	  // relative path
	  char * exec_abs = realpath(exec_name, NULL);
	  char * gen_dir = NULL;
	  if (opts.root_gen) {
		  gen_dir = malloc(strlen(opts.root_gen) + 2);
		  strcpy(gen_dir, opts.root_gen);
		  strcat(gen_dir, "/");
	  } else {
		  gen_dir = utils.getcwd();
	  }

	  cbuild_exec_name = utils.relative_exe(gen_dir, exec_abs);
	  global.free(exec_abs);
	  global.free(gen_dir);
  }

  global.free(makefile.write(pkg, cbuild_exec_name));

  global.free(cbuild_exec_name);
  global.free(opts.root_gen);
  global.free(opts.root_src);
  global.free(opts.cwd);
  file.free(err);
  return pkg;
}

int do_generate(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;

  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }
  if (opts->force) printf("FORCED REBUILD\n");

  Package.t * root = generate(cli->argv[0], cli->name, opts, false);
  if (root == NULL) exit(1);
  Pkg.free(root);
  return 0;
}

int do_build(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;
  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }

  Package.t * root = generate(cli->argv[0], cli->name, opts, false);
  if (root == NULL) exit(1);

  int result = makefile.make(root, makefile.get_makefile_name(root->generated));

  Pkg.free(root);
  if (result != 0) exit(result);
  return 0;
}

int do_run(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;
  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }

  const char * target = cli->argc > 1 ? cli->argv[0] : NULL;
  const char * root_file = cli->argc > 1 ? cli->argv[1] : cli->argv[0];

  Package.t * root = generate(root_file, cli->name, opts, false);
  if (root == NULL) exit(1);
  char * mkfile_name = makefile.get_makefile_name(root->generated);


  int result = 0;
  if (target) {
	  result = makefile.run(root, mkfile_name, target);
  } else {
	  result = makefile.make(root, mkfile_name);
	  if (result != 0) exit(result);
	  char * executable = makefile.get_build_target(root);
	  char * path;
	  asprintf(&path, "%s/%s", opts->output ?: ".", executable);

	  if (target) cli->argv++;
	  cli->argv[0] = path;

	  const char ** argv = cli->argv;
	  if (opts->debug) {
		  fprintf(stderr, "\nRunning the following command:\n");
		  while(*argv != NULL) {
			  fprintf(stderr, "%s ", *argv);
			  argv++;
		  }
		  fprintf(stderr, "\n\n");
	  }

	  result = execv(path, (char * const *)cli->argv);
	  fprintf(stderr, "Error executing %s: %s\n", path, strerror(errno));
	  exit(1);
  }
  exit(result);
  return 0;
}

void clean_generated(Package.t * pkg) {
  if (pkg == NULL || pkg->c_file || pkg->status != PKG_EXPORTED) return;
  pkg->status--;

  if (pkg->generated) {
    printf("unlink: %s\n", pkg->generated);
    global.unlink(pkg->generated);
  }
  if (pkg->header) {
    printf("unlink: %s\n", pkg->header);
    global.unlink(pkg->header);
  }

  hash_each_val(pkg->deps, {
    pkg_import.t * imp = (pkg_import.t *) val;
    clean_generated(imp->pkg);
  });
}

int do_clean(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;

  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }

  Package.t * root = generate(cli->argv[0], cli->name, opts, true);
  if (root == NULL) exit(-1);

  int result = makefile.clean(root, makefile.get_makefile_name(root->generated));
  clean_generated(root);
  if (result != 0) exit(result);
  return 0;
}

int main(int argc, const char ** argv){
  options_t options = {0};

  cli.t * c = cli.new("<root module>");
  cli.flag_bool(c, &options.force, (cli.flag_options) {
      .long_name   = "force",
      .short_name  = "f",
      .description = "force rebuilding assets.",
  });

  cli.flag_bool(c, &options.debug, (cli.flag_options) {
      .long_name   = "debug",
      .short_name  = "d",
      .description = "display debug diagnostics.",
  });

  cli.flag_string(c, &options.output, (cli.flag_options) {
      .long_name   = "output",
      .short_name  = "o",
      .description = "output generated files relative to <output> instead of next to the source files.",
  });

  cli.command(c, "build",    do_build,    "generate code and build.",      true,  &options);
  cli.command(c, "generate", do_generate, "generate .c .h and .mk files.", false, &options);
  cli.command(c, "clean",    do_clean,    "clean generated files.",        false, &options);
  cli.command(c, "run",      do_run,      "build and run a target.",       false, &options);

  int result = cli.parse(c, argc, argv);
  cli.free(c);
  return result;
} 
