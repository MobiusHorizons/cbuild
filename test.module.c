package "main";

#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include "utils/colors.h"

build append CFLAGS "-std=c99";
build append CFLAGS "-D_DEFAULT_SOURCE";
build append CFLAGS "-D_GNU_SOURCE";
build append CFLAGS "-g3";
build set default SANITIZER "-fsanitize=address";
build append CFLAGS "$(SANITIZER)"; // unfortunately this does not work on travis yet.
//build append CFLAGS "-DMEM_DEBUG";


build depends "deps/hash/hash.c";
#include "deps/hash/hash.h"
#include "deps/hash/khash.h"

import Pkg         from "package/index.module.c";
import Package     from "package/package.module.c";
import pkg_import  from "package/import.module.c";
import pkg_export  from "package/export.module.c";
import lex_item    from "lexer/item.module.c";
import stack       from "lexer/stack.module.c";
import string      from "utils/string-stream.module.c";
import strings     from "utils/strings.module.c";
import Errors      from "errors/errors.module.c";
import stream      from "deps/stream/stream.module.c";
import file        from "deps/stream/file.module.c";
import utils       from "utils/utils.module.c";
import json_string from "deps/json/string.module.c";

#define LEN(array) (sizeof(array)/sizeof(array[0]))

struct test_case_s;
typedef bool (*test_fn)(Package.t * pkg, struct test_case_s c, char * out, char ** error);

typedef struct test_case_s {
	char * name;
	char * desc;
	char * input;
	char * output;
	int errors;
	test_fn fn;
	void * data;
} test_case;

typedef struct {
	size_t total;
	size_t passed;
} results_t;

static char * cwd;

static bool check_pkg_name(Package.t * pkg, struct test_case_s c, char * out, char ** error) {
	char * desired_name = (char *) c.data;
	bool passed = strcmp(pkg->name, desired_name) == 0;
	if (!passed) {
		asprintf(error, "Incorrect Package Name: Got '%s' expected '%s'\n", pkg->name, desired_name);
	}
	return passed;
}

static bool check_pkg_target(Package.t * pkg, struct test_case_s c, char * out, char ** error) {
	char * desired_target = (char *) c.data;
	bool passed = strings.cmp(pkg->target, desired_target) == 0;
	if (!passed) {
		asprintf(error, "Incorrect Package Target: Got '%s' expected '%s'\n", pkg->target, desired_target);
	}
	return passed;
}

static const char * var_ops[] = {
	":=",
	"?=",
	"+=",
};

static bool check_pkg_exports(Package.t * pkg, struct test_case_s c, char * out, char ** error) {
	char * should_export = (char *) c.data;
	pkg_export.t * exp = (pkg_export.t*) hash_get(pkg->exports, should_export);
	if (exp == NULL) {
		char * exports = "";
		if (pkg->n_exports) {
			stream.t * buf = string.new_writer(&exports);

			int i;
			for (i = 0; i < pkg->n_exports; i++) {
				pkg_export.t * exp = (pkg_export.t*)pkg->ordered[i];
				stream.printf(buf, "\t%s: '%s',\n", exp->export_name, exp->symbol);
			}

			stream.close(buf);
		}
		asprintf(error, "package '%s' did not export the expected symbol: '%s'.\n%s%s",
				pkg->name, should_export, 
				pkg->n_exports ? "the following symbols are exported:\n" : "No symbols were exported.",
				exports
				);


		return false;
	}
	return true;
}

bool check_vars(Package.t * pkg, struct test_case_s c, char * out, char ** error) {
	Package.var_t * expected = (Package.var_t *) c.data;
	if (pkg->n_variables == 0) {
		asprintf(
				error,
				"No variables exported. Expecting %s %s \"%s\"\n",
				expected->name,
				var_ops[expected->operation],
				expected->value
				);

		return false;
	}

	Package.var_t * actual = &pkg->variables[0];
	if (
			strcmp(actual->name, expected->name) != 0 ||
			strcmp(actual->value, expected->value) != 0 ||
			actual->operation != expected->operation
	   ) {
		asprintf(
				error,
				"Unexpected variable:\n"
				"\tExpected: %s %s \"%s\"\n"
				"\tActual  : %s %s \"%s\"",
				expected->name,
				var_ops[expected->operation],
				expected->value,
				actual->name,
				var_ops[actual->operation],
				actual->value
				);
		return false;
	}

	return true;
}

/** 
 * Checks if the package imports a specific file.
 */
bool check_deps(Package.t * pkg, struct test_case_s c, char * out, char ** error) {

	if (hash_has(pkg->deps, (char*)c.data)) {
		return true;
	}

	char * expected_dep = realpath((char *) c.data, NULL);
	if (expected_dep == NULL) {
		asprintf(error, "Unable to resolve '%s', %s", (char*)c.data, strerror(errno)); 
		return false;
	}

	if (hash_has(pkg->deps, expected_dep)) {
		free(expected_dep);
		return true;
	}

	char * deps = NULL;
	size_t deps_len = 0;
	hash_each_key(pkg->deps, {
			char * dep = (char *) key;
			size_t len = strlen(dep);
			deps = realloc(deps, deps_len + len + 6);
			deps_len += sprintf(deps + deps_len, "\n\t - %s", dep);
			});

	asprintf(
			error,
			"Package %s does not depend on '%s':%s",
			pkg->name,
			expected_dep,
			deps
			);

	free(deps);
	free(expected_dep);
	return false;
}

/**
 * Checks for a rule that matches.
 */
bool check_rules(Package.t * pkg, struct test_case_s c, char * out, char ** error) {
	Package.rule_t * expected = (Package.rule_t*) c.data;

	if (pkg->n_rules == 0) {
		asprintf(
				error,
				"No rules exported. Expecting {\n"
				"    target: %s,\n"
				"    deps: %s,\n"
				"    recipe: %s,\n"
				"    is_generic: %s,\n"
				"}",
				expected->target,
				expected->deps,
				expected->recipe,
				expected->is_generic ? "true" : "false"
				);

		return false;
	}

	Package.rule_t * actual = &pkg->rules[0];
	if (
			(actual->target != expected->target && strcmp(actual->target, expected->target) != 0 ) ||
			(actual->deps != expected->deps && strcmp(actual->deps, expected->deps) != 0) ||
			(actual->recipe != expected->recipe && strcmp(actual->recipe, expected->recipe) != 0) ||
			actual->is_generic != expected->is_generic
	   ) {

		char * expected_recipe = NULL;
		if (expected->recipe) {
			stream.t * expected_recipe_buf = string.new_writer(&expected_recipe);
			json_string.encode(0, expected_recipe_buf, expected->recipe);
			stream.close(expected_recipe_buf);
		} else {
			expected_recipe = strdup("null");
		}

		char * actual_recipe = NULL;
		if (actual->recipe) {
			stream.t * actual_recipe_buf = string.new_writer(&actual_recipe);
			json_string.encode(0, actual_recipe_buf, actual->recipe);
			stream.close(actual_recipe_buf);
		} else {
			actual_recipe = strdup("null");
		}

		asprintf(
				error,
				"Unexpected rule:\n"
				"Expected {\n"
				"    target: %s,\n"
				"    deps: %s,\n"
				"    recipe: %s,\n"
				"    is_generic: %s,\n"
				"}\n\n"
				"Actual {\n"
				"    target: %s,\n"
				"    deps: %s,\n"
				"    recipe: %s,\n"
				"    is_generic: %s,\n"
				"}",
				expected->target,
				expected->deps,
				expected_recipe,
				expected->is_generic ? "true" : "false",
				actual->target,
				actual->deps,
				actual_recipe,
				actual->is_generic ? "true" : "false"
				);

		free(expected_recipe);
		free(actual_recipe);
		return false;
	}

	return true;
}


/**
 * Checks the declarations of the eported symbols.
 */
static bool check_declarations(Package.t * pkg, struct test_case_s c, char * out, char ** error) {
	char * expected = (char *) c.data;
	if (pkg->n_exports == 0) {
		asprintf(
			error,
			"No declaration exported:\nExpected:\n\t%s",
			expected
		);
		return false;
	}

	pkg_export.t * exp = (pkg_export.t *) pkg->ordered[0];
	size_t len = 0;
	int i;

	for (i = 0; i < stack.height(exp->declaration); i++)
		len += stack.peek(exp->declaration, i)->length;

	char * actual = malloc(len + 1);

	len = 0;
	for (i = 0; i < stack.height(exp->declaration); i++) {
		lex_item.t * tok = stack.peek(exp->declaration, i);
		memcpy(actual + len, tok->value, tok->length);
		len += tok->length;
	}
	actual[len] = 0;

	if (strcmp(expected, actual) != 0) {
		asprintf(
			error,
			"Incorrect declaration:\nExpected:\n\t%s\nActual:\n\t%s",
			expected,
			actual
		);

		return false;
	}
	return true;
}

static test_case package[] = {
	{
		.name   = "package_name.module.c",
		.desc   = "It should choose the correct default package name",
		.input  = "export struct a { int a; };",
		.output = "struct package_name_a { int a; };",
		.fn     = check_pkg_name,
		.data   = "package_name",
		.errors = 0,
	},
	{
		.name   = "package_name.module.c",
		.desc   = "It should update the package name",
		.input  = "package \"test\";export struct a { int a; };",
		.output = "struct test_a { int a; };",
		.fn     = check_pkg_name,
		.data   = "test",
		.errors = 0,
	},
	{
		.name   = "package_name.module.c",
		.desc   = "The default package target should be null",
		.input  = "package \"test\";\nexport struct a { int a; };",
		.output = "#define _package_test_\nstruct test_a { int a; };",
		.fn     = check_pkg_target,
		.data   = NULL,
		.errors = 0,
	},
	{
		.name   = "package_name.module.c",
		.desc   = "It should set a default target",
		.input  = "package \"test\" \"test.bin\";\nexport struct a { int a; };",
		.output = "#define _package_test_\nstruct test_a { int a; };",
		.fn     = check_pkg_target,
		.data   = "test.bin",
		.errors = 0,
	},
};

static test_case exports[] = {
	{
		.name   = "export.module.c",
		.desc   = "It should export an int",
		.input  = "export int a;",
		.output = "int export_a;",
		.fn     = check_declarations,
		.data   = "extern int export_a;",
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased int",
		.input  = "export int a as b;",
		.output = "int export_b;",
		.fn     = check_declarations,
		.data   = "extern int export_b;",
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased int",
		.input  = "export int a123 as b = 123;",
		.output = "int export_b = 123;",
		.fn     = check_declarations,
		.data   = "extern int export_b;",
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export  a function",
		.input  = "export int a(int a1, int a2){}",
		.output = "int export_a(int a1, int a2){}",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should preserve whitespace when exporting a function",
		.input  = "export int  a   (    int      a1      ,       int        a2)         {          }",
		.output = "int  export_a   (    int      a1      ,       int        a2)         {          }",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased function",
		.input  = "export int a(int a1, int a2) as b{}",
		.output = "int export_b(int a1, int a2){}",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an enum",
		.input  = "export enum a { a, b, c, d, e, f };",
		.output = "enum export_a { a, b, c, d, e, f };",
		.fn     = check_pkg_exports,
		.data   = "\xE1", // 'a' | 0x80
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an enum with a default value",
		.input  = "export enum a { a = 0, b, c, d, e, f };",
		.output = "enum export_a { a = 0, b, c, d, e, f };",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased enum",
		.input  = "export enum a { a, b, c, d, e, f } as b;",
		.output = "enum export_b { a, b, c, d, e, f };",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export  a union",
		.input  = "export union a { int i; char c; void * v; };",
		.output = "union export_a { int i; char c; void * v; };",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased union",
		.input  = "export union a { int i; char c; void * v; } as b;",
		.output = "union export_b { int i; char c; void * v; };",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export  a struct",
		.input  = "export struct a { int a; };",
		.output = "struct export_a { int a; };",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased struct",
		.input  = "export struct a { int a; } as b;",
		.output = "struct export_b { int a; };",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export a static array of char",
		.input  = "export static const char * names[] = { \"1\" };",
		.output = "static const char * export_names[] = { \"1\" };",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export  a typedef of function pointer",
		.input  = "export typedef int (*a)(int a1, int a2);",
		.output = "typedef int (*export_a)(int a1, int a2);",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased typedef of function pointer",
		.input  = "export typedef int (*a)(int a1, int a2) as b;",
		.output = "typedef int (*export_b)(int a1, int a2);",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export  a typedef of anonymous struct",
		.input  = "export typedef struct { int a; } b;",
		.output = "typedef struct { int a; } export_b;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export  a typedef of named struct",
		.input  = "export typedef struct a { int a; } b;",
		.output = "typedef struct a { int a; } export_b;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export  a self referential struct",
		.input  = "export typedef struct a { struct a * first; struct a * last; } b;",
		.output = "typedef struct a { struct a * first; struct a * last; } export_b;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased typedef of anonymous struct",
		.input  = "export typedef struct { int a; } b as c;",
		.output = "typedef struct { int a; } export_c;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export an aliased typedef of named struct",
		.input  = "export typedef struct a { int a; } b as c;",
		.output = "typedef struct a { int a; } export_c;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export passthrough",
		.input  = "export * from \"test_data/example.module.c\";",
		.output = "#include \"test_data/example.h\"",
		.fn     = check_pkg_exports,
		.data   = "\xf4""est2", // tagged export
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export global",
		.input  = "export global void foo() {}",
		.output = "void foo() {}",
		.fn     = check_pkg_exports,
		.data   = "foo",
		.errors = 0,
	},
	{
		.name   = "export.module.c",
		.desc   = "It should export global from a main package",
		.input  = "package \"main\";\nexport void foo() {}",
		.output = "#define _package_main_\nvoid foo() {}",
		.fn     = check_pkg_exports,
		.data   = "foo",
		.errors = 0,
	},
};


static test_case symbols[] = {
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace symbols from the current package in a declaration",
		.input  = "export typedef struct { int a; } a_t;a_t b;",
		.output = "typedef struct { int a; } symbol_a_t;static symbol_a_t b;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace symbols from the current package in a function type",
		.input  = "export typedef struct { int a; } a_t;a_t b();",
		.output = "typedef struct { int a; } symbol_a_t;static symbol_a_t b();",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace symbols from the current package in function arguments",
		.input  = "export typedef struct { int a; } a_t;void b(a_t c);",
		.output = "typedef struct { int a; } symbol_a_t;static void b(symbol_a_t c);",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace symbols from the current package in an exported function type",
		.input  = "export typedef struct { int a; } a_t;export a_t b();",
		.output = "typedef struct { int a; } symbol_a_t;symbol_a_t symbol_b();",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace symbols from the current package in exported function arguments",
		.input  = "export typedef struct { int a; } a_t;export void b(a_t c);",
		.output = "typedef struct { int a; } symbol_a_t;void symbol_b(symbol_a_t c);",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace enum symbols from the current package in a declaration",
		.input  = "export enum a { a = 0 };enum a b;",
		.output = "enum symbol_a { a = 0 };static enum symbol_a b;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace union symbols from the current package in a declaration",
		.input  = "export union a { int a; };union a b;",
		.output = "union symbol_a { int a; };static union symbol_a b;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace struct symbols from the current package in a declaration",
		.input  = "export struct a { int a; };struct a b;",
		.output = "struct symbol_a { int a; };struct symbol_a b;",
		.fn     = NULL,
		.errors = 0,
	},
	{
		.name   = "symbol.module.c",
		.desc   = "It should replace typed symbols from the current package in a struct body",
		.input  = "export typedef struct { int a; } a;export struct c { a b; };",
		.output = "typedef struct { int a; } symbol_a;struct symbol_c { symbol_a b; };",
		.fn     = NULL,
		.errors = 0,
	},
};

static test_case _imports[] = {
	{
		.name   = "import.module.c",
		.desc   = "It should import other modules",
		.input  = "import example from \"test_data/example.module.c\";",
		.output = "#include \"test_data/example.h\"",
		.fn     = check_deps,
		.data   = "example",
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should depend on local header files",
		.input  = "#include \"test_data/example.h\"",
		.output = "#include \"test_data/example.h\"",
		.fn     = check_deps,
		.data   = "test_data/example.h",
		.errors = 0,
	},
};

static Package.var_t set_var = {
	.name      = "MY_VAR",
	.value     = "value",
	.operation = build_var_set,
};

static Package.var_t set_default_var = {
	.name      = "MY_VAR",
	.value     = "value",
	.operation = build_var_set_default,
};

static Package.var_t append_var = {
	.name      = "MY_VAR",
	.value     = "value",
	.operation = build_var_append,
};

static Package.rule_t basic_rule = {
	.target = "test",
	.deps   = NULL,
	.recipe = "\tsome-command\n",
};

static Package.rule_t rule_with_deps = {
	.target = "test",
	.deps   = "deps",
	.recipe = "\tsome-command\n",
};

static Package.rule_t rule_with_multiple_deps = {
	.target = "test",
	.deps   = "dep1 dep2 dep3",
	.recipe = "\tsome-command\n",
};

static Package.rule_t generic_rule = {
	.target = "%.o",
	.deps   = "%.c",
	.recipe = "\tsome-command $< $@\n",
	.is_generic = true,
};

static test_case build[] = {
	{
		.name   = "build.module.c",
		.desc   = "It should set make variables",
		.input  = "build set MY_VAR \"value\";\n",
		.output = "\n",
		.fn     = check_vars,
		.data   = &set_var,
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should set default make variables",
		.input  = "build set default MY_VAR \"value\";\n",
		.output = "\n",
		.fn     = check_vars,
		.data   = &set_default_var,
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should append to make variables",
		.input  = "build append MY_VAR \"value\";\n",
		.output = "\n",
		.fn     = check_vars,
		.data   = &append_var,
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should depend on a c file",
		.input  = "build depends \"test_data/example.c\";\n",
		.output = "\n",
		.fn     = check_deps,
		.data   = "test_data/example.c",
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should create a regular rule",
		.input  = "build rule \"test\" {\n"
			      "    some-command\n"
				  "}\n",
		.output = "#define _package_build_\n\n",
		.fn     = check_rules,
		.data   = &basic_rule,
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should create a rule with one dep",
		.input  = "build rule \"test\" \"deps\" {\n"
			      "    some-command\n"
				  "}\n",
		.output = "#define _package_build_\n\n",
		.fn     = check_rules,
		.data   = &rule_with_deps,
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should create a rule with multiple deps",
		.input  = "build rule \"test\" \"dep1\" \"dep2\" \"dep3\" {\n"
			      "    some-command\n"
				  "}\n",
		.output = "#define _package_build_\n\n",
		.fn     = check_rules,
		.data   = &rule_with_multiple_deps,
		.errors = 0,
	},
	{
		.name   = "build.module.c",
		.desc   = "It should create a generic rule",
		.input  = "build rule \"%.o\" \"%.c\"{\n"
			      "    some-command $< $@\n"
				  "}\n",
		.output = "#define _package_build_\n\n",
		.fn     = check_rules,
		.data   = &generic_rule,
		.errors = 0,
	},
};

static bool run_test(test_case c) {
	printf(BOLD "  %s: \r" RESET, c.desc); fflush(stdout);

	char * buf = NULL;
	stream.t * in  = string.new_reader(c.input);
	stream.t * out = string.new_writer(&buf);
	Package.options_t opts = {
		.silent = false,
		.force  = true,
		.cwd    = utils.getcwd(),
	};
	Errors.t errors = {0};
	char * fn_err = NULL;
	char * key = NULL;

	asprintf(&key, "%s/%s", cwd, c.name);

	bool desired_output = false;
	bool function_test = false;

	Package.t * p = Pkg._new(key, out, opts);
	if (p) {
		Pkg.parse(p, in, c.name, &errors);
		Pkg.resolve(p, &errors);

		desired_output = buf && strcmp(buf, c.output) == 0;
		function_test  = c.fn ? c.fn(p, c, buf, &fn_err) : true;

		Pkg.free(p);
	}

	bool passed = false;
	if (errors.count == c.errors && desired_output && function_test) {
		printf(GREEN "✓ " RESET BOLD "%s: \n" RESET, c.desc); fflush(stdout);
		passed = true;
	} else {
		printf(RED "✕ " RESET BOLD "%s: \n\n" RESET, c.desc); fflush(stdout);
	}

	if (errors.count) {
		int i;
		for (i = 0; i < errors.count; i++) {
			Errors.print(errors.errors[i], file.new(2), opts.cwd);
		}
	}

	if (!function_test && fn_err != NULL) {
		printf(RED    "%s\n\n" RESET, fn_err);
	}

	if (! desired_output) {
		printf(YELLOW "Input  : '%s'\n" RESET, c.input);
		printf(RED    "Output : '%s'\n" RESET, buf);
		printf(GREEN  "Expect : '%s'\n" RESET, c.output);
	}

	if (!passed) printf("\n");
	global.free(opts.cwd);
	global.free(buf);
	return passed;
}

results_t run_tests(char * group_name, test_case cases[], size_t length) {
	int i;
	size_t passed = 0;
	printf(BOLD "\n=== Test group " UNDERLINE "%s" RESET BOLD " ===\n\n" RESET, group_name);
	for ( i = 0; i < length; i++) {
		if (run_test(cases[i])) passed ++;
	}
	results_t r = {
		.total  = length,
		.passed = passed,
	};
	printf("%s", passed == length ? GREEN : RED);
	printf("\n[%s] (%lu/%lu) tests passed\n" RESET, group_name, passed, length);
	return r;
}

results_t combine_results(results_t a, results_t b) {
	a.total  += b.total;
	a.passed += b.passed;
	return a;
}

int main() {
	cwd = getcwd(NULL, 0);
	results_t r = {0};
	r = combine_results(run_tests("package", package, LEN(package)), r);
	r = combine_results(run_tests("exports", exports, LEN(exports)), r);
	r = combine_results(run_tests("symbols", symbols, LEN(symbols)), r);
	r = combine_results(run_tests("imports", _imports, LEN(_imports)), r);
	r = combine_results(run_tests("build",   build,   LEN(build)), r);

	printf("%s", r.passed == r.total ? GREEN : RED);
	printf("[all tests] (%lu/%lu) tests passed\n" RESET, r.passed, r.total);
	return r.passed == r.total ? 0 : 1;
}
