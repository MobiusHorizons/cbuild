package "errors";

export #include <stdlib.h>
export #include <stdbool.h>
#include <string.h>
#include "../utils/colors.h"

import stream from "../deps/stream/stream.module.c";
import utils from "../utils/utils.module.c";

export enum severity_t {
	ERROR = 1,
	WARNING,
	INFO,
	HINT,
};

export typedef struct {
	long long line;
	long long character;
} position_t ;

export typedef struct {
	char * filename;
	char * text;
	position_t start;
	position_t end;
} context_t;

export typedef struct {
	enum severity_t severity;
	bool is_fatal;
	char * message;
	context_t context;
} error_t;

export typedef struct {
	error_t * errors;
	size_t count;
} errors_t as t;

export int append(errors_t * e, error_t error) {
	void * resized = realloc(e->errors, sizeof(error_t) * (e->count + 1));
	if (resized == NULL) return -1;

	e->errors = resized;
	e->errors[e->count] = error;
	e->count++;
	return e->count;
}

export size_t print(error_t e, stream.t * s, char * cwd) {
	size_t ret = 0;
	if (e.context.filename) {
		char * rel = utils.relative(cwd, e.context.filename);
		ret += stream.printf(s, BOLD "%s:%ld:%d: " RESET, 
			rel,
			e.context.start.line + 1, 
			e.context.start.character + 1
		);
		global.free(rel);
	}

	switch (e.severity) {
		case HINT:
			break;
		case ERROR:
			ret += stream.printf(s, BOLDRED "error: " RESET);
			break;
		case WARNING:
			ret += stream.printf(s, BOLDMAGENTA "warning: " RESET);
			break;
		case INFO:
			ret += stream.printf(s, BOLDBLUE "note: " RESET);
			break;
	}

	ret += stream.printf(s, BOLD "%s\n" RESET, e.message);

	if (e.context.text == NULL) return 1;

	ret += stream.printf(s, "%s\n", e.context.text); // TODO: split this to allow multiple lines.

	char offset[255];
	int i;
	for (i = 0; i < e.context.start.character; i++) {
		switch (e.context.text[i]) {
			case '\t':
				offset[i] = '\t';
				break;
			default:
				offset[i] = ' ';
				break;
		}
	}
	offset[i] = 0;

	ret += stream.printf(s, GREEN "%s^\n" RESET, offset);
	return ret;
}

export void free(error_t * e) {
	global.free(e->message);
	global.free(e->context.filename);
	global.free(e->context.text);
}

export void free_all(errors_t e) {
	size_t i;
	for (i = 0; i < e.count; i++) {
		free(&(e.errors[i]));
	}
	global.free(e.errors);
}

