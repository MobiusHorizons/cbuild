#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

static char escape(const char * c) {
	switch(*c){
		case 'a':
			return '\a';
		case 'b':
			return '\b';
		case 'f':
			return '\f';
		case 'n':
			return '\n';
		case 'r':
			return '\r';
		case 't':
			return '\t';
		case 'v':
			return '\v';
		default:
			return *c;
	}
}

export char * parse_into(const char * input, char * output) {
	const char * c = input;
	char * o = output;


	if (*c != '"') {
		return NULL; // not a quoted string
	}
	c++;

	while(*c != 0 && *c != '"'){
		switch(*c) {
			case '\\':
				c++;
				*o = escape(c);
				break;
			default:
				*o = *c;
				break;
		}
		c++; o++;
	}
	*o = 0;
	return output;
}

export char * parse(char * input) {
	return parse_into(input, input);
}

export char * parse_dup(const char * input) {
	if (strlen(input) < 256) {
		char buf[256];
		char * output = parse_into(input, buf);
		if (output) output = strdup(output);
		return output;
	} else {
		char * buf = strdup(input);
		char * output = parse_into(input, buf);
		if (output) output = strdup(output);
		free(buf);
		return output;
	}
}
