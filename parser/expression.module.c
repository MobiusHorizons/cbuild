
import parser from "./parser.module.c";
import lex_item from "../lexer/item.module.c";
import stack      from "../lexer/stack.module.c";
import identifier from "identifier.module.c";

#include <stdio.h>
#include <stdlib.h>
export #include <stdbool.h>

/**
typedef int (*grammer_atom)(parser.t * p, stack.t * s);
static int log(parser.t * p, stack.t * s, grammer_atom atom, char * name) {
	static int indent;
	size_t i = stack.height(s); // first new item in the stack
	lex_item.t * start = stack.last(s);

	indent++;
	int e = atom(p, s);
	indent--;

	return e;
	if (indent != 0) return e;

	lex_item.t * end = stack.last(s);
	int multiline = end->line > start->line;

	size_t consumed = stack.height(s) - i;
	if (consumed) {
		fprintf(stderr, "%lu:%lu <%s> consumed %lu:%s",
				start->line + 1, start->start - start->line_pos,
				name, stack.height(s) - i, multiline ? "\n" : "");
	} else {
		fprintf(stderr, "%lu:%lu <%s> X",
				start->line + 1, start->start - start->line_pos, name);
	}
	while(i < stack.height(s)) {
		lex_item.t * tok = stack.peek(s, i);
		fprintf(stderr, "%s", tok ? tok->value : "NULL");
		i++;
	}
	fprintf(stderr, "\n%s", multiline ? "\n" : "");
	return e;
}
#define L(fn, p, s) log(p, s, fn, #fn)
*/

char matching(char opening_symbol) {
	switch(opening_symbol) {
		case '{':
			return '}';
		case '}':
			return '{';
		case '(':
			return ')';
		case ')':
			return '(';
		default:
			return 0;
	}
}

static int backtrack(parser.t * p, stack.t * s, size_t checkpoint) {
	while(stack.height(s) > checkpoint) parser.backup(p, stack.pop(s));
	return 0;
}

int parse_block(parser.t * p, stack.t * s, bool is_export, char end) {
	size_t checkpoint = stack.height(s);

	char start = matching(end);
	lex_item.t * tok;
	do {
		tok = parser.token(p, s);
		if (tok->type == item_id) {
			tok = identifier.parse(p, tok, is_export);
		}
		stack.push(s, tok);

		if (tok->type == item_open_symbol && tok->value[0] == start) {
			if (!parse_block(p, s, is_export, end)) {
				return backtrack(p, s, checkpoint);
			}
		}
	} while(
		tok &&
		tok->type != item_eof   &&
		tok->type != item_error &&
		!(tok->type == item_close_symbol && tok->value[0] == end)
	);
	if (tok->type == item_close_symbol) return 1;
	return backtrack(p, s, checkpoint);
}

/**
 * consume the second half of a binary operator. This should be of the form
 *   <operator> <rvalue>
 * where rvalue could be a second operator.
 */
export int binary_operator(parser.t * p, stack.t * s, bool is_export);

/**
 * rvalue tries to represent anything on the right-hand side of an equals sign.
 *
 * In reality it will only cover things that could be an rvalue in the global scope.
 *
 * in general this means for every ( find a matching ), for every { find a matching },
 * and also consume all unary and binary operators.
 */
export int rvalue(parser.t * p, stack.t * s, bool is_export) {
	size_t checkpoint = stack.height(s);

	lex_item.t * tok = parser.token(p, s);
	stack.push(s, tok);
	switch(tok->type) {
		case item_open_symbol:
			// find end of block;
			return parse_block(p, s, is_export, matching(tok->value[0]));
			break;

		case item_id:
			{
				// this can't be a type, so the export never needs to be true.
				tok = identifier.parse(p, stack.pop(s), /* is_export */ is_export);
				stack.push(s, tok);

				checkpoint = stack.height(s);
				tok = parser.token(p, s);
				stack.push(s, tok);
				// function invocation
				if (tok->type == item_open_symbol && tok->value[0] == '(') {
					parse_block(p, s, is_export, ')');
				} else if (tok->type == item_symbol) {
					// TODO call binary_operator here, but also check for unary postfix operators (eg ++ --)
					do {
						tok = parser.next(p);
						stack.push(s, tok);
					} while(tok->type == item_symbol);
					parser.backup(p, stack.pop(s));
					return rvalue(p, s, is_export);
				} else {
					backtrack(p, s, checkpoint);
				}
				return 1;
			}

		case item_number:
			binary_operator(p, s, is_export);
			return 1;

		case item_symbol:
			// unary prefix operators
			if (
					tok->value[0] == '!'  ||
					tok->value[0] == '+'  ||
					tok->value[0] == '-'  ||
					tok->value[0] == '&'  ||
					tok->value[0] == '*'  ||
					tok->value[0] == '~'
			   ) {
				return rvalue(p, s, is_export);
			}

			// Assume this is an operator.
			// TODO: determine if this should roll back or not.
			// TODO: maybe this is actually an error condition, since it can't start an rvalue.
			parser.backup(p, stack.pop(s));
			return binary_operator(p, s, is_export);

		default:
			return backtrack(p, s, checkpoint);
	}
	return 0;
}

// CHAR2 turns two chars into a single number for comparing against.
#define CHAR2(a, b) ((a << 8) | b)

int binary_operator(parser.t * p, stack.t * s, bool is_export) {
	size_t checkpoint = stack.height(s);
	lex_item.t * tok;

	tok = parser.token(p, s);
	stack.push(s, tok);

	if (tok->type != item_symbol) return backtrack(p, s, checkpoint);

	// TODO figure out how tokens are currently lexed.
	switch(CHAR2(tok->value[0], tok->value[1])) {
		/* multiplicative */
		case CHAR2('*',0):
		case CHAR2('/',0):
		case CHAR2('%',0):

		/* addition / subtraction */
		case CHAR2('+',0):
		case CHAR2('-',0):

		/* shift operators */
		case CHAR2('<','<'):
		case CHAR2('>','>'):

		/* relational */
		case CHAR2('<',0):
		case CHAR2('<','='):
		case CHAR2('>',0):
		case CHAR2('>','='):

		/* equality */
		case CHAR2('!','='):
		case CHAR2('=','='):

		/* bitwise */
		case CHAR2('&',0):
		case CHAR2('|',0):
		case CHAR2('^',0):

		/* logical */
		case CHAR2('&','&'):
		case CHAR2('|','|'):
			return rvalue(p, s, is_export);

		default:
			return backtrack(p, s, checkpoint);
	}
}

int lifetime(parser.t * p, stack.t *s, bool is_export) {
	size_t checkpoint = stack.height(s);
	lex_item.t * tok = parser.token(p, s);
	stack.push(s, tok);

	if (tok->type != item_id) return backtrack(p, s, checkpoint);

	if (
		strcmp(tok->value, "static") == 0 ||
		strcmp(tok->value, "const") == 0
	) {
		return 1;
	}

	return backtrack(p, s, checkpoint);
}

export int primative_type(parser.t * p, stack.t *s, bool is_export) {
	size_t start = stack.height(s);
	int count = 0;

	do {
		size_t checkpoint = stack.height(s);
		lex_item.t * tok = parser.token(p, s);
		stack.push(s, tok);

		if (tok->type != item_id) return backtrack(p, s, checkpoint);

		if (
			strcmp(tok->value, "long") == 0 ||
			strcmp(tok->value, "short") == 0 ||
			strcmp(tok->value, "short") == 0 ||
			strcmp(tok->value, "unsigned") == 0
		) {
			count++;
			continue;
		}

		if (
			strcmp(tok->value, "int") == 0 ||
			strcmp(tok->value, "char") == 0 ||
			strcmp(tok->value, "float") == 0 ||
			strcmp(tok->value, "double") == 0
		) {
			return 1;
		}

		if (count) {
			backtrack(p, s, checkpoint);
			return 1;
		} else {
			return backtrack(p, s, start);
		}
	} while (true);

	return 0;
}

export int id(parser.t * p, stack.t * s, bool is_export) {
	size_t checkpoint = stack.height(s);
	lex_item.t * tok = parser.token(p, s);
	stack.push(s, tok);

	if (tok->type != item_id) return backtrack(p, s, checkpoint);

	tok = identifier.parse(p, stack.pop(s), is_export);
	stack.push(s, tok);

	return 1;
}

export int function_ptr(parser.t * p, stack.t * s, bool is_export) {
	size_t checkpoint = stack.height(s);
	lex_item.t * tok;

	{ // next token must be a '('
		tok = parser.token(p, s);
		stack.push(s, tok);

		if (tok->type != item_open_symbol && tok->value[0] != '(') {
			return backtrack(p, s, checkpoint);
		}
	}

	{ // next token must be a '*'
		tok = parser.token(p, s);
		stack.push(s, tok);

		if (tok->type != item_symbol && tok->value[0] != '*') {
			return backtrack(p, s, checkpoint);
		}
	}

	// optional
	id(p, s, is_export);

	{ // next token must be a ')'
		tok = parser.token(p, s);
		stack.push(s, tok);

		if (tok->type != item_symbol && tok->value[0] != ')') {
			return backtrack(p, s, checkpoint);
		}
	}

	{ // next token must be a '('
		tok = parser.token(p, s);
		stack.push(s, tok);

		if (tok->type != item_open_symbol && tok->value[0] != '(') {
			return backtrack(p, s, checkpoint);
		}
	}

	return parse_block(p, s, is_export, ')');
}

export int type(parser.t * p, stack.t * s, bool is_export) {
	size_t checkpoint = stack.height(s);
	lex_item.t * tok;
	lex_item.t * type = NULL;

	// const / static
	while(lifetime(p, s, is_export));

	if (!primative_type(p, s, is_export)) {
		tok = parser.token(p, s);
		stack.push(s, tok);

		if (tok->type != item_id) return backtrack(p, s, checkpoint);

		// if the type is a struct name enum name, or union name, consume that token, and read the type name.
		if (
			strcmp(tok->value, "struct") == 0 ||
			strcmp(tok->value, "union") == 0  ||
			strcmp(tok->value, "enum") == 0
		) {
			type = tok;
			tok = parser.token(p, s);
			stack.push(s, tok);

			if (tok->type == item_id) {
				tok = identifier.parse_typed(p, type, stack.pop(s), is_export);
				stack.push(s, tok);
			} else if (tok->type == item_open_symbol && tok->value[0] == '{') {
				if (!parse_block(p, s, is_export, '}'))  {
					return backtrack(p, s, checkpoint);
				}
			}
		} else {
			tok = identifier.parse(p, stack.pop(s), is_export);
			stack.push(s, tok);
		}
	}

	// we have a valid type, now check for some stuff that can come after
	checkpoint = stack.height(s);

	tok = parser.token(p, s);
	stack.push(s, tok);

	// consume any number of * if this is a pointer type.
	if (tok->type == item_symbol && tok->value[0] == '*') {
		do {
			checkpoint = stack.height(s);
			tok = parser.token(p, s);
			stack.push(s, tok);
		} while(tok->type == item_symbol && tok->value[0] == '*');
		backtrack(p, s, checkpoint);
		return 1;
	}

	backtrack(p, s, checkpoint);
	return 1;
}

export int initializer(parser.t * p, stack.t * s, bool is_export) {
	size_t checkpoint = stack.height(s);
	lex_item.t * tok = NULL;

	tok = parser.token(p, s);
	stack.push(s, tok);

	if (tok->type != item_symbol || tok->value[0] != '=') {
		while(stack.height(s) > checkpoint) parser.backup(p, stack.pop(s));
		return 0;
	}

	return rvalue(p, s, is_export);
}

/**
 * Consumes a declaration.
 *
 * This can either be of two forms:
 *   - <type> <name> = <rvalue>
 *   - <type> (*name)(args)
 */
export int declaration(parser.t * p, stack.t * s, bool is_export) {
	size_t checkpoint = stack.height(s);

	int e = type(p, s, is_export);
	if (!e) return e;

	// Try for the regular syntax (<type <name> = <rvalue>)
	e = id(p, s, is_export);
	if (e) {
		// for array syntax, there may be a [] size specified here.
		size_t checkpoint = stack.height(s);
		lex_item.t * tok = parser.token(p, s);
		stack.push(s, tok);

		if (tok->type == item_open_symbol && tok->value[0] == '[') {
			if (parse_block(p, s, is_export, ']')) return 1;
		}
		backtrack(p, s, checkpoint);

		// Initialization is optional, but if it is there, consume it.
		initializer(p, s, is_export);
		return 1;
	} else {
		// Check for semicolon in case of anonymous union
		size_t checkpoint = stack.height(s);
		lex_item.t * t = parser.token(p, s);
		stack.push(s, t);
		backtrack(p, s, checkpoint);

		if (t->type == item_symbol && t->value[0] == ';') return 1;
	}

	// Try the function pointer syntax;
	e = function_ptr(p, s, is_export);
	if (!e) backtrack(p, s, checkpoint);

	return e;
}
