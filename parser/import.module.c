#include <stdarg.h>
#include <stdio.h>
#include <string.h>

import parser     from "./parser.module.c";
import string     from "./string.module.c";
import lex_item   from "../lexer/item.module.c";
import stack      from "../lexer/stack.module.c";
import pkg_import from "../package/import.module.c";
import pkg_export from "../package/export.module.c";
import Package    from "../package/package.module.c";
import utils      from "../utils/utils.module.c";
import str        from "../utils/strings.module.c";
import Errors     from "../errors/errors.module.c";

#define ERROR_PREFIX "Invalid import syntax: "
static int errorf(parser.t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	parser.verrorf(p, ERROR, false, item, NULL, fmt, args);

	return -1;
}

static lex_item.t * token(parser.t * p, stack.t * s, bool newlines) {
	lex_item.t * item = parser.next(p);
	while(item->type == item_whitespace || (newlines ? item->type == item_newline : false)){
		stack.push(s, item);
		item = parser.next(p);
	}
	return item;
}

export int parse(parser.t * p, lex_item.t * import_token) {
	stack.t * s = stack.new(8);
	stack.push(s, import_token);

	lex_item.t * alias = token(p, s, true);
	if (alias->type != item_id) {
		stack.free(s);
		return errorf(p, alias, ERROR_PREFIX "Expecting identifier, but got %s", lex_item.to_string(alias));
	}

	stack.push(s, alias);
	lex_item.t * from = token(p, s, true);
	if (from->type != item_id || strcmp(from->value, "from") != 0) {
		stack.free(s);
		return errorf(p, from, ERROR_PREFIX "Expecting 'from', but got %s", lex_item.to_string(from));
	}

	stack.push(s, from);
	lex_item.t * filename = token(p, s, true);
	if (filename->type != item_quoted_string) {
		stack.free(s);
		return errorf(p, filename, ERROR_PREFIX "Expecting filename, but got %s", lex_item.to_string(filename));
	}

	stack.push(s, filename);
	lex_item.t * semicolon = token(p, s, false);
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		stack.free(s);
		return errorf(p, semicolon, ERROR_PREFIX "Expecting ';', but got %s", lex_item.to_string(semicolon));
	}

	stack.push(s, semicolon);

	Errors.t errors = {0};
	pkg_import.t * imp = pkg_import.queue(
		str.dup(alias->value),
		str.dup(string.parse(filename->value)),
		p->pkg,
		&errors
	);

	if (errors.count != 0) {
		int i;
		for (i = 0; i < errors.count; i++) {
			Errors.error_t error = errors.errors[i];
			if (errors.errors[i].context.filename) {
				Errors.append(p->errors, error);
			} else {
				parser.severityf(p, error.severity, filename, 
					"importing file '%s': %s",
					filename->value, error.message
				);
			}
		}
	}

	if (imp == NULL) return -1;

	char * include;
	char * rel = NULL;
	if (imp->pkg->header) {
		rel = utils.relative(p->pkg->generated, imp->pkg->header);
	} else {
		char * header = pkg_export.get_header_path(imp->pkg->generated);
		rel = utils.relative(p->pkg->generated, header);
		global.free(header);
	}
	asprintf(&include, "#include \"%s\"", rel);

	lex_item.t * include_token = stack.replace_with(s, import_token, include);
	Package.emit(p->pkg, include_token);
	global.free(rel);

	return 1;
}
