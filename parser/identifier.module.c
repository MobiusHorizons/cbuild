package "parser_identifier";

build depends "../deps/hash/hash.c";
#include "../deps/hash/hash.h"
#include <stdio.h>
#include <stdlib.h>
export #include <stdbool.h>

import Errors     from "../errors/errors.module.c";
import lex_item   from "../lexer/item.module.c";
import stack      from "../lexer/stack.module.c";
import parser     from "./parser.module.c";
import Package    from "../package/package.module.c";
import pkg_export from "../package/export.module.c";
import pkg_import from "../package/import.module.c";
import symbol     from "./symbol.module.c";

hash_t * options = NULL;
static void init_options(){
	options = hash_new();

	hash_set(options, "enum",   (void *) 1);
	hash_set(options, "union",  (void *) 2);
	hash_set(options, "struct", (void *) 3);
}

static lex_item.t * token(parser.t * p, stack.t * s) {
	lex_item.t * item = parser.next(p);
	while(item->type == item_whitespace || item->type == item_newline){
		stack.push(s, item);
		item = parser.next(p);
	}
	return item;
}

static lex_item.t * cleanup(parser.t * p, stack.t * s) {
	while(s->length > 1) {
		parser.backup(p, stack.pop(s));
	}
	lex_item.t * out = stack.pop(s);
	stack.free(s);
	return out;
}

static void rewind_until(parser.t * p, stack.t * s, lex_item.t * item) {
	lex_item.t * i;
	while(s->length > 0 && (i = stack.pop(s)) && !lex_item.equals(i, item)) {
		parser.backup(p, i);
	}
}

static lex_item.t * emit(parser.t * p, stack.t * s) {
	int i;
	for (i = 0; i < s->length -1; i++) {
		Package.emit(p->pkg, s->items[i]);
	}

	lex_item.t * out = stack.pop(s);

	stack.free(s);
	return out;
}


static lex_item.t * parse_type(parser.t * p, stack.t * s, lex_item.t * item, lex_item.t ** type) {
	if (options == NULL) init_options();
	if (!hash_has(options, item->value)) return item;

	*type = item;
	stack.push(s, item);
	return token(p, s);
}

/**
 * parse_import() parse an imported identifier (eg: <namespace>.<symbol>)
 * Returns NULL if the syntax is not an import syntax.
*/
static lex_item.t * parse_import( parser.t *p, stack.t * s,
		lex_item.t * item, lex_item.t ** pkg, lex_item.t ** name ) {
	lex_item.t * temp;

	stack.push(s, item);
	if (item->type != item_id) return NULL;
	temp = item;

	item = parser.next(p);
	stack.push(s, item);

	if (item->type != item_symbol || item->value[0] != '.') return temp;

	item = parser.next(p);
	stack.push(s, item);
	if (item->type != item_id) return NULL;

	*pkg  = temp;
	*name = item;
	return item;
}

lex_item.t * parse_symbol(parser.t *p, stack.t * s, lex_item.t * type, lex_item.t * item) {
	char * name = strdup(item->value);
	if (type) name[0] |= 0x80;

	pkg_export.t * symbol = (pkg_export.t *) hash_get(p->pkg->symbols, name);
	free(name);

	if (symbol == NULL) return cleanup(p, s);

	rewind_until(p, s, item);
	char * symbol_name = NULL;
	if (symbol->symbol == NULL) {
		fprintf(stderr, "package %s, status: %d\n", p->pkg->name, p->pkg->status);
	}

	symbol_name = strdup(symbol->symbol);
	return stack.replace_with(s, item, symbol_name);
}

char * resolve(parser.t * p, pkg_import.t * imp, symbol.ref_t ref, Errors.t * errors) {
	char * name = strdup(ref.name->value);
	if (ref.type) name[0] |= 0x80;

	pkg_export.t * exp = (pkg_export.t *) hash_get(imp->pkg->exports, name);
	free(name);

	if (exp == NULL) {
		parser.errorf(p, ref.name, "Package '%s' does not export the symbol '%s'",
				ref.from->value, ref.name->value
		);
	}

	if (exp) return symbol.name(ref, exp->symbol, NULL);
	return symbol.fallback_name(ref, imp->pkg->name, NULL);
}

export lex_item.t * parse_typed(parser.t * p, lex_item.t * type, lex_item.t * item, bool is_export) {
	lex_item.t * from  = NULL;
	lex_item.t * name  = NULL;
	stack.t * s        = stack.new(8);

	item = parse_import(p, s, item, &from, &name);
	if (item == NULL) return cleanup(p, s);
	if (name == NULL) return parse_symbol(p, s, type, item);

	if (from && strcmp(from->value, "global") == 0) {
		stack.free(s);
		return name;
	}

	pkg_import.t * imp = (pkg_import.t *) hash_get(p->pkg->deps, from->value);
	if (imp == NULL || imp->pkg == NULL) return emit(p, s);

	char * symbol_name = strdup(name->value);
	if (type) {
		symbol_name[0] |= 0x80; // tagged symbol;
	}

	pkg_export.t * exp = (pkg_export.t *) hash_get(imp->pkg->exports, symbol_name);
	free(symbol_name);
	if (exp == NULL) {
		parser.errorf(p, name, "Package '%s' does not export the symbol '%s'",
				from->value, name->value
		);
		return cleanup(p, s);
	}

	if (is_export) pkg_export.export_headers(p->pkg, imp->pkg);

	return stack.replace_with(s, NULL, strdup(exp->symbol));
}

export lex_item.t * parse (parser.t * p, lex_item.t * item, bool is_export) {
	lex_item.t * type  = NULL;
	lex_item.t * from  = NULL;
	lex_item.t * name  = NULL;
	stack.t * s        = stack.new(8);

	item = parse_type(p, s, item, &type);
	item = parse_import(p, s, item, &from, &name);
	if (item == NULL) return cleanup(p, s);
	if (name == NULL) {
		lex_item.t * sym = parse_symbol(p, s, type, item);
		if (type && sym != type) {
			char * resolved = sym->value;
			sym->length = asprintf(&sym->value, "%s %s", type->value, resolved);
			free(resolved);

			type->replaced_by = sym;
		}

		return sym;
	}

	if (strcmp(from->value, "global") == 0) {
		stack.free(s);
		return name;
	}

	symbol.ref_t ref = {
		.type      = type,
		.from      = from,
		.name      = name,
	};

	pkg_import.t * imp = (pkg_import.t *) hash_get(p->pkg->deps, from->value);
	if (imp == NULL) return emit(p, s);

	if (strcmp(name->value, "value") == 0) {
	/* there must be corruption in the package import system. investigate */
		fprintf(stderr, "in package '%s', import %s.%s\n", p->pkg->source_abs, from->value, name->value);
		fprintf(stderr, "  -> import [%s(%d): %s]\n", imp->alias, imp->type, imp->filename);
		fprintf(stderr, "    -> package %s\n", imp->pkg ? imp->pkg->source_abs : "null");
	}

	if (is_export) pkg_export.export_headers(p->pkg, imp->pkg);
	if (imp->type == IMPORT_QUEUED) {
		char * symbol_name = strdup(name->value);
		if (type) {
			symbol_name[0] |= 0x80;
		}
		symbol.t * sym = (symbol.t *) hash_get(imp->symbols, symbol_name);
		if (sym == NULL) {
			sym = symbol.new(symbol_name);
			hash_set(imp->symbols, symbol_name, sym);
		}
		symbol.insert(sym, ref);

		// we replace the symbols in the input with a placeholder,
		// who's value will get updated when this symbol gets resolved.
		lex_item.t * placeholder = stack.replace_with(s, name, "$SYMBOL_REF");
		return placeholder;
	}

	if (imp->pkg == NULL) return emit(p, s);

	char * symbol_name = resolve(p, imp, ref, p->errors);
	lex_item.t * start = type ?: from;
	return stack.replace_with(s, start, symbol_name);
}

