package "parser_build";
import parser     from "./parser.module.c";
import stack      from "../lexer/stack.module.c";
import string     from "./string.module.c";
import lex_item   from "../lexer/item.module.c";
import Package    from "../package/package.module.c";
import pkg_import from "../package/import.module.c";
import str        from "../utils/strings.module.c";

build depends "../deps/hash/hash.c";
#include "../deps/hash/hash.h"

#include <stdarg.h>
#include <string.h>
#include <stdio.h>

static hash_t * options = NULL;

typedef int (*parse_fn)(parser.t * p);
static int parse_depends (parser.t * p);
static int parse_set     (parser.t * p);
static int parse_append  (parser.t * p);
static int parse_rule    (parser.t * p);

static void init_options(){
	options = hash_new();

	hash_set(options, "depends", parse_depends);
	hash_set(options, "set",     parse_set    );
	hash_set(options, "append",  parse_append );
	hash_set(options, "rule",    parse_rule   );
}

static int errorf(parser.t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	char error_string[4096];
	strcpy(error_string, "Invaild build syntax: ");
	strcat(error_string, fmt);

	parser.verrorf(p, ERROR, false, item, NULL, error_string, args);
	return -1;
}

/**********************************************************************************************************************
 * Build options manipulate the resulting makefile.
 *
 * Parses the following syntaxes:
 * - build depends      "<filename>";          # add "<filenme>" to the dependency list for the current file
 * - build set          <variable> "<value>";  # set the value of a makefile valiable         (:=)
 * - build set default  <variable> "<value>";  # set the default value of a makefile variable (?=)
 * - build append       <variable> "<value>";  # append to a makefile variable                (+=)
 **********************************************************************************************************************/
export int parse (parser.t * p, lex_item.t * build_token) {
	if (options == NULL) init_options();

	lex_item.t * item = parser.token(p, NULL);
	parse_fn fn = (parse_fn) hash_get(options, item->value);
	if (fn != NULL)  return fn(p);

	return errorf(p, item, "Expecting one of \n"
			"\t'depends', 'set', 'set default' or 'append', but got %s",
			lex_item.to_string(item)
			);
}

static int parse_depends(parser.t * p) {
	lex_item.t * filename = parser.token(p, NULL);
	if (filename->type != item_quoted_string) {
		return errorf(p, filename, "Expecting a filename but got '%s'", filename->value);
	}

	lex_item.t * semicolon = parser.skip(p, NULL, item_whitespace, 0);
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, "Expecting ';' but got '%s'", lex_item.to_string(semicolon));
	}

	char * error = NULL;
	pkg_import.t * imp = pkg_import.add_c_file(
		p->pkg, 
		string.parse_dup(filename->value),
		&error
	);
	if (imp == NULL) {
		parser.errorf(p, filename, "Error adding dependency: '%s' %s", filename->value, error);
		return -1;
	}
	return 1;
}

export int parse_pp_include (parser.t * p, lex_item.t * filename) {
	pkg_import.t * imp = pkg_import.add_h_file(
		p->pkg, 
		string.parse_dup(filename->value)
	);

	// TODO can this actually happen?
	if (imp == NULL) {
		parser.errorf(p, filename, "Error adding dependency: '%s'", filename->value);
		return -1;
	}
	return 1;
}

static int parse_set(parser.t * p) {
	bool is_default = false;
	bool have_name = false;
	lex_item.t * name;
	do {
		name = parser.token(p, NULL);

		if (name->type != item_id) {
			return errorf(p, name, "Expecting a variable name, but got %s", name->value);
		}

		if (strcmp(name->value, "default") == 0) {
			is_default = true;
		} else {
			have_name = true;
		}
	} while (have_name == false);

	lex_item.t * value = parser.token(p, NULL);

	if (value->type != item_quoted_string) {
		return errorf(p, value, "Expecting a quoted string, but got '%s'", value->value);
	}

	lex_item.t * semicolon = parser.skip(p, NULL, item_whitespace, 0);
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, "Expecting ';' but got '%s'", lex_item.to_string(semicolon));
	}

	Package.var_t v = {0};
	v.name      = str.dup(name->value);
	v.value     = str.dup(string.parse(value->value));
	v.operation = is_default ? build_var_set_default : build_var_set;

	p->pkg->variables = realloc(p->pkg->variables, sizeof(Package.var_t) * (p->pkg->n_variables + 1));
	p->pkg->variables[p->pkg->n_variables] = v;
	p->pkg->n_variables++;

	return 1;
}

static int parse_append(parser.t * p) {
	lex_item.t * name = parser.token(p, NULL);

	if (name->type != item_id) {
		return errorf(p, name, "Expecting a variable name, but got %s", name->value);
	}

	lex_item.t * value = parser.token(p, NULL);

	if (value->type != item_quoted_string) {
		return errorf(p, value, "Expecting a quoted string, but got '%s'", value->value);
	}

	lex_item.t * semicolon = parser.skip(p, NULL, item_whitespace, 0);

	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, "Expecting ';' but got '%s'", lex_item.to_string(semicolon));
	}

	Package.var_t v = {0};
	v.name      = str.dup(name->value);
	v.value     = str.dup(string.parse(value->value));
	v.operation = build_var_append;

	p->pkg->variables = realloc(p->pkg->variables, sizeof(Package.var_t) * (p->pkg->n_variables + 1));
	p->pkg->variables[p->pkg->n_variables] = v;
	p->pkg->n_variables++;
	return 1;
}

/** 
 * for a gerenic rule, expect a generic dependency
 */
static char * parse_dep(parser.t * p, int required) {
	lex_item.t * tok;

	tok = parser.token(p, NULL);
	if (tok->type == item_open_symbol && tok->value[0] == '{') {
		if (required) {
			errorf(p, tok, "Expecting at least one rule dependency, but got '%s'", tok->value);
		}
		parser.backup(p, tok);
		return NULL;
	}

	if (tok->type != item_quoted_string) {
		errorf(
			p, tok, 
			"Expecting a quoted string '%s'", 
			tok->value
		);
		return NULL;
	}

	return string.parse_dup(tok->value);
}

static int parse_rule(parser.t * p) {
	lex_item.t * target = parser.token(p, NULL);
	if (target->type != item_quoted_string) {
		return errorf(p, target, "Expecting a quoted string (target), but got '%s'", target->value);
	}

	char * deps = NULL;
	char * dep = NULL;
	size_t deps_len = 0;
	unsigned char is_generic = index(target->value, '%') != NULL;

	while((dep = parse_dep(p, is_generic && deps == NULL))) {
		deps = realloc(deps, deps_len + strlen(dep) + (deps_len == 0 ? 1 : 2));
		if (deps_len != 0) {
			deps[deps_len++] = ' ';
			deps[deps_len] = 0;
		}
		memcpy(deps + deps_len, dep, strlen(dep));
		deps_len += strlen(dep);
		deps[deps_len] = 0;
		free(dep);
		// TODO: warn if at least one deps is not generic;
	}

	lex_item.t * tok = parser.token(p, NULL);
	if (tok->type != item_open_symbol && tok->value[0] != '{') {
		return errorf(p, tok, "Expecting '{', but got '%s'", tok->value);
	}
	stack.t * lines = stack.new(0);

	tok = NULL;
	do {
		tok = parser.token(p, NULL); // skip whitespace before first symbol.
		while ((tok->type != item_close_symbol || tok->value[0] != '}') && tok->type != item_newline) {
			stack.push(lines, tok);
			tok = parser.next(p);
		}
	} while (tok->value[0] != '}');

	int line;
	lex_item.t * first = stack.first(lines);
	lex_item.t * last  = stack.last(lines);

	Package.rule_t rule = {0};
	rule.target = string.parse_dup(target->value);
	rule.deps   = deps;
	rule.is_generic = is_generic;
	size_t recipe_len = (last->length + last->start) - first->start + 
		(last->line - first->line + 1) * 2 + 1;
	rule.recipe = malloc(recipe_len);
			
	char * r = rule.recipe;

	for (line = first->line; line <= last->line; line++) {
		*(r++) = '\t';
		while((tok = stack.first(lines)) && tok->line == line) {
			memcpy(r, tok->value, tok->length);
			r = r + tok->length;
			stack.shift(lines);
		}
		*(r++) = '\n';
	}
	*r = 0;
	stack.free_items(lines);
	stack.free(lines);

	p->pkg->rules = realloc(p->pkg->rules, sizeof(Package.rule_t) * (p->pkg->n_rules + 1));
	p->pkg->rules[p->pkg->n_rules] = rule;
	p->pkg->n_rules++;
	return 1;
}
