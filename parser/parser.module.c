import lex_item from "../lexer/item.module.c";
import lex      from "../lexer/lex.module.c";
import stack    from "../lexer/stack.module.c";
import cache    from "../lexer/cache.module.c";
import str      from "../utils/strings.module.c";
import utils    from "../utils/utils.module.c";
import Errors    from "../errors/errors.module.c";
import Package  from "../package/package.module.c";

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <stdlib.h>
export #include <stdbool.h>
export #include <stdarg.h>

export struct parser_s;
export typedef void * (*parse_fn)(struct parser_s * lex);

export typedef struct parser_s {
	lex.t     * lexer;
	parse_fn    state;
	stack.t   * items;
	Package.t * pkg;
	char      * filename;
	Errors.t  * errors;
} parser_t as t;

export void fatalf(parser_t * p, lex_item.t * item, const char * fmt, ...);
export void errorf(parser_t * p, lex_item.t * item, const char * fmt, ...);
export void warnf(parser_t * p, lex_item.t * item, const char * fmt, ...);
export void infof(parser_t * p, lex_item.t * item, const char * fmt, ...);
export void hintf(parser_t * p, lex_item.t * item, const char * fmt, ...);


export int parse(lex.t * lexer, parse_fn start, Package.t * pkg, Errors.t * errors) {
	parser_t * p = malloc(sizeof(parser_t));
	p->lexer     = lexer;
	p->state     = start;
	p->items     = stack.new(1);
	p->pkg       = pkg;
	p->errors    = errors;
	p->filename  = realpath(lexer->filename, NULL);

	pkg->source_cache = lexer->cache;
	pkg->dest_cache = cache.new();

	char * cwd = getcwd(NULL, 0);

	char * directory = str.dup(lexer->filename);
	chdir(dirname(directory));
	global.free(directory);

	while (p->state != NULL) p->state = (parse_fn) p->state(p);

	lex.free(lexer);
	stack.free(p->items);
	global.free(p->filename);
	chdir(cwd);
	global.free(cwd);
	global.free(p);

	return (errors ? errors->count : 0);
}

export lex_item.t * next(parser_t * p) {
	lex_item.t * item;
	if (p->items->length) {
		item = stack.pop(p->items);
	} else {
		item = lex.next_item(p->lexer);
	}
	if (item->type == item_error) {
		fatalf(p, item, "Syntax Error: %s", item->value);
	}
	return item;
}

export void backup(parser_t *p, lex_item.t * item) {
	p->items = stack.push(p->items, item);
}

export lex_item.t * skip(parser_t * p, lex_item_stack_t * s, ...) {
	va_list args;
	int len = 0;
	int i;

	enum lex_item.type types[item_total_symbols];
	va_start(args, s);

	do {
		types[len] = va_arg(args, enum lex_item.type);
		len++;
	} while (types[len-1] != 0);
	len--;

	lex_item.t * item = NULL;
	do {
		int do_skip = 0;
		item = next(p);
		for ( i = 0; i < len; i++) {
			if (types[i] == item->type) {
				do_skip = 1;
				break;
			};
		}
		if (do_skip) {
			if (s != NULL) stack.push(s, item);
			continue;
		}
		return item;
	} while (item->type != item_eof && item->type != item_error);

	return item;
}

export lex_item_t * token(parser_t * p, lex_item_stack_t * s) {
	lex_item.t * item = next(p);
	while(
		item->type == item_whitespace || 
		item->type == item_newline    ||
		item->type == item_comment
	){
		if (s != NULL) stack.push(s, item);
		item = next(p);
	}
	return item;
}

static Errors.context_t get_error_context(parser_t * p, lex_item.t * start, lex_item.t * end) {

	if (end == NULL) {
		end = start;
	}

	char * filename = p->filename;

	Errors.context_t ctx = {
		.filename = str.dup(filename),
		.start = {
			.line      = start->line,
			.character = start->start - start->line_pos,
		},
		.end = {
			// TODO: account for PP_newline
			.line      = end->line  + (end->type == item_newline ? 1 : 0),
			.character = end->start - end->line_pos + end->length,
		},
	};

	char * ctx_start = p->lexer->input + start->line_pos;
	char * ctx_end   = &p->lexer->input[end->start];
	if (end->type != item_newline) {
		char * end = ctx_end;
		while(*end != 0 && *end != '\n') end++;
		ctx_end = end;
	}

	ctx.text = malloc(ctx_end - ctx_start + 1);
	memcpy(ctx.text, ctx_start, ctx_end - ctx_start);
	ctx.text[ctx_end - ctx_start] = '\0';

	return ctx;
}

export void verrorf(
	parser_t * p,
	enum Errors.severity_t severity,
	bool is_fatal,
	lex_item.t * start,
	lex_item.t * end,
	const char * fmt,
	va_list args
) {
	Errors.error_t e = {
		.severity = severity,
		.is_fatal = is_fatal,
		.context  = get_error_context(p, start, end),
	};

	vasprintf(&e.message, fmt, args);

	Errors.append(p->errors, e);
}

void fatalf(parser_t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	verrorf(p, ERROR, true, item, NULL, fmt, args);
}

void errorf(parser_t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	verrorf(p, ERROR, false, item, NULL, fmt, args);
}

void warnf(parser_t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	verrorf(p, WARNING, false, item, NULL, fmt, args);
}

void infof(parser_t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	verrorf(p, INFO, false, item, NULL, fmt, args);
}

void hintf(parser_t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	verrorf(p, HINT, false, item, NULL, fmt, args);
}

export void severityf(parser_t * p, enum Errors.severity_t s, lex_item.t * i, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	verrorf(p, s, false, i, NULL, fmt, args);
}

