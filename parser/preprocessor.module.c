package "preprocessor";

import stack      from "../lexer/stack.module.c";
import lex_item   from "../lexer/item.module.c";
import Package    from "../package/package.module.c";

import Build      from "./build.module.c";
import parser     from "./parser.module.c";

export int parse_inline(parser.t * p, stack.t * s) {
	lex_item.t * it = parser.token(p, s);
	stack.push(s, it);

	if (it->type == item_pp_token && strcmp(it->value, "include") == 0) {
		lex_item.t * filename = parser.token(p, s);
		stack.push(s, filename);

		if (filename->type == item_quoted_string) {
			Build.parse_pp_include(p, filename);
		}
	}

	it = parser.skip(p, s, item_whitespace, item_pp_token, item_pp_newline, 0);
	parser.backup(p, it);

	return 0;
}

export int parse(parser.t * p, lex_item.t * it) {
	stack.t * s = stack.new(5);
	stack.push(s, it); // leading hash

	parse_inline(p, s);

	size_t i;
	for (i = 0; i < s->length; i++) {
		Package.emit(p->pkg, s->items[i]);
	}
	stack.free(s);
	return 0;
}
