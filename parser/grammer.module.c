build depends "../deps/hash/hash.c";
#include "../deps/hash/hash.h"

#include <stdio.h>
#include <stdarg.h>

import stream     from "../deps/stream/stream.module.c";

import Errors     from "../errors/errors.module.c";
import lex_item   from "../lexer/item.module.c";
import lex        from "../lexer/lex.module.c";
import stack      from "../lexer/stack.module.c";
import syntax     from "../lexer/syntax.module.c";
import Package    from "../package/package.module.c";
import parser     from "./parser.module.c";

import ParsePackage from "./package.module.c";
import Import       from "./import.module.c";
import Export       from "./export.module.c";
import Build        from "./build.module.c";
import Identifier   from "./identifier.module.c";
import Preprocessor from "./preprocessor.module.c";

typedef int (*keyword_fn)(parser.t * p, lex_item.t * item);

static void * parse_id      (parser.t * p, lex_item.t * item);
static void * parse_keyword (parser.t * p, lex_item.t * item);

static void * parse_c(parser.t * p) {
	lex_item.t * item = NULL;
	lex_item.t * last = NULL;
	int escaped_id = 0;
	do {
		last = item;
		item = parser.next(p);

		switch(item->type) {
			case item_arrow:
				escaped_id = 1;
				Package.emit(p->pkg, item);
				continue;

			case item_eof:
			case item_error:
				break;

			case item_pp_symbol:
				Preprocessor.parse(p, item);
				return parse_c;

			case item_id:
				if (last == NULL || last->type == item_newline) {
					return parse_keyword(p, item);
				}
				if (escaped_id == 0 && !(last->type == item_symbol && last->value[0] == '.')) {
					return parse_id(p, item);
				}
			case item_c_code:
			default:
				Package.emit(p->pkg, item);
		}

		escaped_id = 0;
	} while (item->type != item_eof && item->type != item_error);

	return NULL;
}

static hash_t * keywords = NULL;
static void init_keywords(){
	keywords = hash_new();

	hash_set(keywords, "package", ParsePackage.parse);
	hash_set(keywords, "import",  Import.parse );
	hash_set(keywords, "export",  Export.parse );
	hash_set(keywords, "build",   Build.parse  );
}

static void * parse_keyword(parser.t * p, lex_item.t * item) {
	if (keywords == NULL) init_keywords();
	keyword_fn fn = (keyword_fn) hash_get(keywords, item->value);

	if (fn != NULL) {
		if (fn(p, item)) return parse_c;
		return NULL;
	}

	Export.module_level(p, item);
	return parse_c;
}


static void * parse_id(parser.t * p, lex_item.t * item) {
	item = Identifier.parse(p, item, false);
	if (item->type == item_error) return NULL;
	Package.emit(p->pkg, item);
	return parse_c;
}

export int parse(stream.t * in, const char * filename, Package.t * p, Errors.t * errors) {
	lex.t * lexer = syntax.new(in, filename);
	if (lexer == NULL) return -1;

	return parser.parse(lexer, parse_c, p, errors);
}
