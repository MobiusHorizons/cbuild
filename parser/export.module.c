package "parser_export";

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

build depends "../deps/hash/hash.c";
#include "../deps/hash/hash.h"

import parser     from "./parser.module.c";
import string     from "string.module.c";
import utils      from "../utils/utils.module.c";
import str        from "../utils/strings.module.c";
import identifier from "./identifier.module.c";
import lex_item   from "../lexer/item.module.c";
import stack      from "../lexer/stack.module.c";
import cache      from "../lexer/cache.module.c";
import Package    from "../package/package.module.c";
import pkg_export from "../package/export.module.c";
import pkg_import from "../package/import.module.c";
import Errors     from "../errors/errors.module.c";
import preprocessor from "preprocessor.module.c";
import expression   from "expression.module.c";

enum visibility {
	MODULE = 0,
	EXPORT,
	GLOBAL
};

typedef struct {
	stack.t            * items;
	enum pkg_export.type type;
	enum visibility      visibility;
	bool                 error;
} decl_t;

#define SYNTAX_ERROR "Invalid export syntax: "

static lex_item.t * errorf(parser.t * p, lex_item.t * item, decl_t * decl, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	parser.verrorf(p, ERROR, false, item, NULL, fmt, args);
	decl->error = true;
	return NULL;
}

static void append(decl_t * decl, lex_item.t * value) {
	if (decl->items == NULL) decl->items = stack.new(8);
	stack.push(decl->items, value);
}

static void rewind_until(parser.t * p, decl_t * decl, lex_item.t * value) {
	lex_item.t * item = NULL;
	while((item = stack.last(decl->items)) != NULL && item != value) {
		parser.backup(p, stack.pop(decl->items));
	}

	if (item == value) {
		parser.backup(p, stack.pop(decl->items));
	}
}

static void rewind_whitespace(parser.t * p, decl_t * decl, lex_item.t * value) {
	parser.backup(p, value);

	lex_item.t * item;
	while ((item = stack.last(decl->items)) != NULL && (
				item->type == item_whitespace ||
				item->type == item_newline
			)
		) {
			parser.backup(p, stack.pop(decl->items));
	}
}

#define CHAR4(s) *((uint32_t*)(s))

static lex_item.t * symbol_rename(parser.t * p, decl_t * decl, lex_item.t * name, lex_item.t * alias) {
	int i;
	for (i = 0; i < decl->items->length; i++) {
		lex_item.t * item = decl->items->items[i];
		if (lex_item.equals(name, item)) {
			char * symbol_name = NULL;

			char * export_name = alias ? alias->value : name->value;

			if (decl->visibility == GLOBAL || CHAR4(p->pkg->name) == CHAR4("main")) {
				asprintf(&symbol_name, "%s", export_name);
			} else {
				asprintf(&symbol_name, "%s_%s", p->pkg->name, export_name);
			}

			item = lex_item.replace_value(item, symbol_name);
			decl->items->items[i] = item;
			return item;
		}
	}
	return NULL;
}

static void free_decl(decl_t * decl) {
	stack.free(decl->items);
}

/**
 * Emits the symbol to the output file, and returns the declaration stack which will be used
 * for the generated header.
 */
static stack.t * emit(parser.t * p, decl_t * decl, bool is_function, bool is_extern) {
	lex_item.t * item;

	if (!is_extern) {
		size_t i;
		for (i = 0; i < decl->items->length; i++) {
			Package.emit(p->pkg, decl->items->items[i]);
		}
	}

	// used to as a starting point for finding comments.
	lex_item.t * first_tok = stack.first(decl->items);

	// trim leading whitespace
	while((item = stack.first(decl->items)) && (
		item->type == item_whitespace ||
		item->type == item_newline
	)) stack.shift(decl->items);

	if (decl->type == type_variable) {
		int i = 0;
		while(i < stack.height(decl->items)) {
			item = stack.peek(decl->items, i);
			if (item->type == item_symbol && item->value[0] == '=') break;
			i++;
		}

		while (i < stack.height(decl->items)) {
			stack.pop(decl->items);
		}

		lex_item.t * first = stack.first(decl->items);
		stack.unshift(decl->items, lex_item.new(
			str.dup("extern "),
			item_id,
			first->line,
			first->line_pos,
			first->start
		));
	}

	// trim trailing whitespace
	while((item = stack.last(decl->items)) && (
		item->type == item_whitespace ||
		item->type == item_newline
	)) stack.pop(decl->items);

	// Find comments for the symbol from the cache and prepend them to the declaration.
	if (first_tok->line > 0) {
		cache.t * c = p->pkg->source_cache;
		size_t line = first_tok->line;
		size_t count = 0;

		{ // find the start of the comments
			cache.line_t l = {0};
			size_t last_comment_line = line;
			do {
				line--;
				l = c->lines[line];
				if (l.num_items == 0) continue;
				if (l.num_items > 3) break;

				if (l.items[0]->type == item_comment) {
					last_comment_line = line;
				} else if (l.items[0]->type != item_newline) {
					break;
				}
				count += l.num_items;
			} while (line > 0);
			line = last_comment_line;
		}


		// if there are comments, create a new stack with the comments and declaration.
		if (line < first_tok->line) {
			stack.t * old_decl = decl->items;
			decl->items = stack.new(count + stack.height(old_decl));

			// put the comments in first
			while(line < first_tok->line) {
				cache.line_t l = c->lines[line++];
				if (l.num_items == 0) continue;

				size_t i;
				for (i = 0; i < l.num_items; i++) {
					stack.push(decl->items, l.items[i]);
				}
			}

			// transfer over the declaration
			while(stack.height(old_decl)) {
				stack.push(decl->items, stack.shift(old_decl));
			}

			// free the old stack.
			stack.free(old_decl);
		}
	}

	{ // add 1 to the refcount for everything we are saving so it can be safely freed later.
		size_t i;
		for (i = 0; i < stack.height(decl->items); i++) {
			stack.peek(decl->items, i)->refcount++;
		}
	}

	if (is_function) {
		lex_item.t * last = stack.last(decl->items);
		lex_item.t * semicolon = lex_item.new(
			str.dup(";"),
			item_symbol,
			last->line,
			last->line_pos,
			last->start
		);
		stack.push(decl->items, semicolon);
	}

	return decl->items;
}

static lex_item.t * collect_newlines(parser.t * p, decl_t * decl) {
	lex_item.t * item = parser.next(p);

	while (
		item->type == item_whitespace ||
		item->type == item_newline    ||
		item->type == item_comment
	) {
		if (item->type == item_newline) {
			append(decl, item);
		}

		item = parser.next(p);
	}

	return item;
}

static lex_item.t * collect(parser.t * p, decl_t * decl) {
	if (decl->items == NULL) decl->items = stack.new(8);
	return parser.token(p, decl->items);
}

static lex_item.t * parse_typedef       (parser.t * p, decl_t * decl);
static lex_item.t * parse_struct        (parser.t * p, decl_t * decl);
static lex_item.t * parse_enum          (parser.t * p, decl_t * decl);
static lex_item.t * parse_union         (parser.t * p, decl_t * decl);
static lex_item.t * parse_struct_block  (parser.t * p, decl_t * decl, lex_item.t * start);
static lex_item.t * parse_enum_block    (parser.t * p, decl_t * decl, lex_item.t * start);
static lex_item.t * parse_export_block  (parser.t * p, decl_t * decl);
static lex_item.t * parse_as            (parser.t * p, decl_t * decl);
static lex_item.t * parse_variable      (parser.t * p, decl_t * decl);
static lex_item.t * parse_function      (parser.t * p, decl_t * decl);
static lex_item.t * parse_function_args (parser.t * p, decl_t * decl);
static lex_item.t * parse_pp            (parser.t * p, decl_t * decl);
static long       parse_size_decl     (parser.t * p, decl_t * decl);

static int parse_passthrough (parser.t * p);

static hash_t * export_types = NULL;
static void init_export_types(){
	export_types = hash_new();

	hash_set(export_types, "typedef", parse_typedef);
	hash_set(export_types, "struct",  parse_struct );
	hash_set(export_types, "enum",    parse_enum   );
	hash_set(export_types, "union",   parse_union  );
}

enum variable_modifiers {
	VARIABLE_CONST  = 0x01 << 0,
	VARIABLE_EXTERN = 0x01 << 1,
	VARIABLE_STATIC = 0x01 << 2,
};

static hash_t * modifier_types = NULL;
static void init_modifier_types(){
	modifier_types = hash_new();

	hash_set(modifier_types, "const",   (void*)VARIABLE_CONST);
	hash_set(modifier_types, "extern",  (void*)VARIABLE_EXTERN);
	hash_set(modifier_types, "static",  (void*)VARIABLE_STATIC);
}

typedef lex_item.t * (*export_fn)(parser.t * p, decl_t * decl);

void parse_semicolon(parser.t * p, decl_t * decl) {
	if (decl->error) return;

	size_t initial = stack.height(decl->items);
	lex_item.t * item = collect_newlines(p, decl);

	//TODO make sure this doesn't include spaces before the semicolon.
	if (item->type != item_symbol || item->value[0] != ';') {
		if (decl->items->length > initial) {
			item = decl->items->items[initial];
		}
		errorf(p, item, decl, SYNTAX_ERROR "expecting ';' but got %s", lex_item.to_string(item));
		return;
	}
	append(decl, item);
}

/**
 * Handles local symbols that are not exported.
 *
 * These are modified to be static.
 */
export int module_level(parser.t * p, lex_item.t * item) {
	if (export_types == NULL) init_export_types();
	if (modifier_types == NULL) init_modifier_types();
	parser.backup(p, item);
	if (export_types == NULL) init_export_types();

	decl_t decl  = {0};
	export_fn fn = NULL;
	lex_item.t * name  = NULL;
	bool has_semicolon = false;
	bool is_extern     = false;
	int modifiers = 0;

	lex_item.t * type = collect_newlines(p, &decl);

	switch (type->type) {
		case item_id:
			while (type->type == item_id && hash_has(modifier_types, type->value)) {
				modifiers |= (enum variable_modifiers) hash_get(modifier_types, type->value);
				append(&decl, type);
				type = collect(p, &decl);
			}

			fn = (export_fn) hash_get(export_types, type->value);
			has_semicolon = is_extern || fn != NULL;
			if (fn) append(&decl, type);
			break;
		case item_symbol:
			break;
		case item_open_symbol:
			if (type->value[0] == '{') {
				fn = parse_export_block;
			}
			break;
		case item_pp_symbol:
			fn = parse_pp;
			append(&decl, type);
			break;
		default:
			fn = NULL;
			break;
	}
	if (fn == NULL) {
		parser.backup(p, type);
		name = parse_variable(p, &decl);
	} else {
		name = fn(p, &decl);
	}

	if (has_semicolon) {
		lex_item.t * tok = collect(p, &decl);
		if (tok->type == item_id) {
			tok = identifier.parse(p, tok, decl.visibility != 0);
			if (name == NULL) name = tok;
			append(&decl, tok);
		} else {
			parser.backup(p, tok);
		}
		if (expression.initializer(p, decl.items, decl.visibility != 0)) {
			decl.type = type_variable;
		}
		parse_semicolon(p, &decl);
	}
	if (decl.error) {
		free_decl(&decl);
		return -1;
	}

#ifdef SYMBOL_DEBUG
	fprintf(stderr, "%lu:%lu \t%s %s%s%s%s %s\n",
			(name ? name->line : type->line) + 1,
			(name ?
			 (name->start - name->line_pos) :
			 (type->start - type->line_pos)
			) + 1,
			(name && name->source != name) ? "export" : "local ",
			(modifiers & VARIABLE_EXTERN) ? "extern " : "",
			(modifiers & VARIABLE_STATIC) ? "static " : "",
			(modifiers & VARIABLE_CONST) ? "const " : "",
			pkg_export.get_type_name(decl.type),
			name ? name->value : "<anonymous>"
		   );
#endif

	if (
		name && name->source == name &&
		(decl.type == type_function || decl.type == type_variable) &&
		(modifiers & VARIABLE_STATIC) == 0 &&
		(modifiers & VARIABLE_EXTERN) == 0 &&
		strcmp(name->value, "main") != 0
	) {
		lex_item.t * start = stack.first(decl.items);
		Package.emit(p->pkg, lex_item.new(
			str.dup("static "),
			start->type,
			start->line,
			start->line_pos,
			start->start
		));
	}

	size_t i;
	for (i = 0; i < decl.items->length; i++) {
		Package.emit(p->pkg, stack.peek(decl.items, i));
	}
	free_decl(&decl);
	return 0;

}

export int parse(parser.t * p, lex_item.t * export_token) {
	if (export_types == NULL) init_export_types();
	if (modifier_types == NULL) init_modifier_types();

	decl_t decl  = {0};
	decl.visibility = EXPORT;
	export_fn fn = NULL;
	lex_item.t * name  = NULL;
	lex_item.t * alias = NULL;
	bool has_semicolon = false;
	bool is_extern     = false;
	int modifiers = 0;

	lex_item.t * type = collect_newlines(p, &decl);

	if (type->type == item_id && strcmp(type->value, "global") == 0) {
		type = collect_newlines(p, &decl);
		decl.visibility = GLOBAL;
	}

	switch (type->type) {
		case item_id:
			while (type->type == item_id && hash_has(modifier_types, type->value)) {
				modifiers |= (enum variable_modifiers) hash_get(modifier_types, type->value);
				append(&decl, type);
				type = collect(p, &decl);
			}

			if ((modifiers & VARIABLE_EXTERN) != 0) {
				is_extern = true;
			}

			fn = (export_fn) hash_get(export_types, type->value);
			has_semicolon = is_extern || fn != NULL;
			if (fn) append(&decl, type);
			break;
		case item_symbol:
			if (type->value[0] == '*') {
				return parse_passthrough(p);
			}
			break;
		case item_open_symbol:
			if (type->value[0] == '{') {
				fn = parse_export_block;
			}
			break;
		case item_pp_symbol:
			fn = parse_pp;
			append(&decl, type);
			break;
		default:
			fn = NULL;
			break;
	}

	if (fn == NULL) {
		parser.backup(p, type);
		name = parse_variable(p, &decl);
	} else {
		name = fn(p, &decl);
	}


	alias = parse_as(p, &decl);
	if (has_semicolon) parse_semicolon(p, &decl);
	if (decl.error) {
		free_decl(&decl);
		return -1;
	}

	if (name == NULL) {
		errorf(p, type, &decl, SYNTAX_ERROR "can't export anonymous symbols");
		free_decl(&decl);
		return -1;
	}

	lex_item.t * symbol = NULL;
	if (decl.type > type_header) {
		symbol = symbol_rename(p, &decl, name, alias);

		if (symbol == NULL) {
			errorf(p, type, &decl, "unable to export symbol '%s'", name->value);
			free_decl(&decl);
			return -1;
		}
	}

#ifdef SYMBOL_DEBUG
	fprintf(stderr, "%lu:%lu \texport %s%s%s%s %s (%s)\n",
			(name ? name->line : type->line) + 1,
			(name ?
			 (name->start - name->line_pos) :
			 (type->start - type->line_pos)
			) + 1,
			(modifiers & VARIABLE_EXTERN) ? "extern " : "",
			(modifiers & VARIABLE_STATIC) ? "static " : "",
			(modifiers & VARIABLE_CONST) ? "const " : "",
			pkg_export.get_type_name(decl.type),
			name ? name->value : "<anonymous>",
			symbol ? symbol->value : "<no symbol>"
		   );
#endif

	pkg_export.add(
		str.dup(name ? name->value : ""),
		str.dup(alias ? alias->value : NULL),
		str.dup(symbol ? symbol->value : ""),
		decl.type,
		emit(p, &decl, fn == NULL, is_extern), // TODO: remove fn= checks in favor of decl.type
		p->pkg
	);
	if (fn == parse_export_block) lex_item.free(name);
	return 1;
}

static int parse_passthrough(parser.t * p) {
	decl_t decl = {0};
	lex_item.t * from = collect(p, &decl);
	if (from->type != item_id || strcmp(from->value, "from") != 0) {
		parser.errorf(p, from, SYNTAX_ERROR "Exporting passthrough: expected 'from', but got %s", lex_item.to_string(from));
		free_decl(&decl);
		return -1;
	}

	lex_item.t * filename = collect(p, &decl);
	if (filename->type != item_quoted_string) {
		parser.errorf(p, filename, SYNTAX_ERROR "Exporting passthrough: expected filename, but got %s", lex_item.to_string(filename));
		free_decl(&decl);
		return -1;
	}

	parse_semicolon(p, &decl);
	if (decl.error) {
		free_decl(&decl);
		return -1;
	}

	Errors.t errors = {0};
	pkg_import.t * imp = pkg_import.passthrough(p->pkg, str.dup(string.parse(filename->value)), &errors);
	if (errors.count) {
		int i;
		for (i = 0; i < errors.count; i++) {
			Errors.error_t error = errors.errors[i];
			if (error.context.filename == NULL) {
				// TODO: this seems to be passing the wrong type (error != string)
	 			parser.errorf(p, filename, "exporting passthrough: %s", error);
			} else {
				Errors.append(p->errors, error);
			}
		}
	}

	char * header = NULL;
	char * rel =  utils.relative(p->pkg->generated, imp->pkg->header);
	asprintf(&header, "#include \"%s\"", rel);
	lex_item.t * header_token = stack.replace_with(decl.items, NULL, header);
	Package.emit(p->pkg, header_token);

	//free_decl(&decl);
	global.free(rel);
	return 1;
}

static lex_item.t * parse_as(parser.t * p, decl_t * decl) {
	if (decl->error) return NULL;

	size_t start = decl->items->length;

	lex_item.t * as = collect(p, decl);
	if (as->type != item_id || strcmp("as", as->value) != 0) {
		parser.backup(p, as);
		while(decl->items->length > start) {
			parser.backup(p, stack.pop(decl->items));
		}
		return NULL;
	}

	lex_item.t * alias = collect(p, decl);

	/** Trim whitespace around the as symbol. */
	lex_item.t * tok = NULL;
	while((tok = stack.pop(decl->items)) && tok->type == item_whitespace);
	append(decl, tok);


	if (alias->type != item_id) {
		errorf(p, alias, decl, "expecting identifier but got %s", lex_item.to_string(alias));
		return NULL;
	}

	return alias;
}

static long parse_array_size(parser.t * p, decl_t * decl) {
	long size = 1;

	lex_item.t * item = collect(p, decl);
	if (item->type != item_number) {
		errorf(p, item, decl, "size declaration: expecting a number, but found '%s'", lex_item.to_string(item));
		return size;
	}

	size = atol(item->value);
	append(decl, item);

	item = collect(p, decl);
	if (item->type != item_close_symbol || item->value[0] != ']') {
		errorf(p, item, decl, "size declaration: expecting ']', but found '%s'", lex_item.to_string(item));
		return size;
	}

	append(decl, item);
	return size * parse_size_decl(p, decl); // allow for multidimentional sizes.
}

static long parse_bitslice_size(parser.t * p, decl_t * decl) {
	long size = 1;

	lex_item.t * item = collect(p, decl);
	if (item->type != item_number) {
		errorf(p, item, decl, "size declaration: expecting a number, but found '%s'", lex_item.to_string(item));
		return size;
	}

	return size;
}

/**
 * Parse a size declaration at the end of a variable declaration, and return the value.
 * ie. int[6];
 *        ^~~
 */
static long parse_size_decl(parser.t * p, decl_t * decl) {
	long size = 1;
	lex_item.t * item = collect(p,decl);

	if (item->type == item_open_symbol && item->value[0] == '[') {
		append(decl, item);
		return parse_array_size(p, decl);
	} else if (item->type == item_symbol && item->value[0] == ':') {
		append(decl, item);
		return parse_bitslice_size(p, decl);
	}

	parser.backup(p, item);
	return size;
}

static lex_item.t * parse_function_ptr(parser.t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_variable; // function pointer is a variable
	lex_item.t * star = collect(p, decl);
	if (star->type != item_symbol || star->value[0] != '*') {
		return errorf(p, star, decl, "function pointer: expecting '*' but found '%s'", star->value);
	}
	append(decl, star);

	lex_item.t * name = collect(p, decl);
	if (name->type != item_id) {
		return errorf(p, name, decl, "function pointer: expecting identifier but found '%s'", name->value);
	}
	append(decl, name);

	lex_item.t * item;

	item = collect(p, decl);
	if (item->type != item_close_symbol || item->value[0] != ')') {
		return errorf(p, item, decl, "function pointer: expecting ')' but found '%s'", item->value);
	}
	append(decl, item);

	item = collect(p, decl);
	if (item->type != item_open_symbol || item->value[0] != '(') {
		return errorf(p, item, decl, "function pointer: expecting '(' but found '%s'", item->value);
	}
	append(decl, item);

	parse_function_args(p, decl);

	if (decl->error) return NULL;
	return name;
}

static lex_item.t * parse_typedef (parser.t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_type;
	lex_item.t * type = collect(p, decl);
	if (type->type != item_id) {
		return errorf(p, type, decl, "in typedef: expected identifier");
	}

	export_fn fn = (export_fn) hash_get(export_types, type->value);
	if (fn == NULL) {
		type = identifier.parse(p, type, decl->visibility != 0);
	}
	append(decl, type);

	if (fn != NULL) {
		fn(p, decl); // discard the internal name, it's not critical here.
		if (decl->error) return NULL;
	}

	lex_item.t * item;
	lex_item.t * name = NULL;
	bool function_ptr = false;
	bool as           = false;
	do {
		item = collect(p, decl);
		switch(item->type) {
			case item_eof:
				return errorf(p, item, decl, "in typedef: expected identifier or '('");
			case item_id:
				if (strcmp("as", item->value) == 0) {
					rewind_whitespace(p, decl, item);
					as = true;
					continue;
				}
				if (!function_ptr) name = item;
				break;
			case item_symbol:
				if (item->value[0] == ';') continue;
			case item_open_symbol:
				if (!function_ptr && item->value[0] == '(') {
					function_ptr = true;
					append(decl, item);
					name = parse_function_ptr(p, decl);
					if (decl->error) return name;
					continue;
				}
			default:
				break;
		}

		append(decl, item);
	} while (
			item->type != item_error                             &&
			!(item->type == item_symbol && item->value[0] == ';') &&
			!as
	);

	if (item->type == item_error) {
		decl->error = true;
		return item;
	}

	if (!as) parser.backup(p, item);
	return name;
}

static lex_item.t * parse_struct (parser.t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_struct;
	lex_item.t * name = NULL;
	lex_item.t * type = stack.last(decl->items);
	lex_item.t * item = collect(p, decl);
	if (item->type == item_id) {
		name = identifier.parse_typed(p, type, item, decl->visibility != 0);
		append(decl, name);
		item = collect(p, decl);
	}

	if (item->type != item_open_symbol || item->value[0] != '{') {
		parser.backup(p, item);
		return name;
	}

	append(decl, item);
	parse_struct_block(p, decl, item);

	return (decl->error) ? NULL : name;
}

static lex_item.t * parse_union (parser.t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_union;
	lex_item.t * type = stack.last(decl->items);
	lex_item.t * name = NULL;
	lex_item.t * item = collect(p, decl);

	// union name
	if (item->type == item_id) {
		name = identifier.parse_typed(p, type, item, decl->visibility != 0);
		append(decl, name);
		item = collect(p, decl);
	}

	// if there is a second enum it is a variable declaration
	if (item->type == item_id) {
		if (decl->type == type_union) decl->type = type_variable;
		name = item;
		append(decl, name);

		return (decl->error) ? NULL : name;
	}

	if (item->type != item_open_symbol || item->value[0] != '{') {
		return errorf(p, item, decl, "in union: expecting '{' but got %s", lex_item.to_string(item));
	}

	append(decl, item);
	parse_struct_block(p, decl, item);
	return (decl->error) ? NULL : name;
}

static lex_item.t * parse_enum (parser.t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_enum;

	lex_item.t * name = NULL;
	lex_item.t * type = stack.last(decl->items);
	lex_item.t * item = collect(p, decl);

	// enum name
	if (item->type == item_id) {
		name = identifier.parse_typed(p, type, item, decl->visibility != 0);
		append(decl, name);
		item = collect(p, decl);
	}

	// if there is a second enum it is a variable declaration
	if (item->type == item_id) {
		if (decl->type == type_enum) decl->type = type_variable;
		name = item;
		append(decl, name);

		return (decl->error) ? NULL : name;
	}

	if (item->type != item_open_symbol || item->value[0] != '{') {
		return errorf(p, item, decl, "in enum: expecting '{' but got %s", lex_item.to_string(item));
	}

	append(decl, item);
	parse_enum_block(p, decl, item);
	return (decl->error) ? NULL : name;
}

static lex_item.t * parse_export_block(parser.t * p, decl_t * decl) {
	lex_item.t * item;

	int escaped_id = 0;
	while(true) {
		int record = 0;
		item = collect(p, decl);

		switch(item->type) {
			case item_c_code:
			case item_quoted_string:
			case item_comment:
			case item_symbol:
			case item_pp_symbol:
			case item_pp_newline:
			case item_pp_token:
				record = 1;
				break;

			case item_arrow:
				escaped_id = 1;
				record     = 1;
				break;

			case item_id:
				if (escaped_id == 0) item = identifier.parse(p, item, decl->visibility != 0);
				record     = 1;
				escaped_id = 0;
				break;

			case item_close_symbol:
				if (item->value[0] == '}') {
					return lex_item.replace_value(item, str.dup("block"));
				};
				break;

			case item_eof:
				return errorf(p, item, decl, "in block: unmatched '{'");
			case item_error:
				parser.backup(p, item);
				decl->error = true;
				return NULL;
			default:
				return errorf(p, item, decl, "in block: unexpected input '%s'", lex_item.to_string(item));
		}
		if (record) append(decl, item);
	}

}

static lex_item.t * parse_struct_block( parser.t * p, decl_t * decl, lex_item.t * start) {
	lex_item.t * item;
	while(expression.declaration(p, decl->items, decl->visibility != 0)) {
		parse_semicolon(p, decl);
	}

	// TODO: ensure item is a closing brace;
	item = collect(p, decl);
	if (item && item->type == item_eof) {
		return errorf(p, start, decl, "in struct block: missing matching '}' before end of file");
	}
	append(decl, item);
	return item;
}

static lex_item.t * parse_enum_declaration(parser.t * p, decl_t * decl) {
	lex_item.t * item = collect(p, decl);

	if (item->type != item_id) {
		if (item->type == item_close_symbol && item->value[0] == '}') return item;
		return errorf(p, item, decl, "in enum: expecting identifier but got %s",
				lex_item.to_string(item));
	}
	append(decl, item);

	/*
	if (strcmp("as", item->value) == 0) return item;
	append(decl, item);
	*/

	item = collect(p, decl);
	if (!(item->type == item_symbol && (item->value[0] != ',' || item->value[0] != '='))) {
		return item;
	}
	append(decl, item);

	if (item->value[0] == '=') {
		expression.rvalue(p, decl->items, decl->visibility != 0);
		/**
		item = collect(p, decl);
		if (item->type != item_number) {
			return errorf(p, item, decl, "in enum: expecting a number but got %s",
					lex_item.to_string(item));
		}
		append(decl, item);

		*/
		item = collect(p, decl);
		if (item->type != item_symbol || item->value[0] != ',') {
			return item;
		}
		append(decl, item);
	}

	return item;
}

static lex_item.t * parse_enum_block ( parser.t * p, decl_t * decl, lex_item.t * start) {
	lex_item.t * item;
	do {
		item = parse_enum_declaration(p, decl);
		if (item == NULL || item->type == item_eof) {
				return errorf(p, start, decl, "in enum block: missing matching '}' before end of file");
		}
		if (item->type == item_error) return item;
	} while (!(item->type == item_close_symbol && item->value[0] == '}'));
	append(decl, item);
	return item;
}

static lex_item.t * parse_type (parser.t * p, decl_t * decl) {
	lex_item.t * item;
	lex_item.t * name = NULL;
	int escaped_id = 0;
	do {
		item = collect(p, decl);
		switch(item->type) {
			case item_arrow:
				escaped_id = 1;
				append(decl, item);
				continue;
			case item_id:
				if (escaped_id == 0) item = identifier.parse(p, item, decl->visibility != 0);
				name = item;
				break;
			case item_symbol:
				if (item->value[0] == '*') break;
			case item_open_symbol:
				if (item->value[0] == '(') {
					append(decl, item);
					return name;
				}
			default:
				return errorf(p, item, decl, "in type declaration: expecting identifier or '('");
		}
		append(decl, item);
	} while (item->type != item_open_symbol || item->value[0] != '(');
	return NULL;
}

static lex_item.t * parse_variable (parser.t * p, decl_t * decl) {
	lex_item.t * item;
	lex_item.t * name = NULL;
	bool escaped_id = 0;
	bool escape_till_semicolon = false;
	do {
		item = collect(p, decl);
		if (escape_till_semicolon && (item->type != item_symbol || item->value[0] != ';')) {
			append(decl, item);
			continue;
		}
		switch(item->type) {
			case item_arrow:
				escaped_id = true;
				append(decl, item);
				continue;
			case item_id:
				if (strcmp(item->value, "as") == 0) {
					if (decl->type == type_block) decl->type = type_variable;
					rewind_whitespace(p, decl, item);
					return name;
				}
				if (escaped_id == false) item = identifier.parse(p, item, decl->visibility != 0);
				name = item;
				break;
			case item_symbol:
				if (item->value[0] == '*') break;
				if (item->value[0] == '=' || item->value[0] == ':') {
					escape_till_semicolon = true;
					break;
				}
				if (item->value[0] == ';') {
					parser.backup(p, item);
					if (decl->type == 0) decl->type = type_variable;
					return name;
				}
			case item_open_symbol:
				if (item->value[0] == '(') {
					lex_item.t * star = parser.next(p);
					parser.backup(p, star);
					if (star->type == item_symbol && star->value[0] == '*') {
						append(decl, item);
						return parse_function_ptr(p, decl);
					}
					parser.backup(p, item);
					rewind_until(p, decl, name);
					return parse_function(p, decl);
				}
				if (item->value[0] == '[') {
					append(decl, item);
					item = parser.next(p);
					if (item->type == item_number || item->type == item_id) {
						append(decl, item);
						item = parser.next(p);
					}
					if (item->type != item_close_symbol || item->value[0] != ']') {
						return errorf(p, item, decl,
								"in type declaration: expecting terminating ']' but got %s",
								lex_item.to_string(item)
						);
					}
					break;
				}
			default:
				return errorf(p, item, decl, "in type declaration: expecting identifier or '(' but got %s", lex_item.to_string(item));
		}
		append(decl, item);
	} while (item && item->type != item_eof);
	if (decl->type == 0) decl->type = type_variable;
	return name;
}

static lex_item.t * parse_function_args(parser.t * p, decl_t * decl) {
	int level = 1;
	int escaped_id = 0;
	lex_item.t * item;
	do {
		item = collect(p, decl);
		switch(item->type) {
			case item_arrow:
				escaped_id = 1;
				append(decl, item);
				continue;
			case item_id:
				if (escaped_id == 0) {
					item = identifier.parse(p, item, decl->visibility != 0);
				}
				break;
			case item_open_symbol:
				if (item->value[0] == '(') level++;
				break;
			case item_close_symbol:
				if (item->value[0] == ')') level--;
				break;
			default:
				break;
		}

		escaped_id = 0;
		append(decl, item);
	} while (level > 0 && item && item->type != item_eof && item->type != item_error);
	return item;
}

static lex_item.t * parse_function (parser.t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_function;
	lex_item.t * name = parse_type(p, decl);

	if (decl->error) {
		stack.t * s = emit(p, decl, true, false);
		stack.free_items(s);
		stack.free(s);
		decl->items = NULL;
		return NULL;
	}

	parse_function_args(p, decl);
	if (decl->error) return NULL;

	return name;
}

static lex_item.t * parse_pp(parser.t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_header;
	lex_item.t * symbol = stack.first(decl->items);
	preprocessor.parse_inline(p, decl->items);

	return symbol;
}
