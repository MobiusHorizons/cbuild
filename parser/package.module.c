package "parser_package";

import parser     from "./parser.module.c";
import lex_item   from "../lexer/item.module.c";
import string     from "./string.module.c";
import str        from "../utils/strings.module.c";
import Package    from "../package/package.module.c";

#include <stdarg.h>
#include <stdio.h>

#define ERROR_PREFIX "Invalid package syntax: "

static int errorf(parser.t * p, lex_item.t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	parser.verrorf(p, ERROR, false, item, NULL, fmt, args);
	return -1;
}

export int parse(parser.t * p, lex_item.t * package_token) {
	lex_item.t * target = NULL;
	lex_item.t * name = parser.token(p, NULL);
	if (name->type != item_quoted_string) {
		return errorf(p, name, ERROR_PREFIX "Expecting a quoted string, but got %s", lex_item.to_string(name));
	}

	lex_item.t * semicolon = parser.skip(p, NULL, item_whitespace, 0);
	if (semicolon->type == item_quoted_string) {
		target = semicolon;
		semicolon = parser.skip(p, NULL, item_whitespace, 0);

	}
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, ERROR_PREFIX "Expecting ';' got %s", lex_item.to_string(semicolon));
	}

	hash_del(Package.id_cache, p->pkg->name);
	global.free(p->pkg->name);
	p->pkg->name = str.dup(string.parse(name->value));
	hash_set(Package.id_cache, p->pkg->name, p->pkg);
	if (target) {
		p->pkg->target = str.dup(string.parse(target->value));
	}
	return 1;
}
