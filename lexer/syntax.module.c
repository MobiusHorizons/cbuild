package "lex_syntax";

#include <ctype.h>
#include <string.h>

import lexer    from "./lex.module.c";
import stream   from "../deps/stream/stream.module.c";
import lex_item from "item.module.c";

/* declaration for state functions */
static void * lex_c(lexer.t * lex);

static void * lex_comment(lexer.t * lex);
/*static void * lex_export(lexer.t * lex);*/

static void * lex_whitespace(lexer.t * lex);
static void * lex_id(lexer.t * lex);
static void * lex_number(lexer.t * lex);
static void * lex_quote(lexer.t * lex);
static void * lex_squote(lexer.t * lex);
static void * lex_pp(lexer.t * lex);
static void * lex_pp_token(lexer.t * lex);

export lexer.t * new(stream.t * input, const char * filename) {
	return lexer.new(lex_c, input, filename);
}

static void * emit(enum lex_item.type it, lexer.t * lex, lexer.state_fn fn){
	lexer.backup(lex);

	if (lex->pos > lex->start) {
		lexer.emit(lex, it);
	}

	if (fn == NULL) lexer.next(lex);

	return fn;
}

static void * eof(lexer.t * lex) {
	lexer.backup(lex);

	if (lex->pos > lex->start) {
			lexer.emit(lex, item_c_code);
	}
	lexer.emit(lex, item_eof);

	return NULL;
}

/* lexes standard C code looking for things that might be modular c */
static void * lex_c(lexer.t * lex) {
	char next = 0;

	while (true) {
		char c = lexer.next(lex);

		if (c == 0)      return eof(lex);
		if (isspace(c))  return emit(item_c_code, lex, lex_whitespace);
		if (isalpha(c))  return emit(item_c_code, lex, lex_id);
		if (isdigit(c))  return emit(item_c_code, lex, lex_number);

		switch(c) {
			case '\'':
				return emit(item_c_code, lex, lex_squote);

			case '"':
				return emit(item_c_code, lex, lex_quote);

			case ';':
			case ':':
			case '.':
			case ',':
			case '=':
			case '*':
			case '!':
			case '<':
			case '>':
			case '^':
			case '|':
				emit(item_c_code, lex, NULL);
				lexer.emit(lex, item_symbol);
				break;

			case '-':
				if (lexer.peek(lex) == '>') {
					lexer.next(lex);
					lexer.emit(lex, item_arrow);
				}
				break;

			case '#':
				return emit(item_c_code, lex, lex_pp);

			case '_':
				return emit(item_c_code, lex, lex_id);

			case '/':
				next = lexer.peek(lex);
				if (next == '/' || next == '*')
					return emit(item_c_code, lex, lex_comment);
				break;

			case '(':
			case '[':
			case '{':
				emit(item_c_code, lex, NULL);
				lexer.emit(lex, item_open_symbol);
				break;

			case ')':
			case ']':
			case '}':
				emit(item_c_code, lex, NULL);
				lexer.emit(lex, item_close_symbol);
				break;

		}
	}
}


static void * lex_whitespace(lexer.t * lex) {
	char c;
	while ((c = lexer.next(lex)) != 0 && isspace(c) && c != '\n');
	lexer.backup(lex);
	if (lex->pos > lex->start) {
		lexer.emit(lex, item_whitespace);
	}

	if (c == '\n') {
		c = lexer.next(lex);
		lexer.emit(lex, item_newline);
		return lex_whitespace(lex);
	}

	if (c == 0) return eof(lex);
	return lex_c;
}

static void * lex_oneline_comment(lexer.t * lex) {
	char c;
	while ((c = lexer.next(lex)) != 0 && c != '\n');
	lexer.backup(lex);
	lexer.emit(lex, item_comment);
	if (c == 0) return eof(lex);
	return lex_c;
}

static void * lex_multiline_comment(lexer.t * lex) {
	char c;
	size_t line     = lex->line;
	size_t line_pos = lex->line_pos;
	size_t pos      = lex->pos;
	do {
		while ((c = lexer.next(lex)) != 0 && c != '*');

		if (c == 0) {
			lexer.error_ex(lex,
				line, line_pos, pos,
				"Unterminated multiline comment\n %.*s", 
				lex->length - lex->start,
				lex->input + lex->start
			);
			return eof(lex);
		}

	} while (lexer.peek(lex) != '/');

	lexer.next(lex);
	lexer.emit(lex, item_comment);
	return lex_c;
}

static void * lex_comment(lexer.t * lex) {
	lex->pos++;
	char c = lexer.next(lex);

	if (c == '/') return lex_oneline_comment(lex);
	else          return lex_multiline_comment(lex);
}

static void * lex_number(lexer.t * lex) {
	char c;
	while ((c = lexer.next(lex)) != 0 && isdigit(c));

	// Hexidecimal
	if ((c == 'x') && (lex->pos - lex->start) == 2) {
		while ((c = lexer.next(lex)) != 0 && isxdigit(c));
	}

	// the last character didn't match the number so backup before emitting.
	lexer.backup(lex);
	lexer.emit(lex, item_number);

	if (c == 0) return eof(lex);
	return lex_c;
}

static void * lex_id(lexer.t * lex) {
	char c;
	while ((c = lexer.next(lex)) != 0 && (isalnum(c) || c == '_'));
	lexer.backup(lex);
	lexer.emit(lex, item_id);

	if (c == 0) return eof(lex);
	return lex_c;
}


static void * lex_quote(lexer.t * lex) {
	void * parent_state = lex->parent_state;

	size_t line     = lex->line;
	size_t line_pos = lex->line_pos;
	size_t pos      = lex->pos;
	lex->pos ++;

	char c;
	do {
		c = lexer.next(lex);
		if (c == '\\' && lexer.next(lex) != 0) {
			c = lexer.next(lex);
			continue;
		}
	} while(c != 0 && c != '"' && c != '\n');

	if (c == 0 || c == '\n') {
		return lexer.error_ex(lex, line, line_pos, pos, "Missing terminating '\"' character\n");
	}

	lexer.emit(lex, item_quoted_string);
	return parent_state;
}

static void * lex_squote(lexer.t * lex) {
	size_t line     = lex->line;
	size_t line_pos = lex->line_pos;
	size_t pos      = lex->pos;
	lex->pos ++;

	char c;
	do {
		c = lexer.next(lex);
		if (c == '\\' && lexer.next(lex) != 0) {
			c = lexer.next(lex);
			continue;
		}
	} while(c != 0 && c != '\'');

	if (c == 0) {
		size_t length = lex->pos - lex->start;
		if (length > 10) {
			return lexer.error_ex(lex, 
				line, line_pos, pos,
				"Missing terminating ' character\n%.*s...",
				10, lex->input + lex->start
			);
		} else {
			return lexer.error_ex(lex,
				line, line_pos, pos,
				"Missing terminating ' character\n%s",
				lex->input + lex->start
			);
		}
	}

	lexer.emit(lex, item_char_literal);
	return lex_c;
}

static void * lex_pp_ws(lexer.t * lex) {
	char c;
	while ((c = lexer.next(lex)) != 0 && isspace(c) && c != '\n');
	lexer.backup(lex);
	if (lex->pos > lex->start) {
		lexer.emit(lex, item_whitespace);
	}

	return lex_pp_token;
}

static void * lex_pp_newline(lexer.t * lex) {
	lexer.next(lex); // consume backslash
	char c = lexer.next(lex);
	if (c == '\n') {
		lexer.emit(lex, item_pp_newline);
	}

	return lex_pp_token;
}

static void * lex_pp_ident(lexer.t * lex) {
	char c = lexer.next(lex);
	if (isdigit(c)) {
		lexer.accept_run(lex, "0123456789.");
		lexer.emit(lex, item_pp_token);
		return lex_pp_token;
	}

	if (isalnum(c)) {
		lexer.accept_run(lex, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_");
		lexer.emit(lex, item_pp_token);
		return lex_pp_token;
	}
	return lex_pp_token;
}

static void * lex_pp_token(lexer.t * lex) {
	while(true) {
		char c = lexer.next(lex);

		if (c == '\\')  return emit(item_pp_token, lex, lex_pp_newline);
		if (c == '\n')  return emit(item_pp_token, lex, lex_c);
		if (c == '"')   return emit(item_pp_token, lex, lex_whitespace);
		if (isspace(c)) return emit(item_pp_token, lex, lex_pp_ws);
		if (isalnum(c)) return emit(item_pp_token, lex, lex_pp_ident);
	}
}

static void * lex_pp(lexer.t * lex) {
	lexer.next(lex);
	lexer.emit(lex, item_pp_symbol);
	return lex_pp_token;
}
