package "lex_item_cache";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
import item from "item.module.c";
import stream from "../deps/stream/stream.module.c";
import str from "../utils/strings.module.c";

export typedef struct {
	item.t ** items;
	size_t num_items;
} line_t;

export typedef struct {
	line_t * lines;
	size_t num_lines;
} cache_t as t;

export cache_t * new() {
	cache_t * c = malloc(sizeof(cache_t));
	c->lines = NULL;
	c->num_lines = 0;

	return c;
}

export int insert(cache_t * c, item.t * tok) {
#if LOG_DEBUG 
	fprintf(stderr, "[Cache %p] inserting %s at %d:%d\n", c, item.to_string(tok), tok->line + 1,  tok->start - tok->line_pos + 1);
#endif
	if (tok->line >= c->num_lines) {
		c->lines = realloc(c->lines, (tok->line + 1) * sizeof(line_t));
		memset(&c->lines[c->num_lines], 0, (tok->line - c->num_lines + 1) * sizeof(line_t));
		/*size_t l;*/
		/*for (l = c->num_lines; l < tok->line; l++) {*/
			/*c->lines[l].items     = malloc(sizeof(item.t *));*/
			/*c->lines[l].items[0]  = item.new(str.dup("\n"), item_newline, 0, 0, 0);*/
			/*c->lines[l].num_items = 1;*/
		/*}*/
		c->num_lines = tok->line + 1;
		/*memset(&c->lines[tok->line], 0, sizeof(line_t));*/
	}

	line_t * line = &c->lines[tok->line];
	line->items = realloc(line->items, (line->num_items + 1) * sizeof(item.t *));
	line->items[line->num_items] = tok;
	line->num_items++;
	tok->refcount++;

	return 0;
}

export ssize_t write(cache_t * c, stream.t * s) {
	if (c == NULL || s == NULL) return -1;

	size_t len = 0;
	ssize_t e = 0;
	size_t l, i;
	for (l = 0; l < c->num_lines; l++) {
		line_t * line = c->lines + l;
		for (i = 0; i < line->num_items; i++) {
			item.t * tok = line->items[i];
			e = stream.write(s, tok->value, tok->length);
			if (e <= 0) return e;
			len += e;
		}
	}
	return len;
}

export void free(cache_t * c) {
	if (c == NULL) return;

	size_t l, i;
	for (l = 0; l < c->num_lines; l++) {
		line_t * line = c->lines + l;
		for (i = 0; i < line->num_items; i++) {
			item.free(line->items[i]);
		}
		global.free(line->items);
	}
	global.free(c->lines);
	global.free(c);
}

/** context informaiton for tokens that have been found in the cache. */
export typedef struct {
	size_t   line_no;
	line_t * line;
	size_t   token_no;
	item.t * token;
} token_context_t;

/**
 * retrieve the token at a specific line / column.
 * If no token is found at the specified coordinates, returns NULL
 */
export item.t * token_at(cache_t * c, int line, int col, token_context_t * ctx) {
	if (c == NULL) return NULL;
	if (line > c->num_lines) return NULL;
	if (ctx) ctx->line_no = line;
	fprintf(stderr, "I: finding token at (%d:%d)\n", 
			line + 1, col);

	line_t l = c->lines[line];
	if (ctx) ctx->line = &l;
	size_t i;

	fprintf(stderr, "I: found line %d with %zu items\n", line + 1, l.num_items); 
	for (i = 0; i < l.num_items; i++) {
		item.t * tok = l.items[i];
		size_t pos = tok->line_pos + col;
		fprintf(stderr, "I \t%zu-%zu '%.*s'\n", tok->start - tok->line_pos, tok->start + tok->length - tok->line_pos, 10, tok->value); 
		if (pos - tok->start < tok->length) {
			if (ctx) {
				ctx->token = tok;
				ctx->token_no = i;
			}
			return tok;
		}
	}

	return NULL;
}
