package "lex_item";

import str from "../utils/strings.module.c";

export #include <stdlib.h>
export #include <stdbool.h>
#include <stdio.h>
#include <string.h>

export enum item_type {
	item_error = 0,
	item_eof,
	item_whitespace,
	item_newline,
	item_c_code,
	item_id,
	item_number,
	item_char_literal,
	item_quoted_string,
	item_comment,

	/* preprocessor */
	item_pp_symbol,
	item_pp_newline,
	item_pp_token,

	/* symbols */
	item_symbol,
	item_open_symbol,
	item_close_symbol,
	item_arrow,

	item_total_symbols
} as type;

export const char * type_names[item_total_symbols] = {
	"Error",
	"eof",
	"white space",
	"newline",
	"c code",
	"identifier",
	"number",
	"character literal",
	"string",
	"comment",
	"PP symbol",
	"PP newline",
	"PP token",
	"symbol",
	"open symbol",
	"close symbol",
	"arrow symbol",
};

export typedef struct item_s {
	enum item_type type;
	char         * value;
	size_t         length;
	size_t         line;
	size_t         line_pos;
	size_t         start;
	struct item_s * replaced_by;
	struct item_s * source;
	int refcount;
	/*const char * tracking;*/
} item_t as t;

export const item_t empty = {0};

export item_t * new(char * value, enum item_type type, size_t line, size_t line_pos, size_t start) {
	item_t * i = malloc(sizeof(item_t));

	i->value       = value;
	i->length      = str.len(value);
	i->type        = type;
	i->line        = line;
	i->line_pos    = line_pos;
	i->start       = start;
	i->source      = i;
	i->replaced_by = NULL;
	i->refcount    = 0;
	/*i->tracking    = NULL;*/

	return i;
}

export char * to_string(item_t * item) {
	char * buf = NULL;
	if (item == NULL) {
		return str.dup("(null)");
	}

	const char * name = type_names[item->type];

	switch(item->type) {
		case item_error:
			return str.dup(item->value == NULL ? "(null)" : item->value);

		case item_eof:
			return str.dup("<eof>");

		case item_newline:
			return str.dup(name);

		case item_pp_newline:
			asprintf(&buf, "%s '\\n'", name);
			return buf;

		default:
			if (item->length > 20 || strchr(item->value, '\n') != NULL) {
				asprintf(&buf, "%s '%.*s...(%zu)'", name, 20, item->value, item->length);
				return buf;
			}

			asprintf(&buf, "%s '%s'", name, item->value);
			return buf;
	}
}

export bool equals(item_t * a, item_t * b) {
	return a == b || (
		a != NULL  && b != NULL  &&
		a->type     == b->type     &&
		a->value    == b->value    &&
		a->length   == b->length   &&
		a->line     == b->line     &&
		a->line_pos == b->line_pos &&
		a->start    == b->start
	);
}

export void free(item_t * item) {
	if (item == NULL) return;

	/*if (item->tracking) {*/
		/*char * s = to_string(item);*/
		/*fprintf(stderr, "[ITEM_FREE]: [\"%p\", \"%s\", \"%s\", %d, %lu, %lu]\n", item, item->tracking, s, item->refcount, item->length, sizeof(*item)); */
		/*global.free(s);*/
	/*}*/

	item->refcount--;
	if (item->refcount > 0) return;

	global.free(item->value);
	global.free(item);
}

export item_t * replace_value(item_t * a, char * value) {
	item_t * replacement = new(
		value,
		a->type,
		a->line,
		a->line_pos,
		a->start
	);

	replacement->source = a;
	a->replaced_by = replacement;

	return replacement;
}


// Test function for debugging memory leaks
export void unfreed() {
#ifdef MEM_DEBUG
	size_t count = 0;
	size_t i;
	for (i = 0; i <= item_index; i++) {
		item_t item = cache[i];
		if (item.index) {
			count++;
			printf("\n[%ld]{\n"
				"    value    : '%s',\n"
				"    length   : '%ld',\n"
				"    type     : '%s',\n"
				"    line     : '%ld',\n"
				"    line_pos : '%ld',\n"
				"    start    : '%ld',\n"
				"}\n\n",
				item.index,
				item.value,
				item.length,
				type_names[item.type],
				item.line,
				item.line_pos,
				item.start
			);
		}

		// intentionally lose the value so valgrind reports correctly
		cache[i].value = NULL; 
	}
	if (count > 0) {
		printf("lex_item audit: found %ld unfreed items\n", count);
	}
	item_index = 0;
#endif
}
