package "lex_item_stack";

import lex_item from "./item.module.c";
import str      from "../utils/strings.module.c";

#include <string.h>
export #include <stdlib.h>

export typedef struct {
	lex_item.t  ** buffer;
	lex_item.t  ** items;
	size_t        offset;
	size_t        capacity;
	size_t        length;

} item_stack_t as t;

export item_stack_t * new(size_t count) {
	item_stack_t * s = malloc(sizeof(item_stack_t));

	s->items    = s->buffer = malloc(count * sizeof(lex_item.t *));
	s->capacity = count;
	s->length   = 0;
	s->offset   = 0;

	return s;
}

export item_stack_t * resize(item_stack_t * s, size_t count) {
	if (s->capacity >= count) {
		return s;
	}

	s->buffer = realloc(s->buffer, (count + s->offset) * sizeof(lex_item.t *));
	s->items = s->buffer + s->offset;

	s->capacity = count;
	return s;
}

export item_stack_t * push(item_stack_t * s, lex_item.t * item) {
	s = resize(s, s->length + 1);

	s->items[s->length] = item;
	s->length++;

	return s;
}

export lex_item.t * pop(item_stack_t * s) {
	if (s->length == 0) {
		return lex_item.new(str.dup("No more items"), item_error, 0,0,0);
	}

	s->length--;
	return s->items[s->length];
}

export lex_item.t * shift(item_stack_t * s) {
	if (s->length == 0) {
		return lex_item.new(str.dup("No more items"), item_error, 0,0,0);
	}

	s->length--;
	s->items++;
	s->capacity--;
	return s->buffer[s->offset++];
}

/**
 * Prepends items to the beginning of the stack.
 */
export item_stack_t * unshift(item_stack_t * s, lex_item.t * item) {
	if (s->offset) {
		s->offset--;
		s->items--;
		s->capacity++;
		s->length++;
		s->items[0] = item;

		return s;
	}

	size_t count = s->capacity + 1;
	lex_item.t ** buffer = malloc((count + s->offset) * sizeof(lex_item.t *));
	s->items = buffer;
	s->items[0] = item;
	memcpy(s->items + 1, s->buffer, (s->length * sizeof(lex_item.t *)));
	global.free(s->buffer);
	s->buffer = buffer;
	s->length++;
	s->capacity = count;

	return s;
}

export void free_items(item_stack_t * s) {
	size_t i;
	for (i = 0; i < s->length; i++) { 
		lex_item.free(s->items[i]);
	}
}

export void free(item_stack_t * s) {
	s->length = 0;
	global.free(s->buffer);
	global.free(s);
}

export lex_item.t * first(item_stack_t * s) {
	if (s == NULL || s->length == 0 || s->items == NULL) {
		return NULL;
	}
	return s->items[0];
}

export lex_item.t * last(item_stack_t * s) {
	if (s == NULL || s->length == 0 || s->items == NULL) {
		return NULL;
	}
	return s->items[s->length - 1];
}

export lex_item.t * peek(item_stack_t * s, size_t i) {
	if (s == NULL || s->length <= i || s->items == NULL) {
		return NULL;
	}
	return s->items[i];
}

export size_t height(item_stack_t * s) {
	return s->length;
}

export lex_item.t * replace_with(item_stack_t * s, lex_item.t * item, char * value) {
	if (s == NULL) {
		return NULL;
	}

	if (item == NULL) {
		if (s->length) {
			item = s->items[0];
		} else {
			return NULL;
		}
	}

	lex_item.t * replacement = lex_item.replace_value(item, value);
	item->replaced_by = item;

	size_t i;
	for (i = 0; i < s->length; i++) { 
		s->items[i]->replaced_by = replacement;
	}

	free(s);

	return replacement;
}
