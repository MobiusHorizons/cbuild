package "main";

#include <string.h>
#include <stdio.h>

import json    from "deps/json/json.module.c";
import stream  from "deps/stream/stream.module.c";
import file    from "deps/stream/file.module.c";
import closure from "deps/closure/closure.module.c";

import server from "lsp/rpc/server.module.c";

import initialize    from "lsp/initialize.module.c";
import completion    from "lsp/completion.module.c";
import definition    from "lsp/definition.module.c";
import text_document from "lsp/text-document.module.c";
import state         from "lsp/state.module.c";

int main(int argc, char ** argv) {
	server.debug_t dbg = {};

	if (argc > 1) {
		char * dir = argv[1];
		size_t len = strlen(dir) + strlen("/cbuild-lsp-output") + 1;
		char * buf = malloc(len);

		snprintf(buf, len, "%s/cbuild-lsp-input", dir);
		dbg.input = file.open(buf, O_WRONLY|O_CREAT);
		if (dbg.input->error.code) {
			fprintf(stderr, "Error opening file '%s': %s", 
					buf, dbg.input->error.message);
			return 1;
		}
		
		snprintf(buf, len, "%s/cbuild-lsp-output", dir);
		dbg.output = file.open(buf, O_WRONLY|O_CREAT);
		if (dbg.output->error.code) {
			fprintf(stderr, "Error opening file '%s': %s", 
					buf, dbg.output->error.message);
			return 1;
		}
	}

	stream.t * in  = file.new(0);
	stream.t * out = file.new(1);
	stream.t * err = file.new(2);

	server.t * lsp = server.new(in, out, dbg);

	state.t s = {
		.lsp = lsp, 
		.err = err,
	};

	server.register(lsp,
		"initialize",
		closure.new(initialize.handler, &s)
	);

	server.register(lsp,
		"initialized",
		closure.new(server.ignore, &s)
	);

	server.register(lsp, 
		"textDocument/didOpen",
		closure.new(text_document.open, &s)
	);

	server.register(lsp, 
		"textDocument/didChange",
		closure.new(text_document.change, &s)
	);

	server.register(lsp,
		"textDocument/didClose",
		closure.new(server.ignore, &s)
	);

	server.register(lsp,
		"textDocument/completion",
		closure.new(completion.handler, &s)
	);

	server.register(lsp,
		"textDocument/definition",
		closure.new(definition.handler, &s)
	);

	return server.listen(lsp);
}
