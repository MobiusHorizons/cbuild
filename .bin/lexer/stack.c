#define _package_lex_item_stack_

#include "item.h"
#include "../utils/strings.h"

#include <string.h>
#include <stdlib.h>

typedef struct {
	lex_item_t  ** buffer;
	lex_item_t  ** items;
	size_t        offset;
	size_t        capacity;
	size_t        length;

} lex_item_stack_t;

lex_item_stack_t * lex_item_stack_new(size_t count) {
	lex_item_stack_t * s = malloc(sizeof(lex_item_stack_t));

	s->items    = s->buffer = malloc(count * sizeof(lex_item_t *));
	s->capacity = count;
	s->length   = 0;
	s->offset   = 0;

	return s;
}

lex_item_stack_t * lex_item_stack_resize(lex_item_stack_t * s, size_t count) {
	if (s->capacity >= count) {
		return s;
	}

	s->buffer = realloc(s->buffer, (count + s->offset) * sizeof(lex_item_t *));
	s->items = s->buffer + s->offset;

	s->capacity = count;
	return s;
}

lex_item_stack_t * lex_item_stack_push(lex_item_stack_t * s, lex_item_t * item) {
	s = lex_item_stack_resize(s, s->length + 1);

	s->items[s->length] = item;
	s->length++;

	return s;
}

lex_item_t * lex_item_stack_pop(lex_item_stack_t * s) {
	if (s->length == 0) {
		return lex_item_new(strings_dup("No more items"), item_error, 0,0,0);
	}

	s->length--;
	return s->items[s->length];
}

lex_item_t * lex_item_stack_shift(lex_item_stack_t * s) {
	if (s->length == 0) {
		return lex_item_new(strings_dup("No more items"), item_error, 0,0,0);
	}

	s->length--;
	s->items++;
	s->capacity--;
	return s->buffer[s->offset++];
}

/**
 * Prepends items to the beginning of the stack.
 */
lex_item_stack_t * lex_item_stack_unshift(lex_item_stack_t * s, lex_item_t * item) {
	if (s->offset) {
		s->offset--;
		s->items--;
		s->capacity++;
		s->length++;
		s->items[0] = item;

		return s;
	}

	size_t count = s->capacity + 1;
	lex_item_t ** buffer = malloc((count + s->offset) * sizeof(lex_item_t *));
	s->items = buffer;
	s->items[0] = item;
	memcpy(s->items + 1, s->buffer, (s->length * sizeof(lex_item_t *)));
	free(s->buffer);
	s->buffer = buffer;
	s->length++;
	s->capacity = count;

	return s;
}

void lex_item_stack_free_items(lex_item_stack_t * s) {
	size_t i;
	for (i = 0; i < s->length; i++) { 
		lex_item_free(s->items[i]);
	}
}

void lex_item_stack_free(lex_item_stack_t * s) {
	s->length = 0;
	free(s->buffer);
	free(s);
}

lex_item_t * lex_item_stack_first(lex_item_stack_t * s) {
	if (s == NULL || s->length == 0 || s->items == NULL) {
		return NULL;
	}
	return s->items[0];
}

lex_item_t * lex_item_stack_last(lex_item_stack_t * s) {
	if (s == NULL || s->length == 0 || s->items == NULL) {
		return NULL;
	}
	return s->items[s->length - 1];
}

lex_item_t * lex_item_stack_peek(lex_item_stack_t * s, size_t i) {
	if (s == NULL || s->length <= i || s->items == NULL) {
		return NULL;
	}
	return s->items[i];
}

size_t lex_item_stack_height(lex_item_stack_t * s) {
	return s->length;
}

lex_item_t * lex_item_stack_replace_with(lex_item_stack_t * s, lex_item_t * item, char * value) {
	if (s == NULL) {
		return NULL;
	}

	if (item == NULL) {
		if (s->length) {
			item = s->items[0];
		} else {
			return NULL;
		}
	}

	lex_item_t * replacement = lex_item_replace_value(item, value);
	item->replaced_by = item;

	size_t i;
	for (i = 0; i < s->length; i++) { 
		s->items[i]->replaced_by = replacement;
	}

	lex_item_stack_free(s);

	return replacement;
}
