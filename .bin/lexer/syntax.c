#define _package_lex_syntax_

#include <ctype.h>
#include <string.h>

#include "lex.h"
#include "../deps/stream/stream.h"
#include "item.h"

/* declaration for state functions */
static void * lex_c(lex_t * lex);

static void * lex_comment(lex_t * lex);
/*static void * lex_export(lexer.t * lex);*/

static void * lex_whitespace(lex_t * lex);
static void * lex_id(lex_t * lex);
static void * lex_number(lex_t * lex);
static void * lex_quote(lex_t * lex);
static void * lex_squote(lex_t * lex);
static void * lex_pp(lex_t * lex);
static void * lex_pp_token(lex_t * lex);

lex_t * lex_syntax_new(stream_t * input, const char * filename) {
	return lex_new(lex_c, input, filename);
}

static void * emit(enum lex_item_type it, lex_t * lex, lex_state_fn fn){
	lex_backup(lex);

	if (lex->pos > lex->start) {
		lex_emit(lex, it);
	}

	if (fn == NULL) lex_next(lex);

	return fn;
}

static void * eof(lex_t * lex) {
	lex_backup(lex);

	if (lex->pos > lex->start) {
			lex_emit(lex, item_c_code);
	}
	lex_emit(lex, item_eof);

	return NULL;
}

/* lexes standard C code looking for things that might be modular c */
static void * lex_c(lex_t * lex) {
	char next = 0;

	while (true) {
		char c = lex_next(lex);

		if (c == 0)      return eof(lex);
		if (isspace(c))  return emit(item_c_code, lex, lex_whitespace);
		if (isalpha(c))  return emit(item_c_code, lex, lex_id);
		if (isdigit(c))  return emit(item_c_code, lex, lex_number);

		switch(c) {
			case '\'':
				return emit(item_c_code, lex, lex_squote);

			case '"':
				return emit(item_c_code, lex, lex_quote);

			case ';':
			case ':':
			case '.':
			case ',':
			case '=':
			case '*':
			case '!':
			case '<':
			case '>':
			case '^':
			case '|':
				emit(item_c_code, lex, NULL);
				lex_emit(lex, item_symbol);
				break;

			case '-':
				if (lex_peek(lex) == '>') {
					lex_next(lex);
					lex_emit(lex, item_arrow);
				}
				break;

			case '#':
				return emit(item_c_code, lex, lex_pp);

			case '_':
				return emit(item_c_code, lex, lex_id);

			case '/':
				next = lex_peek(lex);
				if (next == '/' || next == '*')
					return emit(item_c_code, lex, lex_comment);
				break;

			case '(':
			case '[':
			case '{':
				emit(item_c_code, lex, NULL);
				lex_emit(lex, item_open_symbol);
				break;

			case ')':
			case ']':
			case '}':
				emit(item_c_code, lex, NULL);
				lex_emit(lex, item_close_symbol);
				break;

		}
	}
}


static void * lex_whitespace(lex_t * lex) {
	char c;
	while ((c = lex_next(lex)) != 0 && isspace(c) && c != '\n');
	lex_backup(lex);
	if (lex->pos > lex->start) {
		lex_emit(lex, item_whitespace);
	}

	if (c == '\n') {
		c = lex_next(lex);
		lex_emit(lex, item_newline);
		return lex_whitespace(lex);
	}

	if (c == 0) return eof(lex);
	return lex_c;
}

static void * lex_oneline_comment(lex_t * lex) {
	char c;
	while ((c = lex_next(lex)) != 0 && c != '\n');
	lex_backup(lex);
	lex_emit(lex, item_comment);
	if (c == 0) return eof(lex);
	return lex_c;
}

static void * lex_multiline_comment(lex_t * lex) {
	char c;
	size_t line     = lex->line;
	size_t line_pos = lex->line_pos;
	size_t pos      = lex->pos;
	do {
		while ((c = lex_next(lex)) != 0 && c != '*');

		if (c == 0) {
			lex_error_ex(lex,
				line, line_pos, pos,
				"Unterminated multiline comment\n %.*s", 
				lex->length - lex->start,
				lex->input + lex->start
			);
			return eof(lex);
		}

	} while (lex_peek(lex) != '/');

	lex_next(lex);
	lex_emit(lex, item_comment);
	return lex_c;
}

static void * lex_comment(lex_t * lex) {
	lex->pos++;
	char c = lex_next(lex);

	if (c == '/') return lex_oneline_comment(lex);
	else          return lex_multiline_comment(lex);
}

static void * lex_number(lex_t * lex) {
	char c;
	while ((c = lex_next(lex)) != 0 && isdigit(c));

	// Hexidecimal
	if ((c == 'x') && (lex->pos - lex->start) == 2) {
		while ((c = lex_next(lex)) != 0 && isxdigit(c));
	}

	// the last character didn't match the number so backup before emitting.
	lex_backup(lex);
	lex_emit(lex, item_number);

	if (c == 0) return eof(lex);
	return lex_c;
}

static void * lex_id(lex_t * lex) {
	char c;
	while ((c = lex_next(lex)) != 0 && (isalnum(c) || c == '_'));
	lex_backup(lex);
	lex_emit(lex, item_id);

	if (c == 0) return eof(lex);
	return lex_c;
}


static void * lex_quote(lex_t * lex) {
	void * parent_state = lex->parent_state;

	size_t line     = lex->line;
	size_t line_pos = lex->line_pos;
	size_t pos      = lex->pos;
	lex->pos ++;

	char c;
	do {
		c = lex_next(lex);
		if (c == '\\' && lex_next(lex) != 0) {
			c = lex_next(lex);
			continue;
		}
	} while(c != 0 && c != '"' && c != '\n');

	if (c == 0 || c == '\n') {
		return lex_error_ex(lex, line, line_pos, pos, "Missing terminating '\"' character\n");
	}

	lex_emit(lex, item_quoted_string);
	return parent_state;
}

static void * lex_squote(lex_t * lex) {
	size_t line     = lex->line;
	size_t line_pos = lex->line_pos;
	size_t pos      = lex->pos;
	lex->pos ++;

	char c;
	do {
		c = lex_next(lex);
		if (c == '\\' && lex_next(lex) != 0) {
			c = lex_next(lex);
			continue;
		}
	} while(c != 0 && c != '\'');

	if (c == 0) {
		size_t length = lex->pos - lex->start;
		if (length > 10) {
			return lex_error_ex(lex, 
				line, line_pos, pos,
				"Missing terminating ' character\n%.*s...",
				10, lex->input + lex->start
			);
		} else {
			return lex_error_ex(lex,
				line, line_pos, pos,
				"Missing terminating ' character\n%s",
				lex->input + lex->start
			);
		}
	}

	lex_emit(lex, item_char_literal);
	return lex_c;
}

static void * lex_pp_ws(lex_t * lex) {
	char c;
	while ((c = lex_next(lex)) != 0 && isspace(c) && c != '\n');
	lex_backup(lex);
	if (lex->pos > lex->start) {
		lex_emit(lex, item_whitespace);
	}

	return lex_pp_token;
}

static void * lex_pp_newline(lex_t * lex) {
	lex_next(lex); // consume backslash
	char c = lex_next(lex);
	if (c == '\n') {
		lex_emit(lex, item_pp_newline);
	}

	return lex_pp_token;
}

static void * lex_pp_ident(lex_t * lex) {
	char c = lex_next(lex);
	if (isdigit(c)) {
		lex_accept_run(lex, "0123456789.");
		lex_emit(lex, item_pp_token);
		return lex_pp_token;
	}

	if (isalnum(c)) {
		lex_accept_run(lex, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_");
		lex_emit(lex, item_pp_token);
		return lex_pp_token;
	}
	return lex_pp_token;
}

static void * lex_pp_token(lex_t * lex) {
	while(true) {
		char c = lex_next(lex);

		if (c == '\\')  return emit(item_pp_token, lex, lex_pp_newline);
		if (c == '\n')  return emit(item_pp_token, lex, lex_c);
		if (c == '"')   return emit(item_pp_token, lex, lex_whitespace);
		if (isspace(c)) return emit(item_pp_token, lex, lex_pp_ws);
		if (isalnum(c)) return emit(item_pp_token, lex, lex_pp_ident);
	}
}

static void * lex_pp(lex_t * lex) {
	lex_next(lex);
	lex_emit(lex, item_pp_symbol);
	return lex_pp_token;
}
