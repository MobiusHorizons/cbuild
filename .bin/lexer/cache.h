#ifndef _package_lex_item_cache_
#define _package_lex_item_cache_

#include "../deps/stream/stream.h"
#include "item.h"

typedef struct {
	lex_item_t ** items;
	size_t num_items;
} lex_item_cache_line_t;

typedef struct {
	lex_item_cache_line_t * lines;
	size_t num_lines;
} lex_item_cache_t;

lex_item_cache_t * lex_item_cache_new();
int lex_item_cache_insert(lex_item_cache_t * c, lex_item_t * tok);
ssize_t lex_item_cache_write(lex_item_cache_t * c, stream_t * s);
void lex_item_cache_free(lex_item_cache_t * c);

/** context informaiton for tokens that have been found in the cache. */
typedef struct {
	size_t   line_no;
	lex_item_cache_line_t * line;
	size_t   token_no;
	lex_item_t * token;
} lex_item_cache_token_context_t;

/**
 * retrieve the token at a specific line / column.
 * If no token is found at the specified coordinates, returns NULL
 */
lex_item_t * lex_item_cache_token_at(lex_item_cache_t * c, int line, int col, lex_item_cache_token_context_t * ctx);

#endif
