#ifndef _package_lex_syntax_
#define _package_lex_syntax_

#include "../deps/stream/stream.h"
#include "lex.h"

lex_t * lex_syntax_new(stream_t * input, const char * filename);

#endif
