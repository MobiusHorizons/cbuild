#define _package_lex_item_cache_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "item.h"
#include "../deps/stream/stream.h"
#include "../utils/strings.h"

typedef struct {
	lex_item_t ** items;
	size_t num_items;
} lex_item_cache_line_t;

typedef struct {
	lex_item_cache_line_t * lines;
	size_t num_lines;
} lex_item_cache_t;

lex_item_cache_t * lex_item_cache_new() {
	lex_item_cache_t * c = malloc(sizeof(lex_item_cache_t));
	c->lines = NULL;
	c->num_lines = 0;

	return c;
}

int lex_item_cache_insert(lex_item_cache_t * c, lex_item_t * tok) {
#if LOG_DEBUG 
	fprintf(stderr, "[Cache %p] inserting %s at %d:%d\n", c, lex_item_to_string(tok), tok->line + 1,  tok->start - tok->line_pos + 1);
#endif
	if (tok->line >= c->num_lines) {
		c->lines = realloc(c->lines, (tok->line + 1) * sizeof(lex_item_cache_line_t));
		memset(&c->lines[c->num_lines], 0, (tok->line - c->num_lines + 1) * sizeof(lex_item_cache_line_t));
		/*size_t l;*/
		/*for (l = c->num_lines; l < tok->line; l++) {*/
			/*c->lines[l].items     = malloc(sizeof(item.t *));*/
			/*c->lines[l].items[0]  = item.new(str.dup("\n"), item_newline, 0, 0, 0);*/
			/*c->lines[l].num_items = 1;*/
		/*}*/
		c->num_lines = tok->line + 1;
		/*memset(&c->lines[tok->line], 0, sizeof(line_t));*/
	}

	lex_item_cache_line_t * line = &c->lines[tok->line];
	line->items = realloc(line->items, (line->num_items + 1) * sizeof(lex_item_t *));
	line->items[line->num_items] = tok;
	line->num_items++;
	tok->refcount++;

	return 0;
}

ssize_t lex_item_cache_write(lex_item_cache_t * c, stream_t * s) {
	if (c == NULL || s == NULL) return -1;

	size_t len = 0;
	ssize_t e = 0;
	size_t l, i;
	for (l = 0; l < c->num_lines; l++) {
		lex_item_cache_line_t * line = c->lines + l;
		for (i = 0; i < line->num_items; i++) {
			lex_item_t * tok = line->items[i];
			e = stream_write(s, tok->value, tok->length);
			if (e <= 0) return e;
			len += e;
		}
	}
	return len;
}

void lex_item_cache_free(lex_item_cache_t * c) {
	if (c == NULL) return;

	size_t l, i;
	for (l = 0; l < c->num_lines; l++) {
		lex_item_cache_line_t * line = c->lines + l;
		for (i = 0; i < line->num_items; i++) {
			lex_item_free(line->items[i]);
		}
		free(line->items);
	}
	free(c->lines);
	free(c);
}

/** context informaiton for tokens that have been found in the cache. */
typedef struct {
	size_t   line_no;
	lex_item_cache_line_t * line;
	size_t   token_no;
	lex_item_t * token;
} lex_item_cache_token_context_t;

/**
 * retrieve the token at a specific line / column.
 * If no token is found at the specified coordinates, returns NULL
 */
lex_item_t * lex_item_cache_token_at(lex_item_cache_t * c, int line, int col, lex_item_cache_token_context_t * ctx) {
	if (c == NULL) return NULL;
	if (line > c->num_lines) return NULL;
	if (ctx) ctx->line_no = line;
	fprintf(stderr, "I: finding token at (%d:%d)\n", 
			line + 1, col);

	lex_item_cache_line_t l = c->lines[line];
	if (ctx) ctx->line = &l;
	size_t i;

	fprintf(stderr, "I: found line %d with %zu items\n", line + 1, l.num_items); 
	for (i = 0; i < l.num_items; i++) {
		lex_item_t * tok = l.items[i];
		size_t pos = tok->line_pos + col;
		fprintf(stderr, "I \t%zu-%zu '%.*s'\n", tok->start - tok->line_pos, tok->start + tok->length - tok->line_pos, 10, tok->value); 
		if (pos - tok->start < tok->length) {
			if (ctx) {
				ctx->token = tok;
				ctx->token_no = i;
			}
			return tok;
		}
	}

	return NULL;
}
