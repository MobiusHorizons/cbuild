#ifndef _package_lex_
#define _package_lex_

#include "buffer.h"
#include "../deps/stream/stream.h"
#include "item.h"
#include "cache.h"
#include <stdlib.h>
#include <stdbool.h>

struct lex_lexer_s;

typedef void * (*lex_state_fn)(struct lex_lexer_s * lex);

typedef struct lex_lexer_s{
	stream_t * in;
	char     * input;
	char     * filename;
	size_t     length;
	size_t     start;
	size_t     pos;
	size_t     width;
	size_t     line;
	size_t     line_pos;
	lex_buffer_t * items;
	lex_item_cache_t  * cache;
	lex_state_fn   state;
	lex_state_fn   parent_state;
} lex_t;

lex_t * lex_new(lex_state_fn start, stream_t * in, const char * filename);
lex_state_fn lex_errorf(lex_t * lex, const char * fmt, ...);
lex_state_fn lex_error_ex(lex_t * lex, size_t line, size_t start, size_t pos, const char * fmt, ...);
char lex_next(lex_t * lex);
void lex_backup(lex_t * lex);
char lex_peek(lex_t * lex);
bool lex_accept(lex_t * lex, const char * matching);
void lex_accept_run(lex_t * lex, const char * matching);
void lex_ignore(lex_t * lex);
void lex_emit(lex_t * lex, enum lex_item_type it);
lex_item_t * lex_next_item(lex_t * lex);
void lex_free(lex_t * lex);

#endif
