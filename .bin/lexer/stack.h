#ifndef _package_lex_item_stack_
#define _package_lex_item_stack_

#include "item.h"
#include <stdlib.h>

typedef struct {
	lex_item_t  ** buffer;
	lex_item_t  ** items;
	size_t        offset;
	size_t        capacity;
	size_t        length;

} lex_item_stack_t;

lex_item_stack_t * lex_item_stack_new(size_t count);
lex_item_stack_t * lex_item_stack_resize(lex_item_stack_t * s, size_t count);
lex_item_stack_t * lex_item_stack_push(lex_item_stack_t * s, lex_item_t * item);
lex_item_t * lex_item_stack_pop(lex_item_stack_t * s);
lex_item_t * lex_item_stack_shift(lex_item_stack_t * s);

/**
 * Prepends items to the beginning of the stack.
 */
lex_item_stack_t * lex_item_stack_unshift(lex_item_stack_t * s, lex_item_t * item);

void lex_item_stack_free_items(lex_item_stack_t * s);
void lex_item_stack_free(lex_item_stack_t * s);
lex_item_t * lex_item_stack_first(lex_item_stack_t * s);
lex_item_t * lex_item_stack_last(lex_item_stack_t * s);
lex_item_t * lex_item_stack_peek(lex_item_stack_t * s, size_t i);
size_t lex_item_stack_height(lex_item_stack_t * s);
lex_item_t * lex_item_stack_replace_with(lex_item_stack_t * s, lex_item_t * item, char * value);

#endif
