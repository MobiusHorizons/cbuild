#define _package_lex_item_

#include "../utils/strings.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

enum lex_item_type {
	item_error = 0,
	item_eof,
	item_whitespace,
	item_newline,
	item_c_code,
	item_id,
	item_number,
	item_char_literal,
	item_quoted_string,
	item_comment,

	/* preprocessor */
	item_pp_symbol,
	item_pp_newline,
	item_pp_token,

	/* symbols */
	item_symbol,
	item_open_symbol,
	item_close_symbol,
	item_arrow,

	item_total_symbols
};

const char * lex_item_type_names[item_total_symbols] = {
	"Error",
	"eof",
	"white space",
	"newline",
	"c code",
	"identifier",
	"number",
	"character literal",
	"string",
	"comment",
	"PP symbol",
	"PP newline",
	"PP token",
	"symbol",
	"open symbol",
	"close symbol",
	"arrow symbol",
};

typedef struct item_s {
	enum lex_item_type type;
	char         * value;
	size_t         length;
	size_t         line;
	size_t         line_pos;
	size_t         start;
	struct item_s * replaced_by;
	struct item_s * source;
	int refcount;
	/*const char * tracking;*/
} lex_item_t;

const lex_item_t lex_item_empty = {0};

lex_item_t * lex_item_new(char * value, enum lex_item_type type, size_t line, size_t line_pos, size_t start) {
	lex_item_t * i = malloc(sizeof(lex_item_t));

	i->value       = value;
	i->length      = strings_len(value);
	i->type        = type;
	i->line        = line;
	i->line_pos    = line_pos;
	i->start       = start;
	i->source      = i;
	i->replaced_by = NULL;
	i->refcount    = 0;
	/*i->tracking    = NULL;*/

	return i;
}

char * lex_item_to_string(lex_item_t * item) {
	char * buf = NULL;
	if (item == NULL) {
		return strings_dup("(null)");
	}

	const char * name = lex_item_type_names[item->type];

	switch(item->type) {
		case item_error:
			return strings_dup(item->value == NULL ? "(null)" : item->value);

		case item_eof:
			return strings_dup("<eof>");

		case item_newline:
			return strings_dup(name);

		case item_pp_newline:
			asprintf(&buf, "%s '\\n'", name);
			return buf;

		default:
			if (item->length > 20 || strchr(item->value, '\n') != NULL) {
				asprintf(&buf, "%s '%.*s...(%zu)'", name, 20, item->value, item->length);
				return buf;
			}

			asprintf(&buf, "%s '%s'", name, item->value);
			return buf;
	}
}

bool lex_item_equals(lex_item_t * a, lex_item_t * b) {
	return a == b || (
		a != NULL  && b != NULL  &&
		a->type     == b->type     &&
		a->value    == b->value    &&
		a->length   == b->length   &&
		a->line     == b->line     &&
		a->line_pos == b->line_pos &&
		a->start    == b->start
	);
}

void lex_item_free(lex_item_t * item) {
	if (item == NULL) return;

	/*if (item->tracking) {*/
		/*char * s = to_string(item);*/
		/*fprintf(stderr, "[ITEM_FREE]: [\"%p\", \"%s\", \"%s\", %d, %lu, %lu]\n", item, item->tracking, s, item->refcount, item->length, sizeof(*item)); */
		/*global.free(s);*/
	/*}*/

	item->refcount--;
	if (item->refcount > 0) return;

	free(item->value);
	free(item);
}

lex_item_t * lex_item_replace_value(lex_item_t * a, char * value) {
	lex_item_t * replacement = lex_item_new(
		value,
		a->type,
		a->line,
		a->line_pos,
		a->start
	);

	replacement->source = a;
	a->replaced_by = replacement;

	return replacement;
}


// Test function for debugging memory leaks
void lex_item_unfreed() {
#ifdef MEM_DEBUG
	size_t count = 0;
	size_t i;
	for (i = 0; i <= item_index; i++) {
		lex_item_t item = cache[i];
		if (item.index) {
			count++;
			printf("\n[%ld]{\n"
				"    value    : '%s',\n"
				"    length   : '%ld',\n"
				"    type     : '%s',\n"
				"    line     : '%ld',\n"
				"    line_pos : '%ld',\n"
				"    start    : '%ld',\n"
				"}\n\n",
				item.index,
				item.value,
				item.length,
				lex_item_type_names[item.type],
				item.line,
				item.line_pos,
				item.start
			);
		}

		// intentionally lose the value so valgrind reports correctly
		cache[i].value = NULL; 
	}
	if (count > 0) {
		printf("lex_item audit: found %ld unfreed items\n", count);
	}
	item_index = 0;
#endif
}
