#dependencies for package 'cbuild.c'
CFLAGS += -std=c99
CFLAGS += -D_DEFAULT_SOURCE
CFLAGS += -D_GNU_SOURCE
cbuild.o: cbuild.c deps/stream/stream.h cli.h lexer/cache.h package/package.h package/export.h makefile.h errors/errors.h package/index.h utils/fs.h utils/utils.h lexer/item.h package/import.h ../deps/hash/khash.h deps/stream/file.h

#dependencies for package 'deps/stream/stream.c'
deps/stream/stream.o: deps/stream/stream.c

#dependencies for package 'cli.c'
cli.o: cli.c ../deps/hash/hash.h

#dependencies for package 'deps/hash/hash.c'
deps/hash/hash.o: deps/hash/hash.c

#dependencies for package 'lexer/cache.c'
lexer/cache.o: lexer/cache.c deps/stream/stream.h utils/strings.h lexer/item.h

#dependencies for package 'utils/strings.c'
utils/strings.o: utils/strings.c

#dependencies for package 'lexer/item.c'
lexer/item.o: lexer/item.c utils/strings.h

#dependencies for package 'package/package.c'
package/package.o: package/package.c deps/stream/stream.h ../deps/hash/hash.h lexer/cache.h lexer/item.h package/atomic-stream.h errors/errors.h deps/stream/file.h utils/fs.h

#dependencies for package 'package/atomic-stream.c'
package/atomic-stream.o: package/atomic-stream.c deps/stream/stream.h

#dependencies for package 'errors/errors.c'
errors/errors.o: errors/errors.c deps/stream/stream.h utils/utils.h ../utils/colors.h

#dependencies for package 'utils/utils.c'
utils/utils.o: utils/utils.c

#dependencies for package 'deps/stream/file.c'
deps/stream/file.o: deps/stream/file.c deps/stream/stream.h

#dependencies for package 'utils/fs.c'
utils/fs.o: utils/fs.c

#dependencies for package 'package/export.c'
package/export.o: package/export.c deps/stream/stream.h ../deps/hash/hash.h utils/utils.h lexer/item.h utils/strings.h package/package.h lexer/stack.h package/atomic-stream.h

#dependencies for package 'lexer/stack.c'
lexer/stack.o: lexer/stack.c utils/strings.h lexer/item.h

#dependencies for package 'makefile.c'
makefile.o: makefile.c deps/stream/stream.h ../deps/hash/hash.h utils/utils.h package/package.h package/export.h package/import.h ../utils/colors.h utils/fs.h package/atomic-stream.h

#dependencies for package 'package/import.c'
package/import.o: package/import.c parser/symbol.h ../deps/hash/hash.h errors/errors.h package/package.h package/export.h

#dependencies for package 'parser/symbol.c'
parser/symbol.o: parser/symbol.c lexer/item.h errors/errors.h

#dependencies for package 'package/index.c'
package/index.o: package/index.c deps/stream/stream.h ../deps/hash/hash.h lexer/cache.h parser/grammer.h package/import.h package/package.h lexer/stack.h errors/errors.h package/atomic-stream.h utils/fs.h utils/utils.h utils/strings.h lexer/item.h package/export.h deps/stream/file.h parser/parser.h

#dependencies for package 'parser/grammer.c'
parser/grammer.o: parser/grammer.c deps/stream/stream.h ../deps/hash/hash.h lexer/lex.h lexer/syntax.h parser/import.h package/package.h lexer/stack.h parser/identifier.h errors/errors.h parser/build.h parser/preprocessor.h lexer/item.h parser/package.h parser/export.h parser/parser.h

#dependencies for package 'lexer/lex.c'
lexer/lex.o: lexer/lex.c deps/stream/stream.h lexer/buffer.h lexer/cache.h lexer/item.h

#dependencies for package 'lexer/buffer.c'
lexer/buffer.o: lexer/buffer.c utils/strings.h lexer/item.h

#dependencies for package 'lexer/syntax.c'
lexer/syntax.o: lexer/syntax.c lexer/lex.h deps/stream/stream.h lexer/item.h

#dependencies for package 'parser/import.c'
parser/import.o: parser/import.c parser/string.h utils/utils.h lexer/item.h utils/strings.h package/package.h package/export.h lexer/stack.h package/import.h errors/errors.h parser/parser.h

#dependencies for package 'parser/string.c'
parser/string.o: parser/string.c

#dependencies for package 'parser/parser.c'
parser/parser.o: parser/parser.c utils/strings.h lexer/cache.h lexer/item.h utils/utils.h package/package.h lexer/stack.h errors/errors.h lexer/lex.h

#dependencies for package 'parser/identifier.c'
parser/identifier.o: parser/identifier.c ../deps/hash/hash.h lexer/item.h package/package.h package/export.h lexer/stack.h package/import.h errors/errors.h parser/symbol.h parser/parser.h

#dependencies for package 'parser/build.c'
parser/build.o: parser/build.c parser/string.h utils/strings.h lexer/item.h ../deps/hash/hash.h package/package.h lexer/stack.h package/import.h parser/parser.h

#dependencies for package 'parser/preprocessor.c'
parser/preprocessor.o: parser/preprocessor.c lexer/stack.h parser/parser.h lexer/item.h package/package.h parser/build.h

#dependencies for package 'parser/package.c'
parser/package.o: parser/package.c parser/string.h utils/strings.h lexer/item.h package/package.h parser/parser.h

#dependencies for package 'parser/export.c'
parser/export.o: parser/export.c ../deps/hash/hash.h lexer/cache.h package/package.h package/export.h lexer/stack.h parser/identifier.h errors/errors.h parser/preprocessor.h utils/strings.h parser/string.h lexer/item.h utils/utils.h package/import.h parser/expression.h parser/parser.h

#dependencies for package 'parser/expression.c'
parser/expression.o: parser/expression.c lexer/stack.h parser/identifier.h lexer/item.h parser/parser.h

cbuild: cbuild.o deps/stream/stream.o cli.o deps/hash/hash.o lexer/cache.o utils/strings.o lexer/item.o package/package.o package/atomic-stream.o errors/errors.o utils/utils.o deps/stream/file.o utils/fs.o package/export.o lexer/stack.o makefile.o package/import.o parser/symbol.o package/index.o parser/grammer.o lexer/lex.o lexer/buffer.o lexer/syntax.o parser/import.o parser/string.o parser/parser.o parser/identifier.o parser/build.o parser/preprocessor.o parser/package.o parser/export.o parser/expression.o
	$(CC) $(CFLAGS) $(LDFLAGS)  cbuild.o deps/stream/stream.o cli.o deps/hash/hash.o lexer/cache.o utils/strings.o lexer/item.o package/package.o package/atomic-stream.o errors/errors.o utils/utils.o deps/stream/file.o utils/fs.o package/export.o lexer/stack.o makefile.o package/import.o parser/symbol.o package/index.o parser/grammer.o lexer/lex.o lexer/buffer.o lexer/syntax.o parser/import.o parser/string.o parser/parser.o parser/identifier.o parser/build.o parser/preprocessor.o parser/package.o parser/export.o parser/expression.o -o cbuild $(LDLIBS)

cbuild__CLEAN:
	rm -rf cbuild cbuild.o deps/stream/stream.o cli.o deps/hash/hash.o lexer/cache.o utils/strings.o lexer/item.o package/package.o package/atomic-stream.o errors/errors.o utils/utils.o deps/stream/file.o utils/fs.o package/export.o lexer/stack.o makefile.o package/import.o parser/symbol.o package/index.o parser/grammer.o lexer/lex.o lexer/buffer.o lexer/syntax.o parser/import.o parser/string.o parser/parser.o parser/identifier.o parser/build.o parser/preprocessor.o parser/package.o parser/export.o parser/expression.o

#Recipe to rebuild cbuild.mk:
cbuild.mk: ../cbuild.module.c ../deps/stream/stream.module.c ../cli.module.c ../lexer/cache.module.c ../utils/strings.module.c ../lexer/item.module.c ../package/package.module.c ../package/atomic-stream.module.c ../errors/errors.module.c ../utils/utils.module.c ../deps/stream/file.module.c ../utils/fs.module.c ../package/export.module.c ../lexer/stack.module.c ../makefile.module.c ../package/import.module.c ../parser/symbol.module.c ../package/index.module.c ../parser/grammer.module.c ../lexer/lex.module.c ../lexer/buffer.module.c ../lexer/syntax.module.c ../parser/import.module.c ../parser/string.module.c ../parser/parser.module.c ../parser/identifier.module.c ../parser/build.module.c ../parser/preprocessor.module.c ../parser/package.module.c ../parser/export.module.c ../parser/expression.module.c
	./cbuild generate -o . ../cbuild.module.c
