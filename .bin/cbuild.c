#define _package_main_
#define VERSION "v2.0.0"



#include <stdio.h>
#include <stdbool.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include "deps/hash/khash.h"





#include "errors/errors.h"
#include "deps/stream/stream.h"
#include "deps/stream/file.h"
#include "package/index.h"
#include "lexer/item.h"
#include "lexer/cache.h"
#include "package/package.h"
#include "package/import.h"
#include "package/export.h"
#include "utils/utils.h"
#include "utils/fs.h"
#include "makefile.h"
#include "cli.h"

typedef struct {
  bool force;
  bool debug; 
  const char * output;
} options_t;

static package_t * generate(const char * filename, const char * exec_name, options_t * o, bool silent) {
  errors_t errors = {0};

  package_options_t opts = {
	.silent    = silent,
	.force     = o->force,
	.verbosity = o->debug ? DEBUG : SILENT,
	.cwd       = utils_getcwd(),
  };
  stream_t * err = file_new(2);

  if (o->output) {
	
	if (fs_mkdir_recursive(o->output) == -1) {
		//error(1, errno, "can't create output directory %s", o->output);
		fflush(stdout);
		fprintf(stderr, "%s: can't create output directory %s", strerror(errno), o->output);
	} else {
		opts.root_src = fs_dirname(realpath(filename, NULL));
		opts.root_gen = realpath(o->output, NULL);
	}
  }
  package_t * pkg = index_new(filename, &errors, opts);
  // find if there are any sources not in a child of root_src
  // update opts to point to the lowest common denominator.
  if (pkg) {
	  package_parse_file(pkg, filename, &errors);
	  index_resolve(pkg, &errors);
  }


  if (false && pkg && pkg->dest_cache) {
	lex_item_cache_t * c = (lex_item_cache_t *) pkg->source_cache;
	size_t l;
	for (l = 0; l < c->num_lines; l ++) {
		size_t i;
		lex_item_cache_line_t * line = &c->lines[l];
		lex_item_t * replaced = NULL;
		for (i = 0; i < line->num_items; i++) {
			lex_item_t * item = line->items[i];
			/*stream.write(err, item->value, item->length);*/
			if (replaced != NULL) {
				if (replaced == item->replaced_by) {
					stream_write(err, item->value, item->length);
				} else {
					stream_printf(err, "' => '%s'\n", replaced->value);
					replaced = NULL;
				}
			}
			if (replaced == NULL && item->replaced_by) {
				replaced = item->replaced_by;
				stream_printf(err, "%d: '%s", item->line, item->value);
			}
		}
	}
  }

  if (errors.count) {
	char * abs = malloc(strlen(opts.cwd) + strlen(filename) + 1);
	sprintf(abs, "%s%s", opts.cwd, filename);

	int i;
	for (i = 0; i < errors.count; i++) {
		errors_error_t e = errors.errors[i];
		if (!e.context.filename) {
			// handle loading the root module
			e.context.filename = abs;
		}
		errors_print(e, err, opts.cwd);

	}
	stream_printf(err, "got %d errors\n", i);
	errors_free_all(errors);
	index_free(pkg);
	free(abs);
	free(opts.cwd);
	free(opts.root_gen);
	free(opts.root_src);
	file_free(err);

    return NULL;
  }

  if (strcmp(pkg->name, "main") != 0) {
	  package_export_write_headers(pkg);
  }

  char * cbuild_exec_name = NULL;
  if (utils_is_absolute(exec_name) || !utils_is_relative(exec_name)) {
	  // absolute or path based
	  cbuild_exec_name = strdup(exec_name);
  } else {
	  // relative path
	  char * exec_abs = realpath(exec_name, NULL);
	  char * gen_dir = NULL;
	  if (opts.root_gen) {
		  gen_dir = malloc(strlen(opts.root_gen) + 2);
		  strcpy(gen_dir, opts.root_gen);
		  strcat(gen_dir, "/");
	  } else {
		  gen_dir = utils_getcwd();
	  }

	  cbuild_exec_name = utils_relative_exe(gen_dir, exec_abs);
	  free(exec_abs);
	  free(gen_dir);
  }

  free(makefile_write(pkg, cbuild_exec_name));

  free(cbuild_exec_name);
  free(opts.root_gen);
  free(opts.root_src);
  free(opts.cwd);
  file_free(err);
  return pkg;
}

static int do_generate(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;

  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }
  if (opts->force) printf("FORCED REBUILD\n");

  package_t * root = generate(cli->argv[0], cli->name, opts, false);
  if (root == NULL) exit(1);
  index_free(root);
  return 0;
}

static int do_build(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;
  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }

  package_t * root = generate(cli->argv[0], cli->name, opts, false);
  if (root == NULL) exit(1);

  int result = makefile_make(root, makefile_get_makefile_name(root->generated));

  index_free(root);
  if (result != 0) exit(result);
  return 0;
}

static int do_run(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;
  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }

  const char * target = cli->argc > 1 ? cli->argv[0] : NULL;
  const char * root_file = cli->argc > 1 ? cli->argv[1] : cli->argv[0];

  package_t * root = generate(root_file, cli->name, opts, false);
  if (root == NULL) exit(1);
  char * mkfile_name = makefile_get_makefile_name(root->generated);


  int result = 0;
  if (target) {
	  result = makefile_run(root, mkfile_name, target);
  } else {
	  result = makefile_make(root, mkfile_name);
	  if (result != 0) exit(result);
	  char * executable = makefile_get_build_target(root);
	  char * path;
	  asprintf(&path, "%s/%s", opts->output ?: ".", executable);

	  if (target) cli->argv++;
	  cli->argv[0] = path;

	  const char ** argv = cli->argv;
	  if (opts->debug) {
		  fprintf(stderr, "\nRunning the following command:\n");
		  while(*argv != NULL) {
			  fprintf(stderr, "%s ", *argv);
			  argv++;
		  }
		  fprintf(stderr, "\n\n");
	  }

	  result = execv(path, (char * const *)cli->argv);
	  fprintf(stderr, "Error executing %s: %s\n", path, strerror(errno));
	  exit(1);
  }
  exit(result);
  return 0;
}

static void clean_generated(package_t * pkg) {
  if (pkg == NULL || pkg->c_file || pkg->status != PKG_EXPORTED) return;
  pkg->status--;

  if (pkg->generated) {
    printf("unlink: %s\n", pkg->generated);
    unlink(pkg->generated);
  }
  if (pkg->header) {
    printf("unlink: %s\n", pkg->header);
    unlink(pkg->header);
  }

  hash_each_val(pkg->deps, {
    package_import_t * imp = (package_import_t *) val;
    clean_generated(imp->pkg);
  });
}

static int do_clean(cli_t * cli, char * cmd, void * arg) {
  options_t * opts = (options_t*) arg;

  if (cli->argc < 1) {
    fprintf(stderr, "no root module specified\n");
    return -1;
  }

  package_t * root = generate(cli->argv[0], cli->name, opts, true);
  if (root == NULL) exit(-1);

  int result = makefile_clean(root, makefile_get_makefile_name(root->generated));
  clean_generated(root);
  if (result != 0) exit(result);
  return 0;
}

int main(int argc, const char ** argv){
  options_t options = {0};

  cli_t * c = cli_new("<root module>");
  cli_flag_bool(c, &options.force, (cli_flag_options) {
      .long_name   = "force",
      .short_name  = "f",
      .description = "force rebuilding assets.",
  });

  cli_flag_bool(c, &options.debug, (cli_flag_options) {
      .long_name   = "debug",
      .short_name  = "d",
      .description = "display debug diagnostics.",
  });

  cli_flag_string(c, &options.output, (cli_flag_options) {
      .long_name   = "output",
      .short_name  = "o",
      .description = "output generated files relative to <output> instead of next to the source files.",
  });

  cli_command(c, "build",    do_build,    "generate code and build.",      true,  &options);
  cli_command(c, "generate", do_generate, "generate .c .h and .mk files.", false, &options);
  cli_command(c, "clean",    do_clean,    "clean generated files.",        false, &options);
  cli_command(c, "run",      do_run,      "build and run a target.",       false, &options);

  int result = cli_parse(c, argc, argv);
  cli_free(c);
  return result;
} 
