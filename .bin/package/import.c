#define _package_package_import_
#include <strings.h>


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <libgen.h>
#include "../deps/hash/hash.h"
#include <stdbool.h>

#include "../errors/errors.h"
#include "../parser/symbol.h"
#include "package.h"
#include "export.h"


typedef enum {
	IMPORT_QUEUED = 0,
	IMPORT_C_FILE,
	IMPORT_H_FILE,
	IMPORT_PARSED,
} package_import_type_t;

typedef struct {
	char          * alias;
	char          * filename;
	package_import_type_t   type;
	package_t     * pkg;
	hash_t	      * symbols;
} package_import_t;

package_t * package_import_free(package_import_t * imp) {
	if (imp == NULL) return NULL;
	
	package_t * pkg = imp->pkg;
	/*fprintf(stderr, "  -> %s (%lu)\n", imp->filename, pkg ? pkg->refcount : 0);*/
	if (imp->alias == imp->filename) {
		free(imp->alias);
	} else {
		free(imp->alias);
		free(imp->filename);
	}

	if (imp->symbols) {
		hash_each_val(imp->symbols, {
			symbol_free(val);
		});
		hash_free(imp->symbols);
	}

	free(imp);
	return pkg;
}

package_import_t * package_import_add(char * alias, char * filename, package_t * parent, errors_t * errors) {
	package_import_t * imp = malloc(sizeof(package_import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_PARSED;
	imp->pkg      = package_new(filename, errors, parent->opts);
	imp->symbols  = hash_new();
	imp->pkg      = package_parse_file(imp->pkg, filename, errors);

	if (imp->pkg == NULL) return NULL;

	package_export_write_headers(imp->pkg);
	return imp;
}

package_import_t * package_import_queue(char * alias, char * filename, package_t * parent, errors_t * errors) {
	package_import_t * imp = malloc(sizeof(package_import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_QUEUED;
	imp->pkg      = package_new(filename, errors, parent->opts);
	imp->symbols  = hash_new();

	if (imp->pkg == NULL) return NULL;

	if (imp->pkg->status >= PKG_PARSED) {
		imp->type = IMPORT_PARSED;
	}

	return imp;
}

int package_import_reconcile(package_import_t * imp, errors_t * errors) {
	if (imp == NULL || imp->type == IMPORT_C_FILE || imp->type == IMPORT_H_FILE) return 0;

	int i;

	hash_each(imp->symbols, {	
		char * source_abs = strdup(key);
		package_export_t * exp = hash_get(imp->pkg->exports, source_abs);
		if (exp) {
			symbol_resolve(val, exp->symbol);
		} else {
			if (imp->pkg->opts.verbosity >= DEBUG) {
				fprintf(stderr, "package %s doesn't export symbol %s\n", imp->pkg->name, key);
				fprintf(stderr, "deps for imp->pkg->name: [");
			}
			for (i = 0; i < imp->pkg->n_exports; i++) {
				package_export_t * e = imp->pkg->ordered[i];
				if (e->symbol[0] == 0) continue;
				//fprintf(stderr, "%s%s", e->symbol, (i == imp->pkg->n_exports - 1) ? "": ", ");
			}
			if (imp->pkg->opts.verbosity >= DEBUG) {
				fprintf(stderr, "]\n");
			}
			symbol_resolve_error(val, imp->pkg->name, imp->pkg->source_abs, errors);
		}
		free(source_abs);
	});
	imp->type = IMPORT_PARSED;

	return 0;
}

package_import_t * package_import_add_c_file(package_t * parent, char * filename, char ** error) {
	char * alias = realpath(filename, NULL);
	if (alias == NULL) {
		if (error) *error = strerror(errno);
		return NULL;
	}

	package_import_t * imp = calloc(1, sizeof(package_import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_C_FILE;
	imp->pkg      = package_c_file(strdup(alias), parent->opts);

	return imp;
}


package_import_t * package_import_add_h_file(package_t * parent, char * filename) {
	char * alias = realpath(filename, NULL);
	if (alias == NULL) {
		return NULL;
	}

	package_import_t * imp = calloc(1, sizeof(package_import_t));

	hash_set(parent->deps, alias, imp);

	imp->alias    = alias;
	imp->filename = filename;
	imp->type     = IMPORT_H_FILE;
	imp->pkg      = package_c_header(strdup(alias), parent->opts);

	return imp;
}

package_import_t * package_import_passthrough(package_t * parent, char * filename, errors_t * errors) {
	package_import_t * imp = package_import_add(filename, filename, parent, errors);

	if (imp == NULL || imp->pkg == NULL) {
		errors_error_t e = {
			.severity = ERROR,
			.is_fatal = false,
		};

		asprintf(&e.message, "Could not import '%s'", filename);
		errors_append(errors, e);
		return NULL;
	}

	hash_each_val(imp->pkg->exports, {
		package_export_t * exp = (package_export_t *) val;
		hash_set(parent->exports, exp->export_name, exp);
	});

	package_export_export_headers(parent, imp->pkg);
	return imp;
}
