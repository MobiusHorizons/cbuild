#ifndef _package_package_import_
#define _package_package_import_

#include "package.h"
#include "../errors/errors.h"
#include <stdbool.h>

typedef enum {
	IMPORT_QUEUED = 0,
	IMPORT_C_FILE,
	IMPORT_H_FILE,
	IMPORT_PARSED,
} package_import_type_t;

typedef struct {
	char          * alias;
	char          * filename;
	package_import_type_t   type;
	package_t     * pkg;
	hash_t	      * symbols;
} package_import_t;

package_t * package_import_free(package_import_t * imp);
package_import_t * package_import_add(char * alias, char * filename, package_t * parent, errors_t * errors);
package_import_t * package_import_queue(char * alias, char * filename, package_t * parent, errors_t * errors);
int package_import_reconcile(package_import_t * imp, errors_t * errors);
package_import_t * package_import_add_c_file(package_t * parent, char * filename, char ** error);
package_import_t * package_import_add_h_file(package_t * parent, char * filename);
package_import_t * package_import_passthrough(package_t * parent, char * filename, errors_t * errors);

#endif
