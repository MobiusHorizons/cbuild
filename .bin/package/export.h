#ifndef _package_package_export_
#define _package_package_export_

#include "../lexer/stack.h"
#include "package.h"

enum package_export_type {
	type_block = 0,
	type_header,
	type_type,
	type_variable,
	type_function,
	type_enum,
	type_union,
	type_struct,
};

const char * package_export_get_type_name(enum package_export_type t);

typedef struct {
	char             * local_name;
	char             * export_name;
	char             * symbol;
	lex_item_stack_t          * declaration;
	enum package_export_type   type;
} package_export_t;

void package_export_free(package_export_t * exp);

char * package_export_add(
		char * local,
		char * alias,
		char * symbol,
		enum package_export_type type,
		lex_item_stack_t * declaration,
		package_t * parent
);

char * package_export_get_header_path(char * generated);
void package_export_write_headers(package_t * pkg);
void package_export_export_headers(package_t * pkg, package_t * dep);

#endif
