#define _package_index_
#include "../deps/hash/hash.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <libgen.h>

#include "../lexer/item.h"
#include "../lexer/cache.h"
#include "../lexer/stack.h"
#include "../deps/stream/stream.h"
#include "../deps/stream/file.h"
#include "../parser/grammer.h"
#include "../parser/parser.h"
#include "../utils/utils.h"
#include "../utils/strings.h"
#include "../utils/fs.h"
#include "../errors/errors.h"
#include "package.h"
#include "import.h"
#include "export.h"
#include "atomic-stream.h"

static void init_cache() {
	package_path_cache = hash_new();
	package_id_cache   = hash_new();
}

static char * get_header_path(char * generated) {

	char * header = strdup(generated);
	size_t len = strlen(header);
	header[len - 1] = 'h';
	return header;
}

static char * package_name(const char * rel_path) {
	char * buffer   = strdup(rel_path);
	char * filename = basename(buffer);

	filename[strlen(filename) - 2] = 0; // trim the .c extension

	char * c;
	for (c = filename; *c != 0; c++){
		switch(*c) {
			case '.':
			case '-':
			case ' ':
				*c = '_';
				break;
			default:
				break;
		}
	}

	char * name = strdup(filename);
	free(buffer);
	return name;
}

static bool assert_name(const char * relative_path, errors_t * errors) {
	ssize_t len      = strlen(relative_path);
	size_t  suffix_l = strlen(".module.c");

	if (len < suffix_l || strcmp(".module.c", relative_path + len - suffix_l) != 0) {
		errors_error_t e = {
			.severity = WARNING,
			.is_fatal = false,
		};
		asprintf(&e.message, "Unsupported input filename. Expecting '<file>.module.c'");
		errors_append(errors, e);
		return true;
	}
	return false;
}

char * index_generated_name(const char * path, package_options_t opts) {
	char * root_src = opts.root_src ? opts.root_src : "/";
	char * root_gen = opts.root_gen ? opts.root_gen : "/";
	size_t src_l = strlen(root_src);
	size_t gen_l = strlen(root_gen);
	size_t  suffix_l = strlen(".module.c");

	if (strncmp(root_src, path, src_l) == 0) {
		size_t len = strlen(path);
		size_t buffer_l = len - src_l + gen_l - suffix_l + strlen(".c") + 1;
		char * buffer = malloc(buffer_l);
		snprintf(buffer, buffer_l, "%s%.*s.c",
				root_gen,
				(int) (len - src_l - suffix_l),
				path + src_l
		);
		return buffer;
	} else {
		ssize_t len      = strlen(path);
		size_t  suffix_l = strlen(".module.c");
		size_t buffer_l  = len - suffix_l + strlen(".c") + 1;
		char * buffer    = malloc(buffer_l);
		snprintf(buffer, buffer_l, "%.*s.c", (int) (len - suffix_l), path);
		return buffer;
	}
}

int index_reconcile_deps(package_t * p, errors_t * errors);
package_t * index_new(const char * relative_path, errors_t * errors, package_options_t opts);
package_t * index_parse(package_t * p, stream_t * input, const char * relative_path, errors_t * errors);
package_t * index_parse_file(package_t * p, const char * relative_path, errors_t * errors);

static void init() {
	if (package_path_cache == NULL) init_cache();
	if (package_new        == NULL) package_new        = index_new;
	if (package_parse      == NULL) package_parse      = index_parse;
	if (package_parse_file == NULL) package_parse_file = index_parse_file;
}

/** TODO: rename to new */
package_t * index__new(
	char * abs,
	stream_t * out,
	package_options_t opts
) {
	init();

	char * generated = index_generated_name(abs, opts);

	package_t * p = calloc(1, sizeof(package_t));
	p->deps             = hash_new();
	p->exports          = hash_new();
	p->symbols          = hash_new();
	p->exported_headers = hash_new();
	p->ordered          = NULL;
	p->n_exports        = 0;
	p->source_abs       = abs;
	p->generated        = generated;
	p->header           = get_header_path(generated);
	p->status           = PKG_QUEUED;
	p->name             = package_name(p->generated);
	p->out              = out;
	p->opts             = opts;

	hash_set(package_path_cache, abs, p);
	return p;
}

/** TODO: rename to get */
package_t * index_new(
	const char * rel,
	errors_t * errors,
	package_options_t opts
) {
	init();
	assert_name(rel, errors);
	char * key = realpath(rel, NULL);
	if (key == NULL) {
		errors_error_t e = {
			.severity = ERROR,
			.is_fatal = true,
			.message  = strdup(strerror(errno)),
		};

		errors_append(errors, e);
		return NULL;
	}

	package_t * cached = hash_get(package_path_cache, key);
	if (cached != NULL) {
		free(key);
		cached->refcount++;
		return cached;
	}

	return index__new(key, NULL, opts);
}

// Parse a package.
package_t * index_parse(package_t * p, stream_t * input, const char * rel, errors_t * errors) {
	if (p->status >= PKG_PARSED) return p;

#ifndef SYMBOL_DEBUG
	if (p->opts.verbosity >= DEBUG) {
#endif
		fprintf(stderr, "\nParsing '%s', [%s]\n", p->name, p->source_abs);

#ifndef SYMBOL_DEBUG
	}
#endif
	p->status = PKG_PARSING;
	p->errors = grammer_parse(input, rel, p, errors);
	p->status = PKG_PARSED;

	return p;
}

package_t * index_resolve(package_t *p, errors_t * errors) {
	if (p->status >= PKG_RESOLVED) return p;

	if (p->opts.verbosity >= DEBUG) {
		fprintf(stderr, "resolving %s [%s]\n", p->name, p->source_abs);
	}
	p->errors = index_reconcile_deps(p, errors);
	p->status = PKG_RESOLVED;

	if (p->opts.force || (!p->opts.silent && utils_newer(p->source_abs, p->generated))) {
		if (p->out == NULL) {
			char * pdir = fs_dirname(strdup(p->generated));
			fs_mkdir_recursive(pdir);
			free(pdir);
			p->out = atomic_stream_open(p->generated);
			if (p->out->error.code != 0) {
				/*global.free(key);*/

				errors_error_t e = {
					.severity = ERROR,
					.is_fatal = true,
					.message  = strdup(p->out->error.message),
				};
				errors_append(errors, e);

				atomic_stream_abort(p->out);
				return NULL;
			}
		}

		if (p->opts.verbosity >= DEBUG) {
			fprintf(stderr, "Writing %s [%s]\n", p->name, p->generated);
		}

		// replace first blank line with package info.
		if (p->dest_cache && p->dest_cache->num_lines > 1) {
			lex_item_cache_line_t * line = &p->dest_cache->lines[0];

			int i = 0;
			while ( /* find all the whitespace or newline characters */
					i < line->num_items && (
						line->items[i]->type == item_newline ||
						line->items[i]->type == item_whitespace
						)
				  ) i++;

			bool line_empty = (i == line->num_items);

			char * tok = malloc(strlen("_package__") + strlen(p->name) + 1);
			strcpy(tok, "_package_");
			strcat(tok, p->name);
			strcat(tok, "_");

			lex_item_t ** items = line->items;
			line->items = malloc((line->num_items + 5) * sizeof(lex_item_t *));

			int offset = 0;
			line->items[offset++] = lex_item_new(strings_dup("#"),      item_pp_symbol,  0, 0, 0);
			line->items[offset++] = lex_item_new(strings_dup("define"), item_pp_token,   0, 0, 0);
			line->items[offset++] = lex_item_new(strings_dup(" "),      item_whitespace, 0, 0, 0);
			line->items[offset++] = lex_item_new(tok,               item_pp_token,   0, 0, 0);
			if (!line_empty || line->num_items == 0) {
				line->items[offset++] = lex_item_new(strdup("\n"), item_newline, 0, 0, 0);
			}

			for (i = 0; i < line->num_items; i++) {
				line->items[i + offset] = items[i];
			}
			line->num_items += offset;

			free(items);
		}

		lex_item_cache_write(p->dest_cache, p->out);
		stream_close(p->out);
	}

	hash_each_val(p->deps, {
			package_import_t * imp = (package_import_t*) val;
			if (imp->type == IMPORT_PARSED && !imp->pkg->c_file) {
			index_resolve(imp->pkg, errors);
			package_export_write_headers(imp->pkg);
			}
			});

	return p;
}

package_t * index_parse_file(package_t * p, const char * rel, errors_t * errors) {
	stream_t * input = file_open(rel, O_RDONLY);
	if (input->error.code != 0) {
		errors_error_t e = {
			.severity = ERROR,
			.is_fatal = true,
			.message  = strdup(input->error.message),
		};
		errors_append(errors, e);
		return NULL;
	}
	return index_parse(p, input, rel, errors);
}

int index_parse_queued(package_import_t * imp, errors_t * errors) {
	char * rel = utils_relative(imp->pkg->opts.cwd, imp->pkg->source_abs);
	package_t * parsed = index_parse_file(imp->pkg, rel, errors);
	free(rel);

	if (parsed == NULL) return 1;
	return errors->count;
}

/** reconcile_deps updates all of the symbols which were pending from all of this package's dependencies. */
int index_reconcile_deps(package_t * p, errors_t * errors) {
	hash_each_val(p->deps, {
			package_import_t * imp = (package_import_t*) val;
			if (imp->pkg) {
			if (imp->pkg->opts.verbosity >= DEBUG) {
			fprintf(stderr, "    - %s [%s]\n", imp->filename, package_status_names[imp->pkg->status]);
			}
			if (imp->pkg->status <= PKG_QUEUED) index_parse_queued(imp, errors);
			if (imp->pkg->status >= PKG_PARSED) package_import_reconcile(imp, errors);
			}
			});
	return p->errors;
}

void index_free(package_t * pkg) {
	if (pkg == NULL) return;
	if (pkg->refcount > 0) {
		pkg->refcount--;
		return;
	}
	if (package_path_cache && hash_has(package_path_cache, pkg->source_abs)) {
		hash_del(package_path_cache, pkg->source_abs);
	}
	if (package_path_cache && hash_has(package_id_cache, pkg->name)) {
		hash_del(package_id_cache, pkg->name);
	}

	// exports
	int i;
	for (i = 0; i < pkg->n_exports; i++) {
		package_export_free((package_export_t *) pkg->ordered[i]);
	}
	free(pkg->ordered);

	/*fprintf(stderr, "Freeing %s\n", pkg->source_abs);*/
	hash_free(pkg->exports);
	hash_free(pkg->symbols);

	if (pkg->name != pkg->source_abs) free(pkg->source_abs);
	if (pkg->name != pkg->generated)  free(pkg->generated);
	if (pkg->name != pkg->header)     free(pkg->header);
	free(pkg->name);

	// imports
	if (pkg->deps) {
		hash_each_val(pkg->deps, {
			index_free(package_import_free((package_import_t *) val));
		});
		hash_free(pkg->deps);
	}

	for (i = 0; i < pkg->n_variables; i++) {
		package_var_t v = pkg->variables[i];
		free(v.name);
		free(v.value);
	}
	free(pkg->variables);

	for (i = 0; i < pkg->n_rules; i++) {
		package_rule_t r = pkg->rules[i];
		free(r.target);
		free(r.deps);
		free(r.recipe);
	}
	free(pkg->rules);

	if (pkg->exported_headers) {
		hash_each_val(pkg->exported_headers, {
			free(val);
		});
	}
	hash_free(pkg->exported_headers);

	lex_item_cache_free(pkg->source_cache);
	lex_item_cache_free(pkg->dest_cache);

	free(pkg);
}
