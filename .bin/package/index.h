#ifndef _package_index_
#define _package_index_

#include "../deps/stream/stream.h"
#include "import.h"
#include "package.h"
#include "../errors/errors.h"

char * index_generated_name(const char * path, package_options_t opts);
int index_reconcile_deps(package_t * p, errors_t * errors);
package_t * index_new(const char * relative_path, errors_t * errors, package_options_t opts);
package_t * index_parse(package_t * p, stream_t * input, const char * relative_path, errors_t * errors);
package_t * index_parse_file(package_t * p, const char * relative_path, errors_t * errors);

/** TODO: rename to new */
package_t * index__new(
	char * abs,
	stream_t * out,
	package_options_t opts
);

package_t * index_resolve(package_t *p, errors_t * errors);
int index_parse_queued(package_import_t * imp, errors_t * errors);
void index_free(package_t * pkg);

#endif
