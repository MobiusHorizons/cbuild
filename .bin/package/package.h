#ifndef _package_package_
#define _package_package_

#include "../deps/stream/stream.h"
#include "../lexer/item.h"
#include "../lexer/cache.h"
#include "../errors/errors.h"
#include "../deps/hash/hash.h"
#include <stdlib.h>
#include <stdbool.h>

enum package_var_type {
	build_var_set = 0,
	build_var_set_default,
	build_var_append,
};

typedef struct {
	char * name;
	char * value;
	enum package_var_type operation;
} package_var_t;

typedef struct {
	char * target;
	char * deps;
	char * recipe;
	unsigned char is_generic;
} package_rule_t;

enum package_status_e {
	PKG_EMPTY = 0, // uninitialized
	PKG_QUEUED,    // package is collecting references, but is not yet been looked at.
	PKG_PARSING,   // package is in the process of being parsed.
	PKG_PARSED,    // package has been parsed, but not all dependencies have been resolved.
	PKG_RESOLVED,  // package has been parsed and dependencies have been resolved.
	PKG_EXPORTED,  // package has been output to disk.
};

extern const char * package_status_names[];

enum package_verbosity_t {
	SILENT = 0,
	DEBUG,
};

typedef struct {
	bool       force;
	bool       silent;
	enum       package_verbosity_t verbosity;
	char *     root_src;
	char *     root_gen;
	char *     cwd;
} package_options_t;

typedef struct {
	hash_t   * deps;
	hash_t   * exports;
	hash_t   * symbols;
	hash_t   * exported_headers;
	void    ** ordered;
	size_t     n_exports;
	package_var_t    * variables;
	size_t     n_variables;
	package_rule_t   * rules;
	size_t     n_rules;
	size_t     refcount;
	char     * name;
	char     * source_abs;
	char     * generated;
	char     * header;
	char     * target;
	size_t     errors;
	enum package_status_e status;
	bool       c_file;
	package_options_t opts;
	lex_item_cache_t * source_cache;
	lex_item_cache_t * dest_cache;
	stream_t * out;
} package_t;

extern hash_t * package_path_cache;
extern hash_t * package_id_cache;

/* TODO: fix the circrular dependency issue */
extern package_t * (*package_new)(const char * relative_path, errors_t * errors, package_options_t opts);

extern package_t * (*package_parse)(package_t * p, stream_t * input, const char * relative_path, errors_t * errors);
extern package_t * (*package_parse_file)(package_t * p, const char * relative_path, errors_t * errors);

void package_emit(package_t * pkg, lex_item_t * token);
package_t * package_c_file(char * abs_path, package_options_t opts);
package_t * package_c_header(char * abs_path, package_options_t opts);

#endif
