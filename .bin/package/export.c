#define _package_package_export_

#include <stdlib.h>
#include <stdio.h>
#include "../deps/hash/hash.h"
#include <stdbool.h>

#include "package.h"
#include "../lexer/item.h"
#include "../lexer/stack.h"
#include "../deps/stream/stream.h"
#include "atomic-stream.h"
#include "../utils/utils.h"
#include "../utils/strings.h"


enum package_export_type {
	type_block = 0,
	type_header,
	type_type,
	type_variable,
	type_function,
	type_enum,
	type_union,
	type_struct,
};

static const char * type_names[] = {
	"block",
	"header",
	"type",
	"variable",
	"function",
	"enum",
	"union",
	"struct",
};

const char * package_export_get_type_name(enum package_export_type t) {
	return type_names[t];
}

typedef struct {
	char             * local_name;
	char             * export_name;
	char             * symbol;
	lex_item_stack_t          * declaration;
	enum package_export_type   type;
} package_export_t;

void package_export_free(package_export_t * exp) {
	if (exp == NULL) return;

	if (exp->local_name != exp->export_name) {
		free(exp->export_name);
	} 
	free(exp->local_name);
	free(exp->symbol);
	lex_item_stack_free_items(exp->declaration);
	lex_item_stack_free(exp->declaration);
	free(exp);
}

char * package_export_add(
		char * local,
		char * alias,
		char * symbol,
		enum package_export_type type,
		lex_item_stack_t * declaration,
		package_t * parent
) {

	char * export_name = alias == NULL ? local : alias; 
	export_name = strdup(export_name);

	// Tagged types
	if (type == type_struct || type == type_enum || type == type_union) {
		// set the high bit of the character to represent a tagged type.
		export_name[0] |= 0x80;
		local[0] |= 0x80;
	}

	if (type > type_header && hash_has(parent->exports, export_name)) {
		free(local);
		free(alias);
		free(symbol);
		lex_item_stack_free_items(declaration);
		lex_item_stack_free(declaration);
		return export_name;
	}

	package_export_t * exp = malloc(sizeof(package_export_t));

	exp->local_name  = local;
	exp->export_name = export_name;
	exp->symbol      = symbol;
	exp->declaration = declaration;
	exp->type        = type;

	/*fprintf(stderr, "typeof(%s) = %s (%d)\n", symbol, type, exp->type);*/

	parent->ordered = realloc(
		parent->ordered,
		sizeof(void*) * (parent->n_exports + 1)
	);
	parent->ordered[parent->n_exports] = exp;
	parent->n_exports ++;

	hash_set(parent->exports, exp->export_name, exp);
	hash_set(parent->symbols, exp->local_name,  exp);

	/**
	fprintf(stderr, "symbol %s_%s\n", parent->name, export_name);
	fprintf(stderr, "{\n"
			"  local_name : '%s',\n"
			"  export_name: '%s',\n"
			"  symbol     : '%s',\n"
			"  type       : '%s',\n"
			"}\n", local, export_name, symbol, type);
			*/

	return exp->export_name;
}

char * package_export_get_header_path(char * generated) {
	char * header = strings_dup(generated);
	size_t len = strings_len(header);
	header[len - 1] = 'h';
	return header;
}

#define B(a) (a ? "true" : "false")

void package_export_write_headers(package_t * pkg) {
	if (pkg->status <  PKG_RESOLVED) return;
	if (pkg->status >= PKG_EXPORTED) return;

	if (pkg->opts.force == false && (pkg->opts.silent || !utils_newer(pkg->source_abs, pkg->header))) return;

	stream_t * header = atomic_stream_open(pkg->header);
	stream_printf(header, "#ifndef _package_%s_\n" "#define _package_%s_\n\n", pkg->name, pkg->name);

	bool had_newline   = true;
	enum package_export_type last_type = type_header;

	{ // add the headers
		hash_each_val(pkg->exported_headers, {
				had_newline = false;
				stream_write(header, val, strlen(val));
		});
	}

	int i;
	for (i = 0; i < pkg->n_exports; i++){
			package_export_t * exp = (package_export_t *) pkg->ordered[i];

			lex_item_t * first_token = lex_item_stack_first(exp->declaration);
			lex_item_t * last_token  = lex_item_stack_last(exp->declaration);
			bool multiline = first_token && last_token && first_token->line != last_token->line;

			if (had_newline == false && (last_type != exp->type || multiline)) {
				stream_write(header, "\n", 1);
			}

			size_t t;
			for (t = 0; t < exp->declaration->length; t++) {
				lex_item_t * tok = exp->declaration->items[t];
				stream_write(header, tok->value, tok->length);
			}
			stream_write(header, "\n", 1);

			if (multiline) {
				had_newline = true;
				stream_write(header, "\n", 1);
			} else {
				had_newline = false;
			}
			last_type     = exp->type;
	}
	stream_printf(header, "%s#endif\n", had_newline ? "" : "\n");
	stream_close(header);
}

void package_export_export_headers(package_t * pkg, package_t * dep) {
	if (pkg == NULL || dep == NULL) return;
	if (hash_has(pkg->exported_headers, dep->header)) return;

	char * rel  = utils_relative(pkg->generated, dep->header);

	char * include = NULL;
	asprintf(&include, "#include \"%s\"\n", rel);
	hash_set(pkg->exported_headers, dep->header, include);

	free(rel);
}
