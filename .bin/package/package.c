#define _package_package_
#include "../deps/hash/hash.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <libgen.h>

#include "../deps/stream/stream.h"
#include "../deps/stream/file.h"
#include "../errors/errors.h"
#include "../lexer/item.h"
#include "../utils/fs.h"
#include "atomic-stream.h"
#include "../lexer/cache.h"

enum package_var_type {
	build_var_set = 0,
	build_var_set_default,
	build_var_append,
};

typedef struct {
	char * name;
	char * value;
	enum package_var_type operation;
} package_var_t;

typedef struct {
	char * target;
	char * deps;
	char * recipe;
	unsigned char is_generic;
} package_rule_t;

enum package_status_e {
	PKG_EMPTY = 0, // uninitialized
	PKG_QUEUED,    // package is collecting references, but is not yet been looked at.
	PKG_PARSING,   // package is in the process of being parsed.
	PKG_PARSED,    // package has been parsed, but not all dependencies have been resolved.
	PKG_RESOLVED,  // package has been parsed and dependencies have been resolved.
	PKG_EXPORTED,  // package has been output to disk.
};

const char * package_status_names[] = {
	"EMPTY",
	"QUEUED",
	"PARSING",
	"PARSED",
	"RESOLVED",
	"EXPORTED",
};

enum package_verbosity_t {
	SILENT = 0,
	DEBUG,
};

typedef struct {
	bool       force;
	bool       silent;
	enum       package_verbosity_t verbosity;
	char *     root_src;
	char *     root_gen;
	char *     cwd;
} package_options_t;

typedef struct {
	hash_t   * deps;
	hash_t   * exports;
	hash_t   * symbols;
	hash_t   * exported_headers;
	void    ** ordered;
	size_t     n_exports;
	package_var_t    * variables;
	size_t     n_variables;
	package_rule_t   * rules;
	size_t     n_rules;
	size_t     refcount;
	char     * name;
	char     * source_abs;
	char     * generated;
	char     * header;
	char     * target;
	size_t     errors;
	enum package_status_e status;
	bool       c_file;
	package_options_t opts;
	lex_item_cache_t * source_cache;
	lex_item_cache_t * dest_cache;
	stream_t * out;
} package_t;

hash_t * package_path_cache = NULL;
hash_t * package_id_cache = NULL;

/* TODO: fix the circrular dependency issue */
package_t * (*package_new)(const char * relative_path, errors_t * errors, package_options_t opts) = NULL;

package_t * (*package_parse)(package_t * p, stream_t * input, const char * relative_path, errors_t * errors) = NULL;

package_t * (*package_parse_file)(package_t * p, const char * relative_path, errors_t * errors) = NULL;

void package_emit(package_t * pkg, lex_item_t * token) {
	if (token) {
		if (pkg->dest_cache) {
			lex_item_cache_insert(pkg->dest_cache, token);
			/*if (token->source == token) {*/
				/*token->refcount++;*/
			/*}*/
		}
		/*if (pkg->out) {*/
			/*stream.write(pkg->out, token->value, token->length);*/
		/*}*/
	}
}

static char * get_generated_path(char * path, package_options_t opts) {
	char * root_src = opts.root_src ? opts.root_src : "/";
	char * root_gen = opts.root_gen ? opts.root_gen : "/";
	size_t src_l = strlen(root_src);
	size_t gen_l = strlen(root_gen);

	char * filename = basename(path);
	char * suffix = index(filename, '.');
	size_t  suffix_l = strlen(suffix);

	if (strncmp(root_src, path, src_l) == 0) {
		size_t len = strlen(path);
		size_t buffer_l = len - src_l + gen_l + 1;
		char * buffer = malloc(buffer_l);
		snprintf(buffer, buffer_l, "%s%.*s%s",
				 root_gen,
				 (int)(len - src_l - suffix_l), path + src_l,
				 suffix
		);

		return buffer;
	} else {
		ssize_t len      = strlen(path);
		char * buffer    = malloc(len + 1);
		snprintf(buffer, len, "%.*s%s",
				(int)(len - suffix_l), path,
				suffix
		);
		return buffer;
	}
}

static ssize_t copy_generated(char * from, char * to) {
	if (strcmp(from, to) == 0) return 0;

	stream_t * in = file_open(from, O_RDONLY);
	char * pdir = fs_dirname(strdup(to));
	fs_mkdir_recursive(pdir);
	free(pdir);
	stream_t * out = atomic_stream_open(to);
	ssize_t ret = stream_pipe(in, out);
	stream_close(in);
	stream_close(out);
	return ret;
}

package_t * package_c_file(char * abs_path, package_options_t opts) {
	package_t * cached = hash_get(package_path_cache, abs_path);
	if (cached != NULL) {
		cached->refcount++;
		free(abs_path);
		return cached;
	}

	char * generated = get_generated_path(abs_path, opts);
	if (opts.silent == false) {
		copy_generated(abs_path, generated);
	}

	package_t * pkg = calloc(1, sizeof(package_t));

	pkg->name       = abs_path;
	pkg->source_abs = abs_path;
	pkg->generated  = generated;
	pkg->c_file     = true;
	pkg->status     = PKG_PARSED;

	hash_set(package_path_cache, abs_path, pkg);
	return pkg;
}

package_t * package_c_header(char * abs_path, package_options_t opts) {
	package_t * cached = hash_get(package_path_cache, abs_path);
	if (cached != NULL) {
		cached->refcount++;
		free(abs_path);
		return cached;
	}

	char * generated = get_generated_path(abs_path, opts);
	if (opts.silent == false) {
		copy_generated(abs_path, generated);
	}

	package_t * pkg = calloc(1, sizeof(package_t));

	pkg->name       = abs_path;
	pkg->source_abs = abs_path;
	pkg->header     = abs_path;
	pkg->generated  = generated;
	pkg->c_file     = true;
	pkg->status     = PKG_EXPORTED;

	hash_set(package_path_cache, abs_path, pkg);
	return pkg;
}
