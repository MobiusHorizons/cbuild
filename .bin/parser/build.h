#ifndef _package_parser_build_
#define _package_parser_build_

#include "parser.h"
#include "../lexer/item.h"

/**********************************************************************************************************************
 * Build options manipulate the resulting makefile.
 *
 * Parses the following syntaxes:
 * - build depends      "<filename>";          # add "<filenme>" to the dependency list for the current file
 * - build set          <variable> "<value>";  # set the value of a makefile valiable         (:=)
 * - build set default  <variable> "<value>";  # set the default value of a makefile variable (?=)
 * - build append       <variable> "<value>";  # append to a makefile variable                (+=)
 **********************************************************************************************************************/
int parser_build_parse (parser_t * p, lex_item_t * build_token);

int parser_build_parse_pp_include (parser_t * p, lex_item_t * filename);

#endif
