#ifndef _package_string_
#define _package_string_

char * string_parse_into(const char * input, char * output);
char * string_parse(char * input);
char * string_parse_dup(const char * input);

#endif
