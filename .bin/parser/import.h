#ifndef _package_import_
#define _package_import_

#include "parser.h"
#include "../lexer/item.h"

int import_parse(parser_t * p, lex_item_t * import_token);

#endif
