#ifndef _package_expression_
#define _package_expression_

#include "../lexer/stack.h"
#include "parser.h"
#include <stdbool.h>

/** 
 * consume the second half of a binary operator. This should be of the form 
 *   <operator> <rvalue>
 * where rvalue could be a second operator.
 */
int expression_binary_operator(parser_t * p, lex_item_stack_t * s, bool is_export);

/**
 * rvalue tries to represent anything on the right-hand side of an equals sign.
 *
 * In reality it will only cover things that could be an rvalue in the global scope.
 *
 * in general this means for every ( find a matching ), for every { find a matching },
 * and also consume all unary and binary operators.
 */
int expression_rvalue(parser_t * p, lex_item_stack_t * s, bool is_export);

int expression_primative_type(parser_t * p, lex_item_stack_t *s, bool is_export);
int expression_id(parser_t * p, lex_item_stack_t * s, bool is_export);
int expression_function_ptr(parser_t * p, lex_item_stack_t * s, bool is_export);
int expression_type(parser_t * p, lex_item_stack_t * s, bool is_export);
int expression_initializer(parser_t * p, lex_item_stack_t * s, bool is_export);

/**
 * Consumes a declaration. 
 *
 * This can either be of two forms:
 *   - <type> <name> = <rvalue>
 *   - <type> (*name)(args)
 */
int expression_declaration(parser_t * p, lex_item_stack_t * s, bool is_export);

#endif
