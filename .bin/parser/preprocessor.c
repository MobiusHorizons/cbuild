#define _package_preprocessor_

#include "../lexer/stack.h"
#include "../lexer/item.h"
#include "../package/package.h"

#include "build.h"
#include "parser.h"

int preprocessor_parse_inline(parser_t * p, lex_item_stack_t * s) {
	lex_item_t * it = parser_token(p, s);
	lex_item_stack_push(s, it);

	if (it->type == item_pp_token && strcmp(it->value, "include") == 0) {
		lex_item_t * filename = parser_token(p, s);
		lex_item_stack_push(s, filename);

		if (filename->type == item_quoted_string) {
			parser_build_parse_pp_include(p, filename);
		}
	}

	it = parser_skip(p, s, item_whitespace, item_pp_token, item_pp_newline, 0);
	parser_backup(p, it);

	return 0;
}

int preprocessor_parse(parser_t * p, lex_item_t * it) {
	lex_item_stack_t * s = lex_item_stack_new(5);
	lex_item_stack_push(s, it); // leading hash

	preprocessor_parse_inline(p, s);

	size_t i;
	for (i = 0; i < s->length; i++) {
		package_emit(p->pkg, s->items[i]);
	}
	lex_item_stack_free(s);
	return 0;
}
