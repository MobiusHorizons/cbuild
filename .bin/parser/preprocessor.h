#ifndef _package_preprocessor_
#define _package_preprocessor_

#include "../lexer/stack.h"
#include "parser.h"
#include "../lexer/item.h"

int preprocessor_parse_inline(parser_t * p, lex_item_stack_t * s);
int preprocessor_parse(parser_t * p, lex_item_t * it);

#endif
