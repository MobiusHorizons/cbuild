#ifndef _package_parser_
#define _package_parser_

#include "../lexer/item.h"
#include "../lexer/lex.h"
#include "../lexer/stack.h"
#include "../package/package.h"
#include "../errors/errors.h"
#include <stdbool.h>
#include <stdarg.h>

struct parser_parser_s;

typedef void * (*parser_parse_fn)(struct parser_parser_s * lex);

typedef struct parser_parser_s {
	lex_t     * lexer;
	parser_parse_fn    state;
	lex_item_stack_t   * items;
	package_t * pkg;
	char      * filename;
	errors_t  * errors;
} parser_t;

void parser_fatalf(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_errorf(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_warnf(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_infof(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_hintf(parser_t * p, lex_item_t * item, const char * fmt, ...);
int parser_parse(lex_t * lexer, parser_parse_fn start, package_t * pkg, errors_t * errors);
lex_item_t * parser_next(parser_t * p);
void parser_backup(parser_t *p, lex_item_t * item);
lex_item_t * parser_skip(parser_t * p, lex_item_stack_t * s, ...);
lex_item_t * parser_token(parser_t * p, lex_item_stack_t * s);

void parser_verrorf(
	parser_t * p,
	enum errors_severity_t severity,
	bool is_fatal,
	lex_item_t * start,
	lex_item_t * end,
	const char * fmt,
	va_list args
);

void parser_severityf(parser_t * p, enum errors_severity_t s, lex_item_t * i, const char * fmt, ...);

#endif
