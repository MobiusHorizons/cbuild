#define _package_parser_identifier_


#include "../deps/hash/hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "../errors/errors.h"
#include "../lexer/item.h"
#include "../lexer/stack.h"
#include "parser.h"
#include "../package/package.h"
#include "../package/export.h"
#include "../package/import.h"
#include "symbol.h"

static hash_t * options = NULL;
static void init_options(){
	options = hash_new();

	hash_set(options, "enum",   (void *) 1);
	hash_set(options, "union",  (void *) 2);
	hash_set(options, "struct", (void *) 3);
}

static lex_item_t * token(parser_t * p, lex_item_stack_t * s) {
	lex_item_t * item = parser_next(p);
	while(item->type == item_whitespace || item->type == item_newline){
		lex_item_stack_push(s, item);
		item = parser_next(p);
	}
	return item;
}

static lex_item_t * cleanup(parser_t * p, lex_item_stack_t * s) {
	while(s->length > 1) {
		parser_backup(p, lex_item_stack_pop(s));
	}
	lex_item_t * out = lex_item_stack_pop(s);
	lex_item_stack_free(s);
	return out;
}

static void rewind_until(parser_t * p, lex_item_stack_t * s, lex_item_t * item) {
	lex_item_t * i;
	while(s->length > 0 && (i = lex_item_stack_pop(s)) && !lex_item_equals(i, item)) {
		parser_backup(p, i);
	}
}

static lex_item_t * emit(parser_t * p, lex_item_stack_t * s) {
	int i;
	for (i = 0; i < s->length -1; i++) {
		package_emit(p->pkg, s->items[i]);
	}

	lex_item_t * out = lex_item_stack_pop(s);

	lex_item_stack_free(s);
	return out;
}


static lex_item_t * parse_type(parser_t * p, lex_item_stack_t * s, lex_item_t * item, lex_item_t ** type) {
	if (options == NULL) init_options();
	if (!hash_has(options, item->value)) return item;

	*type = item;
	lex_item_stack_push(s, item);
	return token(p, s);
}

/**
 * parse_import() parse an imported identifier (eg: <namespace>.<symbol>)
 * Returns NULL if the syntax is not an import syntax.
*/
static lex_item_t * parse_import( parser_t *p, lex_item_stack_t * s,
		lex_item_t * item, lex_item_t ** pkg, lex_item_t ** name ) {
	lex_item_t * temp;

	lex_item_stack_push(s, item);
	if (item->type != item_id) return NULL;
	temp = item;

	item = parser_next(p);
	lex_item_stack_push(s, item);

	if (item->type != item_symbol || item->value[0] != '.') return temp;

	item = parser_next(p);
	lex_item_stack_push(s, item);
	if (item->type != item_id) return NULL;

	*pkg  = temp;
	*name = item;
	return item;
}

static lex_item_t * parse_symbol(parser_t *p, lex_item_stack_t * s, lex_item_t * type, lex_item_t * item) {
	char * name = strdup(item->value);
	if (type) name[0] |= 0x80;

	package_export_t * symbol = (package_export_t *) hash_get(p->pkg->symbols, name);
	free(name);

	if (symbol == NULL) return cleanup(p, s);

	rewind_until(p, s, item);
	char * symbol_name = NULL;
	if (symbol->symbol == NULL) {
		fprintf(stderr, "package %s, status: %d\n", p->pkg->name, p->pkg->status);
	}

	symbol_name = strdup(symbol->symbol);
	return lex_item_stack_replace_with(s, item, symbol_name);
}

static char * resolve(parser_t * p, package_import_t * imp, symbol_ref_t ref, errors_t * errors) {
	char * name = strdup(ref.name->value);
	if (ref.type) name[0] |= 0x80;

	package_export_t * exp = (package_export_t *) hash_get(imp->pkg->exports, name);
	free(name);

	if (exp == NULL) {
		parser_errorf(p, ref.name, "Package '%s' does not export the symbol '%s'",
				ref.from->value, ref.name->value
		);
	}

	if (exp) return symbol_name(ref, exp->symbol, NULL);
	return symbol_fallback_name(ref, imp->pkg->name, NULL);
}

lex_item_t * parser_identifier_parse_typed(parser_t * p, lex_item_t * type, lex_item_t * item, bool is_export) {
	lex_item_t * from  = NULL;
	lex_item_t * name  = NULL;
	lex_item_stack_t * s        = lex_item_stack_new(8);

	item = parse_import(p, s, item, &from, &name);
	if (item == NULL) return cleanup(p, s);
	if (name == NULL) return parse_symbol(p, s, type, item);

	if (from && strcmp(from->value, "global") == 0) {
		lex_item_stack_free(s);
		return name;
	}

	package_import_t * imp = (package_import_t *) hash_get(p->pkg->deps, from->value);
	if (imp == NULL || imp->pkg == NULL) return emit(p, s);

	char * symbol_name = strdup(name->value);
	if (type) {
		symbol_name[0] |= 0x80; // tagged symbol;
	}

	package_export_t * exp = (package_export_t *) hash_get(imp->pkg->exports, symbol_name);
	free(symbol_name);
	if (exp == NULL) {
		parser_errorf(p, name, "Package '%s' does not export the symbol '%s'",
				from->value, name->value
		);
		return cleanup(p, s);
	}

	if (is_export) package_export_export_headers(p->pkg, imp->pkg);

	return lex_item_stack_replace_with(s, NULL, strdup(exp->symbol));
}

lex_item_t * parser_identifier_parse (parser_t * p, lex_item_t * item, bool is_export) {
	lex_item_t * type  = NULL;
	lex_item_t * from  = NULL;
	lex_item_t * name  = NULL;
	lex_item_stack_t * s        = lex_item_stack_new(8);

	item = parse_type(p, s, item, &type);
	item = parse_import(p, s, item, &from, &name);
	if (item == NULL) return cleanup(p, s);
	if (name == NULL) {
		lex_item_t * sym = parse_symbol(p, s, type, item);
		if (type && sym != type) {
			char * resolved = sym->value;
			sym->length = asprintf(&sym->value, "%s %s", type->value, resolved);
			free(resolved);

			type->replaced_by = sym;
		}

		return sym;
	}

	if (strcmp(from->value, "global") == 0) {
		lex_item_stack_free(s);
		return name;
	}

	symbol_ref_t ref = {
		.type      = type,
		.from      = from,
		.name      = name,
	};

	package_import_t * imp = (package_import_t *) hash_get(p->pkg->deps, from->value);
	if (imp == NULL) return emit(p, s);

	if (strcmp(name->value, "value") == 0) {
	/* there must be corruption in the package import system. investigate */
		fprintf(stderr, "in package '%s', import %s.%s\n", p->pkg->source_abs, from->value, name->value);
		fprintf(stderr, "  -> import [%s(%d): %s]\n", imp->alias, imp->type, imp->filename);
		fprintf(stderr, "    -> package %s\n", imp->pkg ? imp->pkg->source_abs : "null");
	}

	if (is_export) package_export_export_headers(p->pkg, imp->pkg);
	if (imp->type == IMPORT_QUEUED) {
		char * symbol_name = strdup(name->value);
		if (type) {
			symbol_name[0] |= 0x80;
		}
		symbol_t * sym = (symbol_t *) hash_get(imp->symbols, symbol_name);
		if (sym == NULL) {
			sym = symbol_new(symbol_name);
			hash_set(imp->symbols, symbol_name, sym);
		}
		symbol_insert(sym, ref);

		// we replace the symbols in the input with a placeholder,
		// who's value will get updated when this symbol gets resolved.
		lex_item_t * placeholder = lex_item_stack_replace_with(s, name, "$SYMBOL_REF");
		return placeholder;
	}

	if (imp->pkg == NULL) return emit(p, s);

	char * symbol_name = resolve(p, imp, ref, p->errors);
	lex_item_t * start = type ?: from;
	return lex_item_stack_replace_with(s, start, symbol_name);
}

