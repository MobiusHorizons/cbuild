#define _package_expression_
#include "parser.h"
#include "../lexer/item.h"
#include "../lexer/stack.h"
#include "identifier.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**
typedef int (*grammer_atom)(parser.t * p, stack.t * s);
static int log(parser.t * p, stack.t * s, grammer_atom atom, char * name) {
	static int indent;
	size_t i = stack.height(s); // first new item in the stack
	lex_item.t * start = stack.last(s);

	indent++;
	int e = atom(p, s);
	indent--;

	return e;
	if (indent != 0) return e;

	lex_item.t * end = stack.last(s);
	int multiline = end->line > start->line;

	size_t consumed = stack.height(s) - i;
	if (consumed) {
		fprintf(stderr, "%lu:%lu <%s> consumed %lu:%s", 
				start->line + 1, start->start - start->line_pos,
				name, stack.height(s) - i, multiline ? "\n" : "");
	} else {
		fprintf(stderr, "%lu:%lu <%s> X", 
				start->line + 1, start->start - start->line_pos, name);
	}
	while(i < stack.height(s)) {
		lex_item.t * tok = stack.peek(s, i);
		fprintf(stderr, "%s", tok ? tok->value : "NULL");
		i++;
	}
	fprintf(stderr, "\n%s", multiline ? "\n" : "");
	return e;
}
#define L(fn, p, s) log(p, s, fn, #fn)
*/

static char matching(char opening_symbol) {
	switch(opening_symbol) {
		case '{':
			return '}';
		case '(':
			return ')';
		default:
			return 0;
	}
}

static int backtrack(parser_t * p, lex_item_stack_t * s, size_t checkpoint) {
	while(lex_item_stack_height(s) > checkpoint) parser_backup(p, lex_item_stack_pop(s));
	return 0;
}

static int parse_block(parser_t * p, lex_item_stack_t * s, bool is_export, char end) {
	size_t checkpoint = lex_item_stack_height(s);

	lex_item_t * tok;
	do {
		tok = parser_token(p, s);
		if (tok->type == item_id) {
			tok = parser_identifier_parse(p, tok, is_export);
		}
		lex_item_stack_push(s, tok);
	} while(
		tok && 
		tok->type != item_eof   && 
		tok->type != item_error &&
		!(tok->type == item_close_symbol && tok->value[0] == end)
	);
	if (tok->type == item_close_symbol) return 1;
	return backtrack(p, s, checkpoint);
}

/** 
 * consume the second half of a binary operator. This should be of the form 
 *   <operator> <rvalue>
 * where rvalue could be a second operator.
 */
int expression_binary_operator(parser_t * p, lex_item_stack_t * s, bool is_export);

/**
 * rvalue tries to represent anything on the right-hand side of an equals sign.
 *
 * In reality it will only cover things that could be an rvalue in the global scope.
 *
 * in general this means for every ( find a matching ), for every { find a matching },
 * and also consume all unary and binary operators.
 */
int expression_rvalue(parser_t * p, lex_item_stack_t * s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);

	lex_item_t * tok = parser_token(p, s);
	lex_item_stack_push(s, tok);
	switch(tok->type) {
		case item_open_symbol:
			// find end of block;
			return parse_block(p, s, is_export, matching(tok->value[0]));
			break;

		case item_id:
			{
				// this can't be a type, so the export never needs to be true.
				tok = parser_identifier_parse(p, lex_item_stack_pop(s), /* is_export */ is_export); 
				lex_item_stack_push(s, tok);

				checkpoint = lex_item_stack_height(s);
				tok = parser_token(p, s);
				lex_item_stack_push(s, tok);
				// function invocation
				if (tok->type == item_open_symbol && tok->value[0] == '(') {
					parse_block(p, s, is_export, ')');
				} else if (tok->type == item_symbol) {
					// TODO call binary_operator here, but also check for unary postfix operators (eg ++ --)
					do {
						tok = parser_next(p);
						lex_item_stack_push(s, tok);
					} while(tok->type == item_symbol);
					parser_backup(p, lex_item_stack_pop(s));
					return expression_rvalue(p, s, is_export);
				} else {
					backtrack(p, s, checkpoint);
				}
				return 1;
			}

		case item_number:
			expression_binary_operator(p, s, is_export);
			return 1;
			
		case item_symbol:
			// unary prefix operators
			if (
					tok->value[0] == '!'  ||
					tok->value[0] == '+'  ||
					tok->value[0] == '-'  ||
					tok->value[0] == '&'  ||
					tok->value[0] == '*'  ||
					tok->value[0] == '~' 
			   ) {
				return expression_rvalue(p, s, is_export);
			}

			// Assume this is an operator.
			// TODO: determine if this should roll back or not.
			// TODO: maybe this is actually an error condition, since it can't start an rvalue.
			parser_backup(p, lex_item_stack_pop(s));
			return expression_binary_operator(p, s, is_export);

		default:
			return backtrack(p, s, checkpoint);
	}
	return 0;
}

// CHAR2 turns two chars into a single number for comparing against.
#define CHAR2(a, b) ((a << 8) | b)

int expression_binary_operator(parser_t * p, lex_item_stack_t * s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);
	lex_item_t * tok;

	tok = parser_token(p, s);
	lex_item_stack_push(s, tok);

	if (tok->type != item_symbol) return backtrack(p, s, checkpoint);

	// TODO figure out how tokens are currently lexed.
	switch(CHAR2(tok->value[0], tok->value[1])) {
		/* multiplicative */
		case CHAR2('*',0):
		case CHAR2('/',0):
		case CHAR2('%',0):

		/* addition / subtraction */
		case CHAR2('+',0):
		case CHAR2('-',0):

		/* shift operators */
		case CHAR2('<','<'):
		case CHAR2('>','>'):

		/* relational */
		case CHAR2('<',0):
		case CHAR2('<','='):
		case CHAR2('>',0):
		case CHAR2('>','='):

		/* equality */
		case CHAR2('!','='):
		case CHAR2('=','='):

		/* bitwise */
		case CHAR2('&',0):
		case CHAR2('|',0):
		case CHAR2('^',0):

		/* logical */
		case CHAR2('&','&'):
		case CHAR2('|','|'):
			return expression_rvalue(p, s, is_export);

		default:
			return backtrack(p, s, checkpoint);
	}
}

static int lifetime(parser_t * p, lex_item_stack_t *s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);
	lex_item_t * tok = parser_token(p, s);
	lex_item_stack_push(s, tok);

	if (tok->type != item_id) return backtrack(p, s, checkpoint);
	
	if (
		strcmp(tok->value, "static") == 0 ||
		strcmp(tok->value, "const") == 0
	) {
		return 1;
	}

	return backtrack(p, s, checkpoint);
}

int expression_primative_type(parser_t * p, lex_item_stack_t *s, bool is_export) {
	size_t start = lex_item_stack_height(s);
	int count = 0;

	do {
		size_t checkpoint = lex_item_stack_height(s);
		lex_item_t * tok = parser_token(p, s);
		lex_item_stack_push(s, tok);

		if (tok->type != item_id) return backtrack(p, s, checkpoint);
	
		if (
			strcmp(tok->value, "long") == 0 ||
			strcmp(tok->value, "short") == 0 ||
			strcmp(tok->value, "short") == 0 ||
			strcmp(tok->value, "unsigned") == 0
		) {
			count++;
			continue;
		}

		if (
			strcmp(tok->value, "int") == 0 ||
			strcmp(tok->value, "char") == 0 ||
			strcmp(tok->value, "float") == 0 ||
			strcmp(tok->value, "double") == 0
		) {
			return 1;
		}

		if (count) {
			backtrack(p, s, checkpoint);
			return 1;
		} else {
			return backtrack(p, s, start);
		}
	} while (true);

	return 0;
}

int expression_id(parser_t * p, lex_item_stack_t * s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);
	lex_item_t * tok = parser_token(p, s);
	lex_item_stack_push(s, tok);

	if (tok->type != item_id) return backtrack(p, s, checkpoint);

	tok = parser_identifier_parse(p, lex_item_stack_pop(s), is_export);
	lex_item_stack_push(s, tok);

	return 1;
}

int expression_function_ptr(parser_t * p, lex_item_stack_t * s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);
	lex_item_t * tok;

	{ // next token must be a '('
		tok = parser_token(p, s);
		lex_item_stack_push(s, tok);

		if (tok->type != item_open_symbol && tok->value[0] != '(') {
			return backtrack(p, s, checkpoint);
		}
	}

	{ // next token must be a '*'
		tok = parser_token(p, s);
		lex_item_stack_push(s, tok);

		if (tok->type != item_symbol && tok->value[0] != '*') {
			return backtrack(p, s, checkpoint);
		}
	}

	// optional
	expression_id(p, s, is_export);

	{ // next token must be a ')'
		tok = parser_token(p, s);
		lex_item_stack_push(s, tok);

		if (tok->type != item_symbol && tok->value[0] != ')') {
			return backtrack(p, s, checkpoint);
		}
	}

	{ // next token must be a '('
		tok = parser_token(p, s);
		lex_item_stack_push(s, tok);

		if (tok->type != item_open_symbol && tok->value[0] != '(') {
			return backtrack(p, s, checkpoint);
		}
	}

	return parse_block(p, s, is_export, ')');
}

int expression_type(parser_t * p, lex_item_stack_t * s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);
	lex_item_t * tok;
	lex_item_t * expression_type = NULL;

	// const / static
	while(lifetime(p, s, is_export));

	if (!expression_primative_type(p, s, is_export)) {
		tok = parser_token(p, s);
		lex_item_stack_push(s, tok);
		
		if (tok->type != item_id) return backtrack(p, s, checkpoint);

		// if the type is a struct name enum name, or union name, consume that token, and read the type name.
		if (
			strcmp(tok->value, "struct") == 0 ||
			strcmp(tok->value, "union") == 0  ||
			strcmp(tok->value, "enum") == 0
		) {
			expression_type = tok;
			tok = parser_token(p, s);
			lex_item_stack_push(s, tok);

			if (tok->type == item_id) {
				tok = parser_identifier_parse_typed(p, expression_type, lex_item_stack_pop(s), is_export);
				lex_item_stack_push(s, tok);
			} else if (tok->type == item_open_symbol && tok->value[0] == '{') {
				if (!parse_block(p, s, is_export, '}'))  {
					return backtrack(p, s, checkpoint);
				}
			}
		} else {
			tok = parser_identifier_parse(p, lex_item_stack_pop(s), is_export);
			lex_item_stack_push(s, tok);
		}
	}

	// we have a valid type, now check for some stuff that can come after
	checkpoint = lex_item_stack_height(s);

	tok = parser_token(p, s);
	lex_item_stack_push(s, tok);

	// consume any number of * if this is a pointer type.
	if (tok->type == item_symbol && tok->value[0] == '*') {
		do {
			checkpoint = lex_item_stack_height(s);
			tok = parser_token(p, s);
			lex_item_stack_push(s, tok);
		} while(tok->type == item_symbol && tok->value[0] == '*');
		backtrack(p, s, checkpoint);
		return 1;
	}

	backtrack(p, s, checkpoint);
	return 1;
}

int expression_initializer(parser_t * p, lex_item_stack_t * s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);
	lex_item_t * tok = NULL;

	tok = parser_token(p, s);
	lex_item_stack_push(s, tok);

	if (tok->type != item_symbol || tok->value[0] != '=') {
		while(lex_item_stack_height(s) > checkpoint) parser_backup(p, lex_item_stack_pop(s));
		return 0;
	}

	return expression_rvalue(p, s, is_export);
}

/**
 * Consumes a declaration. 
 *
 * This can either be of two forms:
 *   - <type> <name> = <rvalue>
 *   - <type> (*name)(args)
 */
int expression_declaration(parser_t * p, lex_item_stack_t * s, bool is_export) {
	size_t checkpoint = lex_item_stack_height(s);

	int e = expression_type(p, s, is_export);
	if (!e) return e;

	// Try for the regular syntax (<type <name> = <rvalue>)
	e = expression_id(p, s, is_export);
	if (e) {
		// for array syntax, there may be a [] size specified here.
		size_t checkpoint = lex_item_stack_height(s);
		lex_item_t * tok = parser_token(p, s);
		lex_item_stack_push(s, tok);

		if (tok->type == item_open_symbol && tok->value[0] == '[') {
			if (parse_block(p, s, is_export, ']')) return 1;
		}
		backtrack(p, s, checkpoint);

		// Initialization is optional, but if it is there, consume it.
		expression_initializer(p, s, is_export);
		return 1;
	}

	// Try the function pointer syntax;
	e = expression_function_ptr(p, s, is_export);
	if (!e) backtrack(p, s, checkpoint);

	return e;
}
