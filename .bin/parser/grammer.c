#define _package_grammer_
#include "../deps/hash/hash.h"

#include <stdio.h>
#include <stdarg.h>

#include "../deps/stream/stream.h"

#include "../errors/errors.h"
#include "../lexer/item.h"
#include "../lexer/lex.h"
#include "../lexer/stack.h"
#include "../lexer/syntax.h"
#include "../package/package.h"
#include "parser.h"

#include "package.h"
#include "import.h"
#include "export.h"
#include "build.h"
#include "identifier.h"
#include "preprocessor.h"

typedef int (*keyword_fn)(parser_t * p, lex_item_t * item);

static void * parse_id      (parser_t * p, lex_item_t * item);
static void * parse_keyword (parser_t * p, lex_item_t * item);

static void * parse_c(parser_t * p) {
	lex_item_t * item = NULL;
	lex_item_t * last = NULL;
	int escaped_id = 0;
	do {
		last = item;
		item = parser_next(p);

		switch(item->type) {
			case item_arrow:
				escaped_id = 1;
				package_emit(p->pkg, item);
				continue;

			case item_eof:
			case item_error:
				break;

			case item_pp_symbol:
				preprocessor_parse(p, item);
				return parse_c;

			case item_id:
				if (last == NULL || last->type == item_newline) {
					return parse_keyword(p, item);
				}
				if (escaped_id == 0 && !(last->type == item_symbol && last->value[0] == '.')) {
					return parse_id(p, item);
				}
			case item_c_code:
			default:
				package_emit(p->pkg, item);
		}

		escaped_id = 0;
	} while (item->type != item_eof && item->type != item_error);

	return NULL;
}

static hash_t * keywords = NULL;
static void init_keywords(){
	keywords = hash_new();

	hash_set(keywords, "package", parser_package_parse);
	hash_set(keywords, "import",  import_parse );
	hash_set(keywords, "export",  parser_export_parse );
	hash_set(keywords, "build",   parser_build_parse  );
}

static void * parse_keyword(parser_t * p, lex_item_t * item) {
	if (keywords == NULL) init_keywords();
	keyword_fn fn = (keyword_fn) hash_get(keywords, item->value);

	if (fn != NULL) {
		if (fn(p, item)) return parse_c;
		return NULL;
	}

	parser_export_module_level(p, item);
	return parse_c;
}


static void * parse_id(parser_t * p, lex_item_t * item) {
	item = parser_identifier_parse(p, item, false);
	if (item->type == item_error) return NULL;
	package_emit(p->pkg, item);
	return parse_c;
}

int grammer_parse(stream_t * in, const char * filename, package_t * p, errors_t * errors) {
	lex_t * lexer = lex_syntax_new(in, filename);
	if (lexer == NULL) return -1;

	return parser_parse(lexer, parse_c, p, errors);
}
