#ifndef _package_symbol_
#define _package_symbol_

#include "../errors/errors.h"
#include "../lexer/item.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/*
 * symbol represents a symbol that is expected to be exported from a given package.
 */
typedef struct {
	lex_item_t * type;
	lex_item_t * from;
	lex_item_t * name;
	bool         is_export;
} symbol_ref_t;

typedef struct {
	const char * name;
	const char * filename;
	size_t num_refs;
	symbol_ref_t * refs;
} symbol_t;

symbol_t * symbol_new(char * symbol_name);
void symbol_insert(symbol_t * s, symbol_ref_t r);
char * symbol_name(symbol_ref_t r, const char * name, size_t * o_length);

/**
 * If the symbol was not found, this generates an approximate symbol name (assuming no aliasing).
 * This allows parsing to continue and find other errors.
 */
char * symbol_fallback_name(symbol_ref_t r, const char * package_name, size_t * o_length);

void symbol_resolve(symbol_t * s, const char * name);

/** 
 * outputs a fallback best-guess of the symbol name, since it can't be found.
 * This is used so that parsing can continue, and other errors can be reported.
 */
void symbol_resolve_error(symbol_t * s, const char * package_name, const char * filename, errors_t * errors);

void symbol_free(symbol_t * s);

#endif
