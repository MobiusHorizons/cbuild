#define _package_parser_export_

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "../deps/hash/hash.h"

#include "parser.h"
#include "string.h"
#include "../utils/utils.h"
#include "../utils/strings.h"
#include "identifier.h"
#include "../lexer/item.h"
#include "../lexer/stack.h"
#include "../lexer/cache.h"
#include "../package/package.h"
#include "../package/export.h"
#include "../package/import.h"
#include "../errors/errors.h"
#include "preprocessor.h"
#include "expression.h"

enum visibility {
	MODULE = 0,
	EXPORT,
	GLOBAL
};

typedef struct {
	lex_item_stack_t            * items;
	enum package_export_type type;
	enum visibility      visibility;
	bool                 error;
} decl_t;

#define SYNTAX_ERROR "Invalid export syntax: "

static lex_item_t * errorf(parser_t * p, lex_item_t * item, decl_t * decl, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	parser_verrorf(p, ERROR, false, item, NULL, fmt, args);
	decl->error = true;
	return NULL;
}

static void append(decl_t * decl, lex_item_t * value) {
	if (decl->items == NULL) decl->items = lex_item_stack_new(8);
	lex_item_stack_push(decl->items, value);
}

static void rewind_until(parser_t * p, decl_t * decl, lex_item_t * value) {
	lex_item_t * item = NULL;
	while((item = lex_item_stack_last(decl->items)) != NULL && item != value) {
		parser_backup(p, lex_item_stack_pop(decl->items));
	}

	if (item == value) {
		parser_backup(p, lex_item_stack_pop(decl->items));
	}
}

static void rewind_whitespace(parser_t * p, decl_t * decl, lex_item_t * value) {
	parser_backup(p, value);

	lex_item_t * item;
	while ((item = lex_item_stack_last(decl->items)) != NULL && (
				item->type == item_whitespace ||
				item->type == item_newline
			)
		) {
			parser_backup(p, lex_item_stack_pop(decl->items));
	}
}

#define CHAR4(s) *((uint32_t*)(s))

static lex_item_t * symbol_rename(parser_t * p, decl_t * decl, lex_item_t * name, lex_item_t * alias) {
	int i;
	for (i = 0; i < decl->items->length; i++) {
		lex_item_t * item = decl->items->items[i];
		if (lex_item_equals(name, item)) {
			char * symbol_name = NULL;

			char * export_name = alias ? alias->value : name->value;

			if (decl->visibility == GLOBAL || CHAR4(p->pkg->name) == CHAR4("main")) {
				asprintf(&symbol_name, "%s", export_name);
			} else {
				asprintf(&symbol_name, "%s_%s", p->pkg->name, export_name);
			}

			item = lex_item_replace_value(item, symbol_name);
			decl->items->items[i] = item;
			return item;
		}
	}
	return NULL;
}

static void free_decl(decl_t * decl) {
	lex_item_stack_free(decl->items);
}

/**
 * Emits the symbol to the output file, and returns the declaration stack which will be used
 * for the generated header.
 */
static lex_item_stack_t * emit(parser_t * p, decl_t * decl, bool is_function, bool is_extern) {
	lex_item_t * item;

	if (!is_extern) {
		size_t i;
		for (i = 0; i < decl->items->length; i++) {
			package_emit(p->pkg, decl->items->items[i]);
		}
	}

	// used to as a starting point for finding comments.
	lex_item_t * first_tok = lex_item_stack_first(decl->items);

	// trim leading whitespace
	while((item = lex_item_stack_first(decl->items)) && (
		item->type == item_whitespace ||
		item->type == item_newline
	)) lex_item_stack_shift(decl->items);

	if (decl->type == type_variable) {
		int i = 0;
		while(i < lex_item_stack_height(decl->items)) {
			item = lex_item_stack_peek(decl->items, i);
			if (item->type == item_symbol && item->value[0] == '=') break;
			i++;
		}

		while (i < lex_item_stack_height(decl->items)) {
			lex_item_stack_pop(decl->items);
		}

		lex_item_t * first = lex_item_stack_first(decl->items);
		lex_item_stack_unshift(decl->items, lex_item_new(
			strings_dup("extern "),
			item_id,
			first->line,
			first->line_pos,
			first->start
		));
	}

	// trim trailing whitespace
	while((item = lex_item_stack_last(decl->items)) && (
		item->type == item_whitespace ||
		item->type == item_newline
	)) lex_item_stack_pop(decl->items);

	// Find comments for the symbol from the cache and prepend them to the declaration.
	if (first_tok->line > 0) {
		lex_item_cache_t * c = p->pkg->source_cache;
		size_t line = first_tok->line;
		size_t count = 0;

		{ // find the start of the comments
			lex_item_cache_line_t l = {0};
			size_t last_comment_line = line;
			do {
				line--;
				l = c->lines[line];
				if (l.num_items == 0) continue;
				if (l.num_items > 3) break;

				if (l.items[0]->type == item_comment) {
					last_comment_line = line;
				} else if (l.items[0]->type != item_newline) {
					break;
				}
				count += l.num_items;
			} while (line > 0);
			line = last_comment_line;
		}


		// if there are comments, create a new stack with the comments and declaration.
		if (line < first_tok->line) {
			lex_item_stack_t * old_decl = decl->items;
			decl->items = lex_item_stack_new(count + lex_item_stack_height(old_decl));

			// put the comments in first
			while(line < first_tok->line) {
				lex_item_cache_line_t l = c->lines[line++];
				if (l.num_items == 0) continue;

				size_t i;
				for (i = 0; i < l.num_items; i++) {
					lex_item_stack_push(decl->items, l.items[i]);
				}
			}

			// transfer over the declaration
			while(lex_item_stack_height(old_decl)) {
				lex_item_stack_push(decl->items, lex_item_stack_shift(old_decl));
			}

			// free the old stack.
			lex_item_stack_free(old_decl);
		}
	}

	{ // add 1 to the refcount for everything we are saving so it can be safely freed later.
		size_t i;
		for (i = 0; i < lex_item_stack_height(decl->items); i++) {
			lex_item_stack_peek(decl->items, i)->refcount++;
		}
	}

	if (is_function) {
		lex_item_t * last = lex_item_stack_last(decl->items);
		lex_item_t * semicolon = lex_item_new(
			strings_dup(";"),
			item_symbol,
			last->line,
			last->line_pos,
			last->start
		);
		lex_item_stack_push(decl->items, semicolon);
	}

	return decl->items;
}

static lex_item_t * collect_newlines(parser_t * p, decl_t * decl) {
	lex_item_t * item = parser_next(p);

	while (
		item->type == item_whitespace ||
		item->type == item_newline    ||
		item->type == item_comment
	) {
		if (item->type == item_newline) {
			append(decl, item);
		}

		item = parser_next(p);
	}

	return item;
}

static lex_item_t * collect(parser_t * p, decl_t * decl) {
	if (decl->items == NULL) decl->items = lex_item_stack_new(8);
	return parser_token(p, decl->items);
}

static lex_item_t * parse_typedef       (parser_t * p, decl_t * decl);
static lex_item_t * parse_struct        (parser_t * p, decl_t * decl);
static lex_item_t * parse_enum          (parser_t * p, decl_t * decl);
static lex_item_t * parse_union         (parser_t * p, decl_t * decl);
static lex_item_t * parse_struct_block  (parser_t * p, decl_t * decl, lex_item_t * start);
static lex_item_t * parse_enum_block    (parser_t * p, decl_t * decl, lex_item_t * start);
static lex_item_t * parse_export_block  (parser_t * p, decl_t * decl);
static lex_item_t * parse_as            (parser_t * p, decl_t * decl);
static lex_item_t * parse_variable      (parser_t * p, decl_t * decl);
static lex_item_t * parse_function      (parser_t * p, decl_t * decl);
static lex_item_t * parse_function_args (parser_t * p, decl_t * decl);
static lex_item_t * parse_pp            (parser_t * p, decl_t * decl);
static long       parse_size_decl     (parser_t * p, decl_t * decl);

static int parse_passthrough (parser_t * p);

static hash_t * export_types = NULL;
static void init_export_types(){
	export_types = hash_new();

	hash_set(export_types, "typedef", parse_typedef);
	hash_set(export_types, "struct",  parse_struct );
	hash_set(export_types, "enum",    parse_enum   );
	hash_set(export_types, "union",   parse_union  );
}

enum variable_modifiers {
	VARIABLE_CONST  = 0x01 << 0,
	VARIABLE_EXTERN = 0x01 << 1,
	VARIABLE_STATIC = 0x01 << 2,
};

static hash_t * modifier_types = NULL;
static void init_modifier_types(){
	modifier_types = hash_new();

	hash_set(modifier_types, "const",   (void*)VARIABLE_CONST);
	hash_set(modifier_types, "extern",  (void*)VARIABLE_EXTERN);
	hash_set(modifier_types, "static",  (void*)VARIABLE_STATIC);
}

typedef lex_item_t * (*export_fn)(parser_t * p, decl_t * decl);

static void parse_semicolon(parser_t * p, decl_t * decl) {
	if (decl->error) return;

	size_t initial = lex_item_stack_height(decl->items);
	lex_item_t * item = collect_newlines(p, decl);

	//TODO make sure this doesn't include spaces before the semicolon.
	if (item->type != item_symbol || item->value[0] != ';') {
		if (decl->items->length > initial) {
			item = decl->items->items[initial];
		}
		errorf(p, item, decl, SYNTAX_ERROR "expecting ';' but got %s", lex_item_to_string(item));
		return;
	}
	append(decl, item);
}

/**
 * Handles local symbols that are not exported.
 *
 * These are modified to be static.
 */
int parser_export_module_level(parser_t * p, lex_item_t * item) {
	if (export_types == NULL) init_export_types();
	if (modifier_types == NULL) init_modifier_types();
	parser_backup(p, item);
	if (export_types == NULL) init_export_types();

	decl_t decl  = {0};
	export_fn fn = NULL;
	lex_item_t * name  = NULL;
	bool has_semicolon = false;
	bool is_extern     = false;
	int modifiers = 0;

	lex_item_t * type = collect_newlines(p, &decl);

	switch (type->type) {
		case item_id:
			while (type->type == item_id && hash_has(modifier_types, type->value)) {
				modifiers |= (enum variable_modifiers) hash_get(modifier_types, type->value);
				append(&decl, type);
				type = collect(p, &decl);
			}

			fn = (export_fn) hash_get(export_types, type->value);
			has_semicolon = is_extern || fn != NULL;
			if (fn) append(&decl, type);
			break;
		case item_symbol:
			break;
		case item_open_symbol:
			if (type->value[0] == '{') {
				fn = parse_export_block;
			}
			break;
		case item_pp_symbol:
			fn = parse_pp;
			append(&decl, type);
			break;
		default:
			fn = NULL;
			break;
	}
	if (fn == NULL) {
		parser_backup(p, type);
		name = parse_variable(p, &decl);
	} else {
		name = fn(p, &decl);
	}

	if (has_semicolon) {
		lex_item_t * tok = collect(p, &decl);
		if (tok->type == item_id) {
			tok = parser_identifier_parse(p, tok, decl.visibility != 0);
			if (name == NULL) name = tok;
			append(&decl, tok);
		} else {
			parser_backup(p, tok);
		}
		if (expression_initializer(p, decl.items, decl.visibility != 0)) {
			decl.type = type_variable;
		}
		parse_semicolon(p, &decl);
	}
	if (decl.error) {
		free_decl(&decl);
		return -1;
	}

#ifdef SYMBOL_DEBUG
	fprintf(stderr, "%lu:%lu \t%s %s%s%s%s %s\n",
			(name ? name->line : type->line) + 1,
			(name ?
			 (name->start - name->line_pos) :
			 (type->start - type->line_pos)
			) + 1,
			(name && name->source != name) ? "export" : "local ",
			(modifiers & VARIABLE_EXTERN) ? "extern " : "",
			(modifiers & VARIABLE_STATIC) ? "static " : "",
			(modifiers & VARIABLE_CONST) ? "const " : "",
			package_export_get_type_name(decl.type),
			name ? name->value : "<anonymous>"
		   );
#endif

	if (
		name && name->source == name &&
		(decl.type == type_function || decl.type == type_variable) &&
		(modifiers & VARIABLE_STATIC) == 0 &&
		(modifiers & VARIABLE_EXTERN) == 0 &&
		strcmp(name->value, "main") != 0
	) {
		lex_item_t * start = lex_item_stack_first(decl.items);
		package_emit(p->pkg, lex_item_new(
			strings_dup("static "),
			start->type,
			start->line,
			start->line_pos,
			start->start
		));
	}

	size_t i;
	for (i = 0; i < decl.items->length; i++) {
		package_emit(p->pkg, lex_item_stack_peek(decl.items, i));
	}
	free_decl(&decl);
	return 0;

}

int parser_export_parse(parser_t * p, lex_item_t * export_token) {
	if (export_types == NULL) init_export_types();
	if (modifier_types == NULL) init_modifier_types();

	decl_t decl  = {0};
	decl.visibility = EXPORT;
	export_fn fn = NULL;
	lex_item_t * name  = NULL;
	lex_item_t * alias = NULL;
	bool has_semicolon = false;
	bool is_extern     = false;
	int modifiers = 0;

	lex_item_t * type = collect_newlines(p, &decl);

	if (type->type == item_id && strcmp(type->value, "global") == 0) {
		type = collect_newlines(p, &decl);
		decl.visibility = GLOBAL;
	}

	switch (type->type) {
		case item_id:
			while (type->type == item_id && hash_has(modifier_types, type->value)) {
				modifiers |= (enum variable_modifiers) hash_get(modifier_types, type->value);
				append(&decl, type);
				type = collect(p, &decl);
			}

			if ((modifiers & VARIABLE_EXTERN) != 0) {
				is_extern = true;
			}

			fn = (export_fn) hash_get(export_types, type->value);
			has_semicolon = is_extern || fn != NULL;
			if (fn) append(&decl, type);
			break;
		case item_symbol:
			if (type->value[0] == '*') {
				return parse_passthrough(p);
			}
			break;
		case item_open_symbol:
			if (type->value[0] == '{') {
				fn = parse_export_block;
			}
			break;
		case item_pp_symbol:
			fn = parse_pp;
			append(&decl, type);
			break;
		default:
			fn = NULL;
			break;
	}

	if (fn == NULL) {
		parser_backup(p, type);
		name = parse_variable(p, &decl);
	} else {
		name = fn(p, &decl);
	}


	alias = parse_as(p, &decl);
	if (has_semicolon) parse_semicolon(p, &decl);
	if (decl.error) {
		free_decl(&decl);
		return -1;
	}

	if (name == NULL) {
		errorf(p, type, &decl, SYNTAX_ERROR "can't export anonymous symbols");
		free_decl(&decl);
		return -1;
	}

	lex_item_t * symbol = NULL;
	if (decl.type > type_header) {
		symbol = symbol_rename(p, &decl, name, alias);

		if (symbol == NULL) {
			errorf(p, type, &decl, "unable to export symbol '%s'", name->value);
			free_decl(&decl);
			return -1;
		}
	}

#ifdef SYMBOL_DEBUG
	fprintf(stderr, "%lu:%lu \texport %s%s%s%s %s (%s)\n",
			(name ? name->line : type->line) + 1,
			(name ?
			 (name->start - name->line_pos) :
			 (type->start - type->line_pos)
			) + 1,
			(modifiers & VARIABLE_EXTERN) ? "extern " : "",
			(modifiers & VARIABLE_STATIC) ? "static " : "",
			(modifiers & VARIABLE_CONST) ? "const " : "",
			package_export_get_type_name(decl.type),
			name ? name->value : "<anonymous>",
			symbol ? symbol->value : "<no symbol>"
		   );
#endif

	package_export_add(
		strings_dup(name ? name->value : ""),
		strings_dup(alias ? alias->value : NULL),
		strings_dup(symbol ? symbol->value : ""),
		decl.type,
		emit(p, &decl, fn == NULL, is_extern), // TODO: remove fn= checks in favor of decl.type
		p->pkg
	);
	if (fn == parse_export_block) lex_item_free(name);
	return 1;
}

static int parse_passthrough(parser_t * p) {
	decl_t decl = {0};
	lex_item_t * from = collect(p, &decl);
	if (from->type != item_id || strcmp(from->value, "from") != 0) {
		parser_errorf(p, from, SYNTAX_ERROR "Exporting passthrough: expected 'from', but got %s", lex_item_to_string(from));
		free_decl(&decl);
		return -1;
	}

	lex_item_t * filename = collect(p, &decl);
	if (filename->type != item_quoted_string) {
		parser_errorf(p, filename, SYNTAX_ERROR "Exporting passthrough: expected filename, but got %s", lex_item_to_string(filename));
		free_decl(&decl);
		return -1;
	}

	parse_semicolon(p, &decl);
	if (decl.error) {
		free_decl(&decl);
		return -1;
	}

	errors_t errors = {0};
	package_import_t * imp = package_import_passthrough(p->pkg, strings_dup(string_parse(filename->value)), &errors);
	if (errors.count) {
		int i;
		for (i = 0; i < errors.count; i++) {
			errors_error_t error = errors.errors[i];
			if (error.context.filename == NULL) {
				// TODO: this seems to be passing the wrong type (error != string)
	 			parser_errorf(p, filename, "exporting passthrough: %s", error);
			} else {
				errors_append(p->errors, error);
			}
		}
	}

	char * header = NULL;
	char * rel =  utils_relative(p->pkg->generated, imp->pkg->header);
	asprintf(&header, "#include \"%s\"", rel);
	lex_item_t * header_token = lex_item_stack_replace_with(decl.items, NULL, header);
	package_emit(p->pkg, header_token);

	//free_decl(&decl);
	free(rel);
	return 1;
}

static lex_item_t * parse_as(parser_t * p, decl_t * decl) {
	if (decl->error) return NULL;

	size_t start = decl->items->length;

	lex_item_t * as = collect(p, decl);
	if (as->type != item_id || strcmp("as", as->value) != 0) {
		parser_backup(p, as);
		while(decl->items->length > start) {
			parser_backup(p, lex_item_stack_pop(decl->items));
		}
		return NULL;
	}

	lex_item_t * alias = collect(p, decl);

	/** Trim whitespace around the as symbol. */
	lex_item_t * tok = NULL;
	while((tok = lex_item_stack_pop(decl->items)) && tok->type == item_whitespace);
	append(decl, tok);


	if (alias->type != item_id) {
		errorf(p, alias, decl, "expecting identifier but got %s", lex_item_to_string(alias));
		return NULL;
	}

	return alias;
}

static long parse_array_size(parser_t * p, decl_t * decl) {
	long size = 1;

	lex_item_t * item = collect(p, decl);
	if (item->type != item_number) {
		errorf(p, item, decl, "size declaration: expecting a number, but found '%s'", lex_item_to_string(item));
		return size;
	}

	size = atol(item->value);
	append(decl, item);

	item = collect(p, decl);
	if (item->type != item_close_symbol || item->value[0] != ']') {
		errorf(p, item, decl, "size declaration: expecting ']', but found '%s'", lex_item_to_string(item));
		return size;
	}

	append(decl, item);
	return size * parse_size_decl(p, decl); // allow for multidimentional sizes.
}

static long parse_bitslice_size(parser_t * p, decl_t * decl) {
	long size = 1;

	lex_item_t * item = collect(p, decl);
	if (item->type != item_number) {
		errorf(p, item, decl, "size declaration: expecting a number, but found '%s'", lex_item_to_string(item));
		return size;
	}

	return size;
}

/**
 * Parse a size declaration at the end of a variable declaration, and return the value.
 * ie. int[6];
 *        ^~~
 */
static long parse_size_decl(parser_t * p, decl_t * decl) {
	long size = 1;
	lex_item_t * item = collect(p,decl);

	if (item->type == item_open_symbol && item->value[0] == '[') {
		append(decl, item);
		return parse_array_size(p, decl);
	} else if (item->type == item_symbol && item->value[0] == ':') {
		append(decl, item);
		return parse_bitslice_size(p, decl);
	}

	parser_backup(p, item);
	return size;
}

static lex_item_t * parse_function_ptr(parser_t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_variable; // function pointer is a variable
	lex_item_t * star = collect(p, decl);
	if (star->type != item_symbol || star->value[0] != '*') {
		return errorf(p, star, decl, "function pointer: expecting '*' but found '%s'", star->value);
	}
	append(decl, star);

	lex_item_t * name = collect(p, decl);
	if (name->type != item_id) {
		return errorf(p, name, decl, "function pointer: expecting identifier but found '%s'", name->value);
	}
	append(decl, name);

	lex_item_t * item;

	item = collect(p, decl);
	if (item->type != item_close_symbol || item->value[0] != ')') {
		return errorf(p, item, decl, "function pointer: expecting ')' but found '%s'", item->value);
	}
	append(decl, item);

	item = collect(p, decl);
	if (item->type != item_open_symbol || item->value[0] != '(') {
		return errorf(p, item, decl, "function pointer: expecting '(' but found '%s'", item->value);
	}
	append(decl, item);

	parse_function_args(p, decl);

	if (decl->error) return NULL;
	return name;
}

static lex_item_t * parse_typedef (parser_t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_type;
	lex_item_t * type = collect(p, decl);
	if (type->type != item_id) {
		return errorf(p, type, decl, "in typedef: expected identifier");
	}

	export_fn fn = (export_fn) hash_get(export_types, type->value);
	if (fn == NULL) {
		type = parser_identifier_parse(p, type, decl->visibility != 0);
	}
	append(decl, type);

	if (fn != NULL) {
		fn(p, decl); // discard the internal name, it's not critical here.
		if (decl->error) return NULL;
	}

	lex_item_t * item;
	lex_item_t * name = NULL;
	bool function_ptr = false;
	bool as           = false;
	do {
		item = collect(p, decl);
		switch(item->type) {
			case item_eof:
				return errorf(p, item, decl, "in typedef: expected identifier or '('");
			case item_id:
				if (strcmp("as", item->value) == 0) {
					rewind_whitespace(p, decl, item);
					as = true;
					continue;
				}
				if (!function_ptr) name = item;
				break;
			case item_symbol:
				if (item->value[0] == ';') continue;
			case item_open_symbol:
				if (!function_ptr && item->value[0] == '(') {
					function_ptr = true;
					append(decl, item);
					name = parse_function_ptr(p, decl);
					if (decl->error) return name;
					continue;
				}
			default:
				break;
		}

		append(decl, item);
	} while (
			item->type != item_error                             &&
			!(item->type == item_symbol && item->value[0] == ';') &&
			!as
	);

	if (item->type == item_error) {
		decl->error = true;
		return item;
	}

	if (!as) parser_backup(p, item);
	return name;
}

static lex_item_t * parse_struct (parser_t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_struct;
	lex_item_t * name = NULL;
	lex_item_t * type = lex_item_stack_last(decl->items);
	lex_item_t * item = collect(p, decl);
	if (item->type == item_id) {
		name = parser_identifier_parse_typed(p, type, item, decl->visibility != 0);
		append(decl, name);
		item = collect(p, decl);
	}

	if (item->type != item_open_symbol || item->value[0] != '{') {
		parser_backup(p, item);
		return name;
	}

	append(decl, item);
	parse_struct_block(p, decl, item);

	return (decl->error) ? NULL : name;
}

static lex_item_t * parse_union (parser_t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_union;
	lex_item_t * type = lex_item_stack_last(decl->items);
	lex_item_t * name = NULL;
	lex_item_t * item = collect(p, decl);

	// union name
	if (item->type == item_id) {
		name = parser_identifier_parse_typed(p, type, item, decl->visibility != 0);
		append(decl, name);
		item = collect(p, decl);
	}

	// if there is a second enum it is a variable declaration
	if (item->type == item_id) {
		if (decl->type == type_union) decl->type = type_variable;
		name = item;
		append(decl, name);

		return (decl->error) ? NULL : name;
	}

	if (item->type != item_open_symbol || item->value[0] != '{') {
		return errorf(p, item, decl, "in union: expecting '{' but got %s", lex_item_to_string(item));
	}

	append(decl, item);
	parse_struct_block(p, decl, item);
	return (decl->error) ? NULL : name;
}

static lex_item_t * parse_enum (parser_t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_enum;

	lex_item_t * name = NULL;
	lex_item_t * type = lex_item_stack_last(decl->items);
	lex_item_t * item = collect(p, decl);

	// enum name
	if (item->type == item_id) {
		name = parser_identifier_parse_typed(p, type, item, decl->visibility != 0);
		append(decl, name);
		item = collect(p, decl);
	}

	// if there is a second enum it is a variable declaration
	if (item->type == item_id) {
		if (decl->type == type_enum) decl->type = type_variable;
		name = item;
		append(decl, name);

		return (decl->error) ? NULL : name;
	}

	if (item->type != item_open_symbol || item->value[0] != '{') {
		return errorf(p, item, decl, "in enum: expecting '{' but got %s", lex_item_to_string(item));
	}

	append(decl, item);
	parse_enum_block(p, decl, item);
	return (decl->error) ? NULL : name;
}

static lex_item_t * parse_export_block(parser_t * p, decl_t * decl) {
	lex_item_t * item;

	int escaped_id = 0;
	while(true) {
		int record = 0;
		item = collect(p, decl);

		switch(item->type) {
			case item_c_code:
			case item_quoted_string:
			case item_comment:
			case item_symbol:
			case item_pp_symbol:
			case item_pp_newline:
			case item_pp_token:
				record = 1;
				break;

			case item_arrow:
				escaped_id = 1;
				record     = 1;
				break;

			case item_id:
				if (escaped_id == 0) item = parser_identifier_parse(p, item, decl->visibility != 0);
				record     = 1;
				escaped_id = 0;
				break;

			case item_close_symbol:
				if (item->value[0] == '}') {
					return lex_item_replace_value(item, strings_dup("block"));
				};
				break;

			case item_eof:
				return errorf(p, item, decl, "in block: unmatched '{'");
			case item_error:
				parser_backup(p, item);
				decl->error = true;
				return NULL;
			default:
				return errorf(p, item, decl, "in block: unexpected input '%s'", lex_item_to_string(item));
		}
		if (record) append(decl, item);
	}

}

static lex_item_t * parse_struct_block( parser_t * p, decl_t * decl, lex_item_t * start) {
	lex_item_t * item;
	while(expression_declaration(p, decl->items, decl->visibility != 0)) {
		parse_semicolon(p, decl);
	}

	// TODO: ensure item is a closing brace;
	item = collect(p, decl);
	if (item && item->type == item_eof) {
		return errorf(p, start, decl, "in struct block: missing matching '}' before end of file");
	}
	append(decl, item);
	return item;
}

static lex_item_t * parse_enum_declaration(parser_t * p, decl_t * decl) {
	lex_item_t * item = collect(p, decl);

	if (item->type != item_id) {
		if (item->type == item_close_symbol && item->value[0] == '}') return item;
		return errorf(p, item, decl, "in enum: expecting identifier but got %s",
				lex_item_to_string(item));
	}
	append(decl, item);

	/*
	if (strcmp("as", item->value) == 0) return item;
	append(decl, item);
	*/

	item = collect(p, decl);
	if (!(item->type == item_symbol && (item->value[0] != ',' || item->value[0] != '='))) {
		return item;
	}
	append(decl, item);

	if (item->value[0] == '=') {
		expression_rvalue(p, decl->items, decl->visibility != 0);
		/**
		item = collect(p, decl);
		if (item->type != item_number) {
			return errorf(p, item, decl, "in enum: expecting a number but got %s",
					lex_item.to_string(item));
		}
		append(decl, item);

		*/
		item = collect(p, decl);
		if (item->type != item_symbol || item->value[0] != ',') {
			return item;
		}
		append(decl, item);
	}

	return item;
}

static lex_item_t * parse_enum_block ( parser_t * p, decl_t * decl, lex_item_t * start) {
	lex_item_t * item;
	do {
		item = parse_enum_declaration(p, decl);
		if (item == NULL || item->type == item_eof) {
				return errorf(p, start, decl, "in enum block: missing matching '}' before end of file");
		}
		if (item->type == item_error) return item;
	} while (!(item->type == item_close_symbol && item->value[0] == '}'));
	append(decl, item);
	return item;
}

static lex_item_t * parse_type (parser_t * p, decl_t * decl) {
	lex_item_t * item;
	lex_item_t * name = NULL;
	int escaped_id = 0;
	do {
		item = collect(p, decl);
		switch(item->type) {
			case item_arrow:
				escaped_id = 1;
				append(decl, item);
				continue;
			case item_id:
				if (escaped_id == 0) item = parser_identifier_parse(p, item, decl->visibility != 0);
				name = item;
				break;
			case item_symbol:
				if (item->value[0] == '*') break;
			case item_open_symbol:
				if (item->value[0] == '(') {
					append(decl, item);
					return name;
				}
			default:
				return errorf(p, item, decl, "in type declaration: expecting identifier or '('");
		}
		append(decl, item);
	} while (item->type != item_open_symbol || item->value[0] != '(');
	return NULL;
}

static lex_item_t * parse_variable (parser_t * p, decl_t * decl) {
	lex_item_t * item;
	lex_item_t * name = NULL;
	bool escaped_id = 0;
	bool escape_till_semicolon = false;
	do {
		item = collect(p, decl);
		if (escape_till_semicolon && (item->type != item_symbol || item->value[0] != ';')) {
			append(decl, item);
			continue;
		}
		switch(item->type) {
			case item_arrow:
				escaped_id = true;
				append(decl, item);
				continue;
			case item_id:
				if (strcmp(item->value, "as") == 0) {
					if (decl->type == type_block) decl->type = type_variable;
					rewind_whitespace(p, decl, item);
					return name;
				}
				if (escaped_id == false) item = parser_identifier_parse(p, item, decl->visibility != 0);
				name = item;
				break;
			case item_symbol:
				if (item->value[0] == '*') break;
				if (item->value[0] == '=' || item->value[0] == ':') {
					escape_till_semicolon = true;
					break;
				}
				if (item->value[0] == ';') {
					parser_backup(p, item);
					if (decl->type == 0) decl->type = type_variable;
					return name;
				}
			case item_open_symbol:
				if (item->value[0] == '(') {
					lex_item_t * star = parser_next(p);
					parser_backup(p, star);
					if (star->type == item_symbol && star->value[0] == '*') {
						append(decl, item);
						return parse_function_ptr(p, decl);
					}
					parser_backup(p, item);
					rewind_until(p, decl, name);
					return parse_function(p, decl);
				}
				if (item->value[0] == '[') {
					append(decl, item);
					item = parser_next(p);
					if (item->type == item_number || item->type == item_id) {
						append(decl, item);
						item = parser_next(p);
					}
					if (item->type != item_close_symbol || item->value[0] != ']') {
						return errorf(p, item, decl,
								"in type declaration: expecting terminating ']' but got %s",
								lex_item_to_string(item)
						);
					}
					break;
				}
			default:
				return errorf(p, item, decl, "in type declaration: expecting identifier or '(' but got %s", lex_item_to_string(item));
		}
		append(decl, item);
	} while (item && item->type != item_eof);
	if (decl->type == 0) decl->type = type_variable;
	return name;
}

static lex_item_t * parse_function_args(parser_t * p, decl_t * decl) {
	int level = 1;
	int escaped_id = 0;
	lex_item_t * item;
	do {
		item = collect(p, decl);
		switch(item->type) {
			case item_arrow:
				escaped_id = 1;
				append(decl, item);
				continue;
			case item_id:
				if (escaped_id == 0) {
					item = parser_identifier_parse(p, item, decl->visibility != 0);
				}
				break;
			case item_open_symbol:
				if (item->value[0] == '(') level++;
				break;
			case item_close_symbol:
				if (item->value[0] == ')') level--;
				break;
			default:
				break;
		}

		escaped_id = 0;
		append(decl, item);
	} while (level > 0 && item && item->type != item_eof && item->type != item_error);
	return item;
}

static lex_item_t * parse_function (parser_t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_function;
	lex_item_t * name = parse_type(p, decl);

	if (decl->error) {
		lex_item_stack_t * s = emit(p, decl, true, false);
		lex_item_stack_free_items(s);
		lex_item_stack_free(s);
		decl->items = NULL;
		return NULL;
	}

	parse_function_args(p, decl);
	if (decl->error) return NULL;

	return name;
}

static lex_item_t * parse_pp(parser_t * p, decl_t * decl) {
	if (decl->type == 0) decl->type = type_header;
	lex_item_t * symbol = lex_item_stack_first(decl->items);
	preprocessor_parse_inline(p, decl->items);

	return symbol;
}
