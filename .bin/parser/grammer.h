#ifndef _package_grammer_
#define _package_grammer_

#include "../package/package.h"
#include "../deps/stream/stream.h"
#include "../errors/errors.h"

int grammer_parse(stream_t * in, const char * filename, package_t * p, errors_t * errors);

#endif
