#ifndef _package_parser_package_
#define _package_parser_package_

#include "parser.h"
#include "../lexer/item.h"

int parser_package_parse(parser_t * p, lex_item_t * package_token);

#endif
