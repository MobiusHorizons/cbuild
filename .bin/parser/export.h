#ifndef _package_parser_export_
#define _package_parser_export_

#include "parser.h"
#include "../lexer/item.h"

/**
 * Handles local symbols that are not exported.
 *
 * These are modified to be static.
 */
int parser_export_module_level(parser_t * p, lex_item_t * item);

int parser_export_parse(parser_t * p, lex_item_t * export_token);

#endif
