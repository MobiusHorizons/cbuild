#define _package_parser_package_

#include "parser.h"
#include "../lexer/item.h"
#include "string.h"
#include "../utils/strings.h"
#include "../package/package.h"

#include <stdarg.h>
#include <stdio.h>

#define ERROR_PREFIX "Invalid package syntax: "

static int errorf(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	parser_verrorf(p, ERROR, false, item, NULL, fmt, args);
	return -1;
}

int parser_package_parse(parser_t * p, lex_item_t * package_token) {
	lex_item_t * target = NULL;
	lex_item_t * name = parser_token(p, NULL);
	if (name->type != item_quoted_string) {
		return errorf(p, name, ERROR_PREFIX "Expecting a quoted string, but got %s", lex_item_to_string(name));
	}

	lex_item_t * semicolon = parser_skip(p, NULL, item_whitespace, 0);
	if (semicolon->type == item_quoted_string) {
		target = semicolon;
		semicolon = parser_skip(p, NULL, item_whitespace, 0);

	}
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, ERROR_PREFIX "Expecting ';' got %s", lex_item_to_string(semicolon));
	}

	hash_del(package_id_cache, p->pkg->name);
	free(p->pkg->name);
	p->pkg->name = strings_dup(string_parse(name->value));
	hash_set(package_id_cache, p->pkg->name, p->pkg);
	if (target) {
		p->pkg->target = strings_dup(string_parse(target->value));
	}
	return 1;
}
