#define _package_parser_build_
#include "parser.h"
#include "../lexer/stack.h"
#include "string.h"
#include "../lexer/item.h"
#include "../package/package.h"
#include "../package/import.h"
#include "../utils/strings.h"


#include "../deps/hash/hash.h"

#include <stdarg.h>
#include <string.h>
#include <stdio.h>

static hash_t * options = NULL;

typedef int (*parse_fn)(parser_t * p);
static int parse_depends (parser_t * p);
static int parse_set     (parser_t * p);
static int parse_append  (parser_t * p);
static int parse_rule    (parser_t * p);

static void init_options(){
	options = hash_new();

	hash_set(options, "depends", parse_depends);
	hash_set(options, "set",     parse_set    );
	hash_set(options, "append",  parse_append );
	hash_set(options, "rule",    parse_rule   );
}

static int errorf(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	char error_string[4096];
	strcpy(error_string, "Invaild build syntax: ");
	strcat(error_string, fmt);

	parser_verrorf(p, ERROR, false, item, NULL, error_string, args);
	return -1;
}

/**********************************************************************************************************************
 * Build options manipulate the resulting makefile.
 *
 * Parses the following syntaxes:
 * - build depends      "<filename>";          # add "<filenme>" to the dependency list for the current file
 * - build set          <variable> "<value>";  # set the value of a makefile valiable         (:=)
 * - build set default  <variable> "<value>";  # set the default value of a makefile variable (?=)
 * - build append       <variable> "<value>";  # append to a makefile variable                (+=)
 **********************************************************************************************************************/
int parser_build_parse (parser_t * p, lex_item_t * build_token) {
	if (options == NULL) init_options();

	lex_item_t * item = parser_token(p, NULL);
	parse_fn fn = (parse_fn) hash_get(options, item->value);
	if (fn != NULL)  return fn(p);

	return errorf(p, item, "Expecting one of \n"
			"\t'depends', 'set', 'set default' or 'append', but got %s",
			lex_item_to_string(item)
			);
}

static int parse_depends(parser_t * p) {
	lex_item_t * filename = parser_token(p, NULL);
	if (filename->type != item_quoted_string) {
		return errorf(p, filename, "Expecting a filename but got '%s'", filename->value);
	}

	lex_item_t * semicolon = parser_skip(p, NULL, item_whitespace, 0);
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, "Expecting ';' but got '%s'", lex_item_to_string(semicolon));
	}

	char * error = NULL;
	package_import_t * imp = package_import_add_c_file(
		p->pkg, 
		string_parse_dup(filename->value),
		&error
	);
	if (imp == NULL) {
		parser_errorf(p, filename, "Error adding dependency: '%s' %s", filename->value, error);
		return -1;
	}
	return 1;
}

int parser_build_parse_pp_include (parser_t * p, lex_item_t * filename) {
	package_import_t * imp = package_import_add_h_file(
		p->pkg, 
		string_parse_dup(filename->value)
	);

	// TODO can this actually happen?
	if (imp == NULL) {
		parser_errorf(p, filename, "Error adding dependency: '%s'", filename->value);
		return -1;
	}
	return 1;
}

static int parse_set(parser_t * p) {
	bool is_default = false;
	bool have_name = false;
	lex_item_t * name;
	do {
		name = parser_token(p, NULL);

		if (name->type != item_id) {
			return errorf(p, name, "Expecting a variable name, but got %s", name->value);
		}

		if (strcmp(name->value, "default") == 0) {
			is_default = true;
		} else {
			have_name = true;
		}
	} while (have_name == false);

	lex_item_t * value = parser_token(p, NULL);

	if (value->type != item_quoted_string) {
		return errorf(p, value, "Expecting a quoted string, but got '%s'", value->value);
	}

	lex_item_t * semicolon = parser_skip(p, NULL, item_whitespace, 0);
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, "Expecting ';' but got '%s'", lex_item_to_string(semicolon));
	}

	package_var_t v = {0};
	v.name      = strings_dup(name->value);
	v.value     = strings_dup(string_parse(value->value));
	v.operation = is_default ? build_var_set_default : build_var_set;

	p->pkg->variables = realloc(p->pkg->variables, sizeof(package_var_t) * (p->pkg->n_variables + 1));
	p->pkg->variables[p->pkg->n_variables] = v;
	p->pkg->n_variables++;

	return 1;
}

static int parse_append(parser_t * p) {
	lex_item_t * name = parser_token(p, NULL);

	if (name->type != item_id) {
		return errorf(p, name, "Expecting a variable name, but got %s", name->value);
	}

	lex_item_t * value = parser_token(p, NULL);

	if (value->type != item_quoted_string) {
		return errorf(p, value, "Expecting a quoted string, but got '%s'", value->value);
	}

	lex_item_t * semicolon = parser_skip(p, NULL, item_whitespace, 0);

	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		return errorf(p, semicolon, "Expecting ';' but got '%s'", lex_item_to_string(semicolon));
	}

	package_var_t v = {0};
	v.name      = strings_dup(name->value);
	v.value     = strings_dup(string_parse(value->value));
	v.operation = build_var_append;

	p->pkg->variables = realloc(p->pkg->variables, sizeof(package_var_t) * (p->pkg->n_variables + 1));
	p->pkg->variables[p->pkg->n_variables] = v;
	p->pkg->n_variables++;
	return 1;
}

/** 
 * for a gerenic rule, expect a generic dependency
 */
static char * parse_dep(parser_t * p, int required) {
	lex_item_t * tok;

	tok = parser_token(p, NULL);
	if (tok->type == item_open_symbol && tok->value[0] == '{') {
		if (required) {
			errorf(p, tok, "Expecting at least one rule dependency, but got '%s'", tok->value);
		}
		parser_backup(p, tok);
		return NULL;
	}

	if (tok->type != item_quoted_string) {
		errorf(
			p, tok, 
			"Expecting a quoted string '%s'", 
			tok->value
		);
		return NULL;
	}

	return string_parse_dup(tok->value);
}

static int parse_rule(parser_t * p) {
	lex_item_t * target = parser_token(p, NULL);
	if (target->type != item_quoted_string) {
		return errorf(p, target, "Expecting a quoted string (target), but got '%s'", target->value);
	}

	char * deps = NULL;
	char * dep = NULL;
	size_t deps_len = 0;
	unsigned char is_generic = index(target->value, '%') != NULL;

	while((dep = parse_dep(p, is_generic && deps == NULL))) {
		deps = realloc(deps, deps_len + strlen(dep) + (deps_len == 0 ? 1 : 2));
		if (deps_len != 0) {
			deps[deps_len++] = ' ';
			deps[deps_len] = 0;
		}
		memcpy(deps + deps_len, dep, strlen(dep));
		deps_len += strlen(dep);
		deps[deps_len] = 0;
		free(dep);
		// TODO: warn if at least one deps is not generic;
	}

	lex_item_t * tok = parser_token(p, NULL);
	if (tok->type != item_open_symbol && tok->value[0] != '{') {
		return errorf(p, tok, "Expecting '{', but got '%s'", tok->value);
	}
	lex_item_stack_t * lines = lex_item_stack_new(0);

	tok = NULL;
	do {
		tok = parser_token(p, NULL); // skip whitespace before first symbol.
		while ((tok->type != item_close_symbol || tok->value[0] != '}') && tok->type != item_newline) {
			lex_item_stack_push(lines, tok);
			tok = parser_next(p);
		}
	} while (tok->value[0] != '}');

	int line;
	lex_item_t * first = lex_item_stack_first(lines);
	lex_item_t * last  = lex_item_stack_last(lines);

	package_rule_t rule = {0};
	rule.target = string_parse_dup(target->value);
	rule.deps   = deps;
	rule.is_generic = is_generic;
	size_t recipe_len = (last->length + last->start) - first->start + 
		(last->line - first->line + 1) * 2 + 1;
	rule.recipe = malloc(recipe_len);
			
	char * r = rule.recipe;

	for (line = first->line; line <= last->line; line++) {
		*(r++) = '\t';
		while((tok = lex_item_stack_first(lines)) && tok->line == line) {
			memcpy(r, tok->value, tok->length);
			r = r + tok->length;
			lex_item_stack_shift(lines);
		}
		*(r++) = '\n';
	}
	*r = 0;
	lex_item_stack_free_items(lines);
	lex_item_stack_free(lines);

	p->pkg->rules = realloc(p->pkg->rules, sizeof(package_rule_t) * (p->pkg->n_rules + 1));
	p->pkg->rules[p->pkg->n_rules] = rule;
	p->pkg->n_rules++;
	return 1;
}
