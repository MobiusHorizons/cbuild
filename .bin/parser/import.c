#define _package_import_
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "parser.h"
#include "string.h"
#include "../lexer/item.h"
#include "../lexer/stack.h"
#include "../package/import.h"
#include "../package/export.h"
#include "../package/package.h"
#include "../utils/utils.h"
#include "../utils/strings.h"
#include "../errors/errors.h"

#define ERROR_PREFIX "Invalid import syntax: "
static int errorf(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);

	parser_verrorf(p, ERROR, false, item, NULL, fmt, args);

	return -1;
}

static lex_item_t * token(parser_t * p, lex_item_stack_t * s, bool newlines) {
	lex_item_t * item = parser_next(p);
	while(item->type == item_whitespace || (newlines ? item->type == item_newline : false)){
		lex_item_stack_push(s, item);
		item = parser_next(p);
	}
	return item;
}

int import_parse(parser_t * p, lex_item_t * import_token) {
	lex_item_stack_t * s = lex_item_stack_new(8);
	lex_item_stack_push(s, import_token);

	lex_item_t * alias = token(p, s, true);
	if (alias->type != item_id) {
		lex_item_stack_free(s);
		return errorf(p, alias, ERROR_PREFIX "Expecting identifier, but got %s", lex_item_to_string(alias));
	}

	lex_item_stack_push(s, alias);
	lex_item_t * from = token(p, s, true);
	if (from->type != item_id || strcmp(from->value, "from") != 0) {
		lex_item_stack_free(s);
		return errorf(p, from, ERROR_PREFIX "Expecting 'from', but got %s", lex_item_to_string(from));
	}

	lex_item_stack_push(s, from);
	lex_item_t * filename = token(p, s, true);
	if (filename->type != item_quoted_string) {
		lex_item_stack_free(s);
		return errorf(p, filename, ERROR_PREFIX "Expecting filename, but got %s", lex_item_to_string(filename));
	}

	lex_item_stack_push(s, filename);
	lex_item_t * semicolon = token(p, s, false);
	if (semicolon->type != item_symbol || semicolon->value[0] != ';') {
		lex_item_stack_free(s);
		return errorf(p, semicolon, ERROR_PREFIX "Expecting ';', but got %s", lex_item_to_string(semicolon));
	}

	lex_item_stack_push(s, semicolon);

	errors_t errors = {0};
	package_import_t * imp = package_import_queue(
		strings_dup(alias->value),
		strings_dup(string_parse(filename->value)),
		p->pkg,
		&errors
	);

	if (errors.count != 0) {
		int i;
		for (i = 0; i < errors.count; i++) {
			errors_error_t error = errors.errors[i];
			if (errors.errors[i].context.filename) {
				errors_append(p->errors, error);
			} else {
				parser_severityf(p, error.severity, filename, 
					"importing file '%s': %s",
					filename->value, error.message
				);
			}
		}
	}

	if (imp == NULL) return -1;

	char * include;
	char * rel = NULL;
	if (imp->pkg->header) {
		rel = utils_relative(p->pkg->generated, imp->pkg->header);
	} else {
		char * header = package_export_get_header_path(imp->pkg->generated);
		rel = utils_relative(p->pkg->generated, header);
		free(header);
	}
	asprintf(&include, "#include \"%s\"", rel);

	lex_item_t * include_token = lex_item_stack_replace_with(s, import_token, include);
	package_emit(p->pkg, include_token);
	free(rel);

	return 1;
}
