#define _package_symbol_
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "../errors/errors.h"
#include "../lexer/item.h"

/*
 * symbol represents a symbol that is expected to be exported from a given package.
 */
typedef struct {
	lex_item_t * type;
	lex_item_t * from;
	lex_item_t * name;
	bool         is_export;
} symbol_ref_t;

typedef struct {
	const char * name;
	const char * filename;
	size_t num_refs;
	symbol_ref_t * refs;
} symbol_t;

symbol_t * symbol_new(char * symbol_name) {
	symbol_t * s = malloc(sizeof(symbol_t));

	s->name     = symbol_name;
	s->num_refs = 0;
	s->refs     = NULL;

	return s;
}

void symbol_insert(symbol_t * s, symbol_ref_t r) {
	s->refs = realloc(s->refs, (s->num_refs + 1) * sizeof(symbol_ref_t));
	s->refs[s->num_refs++] = r;
}

char * symbol_name(symbol_ref_t r, const char * name, size_t * o_length) {
	char * value = NULL;
	size_t length = 0;

	if (r.type) {
		length = r.type->length + strlen(name) + 2;
		value = malloc(length);
		length = snprintf(value, length, "%s %s", r.type->value, name);
	} else {
		length = strlen(name);
		value = malloc(length + 1);
		strcpy(value, name);
	}

	if (o_length) *o_length = length;
	return value;
}

/**
 * If the symbol was not found, this generates an approximate symbol name (assuming no aliasing).
 * This allows parsing to continue and find other errors.
 */
char * symbol_fallback_name(symbol_ref_t r, const char * package_name, size_t * o_length) {
	size_t length = (r.type ? r.type->length + 1 : 0) + strlen(package_name) + r.name->length + 2;
	char * value = malloc(length);

	if (r.type) {
		snprintf(value, length, "%s %s_%s",
			r.type->value,
			package_name,
			r.name->value
		);
	} else {
		snprintf(value, length, "%s_%s",
			package_name,
			r.name->value
		);
	}

	if (o_length) *o_length = length;
	return value;
}

void symbol_resolve(symbol_t * s, const char * name) {
	int i;
	for (i = 0; i < s->num_refs; i++) {
		symbol_ref_t r = s->refs[i];
		lex_item_t * sym = r.name->replaced_by;
		sym->value = symbol_name(r, name, &sym->length);
	}
}

/** 
 * outputs a fallback best-guess of the symbol name, since it can't be found.
 * This is used so that parsing can continue, and other errors can be reported.
 */
void symbol_resolve_error(symbol_t * s, const char * package_name, const char * filename, errors_t * errors) {
	int i;
	for (i = 0; i < s->num_refs; i++) {
		symbol_ref_t r = s->refs[i];
		lex_item_t * sym = r.name->replaced_by;

		// TODO: add context from cache, and potentially add that to the Errors package.
		errors_error_t e = {
			.severity = ERROR,
			.is_fatal = false,
			.context  = {
				.filename = strdup(filename),
				.start = {
					.line      = r.name->line,
					.character = r.name->start - r.name->line_pos,
				},
				.end = {
					.line      = r.name->line,
					.character = r.name->start - r.name->line_pos + r.name->length,
				}
			}
		};

		asprintf(&e.message, "Package '%s' does not export the symbol '%s'",
				r.from->value, r.name->value);

		errors_append(errors, e);
		sym->value = symbol_fallback_name(r, package_name, &sym->length);
	}
}

void symbol_free(symbol_t * s) {
	free(s->refs);
	free(s);
}
