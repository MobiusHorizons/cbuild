#define _package_parser_
#include "../lexer/item.h"
#include "../lexer/lex.h"
#include "../lexer/stack.h"
#include "../lexer/cache.h"
#include "../utils/strings.h"
#include "../utils/utils.h"
#include "../errors/errors.h"
#include "../package/package.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

struct parser_parser_s;
typedef void * (*parser_parse_fn)(struct parser_parser_s * lex);

typedef struct parser_parser_s {
	lex_t     * lexer;
	parser_parse_fn    state;
	lex_item_stack_t   * items;
	package_t * pkg;
	char      * filename;
	errors_t  * errors;
} parser_t;

void parser_fatalf(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_errorf(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_warnf(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_infof(parser_t * p, lex_item_t * item, const char * fmt, ...);
void parser_hintf(parser_t * p, lex_item_t * item, const char * fmt, ...);


int parser_parse(lex_t * lexer, parser_parse_fn start, package_t * pkg, errors_t * errors) {
	parser_t * p = malloc(sizeof(parser_t));
	p->lexer     = lexer;
	p->state     = start;
	p->items     = lex_item_stack_new(1);
	p->pkg       = pkg;
	p->errors    = errors;
	p->filename  = realpath(lexer->filename, NULL);

	pkg->source_cache = lexer->cache;
	pkg->dest_cache = lex_item_cache_new();

	char * cwd = getcwd(NULL, 0);

	char * directory = strings_dup(lexer->filename);
	chdir(dirname(directory));
	free(directory);

	while (p->state != NULL) p->state = (parser_parse_fn) p->state(p);

	lex_free(lexer);
	lex_item_stack_free(p->items);
	free(p->filename);
	chdir(cwd);
	free(cwd);
	free(p);

	return (errors ? errors->count : 0);
}

lex_item_t * parser_next(parser_t * p) {
	lex_item_t * item;
	if (p->items->length) {
		item = lex_item_stack_pop(p->items);
	} else {
		item = lex_next_item(p->lexer);
	}
	if (item->type == item_error) {
		parser_fatalf(p, item, "Syntax Error: %s", item->value);
	}
	return item;
}

void parser_backup(parser_t *p, lex_item_t * item) {
	p->items = lex_item_stack_push(p->items, item);
}

lex_item_t * parser_skip(parser_t * p, lex_item_stack_t * s, ...) {
	va_list args;
	int len = 0;
	int i;

	enum lex_item_type types[item_total_symbols];
	va_start(args, s);

	do {
		types[len] = va_arg(args, enum lex_item_type);
		len++;
	} while (types[len-1] != 0);
	len--;

	lex_item_t * item = NULL;
	do {
		int do_skip = 0;
		item = parser_next(p);
		for ( i = 0; i < len; i++) {
			if (types[i] == item->type) {
				do_skip = 1;
				break;
			};
		}
		if (do_skip) {
			if (s != NULL) lex_item_stack_push(s, item);
			continue;
		}
		return item;
	} while (item->type != item_eof && item->type != item_error);

	return item;
}

lex_item_t * parser_token(parser_t * p, lex_item_stack_t * s) {
	lex_item_t * item = parser_next(p);
	while(
		item->type == item_whitespace || 
		item->type == item_newline    ||
		item->type == item_comment
	){
		if (s != NULL) lex_item_stack_push(s, item);
		item = parser_next(p);
	}
	return item;
}

static errors_context_t get_error_context(parser_t * p, lex_item_t * start, lex_item_t * end) {

	if (end == NULL) {
		end = start;
	}

	char * filename = p->filename;

	errors_context_t ctx = {
		.filename = strings_dup(filename),
		.start = {
			.line      = start->line,
			.character = start->start - start->line_pos,
		},
		.end = {
			// TODO: account for PP_newline
			.line      = end->line  + (end->type == item_newline ? 1 : 0),
			.character = end->start - end->line_pos + end->length,
		},
	};

	char * ctx_start = p->lexer->input + start->line_pos;
	char * ctx_end   = &p->lexer->input[end->start];
	if (end->type != item_newline) {
		char * end = ctx_end;
		while(*end != 0 && *end != '\n') end++;
		ctx_end = end;
	}

	ctx.text = malloc(ctx_end - ctx_start + 1);
	memcpy(ctx.text, ctx_start, ctx_end - ctx_start);
	ctx.text[ctx_end - ctx_start] = '\0';

	return ctx;
}

void parser_verrorf(
	parser_t * p,
	enum errors_severity_t severity,
	bool is_fatal,
	lex_item_t * start,
	lex_item_t * end,
	const char * fmt,
	va_list args
) {
	errors_error_t e = {
		.severity = severity,
		.is_fatal = is_fatal,
		.context  = get_error_context(p, start, end),
	};

	vasprintf(&e.message, fmt, args);

	errors_append(p->errors, e);
}

void parser_fatalf(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	parser_verrorf(p, ERROR, true, item, NULL, fmt, args);
}

void parser_errorf(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	parser_verrorf(p, ERROR, false, item, NULL, fmt, args);
}

void parser_warnf(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	parser_verrorf(p, WARNING, false, item, NULL, fmt, args);
}

void parser_infof(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	parser_verrorf(p, INFO, false, item, NULL, fmt, args);
}

void parser_hintf(parser_t * p, lex_item_t * item, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	parser_verrorf(p, HINT, false, item, NULL, fmt, args);
}

void parser_severityf(parser_t * p, enum errors_severity_t s, lex_item_t * i, const char * fmt, ...) {
	va_list args;
	va_start(args, fmt);
	parser_verrorf(p, s, false, i, NULL, fmt, args);
}

