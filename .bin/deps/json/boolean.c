#define _package_json_boolean_

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "../json-parser/json.h"

#include "../stream/stream.h"
#include "json.h"

static int type() {
	static int t;
	if (t == 0) t = json_register("boolean");
	return t;
}

static int boolean_marshal(void * _ctx, json_marshal_args * args, const void * src) {
	bool * val = (bool*)src;
	return stream_write(args->dest, (*val) ? "true" : "false", (*val) ? 4 : 5);
}

static int boolean_unmarshal(void * _ctx, void * dest, json_value * src) {
	bool * p = (bool*)dest;
	*p = src->u.dbl;
	return 0;
}

json_t * json_boolean_new() {
	static json_t * j;

	if (j == NULL) {
		j = malloc(sizeof(json_t));

		j->ctx        = NULL;
		j->type       = type();
		j->is_pointer = false;
		j->marshal    = boolean_marshal;
		j->unmarshal  = boolean_unmarshal;
	}

	return j;
}
