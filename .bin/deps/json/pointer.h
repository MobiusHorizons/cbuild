#ifndef _package_json_pointer_
#define _package_json_pointer_

#include "json.h"

json_t * json_pointer_new(size_t size, json_t * field);

#endif
