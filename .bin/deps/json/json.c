#define _package_json_


#include "../json-parser/json.h"
#include <stdbool.h>

#include "../stream/stream.h"

typedef struct {
	bool pretty;
	unsigned indent_width;
	unsigned indent;
	stream_t * dest;
} json_marshal_args;

typedef int (*json_marshal_t  )(void * ctx, json_marshal_args * args, const void * src);
typedef int (*json_unmarshal_t)(void * ctx, void * dest, json_value * src);

static struct {
	const char ** names;
	size_t        size;
} registered = {0};

typedef struct {
	int         type;
	void      * ctx;
	bool        is_pointer;
	json_marshal_t   marshal;
	json_unmarshal_t unmarshal;
} json_t;

int json_register(const char * name){
	if (registered.size == 0) { // init
		registered.names    = malloc(1 * sizeof(char*));
		registered.names[0] = "error";
		registered.size     = 1;
	}
	int next = registered.size;
	registered.names = realloc(registered.names, (registered.size + 1) * sizeof(char*));

	registered.names[next] = name;
	registered.size++;

	return next;
}

int json_marshal_ex(json_t *j, json_marshal_args * args, void * src) {
	return j->marshal(j->ctx, args, src);
}

int json_marshal(json_t *j, stream_t * dest, void * src) {
	json_marshal_args args = {
		.dest         = dest,
		.pretty       = 0,
		.indent_width = 0,
		.indent       = 0,
	};
	return json_marshal_ex(j, &args, src);
}

int json_marshal_pretty(json_t *j, stream_t * dest, void * src, short indent) {
	json_marshal_args args = {
		.dest         = dest,
		.pretty       = 1,
		.indent_width = indent,
		.indent       = 0,
	};
	return json_marshal_ex(j, &args, src);
}

int json_unmarshal_ex(json_t * j, void * dest, json_value * value) {
	return j->unmarshal(j->ctx, dest, value);
}

int json_unmarshal(json_t * j, void * dest, const char * src, size_t length) {
	json_value * value = json_parse(src, length);
	if (value == NULL) {
		return -1;
	}
	return json_unmarshal_ex(j, dest, value);
}

bool json_is_null(json_t * j, void * src) {
	if (j->is_pointer) {
		void ** p = (void **)src;
		return p == NULL || *p == NULL;
	}
	return false;
}
