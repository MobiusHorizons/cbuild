#define _package_json_array_

#include <string.h>

#include "../stream/stream.h"

#include "json.h"
#include "pointer.h"
#include "integer.h"

#include "../json-parser/json.h"
#include <stddef.h>

static int type() {
	static int t;
	if (t == 0) t = json_register("array");
	return t;
}

typedef struct {
	size_t        size;
	size_t        item_size;
	json_t      * item;
	ptrdiff_t     length_offset;
	unsigned char length_width;
	bool          length_signed;
} ctx_t;

static bool element_not_null(const char * el, size_t width) {
	int i;
	for (i = 0; i < width; i++) {
		if (el[i] != 0) return true;
	}
	return false;
}

static int dynamic_marshal(void * _ctx, json_marshal_args * args, const void * src) {
	ctx_t * ctx = (ctx_t*)_ctx;
	char * ptr = (char*)src;
	char ** elements = (char **) src;

	if (ptr == NULL || *elements == NULL) {
		return stream_write(args->dest, "null", 4);
	}

	long long int length = 0;
	if (ctx->length_offset) {
		length = json_integer_extract(
			ptr + ctx->length_offset,
			ctx->length_width,
			ctx->length_signed
		);
	} else if (ctx->item_size == sizeof(void*)) {
		const void *** elements = (const void ***) src;
		const void ** i = *elements;
		while(i[length]) length++;
	} else {
		size_t size = ctx->item_size;
		const char ** elements = (const char **) src;
		while(element_not_null(*elements + (length * size), size)) {
			length++;
		}
	}

	long long int i;
	stream_write(args->dest, "[", 1);
	args->indent += args->indent_width;
	for (i = 0; i < length; i++) {
		if (args->indent > 0) {
			stream_printf(args->dest, "\n%*.s", args->indent, " ");
		}
		json_marshal_ex(ctx->item, args, (*elements + (ctx->item_size * i)));
		if (i < length - 1) {
			stream_write(args->dest, ",", 1);
		}
	}
	args->indent -= args->indent_width;
	if (args->pretty) {
		stream_printf(args->dest, "\n%*.s]", args->indent, " ");
	} else {
		stream_write(args->dest, "]", 1);
	}

	return 0;
}

static int dynamic_unmarshal(void * _ctx, void * dest, json_value * src) {
	if (src == NULL || src->type != json_array) return 0;

	ctx_t * ctx = (ctx_t*)_ctx;
	char *  ptr = (char*)dest;
	char ** elements = (char**)dest;
	unsigned int length = src->u.array.length;

	if (ctx->length_offset) {
		*elements = malloc(length * ctx->item_size);
	} else {
		*elements = malloc((length + 1) * ctx->item_size);
	}

	unsigned int i;
	for (i = 0; i < length; i++) {
		json_value * v = src->u.array.values[i];
		json_unmarshal_ex(ctx->item, *elements + (ctx->item_size * i), v);
	}

	if (ctx->length_offset) {
		long long int l = length;
		return json_integer_insert(l, ptr + ctx->length_offset, ctx->length_width, ctx->length_signed);
	} else if (ctx->item_size == sizeof(void*)) {
		void ** last = (void **)(*elements + (ctx->item_size * length));
		*last = NULL;
		return 0;
	} else {
		size_t size = ctx->item_size;
		char * element = *elements + (size * length);
		int i;
		for (i = 0; i < size; i++) {
			element[i] = 0;
		}
		return 0;
	}
}

json_t * json_array_with_length(
	size_t item_size, json_t * item,
	ptrdiff_t length_offset, unsigned char length_width, bool length_signed
) {
	ctx_t * ctx = malloc(sizeof(ctx_t));

	ctx->size          = 0;
	ctx->item_size     = item_size;
	ctx->item          = item;
	ctx->length_offset = length_offset;
	ctx->length_width  = length_width;
	ctx->length_signed = length_signed;

	json_t * j = malloc(sizeof(json_t));

	j->type       = type();
	j->is_pointer = true;
	j->ctx        = ctx;
	j->unmarshal  = dynamic_unmarshal;
	j->marshal    = dynamic_marshal;

	return j;
}

// Indicates a null terminated array of pointers.
json_t * json_array_of_pointers(json_t * item){
	ctx_t * ctx = malloc(sizeof(ctx_t));

	ctx->size          = 0;
	ctx->item_size     = sizeof(void*);
	ctx->item          = item;
	ctx->length_offset = 0;
	ctx->length_width  = 0;
	ctx->length_signed = false;

	json_t * j = malloc(sizeof(json_t));

	j->type       = type();
	j->is_pointer = true;
	j->ctx        = ctx;
	j->unmarshal  = dynamic_unmarshal;
	j->marshal    = dynamic_marshal;

	return j;
}

json_t * json_array_dynamic(json_t * item, size_t size) {
	ctx_t * ctx = malloc(sizeof(ctx_t));

	ctx->size          = 0;
	ctx->item_size     = size,
	ctx->item          = item;
	ctx->length_offset = 0;
	ctx->length_width  = 0;
	ctx->length_signed = false;

	json_t * j = malloc(sizeof(json_t));

	j->type       = type();
	j->is_pointer = true;
	j->ctx        = ctx;
	j->unmarshal  = dynamic_unmarshal;
	j->marshal    = dynamic_marshal;

	return j;
}
