#ifndef _package_json_
#define _package_json_

#include "../stream/stream.h"
#include "../json-parser/json.h"
#include <stdbool.h>

typedef struct {
	bool pretty;
	unsigned indent_width;
	unsigned indent;
	stream_t * dest;
} json_marshal_args;

typedef int (*json_marshal_t  )(void * ctx, json_marshal_args * args, const void * src);
typedef int (*json_unmarshal_t)(void * ctx, void * dest, json_value * src);

typedef struct {
	int         type;
	void      * ctx;
	bool        is_pointer;
	json_marshal_t   marshal;
	json_unmarshal_t unmarshal;
} json_t;

int json_register(const char * name);
int json_marshal_ex(json_t *j, json_marshal_args * args, void * src);
int json_marshal(json_t *j, stream_t * dest, void * src);
int json_marshal_pretty(json_t *j, stream_t * dest, void * src, short indent);
int json_unmarshal_ex(json_t * j, void * dest, json_value * value);
int json_unmarshal(json_t * j, void * dest, const char * src, size_t length);
bool json_is_null(json_t * j, void * src);

#endif
