#ifndef _package_json_array_
#define _package_json_array_

#include "json.h"
#include "../json-parser/json.h"
#include <stddef.h>

json_t * json_array_with_length(
	size_t item_size, json_t * item,
	ptrdiff_t length_offset, unsigned char length_width, bool length_signed
);

// Indicates a null terminated array of pointers.
json_t * json_array_of_pointers(json_t * item);

json_t * json_array_dynamic(json_t * item, size_t size);

#endif
