#ifndef _package_json_string_
#define _package_json_string_

#include "../stream/stream.h"
#include "json.h"
#include "../json-parser/json.h"

int json_string_encode(size_t len, stream_t * dest, const char * start);
json_t * json_string_new(size_t size);
json_t * json_string_char();

#endif
