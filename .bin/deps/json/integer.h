#ifndef _package_json_integer_
#define _package_json_integer_

#include "json.h"
#include <stdbool.h>
#include <stdint.h>

long long json_integer_extract(const void * src, unsigned char width, bool is_signed);
int json_integer_insert(long long value, void * dest, unsigned char width, bool is_signed);
json_t * json_integer_new(unsigned char width, bool is_signed);
json_t * json_integer_uint8();
json_t * json_integer_uint16();
json_t * json_integer_uint32();
json_t * json_integer_uint64();
json_t * json_integer_int8();
json_t * json_integer_int16();
json_t * json_integer_int32();
json_t * json_integer_int64();

#endif
