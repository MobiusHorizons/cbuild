#ifndef _package_json_value_
#define _package_json_value_

#include "json.h"

typedef struct {
	void       * value;
	json_value * parsed;
	json_t     * type;
} json_value_t;

json_t * json_value_j();
json_value_t json_value_from(json_t * type, void * value);
json_value_t json_value_null();

#endif
