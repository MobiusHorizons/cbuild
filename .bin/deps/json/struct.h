#ifndef _package_json_struct_
#define _package_json_struct_

#include "json.h"
#include "../json-parser/json.h"
#include <stddef.h>

json_t * json_struct_new();
int json_struct_field(json_t * j, const char * name, size_t offset, json_t * field_type);

#endif
