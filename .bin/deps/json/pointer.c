#define _package_json_pointer_

#include "../json-parser/json.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../stream/stream.h"
#include "json.h"

typedef struct {
	size_t size;
	json_t * field;
} ptr_ctx_t;

static int type() {
	static int t;
	if (t == 0) t = json_register("pointer");
	return t;
}

static int pointer_marshal(void *_ctx, json_marshal_args * args, const void * src) {
	ptr_ctx_t * ctx = (ptr_ctx_t*)_ctx;

	if (src) {
		void * const* ptr = (void * const*)src;
		if (*ptr != NULL) {
			return json_marshal_ex(ctx->field, args, *ptr);
		}
	}
	return stream_write(args->dest, "null", 4);
}

static int pointer_unmarshal(void * _ctx, void * dest, json_value * src) {
	ptr_ctx_t * ctx = (ptr_ctx_t*) _ctx;
	void ** ptr = (void **)dest;

	if (dest == NULL) {
		return -1;
	}

	if (src->type == json_null) {
		*ptr = NULL;
		return 0;
	}

	*ptr = malloc(ctx->size);
	memset(*ptr, 0, ctx->size);
	return json_unmarshal_ex(ctx->field, *ptr, src);
}

json_t * json_pointer_new(size_t size, json_t * field) {
	json_t * j = malloc(sizeof(json_t));

	ptr_ctx_t * ctx = malloc(sizeof(ptr_ctx_t));
	ctx->size  = size;
	ctx->field = field;

	j->type       = type();
	j->is_pointer = true;
	j->ctx        = ctx;
	j->marshal    = pointer_marshal;
	j->unmarshal  = pointer_unmarshal;

	return j;
}
