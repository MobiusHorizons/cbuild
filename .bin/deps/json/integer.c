#define _package_json_integer_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../json-parser/json.h"

#include "../stream/stream.h"
#include "json.h"

static int type() {
	static int t;
	if (t == 0) t = json_register("integer");
	return t;
}

typedef struct {
	unsigned char width; 
	bool is_signed;
} ctx_t;

long long json_integer_extract(const void * src, unsigned char width, bool is_signed) {

	unsigned long long int val = 0;
	memcpy(&val, src, width);
	
	if (is_signed) {
		long long int s = val;
		int offset = (sizeof(val) - width) * 8;
		s = (val << offset); s >>= offset;
		return s;
	} else {
		return val;
	}
}
static int int_marshal(void * _ctx, json_marshal_args * args, const void * src) {
	ctx_t ctx;
	memcpy(&ctx, &_ctx, sizeof(ctx));
	unsigned width = (ctx.width);

	unsigned long long int val = 0;
	memcpy(&val, src, width);

	if (ctx.is_signed) {
		long long int s = val;
		int offset = (sizeof(val) - width) * 8;
		s = (val << offset); s >>= offset;
		stream_printf(args->dest, "%ld", s);
	} else {
		stream_printf(args->dest, "%zd", val);
	}
	return 0;
}

static short size_required(long long value, bool is_signed) {
	long long v = value;
	if (v < 0) v = -1 * (v + 1);
	short size = (sizeof(value) * 8) - __builtin_clzll(v);
	return is_signed ? size + 1 : size;
}

int json_integer_insert(long long value, void * dest, unsigned char width, bool is_signed) {
	if (value < 0 && !is_signed) {
		printf("err: %lld signed into unsigned\n", value);
		return -1; // Error message for signed into unsigned
	}

	if (value != 0) {
		short required_width = size_required(value, is_signed);
		if (required_width > (width * 8)) {
			printf("err: overflow: %lld needs %d bits to hold, but we only have %d.\n", value, required_width, width * 8);
			return -1; // Error: overflow
		}
	}
	memcpy(dest, &value, width);
	return 0;
}

static int int_unmarshal(void * _ctx, void * dest, json_value * src) {
	ctx_t ctx;
	memcpy(&ctx, &_ctx, sizeof(ctx));

	if (src && src->type == json_integer) {
		long long value = src->u.integer;
		return json_integer_insert(value, dest, ctx.width, ctx.is_signed);
	}
	return 0;
}

json_t * json_integer_new(unsigned char width, bool is_signed) {
	json_t * j = malloc(sizeof(json_t));

	ctx_t ctx = {
		.width = width,
		.is_signed = is_signed,
	};
	memcpy(&j->ctx, &ctx, sizeof(ctx));

	j->type       = type();
	j->is_pointer = false;
	j->marshal    = int_marshal;
	j->unmarshal  = int_unmarshal;

	return j;
}

json_t * json_integer_uint8() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(1, 0);
	return r;
}

json_t * json_integer_uint16() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(2, 0);
	return r;
}

json_t * json_integer_uint32() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(4, 0);
	return r;
}

json_t * json_integer_uint64() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(8, 0);
	return r;
}

json_t * json_integer_int8() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(1, 1);
	return r;
}

json_t * json_integer_int16() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(2, 1);
	return r;
}

json_t * json_integer_int32() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(4, 1);
	return r;
}

json_t * json_integer_int64() {
	static json_t * r;
	if (r == NULL) r = json_integer_new(8, 1);
	return r;
}
