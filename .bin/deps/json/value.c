#define _package_json_value_

#include "../json-parser/json.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "../stream/stream.h"
#include "string.h"
#include "json.h"

static int type() {
	static int t;
	if (t == 0) t = json_register("value");
	return t;
}

typedef struct {
	void       * value;
	json_value * parsed;
	json_t     * type;
} json_value_t;


static int marshal_json_value(json_marshal_args * args, json_value * v) {
	stream_t * s = args->dest;
	bool pretty = args->pretty;

	unsigned int i;
	unsigned int length = 0;

	switch (v->type) {
		case json_null:
			return stream_write(s, "null", 4);
		case json_boolean:
			return stream_write(s, v->u.boolean ? "true" : "false", v->u.boolean ? 4 : 5);
		case json_integer:
			return stream_printf(s, "%ld", v->u.integer);
		case json_double:
			return stream_printf(s, "%lf", v->u.dbl);
		case json_string:
			return json_string_encode(v->u.string.length, s, v->u.string.ptr);
		case json_object:
			length = v->u.object.length;
			stream_write(s, "{\n", pretty ? 2 : 1);

			if (pretty) args->indent += args->indent_width;
			for (i = 0; i < length; i++) {
				json_object_entry entry = v->u.object.values[i];
				if (pretty) stream_printf(s, "%*s", args->indent, " ");
				json_string_encode(entry.name_length, s, entry.name);
				stream_write(s, ": ", pretty ? 2 : 1); 

				marshal_json_value(args, entry.value);
				if (i < length - 1) {
					stream_write(s, ",\n", pretty ? 2 : 1);
				} else if (pretty) {
					stream_write(s, "\n", 1);
				}
			}
			if (pretty) args->indent -= args->indent_width;

			return stream_printf(s, "%*.s}", args->indent, " ");
		case json_array:
			length = v->u.array.length;
			stream_write(s, "[\n", pretty ? 2 : 1);

			if (pretty) args->indent += args->indent_width;
			for (i = 0; i < length; i++) {
				stream_printf(s, "%*.s", args->indent, " ");
				marshal_json_value(args, v->u.array.values[i]);
				if (i < length - 1) {
					stream_write(s, ",\n", pretty ? 2 : 1);
				} else if (pretty) {
					stream_write(s, "\n", 1);
				}
			}
			if (pretty) args->indent -= args->indent_width;

			return stream_printf(s, "%*.s]", args->indent, " ");
	}
}

static int value_marshal(void *ctx, json_marshal_args * args, const void * src) {
	stream_t * s = args->dest;
	bool pretty = args->pretty;

	json_value_t * val = (json_value_t *)src;
	if (val == NULL || (val->parsed == NULL && val->value == NULL)) {
		return stream_write(s, "null", strlen("null"));
	}

	if (val->type && val->value) {
		return json_marshal_ex(val->type, args, val->value);
	}

	json_value * v = val->parsed;
	return marshal_json_value(args, v);
}

static int value_unmarshal(void * ctx, void * dest, json_value * src) {
	if (dest == NULL) return -1;

	json_value_t * v = (json_value_t *) dest; 
	v->parsed = src;

	if (v->type && v->value) {
		return json_unmarshal_ex(v->type, v->value, v->parsed);
	} else {
		v->value = v->parsed;
		v->type = NULL;
	}

	return 0;
}


json_t * json_value_j() {
	static json_t v;
	if (v.type == 0) {
		v.type       = type();
		v.ctx        = NULL;
		v.is_pointer = true;
		v.marshal    = value_marshal;
		v.unmarshal  = value_unmarshal;
	}
	return &v;
}

json_value_t json_value_from(json_t * type, void * value) {
	json_value_t v = {
		.type = type,
		.parsed = NULL,
		.value = value,
	};
	
	return v;
}

json_value_t json_value_null() {
	json_value_t v = {0};
	return v;
}
