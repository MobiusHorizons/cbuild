#ifndef _package_makefile_
#define _package_makefile_

#include "package/package.h"

char * makefile_get_target(package_t * pkg);
char * makefile_get_build_target(package_t * pkg);
int makefile_make(package_t * pkg, char * makefile);
int makefile_run(package_t * pkg, char * makefile, const char * target);
int makefile_clean(package_t * pkg, char * makefile);
char * makefile_get_makefile_name(const char * path);
char * makefile_write(package_t * pkg, const char * cbuild_exec);

#endif
