#define _package_main_

#include <string.h>
#include <stdio.h>

#include "deps/json/json.h"
#include "deps/stream/stream.h"
#include "deps/stream/file.h"
#include "deps/closure/closure.h"

#include "lsp/rpc/server.h"

#include "lsp/initialize.h"
#include "lsp/completion.h"
#include "lsp/definition.h"
#include "lsp/text-document.h"
#include "lsp/state.h"

int main(int argc, char ** argv) {
	json_rpc_server_debug_t dbg = {};

	if (argc > 1) {
		char * dir = argv[1];
		size_t len = strlen(dir) + strlen("/cbuild-lsp-output") + 1;
		char * buf = malloc(len);

		snprintf(buf, len, "%s/cbuild-lsp-input", dir);
		dbg.input = file_open(buf, O_WRONLY|O_CREAT);
		if (dbg.input->error.code) {
			fprintf(stderr, "Error opening file '%s': %s", 
					buf, dbg.input->error.message);
			return 1;
		}
		
		snprintf(buf, len, "%s/cbuild-lsp-output", dir);
		dbg.output = file_open(buf, O_WRONLY|O_CREAT);
		if (dbg.output->error.code) {
			fprintf(stderr, "Error opening file '%s': %s", 
					buf, dbg.output->error.message);
			return 1;
		}
	}

	stream_t * in  = file_new(0);
	stream_t * out = file_new(1);
	stream_t * err = file_new(2);

	json_rpc_server_t * lsp = json_rpc_server_new(in, out, dbg);

	state_t s = {
		.lsp = lsp, 
		.err = err,
	};

	json_rpc_server_register(lsp,
		"initialize",
		closure_new(initialize_handler, &s)
	);

	json_rpc_server_register(lsp,
		"initialized",
		closure_new(json_rpc_server_ignore, &s)
	);

	json_rpc_server_register(lsp, 
		"textDocument/didOpen",
		closure_new(text_document_open, &s)
	);

	json_rpc_server_register(lsp, 
		"textDocument/didChange",
		closure_new(text_document_change, &s)
	);

	json_rpc_server_register(lsp,
		"textDocument/didClose",
		closure_new(json_rpc_server_ignore, &s)
	);

	json_rpc_server_register(lsp,
		"textDocument/completion",
		closure_new(completion_handler, &s)
	);

	json_rpc_server_register(lsp,
		"textDocument/definition",
		closure_new(definition_handler, &s)
	);

	return json_rpc_server_listen(lsp);
}
