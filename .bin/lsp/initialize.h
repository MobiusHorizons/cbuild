#ifndef _package_initialize_
#define _package_initialize_

#include "../deps/json/json.h"
#include "types/server-capabilities.h"

typedef struct {
	server_capabilities_t capabilities;
	server_capabilities_server_info_t server_info;
} initialize_results_t;

json_t * initialize_results_j();
void * initialize_handler(void * _ctx, void * _arg);

#endif
