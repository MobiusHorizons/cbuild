#ifndef _package_text_document_
#define _package_text_document_

void * text_document_open(void * _ctx, void * _arg);
void * text_document_change(void * _ctx, void * _arg);

#endif
