#define _package_json_rpc_server_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../deps/json/json.h"
#include "../../deps/json/value.h"
#include "../../deps/json/string.h"
#include "../../deps/stream/stream.h"
#include "../../deps/stream/file.h"
#include "../../deps/closure/closure.h"
#include "tap-stream.h"


#include "../../deps/hash/hash.h"

#include "decode.h"
#include "request_msg.h"
#include "response_msg.h"
#include "encode.h"

typedef struct {
	closure_t handler;
	char method [];
} json_rpc_server_handler_t;

typedef struct {
	stream_t * input;
	stream_t * output;
} json_rpc_server_debug_t;

typedef struct {
	stream_t * input;
	stream_t * output;
	rpc_encode_t * encoder;
	json_rpc_server_debug_t debug;
	hash_t * handlers;
	json_rpc_server_handler_t * default_handler;
	closure_t on_message;
	closure_t on_error;
	long int id;
} json_rpc_server_t;

int json_rpc_server_notify(json_rpc_server_t * server, char * method, json_value_t params) {
	rpc_request_msg_t req = {
		.jsonrpc = "2.0",
		.method = method,
		.params = params,
		.id = json_value_null(),
	};

	json_marshal(rpc_request_msg_j(), rpc_encode_stream(server->encoder), &req);
	rpc_encode_send(server->encoder);
}

static int respond(json_rpc_server_t * server, rpc_response_t * resp) {
	json_marshal(rpc_response_j(), rpc_encode_stream(server->encoder), resp);
	return rpc_encode_send(server->encoder);
}

typedef struct {
	rpc_request_msg_t req;
	rpc_response_t res;
	json_rpc_server_t * server;
} json_rpc_server_handler_args_t;

static void * on_message(void * ctx, void * arg) {
	json_rpc_server_t * server = (json_rpc_server_t *) ctx;

	const char * message = (const char *) arg;
	json_rpc_server_handler_args_t args = {
		.res = {
			.jsonrpc = "2.0",
			.error   = NULL,
			.result  = json_value_null(),
		},
		.server = server, 
	};

	int e = json_unmarshal(rpc_request_msg_j(), &args.req, message, strlen(message));
	if (e == -1) return NULL;
	args.res.id = args.req.id;
	
	json_rpc_server_handler_t * h = server->default_handler;
	h = hash_get(server->handlers, args.req.method ?: "");
	if (h == NULL) {
		char * m = NULL;
		asprintf(&m, "Method not found '%s'", args.req.method);
		args.res.error = rpc_response_error(-32601, "Method not found", json_value_from(
					json_string_new(0),
					&args.req.method
		));
		respond(server, &args.res);
		free(arg);
		return NULL;
	} 
	closure_call(h->handler, &args);
	free(arg);
	return NULL;
}

static void * on_error(void * _ctx, void * _arg) {
	/*printf("ERROR: %s", _arg);*/
	return NULL;
}

json_rpc_server_t * json_rpc_server_new(stream_t * input, stream_t * output, json_rpc_server_debug_t debug) {
	json_rpc_server_t * server = malloc(sizeof(json_rpc_server_t));

	if (debug.input)  input  = tap_stream_all(input, debug.input);
	if (debug.output) output = tap_stream_all(output, debug.output);

	server->input      = input;
	server->output     = output;
	server->encoder    = rpc_encode_new(output);
	server->handlers   = hash_new();
	server->on_message = closure_new(on_message, server);
	server->on_error   = closure_new(on_error, server);

	return server;
}

int json_rpc_server_register(json_rpc_server_t * server, const char * method, closure_t handler) {
	json_rpc_server_handler_t * h = malloc(sizeof(*h) + sizeof(char[strlen(method) + 1 ]));
	h->handler = handler;
	strcpy(h->method, method);
	hash_set(server->handlers, h->method, h);

	return 1;
}

int json_rpc_server_register_default(json_rpc_server_t * server, closure_t handler) {
	const char * method = "default";
	json_rpc_server_handler_t * h = malloc(sizeof(*h) + sizeof(char[strlen(method) + 1 ]));
	h->handler = handler;
	strcpy(h->method, "default");
	server->default_handler = h;

	return 1;
}

int json_rpc_server_listen(json_rpc_server_t * server) {
	rpc_decode_t * d = rpc_decode_new(server->input, server->on_message, server->on_error);
	return rpc_decode_listen(d);
}

int json_rpc_server_error(json_rpc_server_handler_args_t * ctx, long long code, const char * message, json_value_t data) {
	ctx->res.error = rpc_response_error(code, message, data);
	ctx->res.result = json_value_null();
	return respond(ctx->server, &ctx->res);
}

int json_rpc_server_response(json_rpc_server_handler_args_t * ctx, json_value_t result) {
	ctx->res.result = result;
	return respond(ctx->server, &ctx->res);
}

void * json_rpc_server_ignore(void * _ctx, void * _arg) {
	return NULL;
}
