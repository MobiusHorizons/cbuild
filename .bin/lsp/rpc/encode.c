#define _package_rpc_encode_

#include <stdlib.h>

#include "../../deps/stream/stream.h"
#include "../../utils/string-stream.h"

typedef struct {
	stream_t * out;
	stream_t * buffer;
} rpc_encode_t; 

rpc_encode_t * rpc_encode_new(stream_t * out) {
	rpc_encode_t * e = malloc(sizeof(rpc_encode_t));

	e->out    = out;
	e->buffer = string_stream_new_writer(NULL);

	return e;
}

stream_t * rpc_encode_stream(rpc_encode_t * e) {
	return e->buffer;
}

int rpc_encode_send(rpc_encode_t * e) {
	size_t length = string_stream_get_buffer_length(e->buffer);
	char * buf = string_stream_get_buffer(e->buffer);

	stream_printf(e->out, "Content-Length: %ld\r\n\r\n", length);
	stream_write(e->out, buf, length);

	stream_close(e->buffer);
	e->buffer = string_stream_new_writer(NULL);

	return length;
}
