#ifndef _package_process_stream_
#define _package_process_stream_

#include "../../deps/stream/stream.h"

int process_stream_type();
stream_t * process_stream_new(const char * executable, char * const * argv);

#endif
