#ifndef _package_json_rpc_server_
#define _package_json_rpc_server_

#include "request_msg.h"
#include "../../deps/closure/closure.h"
#include "../../deps/stream/stream.h"
#include "encode.h"
#include "../../deps/json/value.h"
#include "response_msg.h"
#include "../../deps/hash/hash.h"

typedef struct {
	closure_t handler;
	char method [];
} json_rpc_server_handler_t;

typedef struct {
	stream_t * input;
	stream_t * output;
} json_rpc_server_debug_t;

typedef struct {
	stream_t * input;
	stream_t * output;
	rpc_encode_t * encoder;
	json_rpc_server_debug_t debug;
	hash_t * handlers;
	json_rpc_server_handler_t * default_handler;
	closure_t on_message;
	closure_t on_error;
	long int id;
} json_rpc_server_t;

int json_rpc_server_notify(json_rpc_server_t * server, char * method, json_value_t params);

typedef struct {
	rpc_request_msg_t req;
	rpc_response_t res;
	json_rpc_server_t * server;
} json_rpc_server_handler_args_t;

json_rpc_server_t * json_rpc_server_new(stream_t * input, stream_t * output, json_rpc_server_debug_t debug);
int json_rpc_server_register(json_rpc_server_t * server, const char * method, closure_t handler);
int json_rpc_server_register_default(json_rpc_server_t * server, closure_t handler);
int json_rpc_server_listen(json_rpc_server_t * server);
int json_rpc_server_error(json_rpc_server_handler_args_t * ctx, long long code, const char * message, json_value_t data);
int json_rpc_server_response(json_rpc_server_handler_args_t * ctx, json_value_t result);
void * json_rpc_server_ignore(void * _ctx, void * _arg);

#endif
