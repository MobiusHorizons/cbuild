#ifndef _package_json_rpc_client_
#define _package_json_rpc_client_

#include "response_msg.h"
#include "../../deps/stream/stream.h"
#include "encode.h"
#include "request_msg.h"
#include "../../deps/closure/closure.h"
#include "decode.h"
#include "../../deps/json/value.h"
#include "../../deps/hash/hash.h"

typedef struct {
	stream_t * input;
	stream_t * output;
	stream_t * log;

	rpc_encode_t * encoder;
	rpc_decode_t * decoder;
	closure_t on_message;
	closure_t on_error;
	rpc_response_t  message;
	rpc_request_msg_t  notification;
	uint32_t index;
	long int id;
} json_rpc_client_t;

int json_rpc_client_next(json_rpc_client_t * client);
rpc_response_t json_rpc_client_request(json_rpc_client_t * client, char * method, json_value_t params);
json_rpc_client_t * json_rpc_client_new(stream_t * input, stream_t * output);

#endif
