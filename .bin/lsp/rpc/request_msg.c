#define _package_rpc_request_msg_

#include "../../deps/json/json.h"
#include "../../deps/json/struct.h"
#include "../../deps/json/string.h"
#include "../../deps/json/integer.h"
#include "../../deps/json/value.h"

typedef struct {
	char       * jsonrpc;
	char       * method;
	json_value_t      params;
	json_value_t      id;
} rpc_request_msg_t;

json_t * rpc_request_msg_j() {
	static json_t * r;
	if (r == NULL) {
		r = json_struct_new();
		json_struct_field(r, "jsonrpc", offsetof(rpc_request_msg_t, jsonrpc), json_string_new(0));
		json_struct_field(r, "method", offsetof(rpc_request_msg_t, method), json_string_new(0));
		json_struct_field(r, "params", offsetof(rpc_request_msg_t, params), json_value_j());
		json_struct_field(r, "id", offsetof(rpc_request_msg_t, id), json_value_j());
	}
	return r;
}
