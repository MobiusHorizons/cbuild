#ifndef _package_rpc_encode_
#define _package_rpc_encode_

#include "../../deps/stream/stream.h"

typedef struct {
	stream_t * out;
	stream_t * buffer;
} rpc_encode_t;

rpc_encode_t * rpc_encode_new(stream_t * out);
stream_t * rpc_encode_stream(rpc_encode_t * e);
int rpc_encode_send(rpc_encode_t * e);

#endif
