#define _package_rpc_decode_



#define BUFSIZE 4096

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdbool.h>

#include "../../deps/stream/stream.h"
#include "../../deps/closure/closure.h"

typedef struct {
	stream_t * input;
	char * buffer;
	char * cursor;
	size_t length;
	size_t buffer_length;
	closure_t on_message;
	closure_t on_error;
} rpc_decode_t;

typedef struct {
	char * start;
	size_t length;
} substring_t;

static int next_page(rpc_decode_t * s) {
	size_t offset = s->length - (s->cursor - s->buffer);
	memmove(s->buffer, s->cursor, offset); // move bytes to the beginning.
	s->cursor = s->buffer;
	s->length = offset;
	size_t read = stream_read(s->input, s->cursor + offset, s->buffer_length - s->length);
	if (read < 0) {
		// onError;
		return 0;
	} else if (read == 0) {
		return 0; // end but not an error.
	} 

	s->length += read;
	return 1;
}

static substring_t next_header(rpc_decode_t * s) {
	char * end = NULL;
	do {
		end = memmem(s->cursor, s->length - (s->cursor - s->buffer), "\r\n", 2);
		if (end == NULL) {
			if (!next_page(s)) return (substring_t){0};
		}
	} while (end == NULL);
	substring_t header = {
		.start  = s->cursor,
		.length = end - s->cursor,
	};
	s->cursor = end + 2;
	return header;
}

static int handle_body(rpc_decode_t * s, size_t length) {
	char * message = malloc(length + 1);
	message[length] = 0;

	size_t remaining_buffer = s->length - (s->cursor - s->buffer);
	size_t from_buffer = remaining_buffer >= length ? length : remaining_buffer;

	if (memcpy(message, s->cursor, from_buffer) == NULL) {
		perror("error copying memory");
		return 1;
	}
	s->cursor += from_buffer;
	size_t got = from_buffer;

	while (got < length) {
		size_t read = stream_read(s->input, message + got, length - got);
		if (read < 0) {
			return 3;
		}
		got += read;
	}
	closure_call(s->on_message, message);
	return 0;
}


rpc_decode_t * rpc_decode_new(stream_t * input, closure_t on_message, closure_t on_error) {
	rpc_decode_t * s = malloc(sizeof(rpc_decode_t) + BUFSIZE);
	s->input = input;
	s->buffer = (char *)s + sizeof(rpc_decode_t);
	s->buffer_length = BUFSIZE;
	s->length = 0;
	s->cursor = s->buffer;
	s->on_message = on_message;
	s->on_error = on_error;

	return s;
}

int rpc_decode_next(rpc_decode_t * s) {
	size_t content_length = 0;
	while(true) {
		substring_t header = next_header(s);

		if (header.length == 0){
			if (content_length == 0) {
				closure_call(s->on_error, "Expected Content-Length:");
				return 1;
			}
			return handle_body(s, content_length);
		}
		if (header.length < strlen("content-length:")) continue; // ignore headers other than content-length
		if (strncasecmp(header.start, "content-length:", strlen("content-length:")) == 0) {
			content_length = strtoul(header.start + strlen("content-length:"), NULL, 10);
		}
	}
}

int rpc_decode_listen(rpc_decode_t * s) {
	int e = 0;

	while((e = rpc_decode_next(s)) == 0);

	return e;
}
