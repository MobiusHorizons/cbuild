#ifndef _package_rpc_request_msg_
#define _package_rpc_request_msg_

#include "../../deps/json/value.h"
#include "../../deps/json/json.h"

typedef struct {
	char       * jsonrpc;
	char       * method;
	json_value_t      params;
	json_value_t      id;
} rpc_request_msg_t;

json_t * rpc_request_msg_j();

#endif
