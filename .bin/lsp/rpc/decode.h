#ifndef _package_rpc_decode_
#define _package_rpc_decode_

#include "../../deps/stream/stream.h"
#include "../../deps/closure/closure.h"

typedef struct {
	stream_t * input;
	char * buffer;
	char * cursor;
	size_t length;
	size_t buffer_length;
	closure_t on_message;
	closure_t on_error;
} rpc_decode_t;

rpc_decode_t * rpc_decode_new(stream_t * input, closure_t on_message, closure_t on_error);
int rpc_decode_next(rpc_decode_t * s);
int rpc_decode_listen(rpc_decode_t * s);

#endif
