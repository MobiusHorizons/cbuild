#ifndef _package_tap_stream_
#define _package_tap_stream_

#include "../../deps/stream/stream.h"

int tap_stream_type();
stream_t * tap_stream_input(stream_t * source, stream_t * dest);
stream_t * tap_stream_output(stream_t * source, stream_t * dest);
stream_t * tap_stream_all(stream_t * source, stream_t * dest);

#endif
