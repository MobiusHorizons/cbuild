#define _package_json_rpc_client_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../../deps/json/json.h"
#include "../../deps/json/value.h"
#include "../../deps/json/integer.h"
#include "../../deps/stream/stream.h"
#include "../../deps/stream/file.h"
#include "../../deps/closure/closure.h"


#include "../../deps/hash/hash.h"

#include "request_msg.h"
#include "response_msg.h"
#include "decode.h"
#include "encode.h"

typedef struct {
	stream_t * input;
	stream_t * output;
	stream_t * log;

	rpc_encode_t * encoder;
	rpc_decode_t * decoder;
	closure_t on_message;
	closure_t on_error;
	rpc_response_t  message;
	rpc_request_msg_t  notification;
	uint32_t index;
	long int id;
} json_rpc_client_t;

int json_rpc_client_next(json_rpc_client_t * client) {
	return rpc_decode_next(client->decoder);
}

rpc_response_t json_rpc_client_request(json_rpc_client_t * client, char * method, json_value_t params) {
	client->index ++;
	rpc_request_msg_t req = {
		.jsonrpc = "2.0",
		.method = method,
		.params = params,
		.id = json_value_from(json_integer_uint32(), &client->index),
	};

	json_marshal(rpc_request_msg_j(), rpc_encode_stream(client->encoder), &req);
	rpc_encode_send(client->encoder);
	json_rpc_client_next(client);

	return client->message;
}

static int respond(json_rpc_client_t * client, rpc_response_t * resp) {
	json_marshal(rpc_response_j(), rpc_encode_stream(client->encoder), resp);
	return rpc_encode_send(client->encoder);
}

static void * on_message(void * ctx, void * arg) {
	json_rpc_client_t * client = (json_rpc_client_t *) ctx;

	const char * message = (const char *) arg;
	stream_printf(client->log, "\n==> %s\n", message);

	json_value_t m = {0};
	json_unmarshal(json_value_j(), &m, message, strlen(message));

	rpc_response_t  res = {0};
	json_unmarshal_ex(rpc_response_j(), &res, m.parsed);

	rpc_request_msg_t  req = {0};
	json_unmarshal_ex(rpc_request_msg_j(), &req, m.parsed);

	if (req.method != NULL) { 
		client->notification = req;
	} else if (json_is_null(json_value_j(), &res.id)) {
		client->message = res;
	}

	free(arg);
	return NULL;
}

static void * on_error(void * _ctx, void * _arg) {
	fprintf(stderr, "ERROR: %s", _arg);
	return NULL;
}

json_rpc_client_t * json_rpc_client_new(stream_t * input, stream_t * output) {
	json_rpc_client_t * client = malloc(sizeof(json_rpc_client_t));

	client->input      = input;
	client->output     = output;
	client->encoder    = rpc_encode_new(output);
	client->log        = file_new(2);
	client->on_message = closure_new(on_message, client);
	client->on_error   = closure_new(on_error, client);
	client->decoder    = rpc_decode_new(input, client->on_message, client->on_error);
	client->index      = 0;

	return client;
}
