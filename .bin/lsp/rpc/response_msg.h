#ifndef _package_rpc_response_
#define _package_rpc_response_

#include "../../deps/json/value.h"
#include "../../deps/json/json.h"

typedef struct {
	long long    code;
	char       * message;
	json_value_t      data;
} rpc_response_error_t;

json_t * rpc_response_error_j();

typedef struct {
	char       * jsonrpc;
	json_value_t      result;
	rpc_response_error_t    * error;
	json_value_t      id;
} rpc_response_t;

json_t * rpc_response_j();
rpc_response_error_t * rpc_response_error(long long code, const char * message, json_value_t data);

#endif
