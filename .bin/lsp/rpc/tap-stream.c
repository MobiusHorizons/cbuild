#define _package_tap_stream_
#include "../../deps/stream/stream.h"

static int _type;

int tap_stream_type() {
  if (_type == 0) {
    _type = stream_register("tap");
  }

  return _type;
}

typedef struct {
	/** The log stream. */
	stream_t * dest;

	/** The real input/output stream. */
	stream_t * source;
} context;


static ssize_t tap_read(void * _ctx, void * buf, size_t nbyte, stream_error_t * error) {
  context * ctx = (context *) _ctx;
  stream_t * s = ctx->source;
  ssize_t len = s->read(s->ctx, buf, nbyte, &s->error);

  if (len) stream_write(ctx->dest, buf, len);

  return len;
}

static ssize_t tap_write(void * _ctx, void * buf, size_t nbyte, stream_error_t * error) {
  context * ctx = (context *) _ctx;
  stream_t * s = ctx->source;

  ssize_t len = s->write(s->ctx, buf, nbyte, &s->error);
  if (len) stream_write(ctx->dest, buf, len);

  return len;
}

static ssize_t tap_close(void * _ctx, stream_error_t * error) {
	context * ctx = (context *) _ctx;
	stream_t * s = ctx->source;

	stream_close(ctx->dest);
	ssize_t e = s->close(s->ctx, &s->error);
	if (e != -1) {
		free(ctx);
		free(s);
	}
	return e;
}

stream_t * tap_stream_input(stream_t * source, stream_t * dest) {
	context * ctx = (context *) malloc(sizeof(context));
	ctx->source = source;
	ctx->dest = dest;

	stream_t * s = malloc(sizeof(stream_t));

	s->ctx   = ctx;
	s->read  = tap_read;
	s->write = NULL;
	s->pipe  = NULL;
	s->close = tap_close;
	s->type  = tap_stream_type();

	s->error.code    = 0;
	s->error.message = NULL;

	return s;
}

stream_t * tap_stream_output(stream_t * source, stream_t * dest) {
	context * ctx = (context *) malloc(sizeof(context));
	ctx->source = source;
	ctx->dest = dest;

	stream_t * s = malloc(sizeof(stream_t));

	s->ctx   = ctx;
	s->read  = NULL;
	s->write = tap_write;
	s->pipe  = NULL;
	s->close = tap_close;
	s->type  = tap_stream_type();

	s->error.code    = 0;
	s->error.message = NULL;

	return s;
}

stream_t * tap_stream_all(stream_t * source, stream_t * dest) {
	context * ctx = (context *) malloc(sizeof(context));
	ctx->source = source;
	ctx->dest = dest;

	stream_t * s = malloc(sizeof(stream_t));

	s->ctx   = ctx;
	s->read  = tap_read;
	s->write = tap_write;
	s->pipe  = NULL;
	s->close = tap_close;
	s->type  = tap_stream_type();

	s->error.code    = 0;
	s->error.message = NULL;

	return s;
}
