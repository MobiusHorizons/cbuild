#define _package_rpc_response_

#include "../../deps/json/json.h"
#include "../../deps/json/struct.h"
#include "../../deps/json/string.h"
#include "../../deps/json/integer.h"
#include "../../deps/json/value.h"
#include "../../deps/json/pointer.h"

typedef struct {
	long long    code;
	char       * message;
	json_value_t      data;
} rpc_response_error_t;

json_t * rpc_response_error_j() {
	static json_t * r;
	if (r == NULL) {
		r = json_struct_new();
		json_struct_field(r, "code", offsetof(rpc_response_error_t, code), json_integer_int64());
		json_struct_field(r, "message", offsetof(rpc_response_error_t, message), json_string_new(0));
		json_struct_field(r, "data", offsetof(rpc_response_error_t, data), json_value_j());
	}
	return r;
}

typedef struct {
	char       * jsonrpc;
	json_value_t      result;
	rpc_response_error_t    * error;
	json_value_t      id;
} rpc_response_t;

json_t * rpc_response_j() {
	static json_t * r;
	if (r == NULL) {
		r = json_struct_new();
		json_struct_field(r, "jsonrpc", offsetof(rpc_response_t, jsonrpc), json_string_new(0));
		json_struct_field(r, "result", offsetof(rpc_response_t, result), json_value_j());
		json_struct_field(r,
			"error",
			offsetof(rpc_response_t, error),
			json_pointer_new(sizeof(rpc_response_error_t), rpc_response_error_j())
		);
		json_struct_field(r, "id", offsetof(rpc_response_t, id), json_value_j());
	}
	return r;
}

rpc_response_error_t * rpc_response_error(long long code, const char * message, json_value_t data) {
	rpc_response_error_t * e = malloc(sizeof(rpc_response_error_t));

	e->code    = code;
	e->message = message;
	e->data    = data;

	return e;
}
