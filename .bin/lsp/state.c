#define _package_state_
#include "../package/package.h"
#include "../deps/stream/stream.h"

#include "types/diagnostic.h"
#include "rpc/client.h"
#include "rpc/server.h"

typedef struct {
	json_rpc_server_t     * lsp;
	stream_t     * err;
	stream_t     * log;
	char         * input_buffer;
	char         * compiled;
	//client.t     * clangd;
	diagnostic_t   diagnostics;
	char * root_uri;
	package_options_t opts;
	package_t    * pkg;
} state_t;
