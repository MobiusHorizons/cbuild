#define _package_text_document_

#include <stdio.h>
#include <string.h>

#include "../deps/json/json.h"
#include "../deps/json/struct.h"
#include "../deps/json/array.h"
#include "../deps/json/value.h"

#include "rpc/server.h"
#include "rpc/client.h"
#include "rpc/request_msg.h"
#include "rpc/response_msg.h"

#include "types/diagnostic.h"
#include "types/text-document.h"

#include "compile.h"
#include "state.h"

typedef struct {
	lsp_text_document_t doc;
} open_params_t;

static json_t * open_params_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "textDocument", offsetof(open_params_t, doc), lsp_text_document_j());
	}

	return j;
}

void * text_document_open(void * _ctx, void * _arg) {
	state_t * ctx = (state_t*) _ctx;
	json_rpc_server_handler_args_t * args = (json_rpc_server_handler_args_t*)_arg;

	open_params_t params = {0};
	json_unmarshal_ex(open_params_j(), &params, args->req.params.parsed);

	char * generated = compile_check(params.doc.uri, params.doc.text, ctx);

	if (generated) {
		char * uri = malloc(strlen("file://") + strlen(generated) + 1);
		sprintf(uri, "file://%s", generated);

		open_params_t p = {
			.doc = {
				.uri        = params.doc.uri,
				.languageId = "c",
				.version    = params.doc.version,
				.text       = ctx->compiled,
			},
		};

		//json.marshal_pretty(open_params_j(), ctx->log, &p, 4);
	}
	//client.request(ctx->clangd, "textDocument/didOpen", any.from(open_params_j(), &p));

#if 0
	rpc_request_msg_t n = ctx->clangd->notification;
	if (
		n.method && 
		strcmp(n.method , "textDocument/publishDiagnostics") == 0
	) {
		diagnostic_publish_diagnostic_t diagnostic = {0};
		json_unmarshal_ex(diagnostic_publish_diagnostic_j(), &diagnostic, n.params.parsed);
		/*diagnostic.uri = params.doc.uri;*/

		json_rpc_server_notify(ctx->lsp, "textDocument/publishDiagnostics", n.params);
		/*any.from(*/
			/*Diagnostic.publish_diagnostic_j(),*/
			/*&diagnostic*/
		/*));*/
	}
#endif
	return NULL;
}

typedef struct {
	lsp_text_document_t doc;
	lsp_text_document_change_t * changes;
	unsigned long count;
} change_params_t;

static json_t * change_params_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "contentChanges", 
				offsetof(change_params_t, changes),
				json_array_with_length(
					sizeof(lsp_text_document_change_t), lsp_text_document_change_j(),
					offsetof(change_params_t, changes) -
					offsetof(change_params_t, count),
					sizeof(unsigned long), false
					)
				);
		json_struct_field(j, "textDocument", offsetof(change_params_t, doc),lsp_text_document_j());
	}

	return j;
}

void * text_document_change(void * _ctx, void * _arg) {
	state_t * ctx = (state_t*) _ctx;
	json_rpc_server_handler_args_t * args = (json_rpc_server_handler_args_t*)_arg;

	change_params_t params = {0};
	json_unmarshal_ex(change_params_j(), &params, args->req.params.parsed);

	compile_check(params.doc.uri, params.changes[0].text, ctx);

#if 0
	change_params_t p = {
		.doc = {
			.uri        = params.doc.uri,
			.languageId = "c",
			.version    = params.doc.version,
		},
		.count = 1,
	};
	p.changes = calloc(sizeof(lsp_text_document_change_t), 1);
	p.changes[0].text = ctx->compiled;

#
	json.marshal_pretty(change_params_j(), ctx->log, &p, 4);
	json_rpc_client_request(ctx->clangd, "textDocument/didChange", json_value_from(change_params_j(), &p));

	//Req.t n = ctx->clangd->notification;
	if (
		n.method && 
		strcmp(n.method , "textDocument/publishDiagnostics") == 0
	) {
		diagnostic_publish_diagnostic_t diagnostic = {0};
		json_unmarshal_ex(diagnostic_publish_diagnostic_j(), &diagnostic, n.params.parsed);
		diagnostic.uri = params.doc.uri;

		//server.notify(ctx->lsp, "textDocument/publishDiagnostics", n.params);
		json_rpc_server_notify(ctx->lsp, "textDocument/publishDiagnostics",
		json_value_from(
			diagnostic_publish_diagnostic_j(),
			&diagnostic
		));
	}
#endif
	return NULL;
}
