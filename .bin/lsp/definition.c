#define _package_definition_
#include <ctype.h>
#include <stdio.h>

#include "../deps/stream/stream.h"

#include "../deps/json/json.h"
#include "../deps/json/array.h"
#include "../deps/json/value.h"

#include "../package/package.h"
#include "../package/import.h"
#include "../package/export.h"
#include "../lexer/cache.h"
#include "../lexer/stack.h"
#include "../lexer/item.h"

#include "rpc/server.h"

#include "types/text-document.h"

#include "state.h"

#define token_range(T) { .line = T->line, .character = T->start - T->line_pos }

struct location_array_t {
	lsp_text_document_location_t * values;
	uint8_t length;
};

static json_t * location_array_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_array_with_length(
			sizeof(lsp_text_document_location_t), lsp_text_document_location_j(), 
			offsetof(struct location_array_t, length) -
			offsetof(struct location_array_t, values),
			1, false
		);
	}

	return j;
}

static void * no_result(json_rpc_server_handler_args_t * args) {
	struct location_array_t resp = {0};
	json_rpc_server_response(args, json_value_from(location_array_j(), &resp));
	return NULL;
}

static void * result(
		json_rpc_server_handler_args_t * args,
		state_t * s,
		char * uri,
		lex_item_t * start,
		lex_item_t * end
) {

		lsp_text_document_location_t loc = {
			.uri = uri,
			.range = {
				.start = token_range(start),
				.end   = token_range(end),
			},
		};

		struct location_array_t resp = {
			.length = 1,
			.values = &loc,
		};

		stream_write(s->err, "I: ", 3);
		json_marshal(lsp_text_document_location_j(), s->err, &resp.values[0]);
		stream_write(s->err, "\n", 1);
	
		json_rpc_server_response(args, json_value_from(location_array_j(), &resp));
	return NULL;
}

static void * exp_result(
	json_rpc_server_handler_args_t * args,
	state_t * s,
	package_t * pkg,
	package_export_t * exp
) {
	size_t i = 0;
	lex_item_t * symbol = NULL;
	do {
		symbol = lex_item_stack_peek(exp->declaration, i++);
	} while(symbol && strcmp(symbol->value, exp->symbol) != 0);
	if (symbol == NULL) symbol = lex_item_stack_first(exp->declaration);

	lex_item_t * end   = lex_item_stack_last(exp->declaration);
	char * uri = malloc(strlen("file://") + strlen(pkg->source_abs) + 1);
	strcpy(uri, "file://");
	strcpy(uri + strlen("file://"), pkg->source_abs);
	result(args, s, uri, symbol, end);

	free(uri);
	return NULL;
}


void * definition_handler(void * _ctx, void * _arg) {
	state_t * s = (state_t*) _ctx;
	json_rpc_server_handler_args_t * args = (json_rpc_server_handler_args_t*)_arg;

	lsp_text_document_position_params_t params = {0};
	json_unmarshal_ex(lsp_text_document_position_params_j(), &params, args->req.params.parsed);

	package_t * p = s->pkg;
	if (!p) return no_result(args);

	lex_item_cache_token_context_t tok_ctx = {0};
	lex_item_t * tok = lex_item_cache_token_at(
		p->source_cache, 
		params.position.line,
		params.position.character,
		&tok_ctx
	);

	if (tok == NULL) {
		fprintf(stderr, "I: No token found at (%lld:%lld).\n", params.position.line + 1, params.position.character);
		return no_result(args);
	}

	if (tok->replaced_by == NULL) {
		fprintf(stderr, "I: '%s'(%lld:%lld) is not defined by cbuild.\n", tok->value, params.position.line + 1, params.position.character);
		return no_result(args);
	}

	lex_item_t * it;
	// <pkg>._<foo>_ case (cursor is somewhere in foo). 
	if (tok_ctx.token_no >= 2) {
		it = tok_ctx.line->items[tok_ctx.token_no - 1];
		//fprintf(stderr, "I: %s\n", item.to_string(it));
		if (it && it->type == item_symbol && it->value[0] == '.' && it->replaced_by == tok->replaced_by) {
			lex_item_t * pkg = tok_ctx.line->items[tok_ctx.token_no - 2];
			if (pkg && pkg->replaced_by == tok->replaced_by) {
				/**
				 * This means that the token belongs to a renamed symbol. 
				 *
				 * If pkg is also replaced by the same symbol then 
				 */
				fprintf(stderr, "I: Attempting to find definition of package symbol %s.%s\n", pkg->value, tok->value);
				package_import_t * imp = hash_get(p->deps, pkg->value);
				if (imp == NULL) return no_result(args);
				package_export_t * exp = hash_get(imp->pkg->exports, tok->value);
				return exp_result(args, s, imp->pkg, exp);
			}
		}
	}

	if (tok->type == item_id) {
		fprintf(stderr, "I: Attempting to find definition of local symbol '%s'\n", tok->value);

		// local symbol case 
		package_export_t * exp = hash_get(p->exports, tok->value);
		if (exp) return exp_result(args, s, p, exp);

		// local package case
		package_import_t * imp = hash_get(p->deps, tok->value);
		if (imp) {
			//TODO: track import symbols in the import definition.
		}
	}

	fprintf(stderr, "I: Cannot define %s (%lld:%lld)\n", lex_item_to_string(tok), params.position.line + 1, params.position.character);
	return no_result(args);
}
