#define _package_initialize_
#include <string.h>
#include <stdio.h>

#include "../deps/json/json.h"
#include "../deps/json/struct.h"
#include "../deps/json/array.h"
#include "../deps/json/value.h"
#include "../deps/stream/stream.h"
#include "../deps/stream/file.h"

#include "rpc/client.h"
#include "rpc/process-stream.h"
#include "rpc/server.h"
#include "rpc/request_msg.h"
#include "rpc/response_msg.h"

#include "types/server-capabilities.h"
#include "types/initialize-params.h"

#include "state.h"

typedef struct {
	server_capabilities_t capabilities;
	server_capabilities_server_info_t server_info;
} initialize_results_t;

json_t * initialize_results_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "capabilities",
			offsetof(initialize_results_t, capabilities),
			server_capabilities_j()
		);

		json_struct_field(j, "serverInfo",
			offsetof(initialize_results_t, server_info),
			server_capabilities_server_info_j()
		);
	}
	return j;
}

void * initialize_handler(void * _ctx, void * _arg) {
	state_t * ctx = (state_t*) _ctx;
	json_rpc_server_handler_args_t * args = (json_rpc_server_handler_args_t*)_arg;

	type_initialize_params_t params = {0};
	json_unmarshal_ex(type_initialize_params_j(), &params, args->req.params.parsed);


	ctx->opts.silent    = false;
	ctx->opts.force     = false;
	ctx->opts.verbosity = SILENT;

	if (params.root_uri && strncmp(params.root_uri, "file://", strlen("file://")) == 0) {
		size_t len = strlen(params.root_uri);
		int has_trailing_slash = params.root_uri[len -1] == '/';

		ctx->root_uri = malloc(len + 2);
		sprintf(ctx->root_uri, "%s%s", params.root_uri, has_trailing_slash ? "" : "/");
		ctx->opts.cwd = strdup(ctx->root_uri + strlen("file://"));
	} else if (params.root_path) {
		size_t len = strlen(params.root_uri);
		int has_trailing_slash = params.root_uri[len -1] == '/';

		ctx->root_uri = malloc(len + strlen("file://") + 2);
		sprintf(ctx->root_uri, "file://%s%s", params.root_path, has_trailing_slash ? "" : "/");
		ctx->opts.cwd = strdup(ctx->root_uri + strlen("file://"));
	}

	char * const clangd_args[] = { 
		"clangd",
		NULL,
	};

	server_capabilities_completion_options_t completion_opts = {
		.trigger_characters = ".",
	};

	initialize_results_t res = {
		.capabilities = {
			.completion_provider = &completion_opts,
			.text_document_sync = {
				.open_close = true,
				.change = text_document_sync_full,
			},
			.definition_provider = true,
		},
		.server_info = {
			.name = "cbuild",
		},
	};
	json_rpc_server_response(args, json_value_from(initialize_results_j(), &res));

	//stream.t * clangd = process.new("clangd", clangd_args);
	//ctx->clangd = client.new(clangd, clangd);

	//Resp.t resp = client.request(ctx->clangd, "initialize", args->req.params);
	//stream.t * log = file.open("/home/paul/clang-log.log", O_WRONLY | O_CREAT);
	//ctx->log = log;

//	server.response(args, resp.result);
}

