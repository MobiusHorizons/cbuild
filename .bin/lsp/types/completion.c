#define _package_completion_types_

#include "text-document.h"

#include "../../deps/json/json.h"
#include "../../deps/json/struct.h"
#include "../../deps/json/string.h"
#include "../../deps/json/integer.h"
#include "../../deps/json/boolean.h"
#include "../../deps/json/array.h"


#include <stdint.h>


enum completion_types_trigger_kind {
	trigger_none = 0,
	trigger_invoked = 1,
	trigger_character = 2,
	trigger_incomplete = 3,
};

typedef struct {
	uint8_t kind;
	char trigger;
} completion_types_context_t;

json_t * completion_types_context_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "triggerKind", offsetof(completion_types_context_t, kind), json_integer_uint8());
		json_struct_field(j, "triggerCharacter", offsetof(completion_types_context_t, trigger), json_string_char());
 	}

	return j;
}

typedef struct {
	completion_types_context_t context;
	lsp_text_document_t document;
	lsp_text_document_position_t position;
} completion_types_params_t;

json_t * completion_types_params_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "context",
			offsetof(completion_types_params_t, context),
			completion_types_context_j()
		);

		json_struct_field(j, "textDocument",
			offsetof(completion_types_params_t, document),
			lsp_text_document_j()
		);

		json_struct_field(j, "position",
			offsetof(completion_types_params_t, position),
			lsp_text_document_position_j()
		);
	}

	return j;
}

typedef struct {
	char * label;
	bool preselect;
} completion_types_item_t;

json_t * completion_types_item_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "label", offsetof(completion_types_item_t, label), json_string_new(0));
		json_struct_field(j, "preselect", offsetof(completion_types_item_t, preselect), json_boolean_new());
	}

	return j;
}

typedef struct {
	bool is_incomplete;
	completion_types_item_t * items;
	uint16_t length;
} completion_types_list_t;

json_t * completion_types_list_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "isIncomplete", offsetof(completion_types_list_t, is_incomplete), json_boolean_new());

		json_struct_field(j, "items", offsetof(completion_types_list_t, items),
			json_array_with_length(
				sizeof(completion_types_item_t), completion_types_item_j(),
				offsetof(completion_types_list_t, length) - offsetof(completion_types_list_t, items),
				2, false
			)
		);
	}

	return j;
}
