#ifndef _package_server_capabilities_
#define _package_server_capabilities_

#include "../../deps/json/json.h"

enum server_capabilities_text_document_sync_kind {
	text_document_sync_none = 0,
	text_document_sync_full = 1,
	text_document_sync_incremental = 2,
};

typedef struct {
	bool open_close;
	uint8_t change;
	bool will_save;
	bool will_save_wait_until;
	bool save;
} server_capabilities_text_document_sync_options_t;

json_t * server_capabilities_text_document_sync_options_j();

typedef struct {
	bool resolve_provider;
	char * trigger_characters;
} server_capabilities_completion_options_t;

json_t *	server_capabilities_completion_options_j();

typedef struct {
	bool hover_provider;
	bool definition_provider;
	bool references_provider;
	bool document_highlight_provider;
	bool document_symbol_provider;
	bool workspace_symbol_provider;
	server_capabilities_text_document_sync_options_t text_document_sync;
	server_capabilities_completion_options_t * completion_provider;
	/*signature_help_options * signature_help_provider;*/
} server_capabilities_t;

json_t * server_capabilities_j();

typedef struct {
	/** The name of the server  as defined by the server */
	char * name;

	/** The server's version as defined by the server. */
	char * version;
} server_capabilities_server_info_t;

json_t * server_capabilities_server_info_j();

#endif
