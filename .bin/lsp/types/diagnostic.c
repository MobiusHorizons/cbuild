#define _package_diagnostic_
#include <stdint.h>

#include "../../deps/json/json.h"
#include "../../deps/json/struct.h"
#include "../../deps/json/integer.h"
#include "../../deps/json/string.h"
#include "../../deps/json/array.h"

#include "text-document.h"

enum diagnostic_sevarity_t {
	error = 1,
	warning,
	information,
	hint,
};

typedef struct {
	lsp_text_document_location_t location;
	char * message;
} diagnostic_related_information_t;

json_t * diagnostic_related_information_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "location", offsetof(diagnostic_related_information_t, location), lsp_text_document_location_j());
		json_struct_field(j, "message", offsetof(diagnostic_related_information_t, message), json_string_new(0));
	}

	return j;
}

typedef struct {
	lsp_text_document_range_t range;
	uint16_t severity;
	char * code;
	char * source;
	char * message;
	diagnostic_related_information_t * related_information;
	uint16_t related_count;
} diagnostic_t;

json_t * diagnostic_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "range", offsetof(diagnostic_t, range), lsp_text_document_range_j());
		json_struct_field(j, "severity", offsetof(diagnostic_t, severity), json_integer_uint16());
		json_struct_field(j, "code", offsetof(diagnostic_t, code), json_string_new(0));
		json_struct_field(j, "source", offsetof(diagnostic_t, source), json_string_new(0));
		json_struct_field(j, "message", offsetof(diagnostic_t, message), json_string_new(0));
		json_struct_field(j, "relatedInformation", offsetof(diagnostic_t, related_information),
			json_array_with_length(
				sizeof(diagnostic_related_information_t),
				diagnostic_related_information_j(),
				offsetof(diagnostic_t, related_count) -
				offsetof(diagnostic_t, related_information),
				2, false
			)
		);
	}

	return j;
}

typedef struct {
	char * uri;
	diagnostic_t * diagnostics;
	uint16_t count;
} diagnostic_publish_diagnostic_t;

json_t * diagnostic_publish_diagnostic_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "uri", offsetof(diagnostic_publish_diagnostic_t, uri), json_string_new(0));
		json_struct_field(j, "diagnostics", offsetof(diagnostic_publish_diagnostic_t, diagnostics),
			json_array_with_length(
				sizeof(diagnostic_t), diagnostic_j(),
				offsetof(diagnostic_publish_diagnostic_t, count) -
				offsetof(diagnostic_publish_diagnostic_t, diagnostics),
				2, false
			)
		);
	}
	return j;
}
