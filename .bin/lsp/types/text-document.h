#ifndef _package_lsp_text_document_
#define _package_lsp_text_document_

#include "../../deps/json/json.h"

typedef struct {
	long long int line;
	long long int character;
} lsp_text_document_position_t;

json_t * lsp_text_document_position_j();

typedef struct {
	lsp_text_document_position_t start;
	lsp_text_document_position_t end;
} lsp_text_document_range_t;

json_t * lsp_text_document_range_j();

typedef struct {
	lsp_text_document_range_t * range;
	long long int range_length;
	char * text;
} lsp_text_document_change_t;

json_t * lsp_text_document_change_j();

typedef struct {
	char * uri;
	lsp_text_document_range_t range;
} lsp_text_document_location_t;

json_t * lsp_text_document_location_j();

typedef struct {
	char * text;
	char * uri;
	char * languageId;
	unsigned long long version;
} lsp_text_document_t;

json_t * lsp_text_document_j();

typedef struct {
	char * label;
	bool preselect;
} lsp_text_document_item_t;

json_t * lsp_text_document_item_j();

typedef struct {
	bool is_incomplete;
	lsp_text_document_item_t * items;
	uint16_t length;
} lsp_text_document_list_t;

json_t * lsp_text_document_list_j();

typedef struct {
	lsp_text_document_t document;
	lsp_text_document_position_t position;
} lsp_text_document_position_params_t;

json_t * lsp_text_document_position_params_j();

#endif
