#ifndef _package_diagnostic_
#define _package_diagnostic_

#include "../../deps/json/json.h"
#include "text-document.h"

enum diagnostic_sevarity_t {
	error = 1,
	warning,
	information,
	hint,
};

typedef struct {
	lsp_text_document_location_t location;
	char * message;
} diagnostic_related_information_t;

json_t * diagnostic_related_information_j();

typedef struct {
	lsp_text_document_range_t range;
	uint16_t severity;
	char * code;
	char * source;
	char * message;
	diagnostic_related_information_t * related_information;
	uint16_t related_count;
} diagnostic_t;

json_t * diagnostic_j();

typedef struct {
	char * uri;
	diagnostic_t * diagnostics;
	uint16_t count;
} diagnostic_publish_diagnostic_t;

json_t * diagnostic_publish_diagnostic_j();

#endif
