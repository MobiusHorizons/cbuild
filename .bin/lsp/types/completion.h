#ifndef _package_completion_types_
#define _package_completion_types_

#include "../../deps/json/json.h"
#include "text-document.h"

#include <stdint.h>

enum completion_types_trigger_kind {
	trigger_none = 0,
	trigger_invoked = 1,
	trigger_character = 2,
	trigger_incomplete = 3,
};

typedef struct {
	uint8_t kind;
	char trigger;
} completion_types_context_t;

json_t * completion_types_context_j();

typedef struct {
	completion_types_context_t context;
	lsp_text_document_t document;
	lsp_text_document_position_t position;
} completion_types_params_t;

json_t * completion_types_params_j();

typedef struct {
	char * label;
	bool preselect;
} completion_types_item_t;

json_t * completion_types_item_j();

typedef struct {
	bool is_incomplete;
	completion_types_item_t * items;
	uint16_t length;
} completion_types_list_t;

json_t * completion_types_list_j();

#endif
