#define _package_lsp_text_document_

#include "../../deps/json/json.h"
#include "../../deps/json/boolean.h"
#include "../../deps/json/array.h"
#include "../../deps/json/struct.h"
#include "../../deps/json/string.h"
#include "../../deps/json/integer.h"
#include "../../deps/json/value.h"
#include "../../deps/json/pointer.h"

typedef struct {
	long long int line;
	long long int character;
} lsp_text_document_position_t;

json_t * lsp_text_document_position_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "line", offsetof(lsp_text_document_position_t, line), json_integer_uint64());
		json_struct_field(j, "character", offsetof(lsp_text_document_position_t, character), json_integer_uint64());
	}
	return j;
}

typedef struct {
	lsp_text_document_position_t start;
	lsp_text_document_position_t end;
} lsp_text_document_range_t;

json_t * lsp_text_document_range_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "start", offsetof(lsp_text_document_range_t, start), lsp_text_document_position_j());
		json_struct_field(j, "end", offsetof(lsp_text_document_range_t, end), lsp_text_document_position_j());
	}
	return j;
}

typedef struct {
	lsp_text_document_range_t * range;
	long long int range_length;
	char * text;
} lsp_text_document_change_t;

json_t * lsp_text_document_change_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "range", offsetof(lsp_text_document_change_t, range), 
				json_pointer_new(sizeof(lsp_text_document_range_t), lsp_text_document_range_j()));
		json_struct_field(j, "rangeLength", offsetof(lsp_text_document_change_t, range_length),
				json_integer_uint64());
		json_struct_field(j, "text", offsetof(lsp_text_document_change_t, text),
				json_string_new(0));
	}
	return j;
}

typedef struct {
	char * uri;
	lsp_text_document_range_t range;
} lsp_text_document_location_t;

json_t * lsp_text_document_location_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "uri", offsetof(lsp_text_document_location_t, uri), json_string_new(0));
		json_struct_field(j, "range", offsetof(lsp_text_document_location_t, range), lsp_text_document_range_j());
	}
	return j;
}

typedef struct {
	char * text;
	char * uri;
	char * languageId;
	unsigned long long version;
} lsp_text_document_t;

json_t * lsp_text_document_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "text", offsetof(lsp_text_document_t, text), json_string_new(0));
		json_struct_field(j, "version", offsetof(lsp_text_document_t, version), json_integer_uint64());
		json_struct_field(j, "uri", offsetof(lsp_text_document_t, uri), json_string_new(0));
		json_struct_field(j, "languageId", offsetof(lsp_text_document_t, languageId), json_string_new(0));
	}
	return j;
}

typedef struct {
	char * label;
	bool preselect;
} lsp_text_document_item_t;

json_t * lsp_text_document_item_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "label", offsetof(lsp_text_document_item_t, label), json_string_new(0));
		json_struct_field(j, "preselect", offsetof(lsp_text_document_item_t, preselect), json_boolean_new());
	}

	return j;
}

typedef struct {
	bool is_incomplete;
	lsp_text_document_item_t * items;
	uint16_t length;
} lsp_text_document_list_t;

json_t * lsp_text_document_list_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "isIncomplete", offsetof(lsp_text_document_list_t, is_incomplete), json_boolean_new());

		json_struct_field(j, "items", offsetof(lsp_text_document_list_t, items),
			json_array_with_length(
				sizeof(lsp_text_document_item_t), lsp_text_document_item_j(),
				offsetof(lsp_text_document_list_t, length) - offsetof(lsp_text_document_list_t, items),
				2, false
			)
		);
	}

	return j;
}

typedef struct {
	lsp_text_document_t document;
	lsp_text_document_position_t position;
} lsp_text_document_position_params_t;

json_t * lsp_text_document_position_params_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "textDocument",
			offsetof(lsp_text_document_position_params_t, document),
			lsp_text_document_j()
		);

		json_struct_field(j, "position",
			offsetof(lsp_text_document_position_params_t, position),
			lsp_text_document_position_j()
		);
	}

	return j;
}
