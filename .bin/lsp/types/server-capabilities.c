#define _package_server_capabilities_

#include "../../deps/json/json.h"
#include "../../deps/json/struct.h"
#include "../../deps/json/integer.h"
#include "../../deps/json/string.h"
#include "../../deps/json/array.h"
#include "../../deps/json/boolean.h"
#include "../../deps/json/pointer.h"

enum server_capabilities_text_document_sync_kind {
	text_document_sync_none = 0,
	text_document_sync_full = 1,
	text_document_sync_incremental = 2,
};

typedef struct {
	bool open_close;
	uint8_t change;
	bool will_save;
	bool will_save_wait_until;
	bool save;
} server_capabilities_text_document_sync_options_t;

json_t * server_capabilities_text_document_sync_options_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "openClose", 
			offsetof(server_capabilities_text_document_sync_options_t, open_close),
			json_boolean_new()
		);

		json_struct_field(j, "change", 
			offsetof(server_capabilities_text_document_sync_options_t, change),
			json_integer_uint8()
		);

		json_struct_field(j, "willSave", 
			offsetof(server_capabilities_text_document_sync_options_t, will_save),
			json_boolean_new()
		);

		json_struct_field(j, "willSaveWaitUntil", 
			offsetof(server_capabilities_text_document_sync_options_t, will_save_wait_until),
			json_boolean_new()
		);

		json_struct_field(j, "save",
			offsetof(server_capabilities_text_document_sync_options_t, save),
			json_boolean_new()
		);
	}

	return j;
}

typedef struct {
	bool resolve_provider;
	char * trigger_characters;
} server_capabilities_completion_options_t;

json_t *	server_capabilities_completion_options_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "resolveProvider",
			offsetof(server_capabilities_completion_options_t, resolve_provider),
			json_boolean_new()
		);

		json_struct_field(j, "triggerCharacters",
			offsetof(server_capabilities_completion_options_t, trigger_characters),
			json_array_dynamic(json_string_char(), 1)
		);
	}

	return j;
}

typedef struct {
	bool hover_provider;
	bool definition_provider;
	bool references_provider;
	bool document_highlight_provider;
	bool document_symbol_provider;
	bool workspace_symbol_provider;
	server_capabilities_text_document_sync_options_t text_document_sync;
	server_capabilities_completion_options_t * completion_provider;
	/*signature_help_options * signature_help_provider;*/
} server_capabilities_t;

json_t * server_capabilities_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();
		json_struct_field(j, "textDocumentSync",
			offsetof(server_capabilities_t, text_document_sync),
			server_capabilities_text_document_sync_options_j()
		);

		json_struct_field(j, "hoverProvider",
			offsetof(server_capabilities_t, hover_provider),
			json_boolean_new()
		);

		json_struct_field(j, "completionProvider",
			offsetof(server_capabilities_t, completion_provider),
			json_pointer_new(
				sizeof(server_capabilities_completion_options_t), server_capabilities_completion_options_j()
			)
		);

		// signatureHelpProvider

		json_struct_field(j, "definitionProvider",
			offsetof(server_capabilities_t, definition_provider),
			json_boolean_new()
		);

		// typeDefinitionProvider
		// implementationProvider

		json_struct_field(j, "referencesProvider",
			offsetof(server_capabilities_t, references_provider),
			json_boolean_new()
		);

		json_struct_field(j, "documentHighlightProvider",
			offsetof(server_capabilities_t, document_highlight_provider),
			json_boolean_new()
		);

		json_struct_field(j, "documentSymbolProvider",
			offsetof(server_capabilities_t, document_symbol_provider),
			json_boolean_new()
		);

		json_struct_field(j, "workspaceSymbolProvider",
			offsetof(server_capabilities_t, workspace_symbol_provider),
			json_boolean_new()
		);

	}
	return j;
}

typedef struct {
	/** The name of the server  as defined by the server */
	char * name;

	/** The server's version as defined by the server. */
	char * version;
} server_capabilities_server_info_t;

json_t * server_capabilities_server_info_j() {
	static json_t * j;
	if (j == NULL) {
		j = json_struct_new();

		json_struct_field(j, "name",
			offsetof(server_capabilities_server_info_t, name),
			json_string_new(0)
		);

		json_struct_field(j, "version",
			offsetof(server_capabilities_server_info_t, version),
			json_string_new(0)
		);

	}
	return j;
}
