#define _package_type_initialize_params_

#include <stdint.h>
#include "../../deps/json/json.h"
#include "../../deps/json/struct.h"
#include "../../deps/json/integer.h"
#include "../../deps/json/string.h"
#include "../../deps/json/array.h"
#include "../../deps/json/boolean.h"
#include "../../deps/json/pointer.h"

/**
 * Partial implementation of InitializeParameters from 
 * https://microsoft.github.io/language-server-protocol/specification#initialize.
 */
typedef struct {
	/**
	 * The process Id of the parent process that started
	 * the server. Is 0 if the process has not been started by another process.
	 * If the parent process is not alive then the server should exit (see exit notification) its process.
	 */
	int64_t process_id;

	/**
	 * The rootPath of the workspace. Is null
	 * if no folder is open.
	 *
	 * @deprecated in favour of root_uri.
	 */
	char * root_path;

	/**
	 * The rootUri of the workspace. Is null if no
	 * folder is open. If both `rootPath` and `rootUri` are set
	 * `rootUri` wins.
	 */
	char * root_uri;

} type_initialize_params_t;

json_t * type_initialize_params_j() {
	static json_t * j;

	if (j == NULL) {
		j = json_struct_new();

		json_struct_field(j, "processId", 
			offsetof(type_initialize_params_t, process_id),
			json_integer_int64()
		);

		json_struct_field(j, "rootPath", 
			offsetof(type_initialize_params_t, root_path),
			json_string_new(0)
		);

		json_struct_field(j, "rootUri", 
			offsetof(type_initialize_params_t, root_uri),
			json_string_new(0)
		);
	}

	return j;
}
