#ifndef _package_type_initialize_params_
#define _package_type_initialize_params_

#include "../../deps/json/json.h"
#include <stdint.h>

/**
 * Partial implementation of InitializeParameters from 
 * https://microsoft.github.io/language-server-protocol/specification#initialize.
 */
typedef struct {
	/**
	 * The process Id of the parent process that started
	 * the server. Is 0 if the process has not been started by another process.
	 * If the parent process is not alive then the server should exit (see exit notification) its process.
	 */
	int64_t process_id;

	/**
	 * The rootPath of the workspace. Is null
	 * if no folder is open.
	 *
	 * @deprecated in favour of root_uri.
	 */
	char * root_path;

	/**
	 * The rootUri of the workspace. Is null if no
	 * folder is open. If both `rootPath` and `rootUri` are set
	 * `rootUri` wins.
	 */
	char * root_uri;

} type_initialize_params_t;

json_t * type_initialize_params_j();

#endif
