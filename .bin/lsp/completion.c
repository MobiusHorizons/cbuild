#define _package_completion_
#include <ctype.h>

#include "../deps/json/json.h"
#include "../deps/json/value.h"

#include "../package/index.h"
#include "../package/import.h"
#include "../package/export.h"
#include "../package/package.h"
#include "../errors/errors.h"
#include "../utils/string-stream.h"

#include "types/completion.h"

#include "rpc/server.h"
#include "rpc/encode.h"
#include "rpc/request_msg.h"
#include "rpc/response_msg.h"

#include "state.h"

void * completion_handler(void * _ctx, void * _arg) {
	state_t * s = (state_t*) _ctx;
	json_rpc_server_handler_args_t * args = (json_rpc_server_handler_args_t*)_arg;

	completion_types_params_t params = {0};
	json_unmarshal_ex(completion_types_params_j(), &params, args->req.params.parsed);
	const char * line = s->input_buffer;
	int i = 0;
	for (i = 0; line >= s->input_buffer && i < params.position.line; i++) {
		line = strchr(line, '\n') + 1;
	}

	const char * end = line + params.position.character;
	const char * sep = end;
	char * ident = NULL;
	const char * c = end;
	while(c > line && *c != '.') c--;

	if (c > line) {
		sep = c;
	}

	while(c >= line && c-- && (isalnum(*c) || *c == '_'));
	c++;

	if (c >= line) {
		ident = malloc(sep - c + 1);
		strncpy(ident, c, sep - c);
		ident[sep - c] = 0;
	}

	char * filter = "";
	size_t filter_len = 0;

	if (end > sep) {
		filter_len = end - sep - 1;
		filter = malloc(end - sep);
		strncpy(filter, sep + 1, filter_len);
		filter[filter_len] = 0;
	}

	/*fprintf(stderr, "(%d)> <%s>.(%s)\n", */
		/*params.context.kind, */
		/*ident,*/
		/*filter*/
	/*);*/

	completion_types_list_t result = {0};

	package_import_t * imp = hash_get(s->pkg->deps, ident);
	if (imp && imp->pkg) {
		/*fprintf(stderr, "Package %s\n", imp->pkg->name);*/

		result.items = malloc(sizeof(completion_types_item_t) * imp->pkg->n_exports);
		result.is_incomplete = true;

		int i;
		for (i = 0; i < imp->pkg->n_exports; i++){
			package_export_t * exp = (package_export_t *) imp->pkg->ordered[i];
			if (exp->type == type_header || exp->type == type_block) continue;
			if (strncmp(filter, exp->export_name, filter_len) != 0) continue;

			result.items[result.length].label = exp->export_name;
			result.items[result.length].preselect = result.length == 0;
			result.length++;
			/*fprintf(stderr, "  -> %s\n", exp->export_name);*/
		}
		/*fprintf(stderr, "(%d)\n", result.length);*/
	}

	json_rpc_server_response(args, json_value_from(completion_types_list_j(), &result));
	return NULL;
}
