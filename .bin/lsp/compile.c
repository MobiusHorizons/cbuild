#define _package_compile_
#include <stdio.h>

#include "../deps/json/json.h"
#include "../deps/json/value.h"
#include "../deps/stream/stream.h"

#include "../package/index.h"
#include "../package/import.h"
#include "../package/export.h"
#include "../package/package.h"
#include "../errors/errors.h"
#include "../utils/string-stream.h"

#include "rpc/server.h"

#include "types/diagnostic.h"

#include "state.h"

static diagnostic_t convert_error(errors_error_t e) {
	diagnostic_t d = {
		.range = {
			.start = {
				.line      = e.context.start.line,
				.character = e.context.start.character,
			},
			.end = {
				.line      = e.context.end.line,
				.character = e.context.end.character,
			},
		},
		.severity = e.severity,
		.source   = "cbuild",
		.message  = e.message,
	};

	return d;
}

static void publish_cbuild_errors(char * uri, errors_t * errors, state_t * s) {
	diagnostic_publish_diagnostic_t params = {
		.uri   = uri,
		.count = 0,
	};
	params.diagnostics = malloc(sizeof(diagnostic_t) * (errors->count));

	char * filename = uri + strlen("file://");

	int i;
	for (i = 0; i < errors->count; i++) {
	/*printf("%s => %s", filename, errors->errors[i].context.filename);*/
		/*if (strcmp(filename, errors->errors[i].context.filename) == 0) {*/
			params.diagnostics[params.count++] = convert_error(errors->errors[i]);
		/*}*/
	}

	json_rpc_server_notify(s->lsp, "textDocument/publishDiagnostics", json_value_from(
		diagnostic_publish_diagnostic_j(),
		&params
	));
}

char * compile_check(char * uri, char * text, state_t * s) {
	fprintf(stderr, "I: checking '%s'\n", uri);
	static int invocation;
	s->input_buffer = text;
	stream_t * in  = string_stream_new_reader(text);
	stream_t * out = string_stream_new_writer(&s->compiled);
	errors_t errors = {0};

	char * relative = uri;
	/**
	if (strncmp(relative, "file://", 7) == 0) {
		relative += strlen("file://");
	}
	*/
	if (strncmp(relative, s->root_uri, strlen(s->root_uri)) == 0) {
		relative += strlen(s->root_uri);
	}

	fprintf(stderr, "I: Pkg.new('%s', ...)\n", relative);
	if (s->pkg) {
		index_free(s->pkg);
	}
	s->pkg = index_new(relative, &errors, s->opts);
	if (s->pkg) {
		s->pkg->out = out;
		index_parse(s->pkg, in, relative, &errors);
		index_resolve(s->pkg, &errors);
	}

	//s->compiled = StringStream.get_buffer(out);
	/*char * buf = StringStream.get_buffer(out);*/
	stream_printf(s->err, 
		  "\n===================(%s)====================\n"
		"%s\n===========================================\n",
		s->pkg->generated, s->compiled
	);

	/*Pkg.free(p);*/

	publish_cbuild_errors(uri, &errors, s);
	return s->pkg ? s->pkg->generated : NULL;
}
