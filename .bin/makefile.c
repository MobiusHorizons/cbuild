#define _package_makefile_

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/wait.h>
#include "utils/colors.h"


#include "deps/hash/hash.h"

#include "package/package.h"
#include "package/export.h"
#include "package/import.h"
#include "package/atomic-stream.h"
#include "utils/utils.h"
#include "utils/fs.h"
#include "deps/stream/stream.h"

static const char * ops[] = {
	":=",
	"?=",
	"+=",
};

typedef struct {
	char * makefile;
	char * makefile_dir;
	char * makefile_base;
	char * target;
	char * build_target;
	char * cwd;
} makevars;

char * makefile_get_target(package_t * pkg) {
	char * target = strdup(basename(pkg->generated));

	char * ext = strrchr(target, '.');
	if (strcmp(pkg->name, "main") == 0) {
		*ext = 0;
	} else {
		ext[1] = 'a';
	}

	return target;
}

char * makefile_get_build_target(package_t * pkg) {
	char * target = makefile_get_target(pkg);
	char * build_target = NULL;

	if (pkg->target) {
		char * base = index(pkg->target, '%');
		char * ext = strrchr(target, '.');
		if (base) {
			asprintf(
				&build_target,
				"%.*s%.*s%s", 
				(int)(base - pkg->target), pkg->target,
				(int)(ext - target), target,
				base + 1
			);
		} else {
			asprintf(
				&build_target,
				"%.*s__%s", 
				(int)(ext - target), target,
				pkg->target
			);
		}
		free(target);
	} else {
		build_target = target;
	}
	
	return build_target;
}

static makevars get_makevars(package_t * pkg, char * makefile) {
	char * buf = strdup(makefile);

	makevars v = {
		.makefile      = makefile,
		.makefile_base = strdup(basename(makefile)),
		.makefile_dir  = strdup(dirname(buf)),
		.target        = makefile_get_target(pkg),
		.build_target  = makefile_get_build_target(pkg),
		.cwd           = utils_getcwd(),
	};

	free(buf);

	chdir(v.makefile_dir);
	return v;
}


static int clear_makevars(makevars v, int result, char * cmd) {
	chdir(v.cwd);

	free(v.cwd);
	free(v.target);
	free(v.build_target);
	free(v.makefile_dir);
	free(v.makefile_base);
	free(v.makefile);
	free(cmd);

	if (result == 0 || result == -1) return result;
	if (result == 127) return -1;
	return WEXITSTATUS(result);
}

static int run_make(makevars v, char * cmd) {
	setenv("BUILD", v.makefile_dir, 1);
	return clear_makevars(v, system(cmd), cmd);
}

int makefile_make(package_t * pkg, char * makefile) {
	if (pkg == NULL) return -1;
	makevars v = get_makevars(pkg, makefile);

	char * cmd;
	asprintf(&cmd, "make -f %s %s", v.makefile_base, v.build_target);

	return run_make(v, cmd);
}

int makefile_run(package_t * pkg, char * makefile, const char * target) {
	if (pkg == NULL) return -1;
	makevars v = get_makevars(pkg, makefile);

	setenv("BUILD", v.makefile_dir, 1);

	char * pkg_name = NULL;
	if (strcmp(pkg->name, "main") == 0) {
		pkg_name = strdup(basename(pkg->generated));
		char * ext = strrchr(pkg_name, '.');
		*ext = 0;
	} else {
		pkg_name = strdup(pkg->name);
	}

	char * cmd;
	asprintf(&cmd, "make -f %s %s__%s", v.makefile_base, pkg_name, target);
 
	return clear_makevars(v, system(cmd), cmd);
}

int makefile_clean(package_t * pkg, char * makefile) {
	return makefile_run(pkg, makefile, "CLEAN");
}

static char * write_deps(package_t * pkg, package_t * root, stream_t * out, char * deps) {
	if (pkg == NULL || pkg->status == PKG_EXPORTED) return deps;
	pkg->status = PKG_EXPORTED;

	char * source = utils_relative(root->generated, pkg->generated);
	char * object = strdup(source);
	object[strlen(source) - 1] = 'o';

	size_t len = deps == NULL ? 0 : strlen(deps);
	deps = realloc(deps, len + strlen(object) + 2);
	sprintf(deps + len, " %s", object);

	stream_printf(out, "#dependencies for package '%s'\n", source);

	int i;
	for (i = 0; i < pkg->n_variables; i++) {
		package_var_t v = pkg->variables[i];
		stream_printf(out, "%s %s %s\n", v.name, ops[v.operation], v.value);
	}

	stream_printf(out, "%s: %s", object, source);
	free(source);
	free(object);

	if (pkg->deps == NULL) {
		stream_printf(out,"\n\n");
		return deps;
	}

	hash_each_val(pkg->deps, {
			package_import_t * dep = (package_import_t *) val;
			if (dep && dep->pkg && dep->pkg->header) {
				char * path = utils_relative(root->generated, dep->pkg->header);
				stream_printf(out, " %s", path);
				free(path);
			}
	});
	stream_printf(out,"\n\n");

	hash_each_val(pkg->deps, {
			package_import_t * dep = (package_import_t *)val;
			deps = write_deps(dep->pkg, root, out, deps);
	});

	return deps;
}

char * makefile_get_makefile_name(const char * path) {
	size_t len = strlen(path);
	size_t name_l = len - strlen(".c") + strlen(".mk") + 1;
	char * name = malloc(name_l);
	snprintf(name, name_l, "%.*s.mk", (int)(len - strlen(".c")), path);
	return name;
}

static void write_mkfile_deps(package_t * p, stream_t * mkfile, char * filename, hash_t * seen) {
	if (p == NULL || p->deps == NULL) return;
	if (hash_has(seen, p->source_abs)) return;

	char * rel = utils_relative(filename, p->source_abs);
	stream_write(mkfile, " ", 1);
	stream_write(mkfile, rel, strlen(rel));
	free(rel);
	hash_set(seen, p->source_abs, p);

	hash_each_val(p->deps, {
			package_import_t * dep = (package_import_t *)val;
			write_mkfile_deps(dep->pkg, mkfile, filename, seen);
	})
}


char * makefile_write(package_t * pkg, const char * cbuild_exec) {
	char * target = makefile_get_target(pkg);
	char * mkfile_name = makefile_get_makefile_name(pkg->generated);
	stream_t * mkfile = atomic_stream_open(mkfile_name);

	char * deps = write_deps(pkg, pkg, mkfile, NULL);

	if (strcmp(pkg->name, "main") == 0) {
		stream_printf(mkfile, "%s:%s\n", target, deps);
		stream_printf(mkfile, "\t$(CC) $(CFLAGS) $(LDFLAGS) %s -o %s $(LDLIBS)\n\n", deps, target);
	} else {
		char * buf = strdup(pkg->generated);
		char * base = basename(buf);
		asprintf(&target, "%.*s.a", (int)strlen(base) - 2, base);
		free(buf);

		stream_printf(mkfile, "%s:%s\n",target, deps);
		stream_printf(mkfile, "\tar rcs $@ $^\n\n");
	}

	int i;
	for(i = 0; i < pkg->n_rules; i++) {
		package_rule_t r = pkg->rules[i];
		if (r.is_generic) {
			stream_printf(mkfile, "%s: %s\n",
					r.target,
					r.deps ?: "");
		} else {
			stream_printf(mkfile, "%s__%s: %s%s%s\n",
					target, r.target,
					target, r.deps ? " " : "", r.deps ?: "");
		}
		stream_printf(mkfile, "%s\n", r.recipe);
	}

	stream_printf(mkfile, "%s__CLEAN:\n", target);
	stream_printf(mkfile, "\trm -rf %s%s\n\n", target, deps);
	free(target);

	{ // Write the makefile's dependencies into the makefile so it can regenerate itself.
		stream_printf(mkfile, "#Recipe to rebuild %s:\n", basename(mkfile_name));
		stream_printf(mkfile, "%s:", basename(mkfile_name));
		hash_t * deps_cache = hash_new();
		write_mkfile_deps(pkg, mkfile, mkfile_name, deps_cache);
		hash_free(deps_cache);

		char * root_rel = utils_relative(mkfile_name, pkg->source_abs);
		stream_printf(mkfile, "\n\t%s generate -o . %s\n", cbuild_exec, root_rel);

		free(root_rel);
	}

	stream_close(mkfile);
	free(deps);

	return mkfile_name;
}
