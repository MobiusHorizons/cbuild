#define _package_fs_
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#define SEP '/'
#define ESCAPE '\\'

static ssize_t prev_sep(const char * path, size_t offset, char sep, char escape) {
	while(offset > 0) {
		if (path[offset] == sep) {
			if (offset > 0 && path[offset - 1] == escape) { // this was an escaped separator
				offset--;
				continue;
			}
			return offset;
		}
		offset--;
	}
	return -1;
}

char * fs_basename(char * path) {
	size_t end = strlen(path);
	ssize_t split = prev_sep(path, end, SEP, ESCAPE);
	if (split == end) {
		path[split] = 0;
		split = prev_sep(path, end - 1, SEP, ESCAPE);
	}

	if (split == -1) return path;
	return &path[split];
}

char * fs_dirname(char * path) {
	if (path == NULL) return NULL;
	size_t end = strlen(path);
	ssize_t split = prev_sep(path, end, SEP, ESCAPE);
	if (split == end) split = prev_sep(path, end - 1, SEP, ESCAPE);

	if (split == -1) {
		return ".";
	}

	path[split] = 0;
	return path;
}

/**
 * mkdir_recursive(path) takes an absolute or relative path, and creates the directory
 * even if it has to create parent directories.
 *
 * it returns a null terminated string with the absolute path of the created directory.
 */
int fs_mkdir_recursive(const char * path) {
	char * p = strdup(path);
	int levels = 0;

	while(mkdir(p, 0777) == -1) {
		if (errno == EEXIST) {
			free(p);
			return 0;
		}
		if (errno != ENOENT) return -1;
		p = fs_dirname(p);
		if (p[0] == '.' && p[1] == 0) return -1;
		levels ++;
	}

	size_t l = 0;
	while(levels > 0) {
		l += strlen(p + l);
		p[l] = SEP;
		if (mkdir(p, 0777) == -1) return -1;
		levels --;
	}
	free(p);
	return 0;
}
