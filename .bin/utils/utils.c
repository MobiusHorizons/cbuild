#define _package_utils_
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#define SEP '/'

static int count_levels(const char * path){
	int i = 0, levels = 0;
	bool escaped = false;

	while(path[i] != '\0'){
		if (escaped) {
			escaped = false;
			continue;
		}
		if (path[i] == '\\') escaped = true;
		if (path[i] == SEP) levels++;
		i++;
	}

	return levels;
}

char * utils_getcwd() {
	size_t s = 512 / 2;
	char * cwd = NULL;
	do {
		s *= 2;
		cwd = malloc(s);
	} while (getcwd(cwd, s - 1) == NULL);

	strcat(cwd, "/");
	return cwd;
}

static char * rel(const char * from, const char * to, int is_executable) {

	int last_sep = 0;

	{ // Determine shared prefix.
		int i = 0;
		while(from[i] != 0 && to[i] != 0 && from[i] == to[i]){
			if (from[i] == SEP){
				last_sep = i;
			}
			i++;
		}
	}

	int dots = count_levels(&from[last_sep +1]);

	char * rel = malloc((dots == 0 ? 2 : (dots * 3)) + strlen(&to[last_sep]) + 1);
	rel[0] = 0;
	if (dots == 0 && is_executable){
			strcpy(rel, "./");
	}
	while (dots--){
		strcat(rel, "../");
	}
	strcat(rel, &to[last_sep + 1]);
	return rel;
}

/**
 * Gets a relative path between two absolute paths.
 * This version does not prepend './' if the files are in the same directory.
 *
 * If that behavior is desired, use relative_exe
 */
char * utils_relative(const char * from, const char * to) {
	return rel(from, to, 0);
}

/**
 * Gets a relative path between two absolute paths.
 * This version will prepend './' for files in the same directory as is necessary 
 * with executables.
 *
 * If no './' is desired, use relative
 */
char * utils_relative_exe(const char * from, const char * to) {
	return rel(from, to, 1);
}

int utils_is_absolute(const char * path) {
	return path[0] == '/';
}

int utils_is_relative(const char * path) {
	// TODO: handle escaped forward slashes
	return index(path, '/') != NULL;
}

/**
 * Gets the path to the currently running executable based on the invocation name.
 *
 * @param: argv_0 invoked program name.
 *
 * If the invocation was an absolute path or relative path, that path will be return
 * *NOTE*: In order to correctly resolve relative path names, this should
 * be run with the same CWD that the program had when it was launched.
 *
 * If the executable could not be found, the function returns NULL.
 */
char * utils_exec_name(const char * argv_0) {
	if (utils_is_absolute(argv_0)) {
		return strdup(argv_0);
	}

	if (utils_is_relative(argv_0)) {
		return realpath(argv_0, NULL);
	}

	// argv_0 sohuld be in PATH;
	char * path = getenv("PATH");
	if (path == NULL) return NULL;
	path = strdup(path);
	char * buf = malloc(strlen(path) + strlen(argv_0) + 2);

	char * p = path;
	char * e = NULL;
	do {
		e = index(p, ':');
		if (e) *e = 0;

		strcpy(buf, p);
		strcat(buf, "/");
		strcat(buf, argv_0);

		if (access(buf, X_OK) == 0) {
			char * abs = realpath(buf, NULL);
			free(path);
			free(buf);
			return abs;
		}
		p = e+1;
	} while(e != NULL);

	free(path);
	free(buf);
	return NULL;
}

bool utils_newer(const char * a, const char * b) {
	struct stat sta;
	struct stat stb;

	int resa = lstat(a, &sta);
	if (resa == -1) return false;

	int resb = lstat(b, &stb);
	if (resb == -1) return true;


#ifdef __MACH__
	if (sta.st_mtimespec.tv_sec == stb.st_mtimespec.tv_sec) {
		return sta.st_mtimespec.tv_nsec > stb.st_mtimespec.tv_nsec;
	}
	return sta.st_mtimespec.tv_sec > stb.st_mtimespec.tv_sec;
#else
	if (sta.st_mtim.tv_sec == stb.st_mtim.tv_sec) {
		return sta.st_mtim.tv_nsec > stb.st_mtim.tv_nsec;
	}
	return sta.st_mtim.tv_sec > stb.st_mtim.tv_sec;
#endif
}
