#define _package_strings_
#include <string.h>
#include <stdlib.h>

char * strings_dup(const char * value) {
	if (value == NULL) {
		return NULL;
	} else {
		return strdup(value);
	}
}

int strings_cmp(char * a, char * b) {
	if (a == b) return 0;
	if (a == NULL) return 1;
	if (b == NULL) return -1;
	return strcmp(a, b);
}

size_t strings_len(const char * value) {
	if (value == NULL) {
		return 0;
	} else {
		return strlen(value);
	}
}

