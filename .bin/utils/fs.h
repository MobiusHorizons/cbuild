#ifndef _package_fs_
#define _package_fs_

char * fs_basename(char * path);
char * fs_dirname(char * path);

/**
 * mkdir_recursive(path) takes an absolute or relative path, and creates the directory
 * even if it has to create parent directories.
 *
 * it returns a null terminated string with the absolute path of the created directory.
 */
int fs_mkdir_recursive(const char * path);

#endif
