#ifndef _package_strings_
#define _package_strings_

#include <stdlib.h>

char * strings_dup(const char * value);
int strings_cmp(char * a, char * b);
size_t strings_len(const char * value);

#endif
