#ifndef _package_utils_
#define _package_utils_

#include <stdbool.h>

char * utils_getcwd();

/**
 * Gets a relative path between two absolute paths.
 * This version does not prepend './' if the files are in the same directory.
 *
 * If that behavior is desired, use relative_exe
 */
char * utils_relative(const char * from, const char * to);

/**
 * Gets a relative path between two absolute paths.
 * This version will prepend './' for files in the same directory as is necessary 
 * with executables.
 *
 * If no './' is desired, use relative
 */
char * utils_relative_exe(const char * from, const char * to);

int utils_is_absolute(const char * path);
int utils_is_relative(const char * path);

/**
 * Gets the path to the currently running executable based on the invocation name.
 *
 * @param: argv_0 invoked program name.
 *
 * If the invocation was an absolute path or relative path, that path will be return
 * *NOTE*: In order to correctly resolve relative path names, this should
 * be run with the same CWD that the program had when it was launched.
 *
 * If the executable could not be found, the function returns NULL.
 */
char * utils_exec_name(const char * argv_0);

bool utils_newer(const char * a, const char * b);

#endif
