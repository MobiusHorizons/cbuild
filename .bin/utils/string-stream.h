#ifndef _package_string_stream_
#define _package_string_stream_

#include "../deps/stream/stream.h"
#include <fcntl.h>

int string_stream_type();
stream_t * string_stream_new_reader(const char * input);
stream_t * string_stream_new_writer(char ** buf);
char * string_stream_get_buffer(stream_t * s);
size_t string_stream_get_buffer_length(stream_t * s);

#endif
