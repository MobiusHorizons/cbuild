#ifndef _package_errors_
#define _package_errors_

#include "../deps/stream/stream.h"
#include <stdlib.h>
#include <stdbool.h>

enum errors_severity_t {
	ERROR = 1,
	WARNING,
	INFO,
	HINT,
};

typedef struct {
	long long line;
	long long character;
} errors_position_t ;

typedef struct {
	char * filename;
	char * text;
	errors_position_t start;
	errors_position_t end;
} errors_context_t;

typedef struct {
	enum errors_severity_t severity;
	bool is_fatal;
	char * message;
	errors_context_t context;
} errors_error_t;

typedef struct {
	errors_error_t * errors;
	size_t count;
} errors_t;

int errors_append(errors_t * e, errors_error_t error);
size_t errors_print(errors_error_t e, stream_t * s, char * cwd);
void errors_free(errors_error_t * e);
void errors_free_all(errors_t e);

#endif
