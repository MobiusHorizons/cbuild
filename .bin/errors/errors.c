#define _package_errors_

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "../utils/colors.h"

#include "../deps/stream/stream.h"
#include "../utils/utils.h"

enum errors_severity_t {
	ERROR = 1,
	WARNING,
	INFO,
	HINT,
};

typedef struct {
	long long line;
	long long character;
} errors_position_t ;

typedef struct {
	char * filename;
	char * text;
	errors_position_t start;
	errors_position_t end;
} errors_context_t;

typedef struct {
	enum errors_severity_t severity;
	bool is_fatal;
	char * message;
	errors_context_t context;
} errors_error_t;

typedef struct {
	errors_error_t * errors;
	size_t count;
} errors_t;

int errors_append(errors_t * e, errors_error_t error) {
	void * resized = realloc(e->errors, sizeof(errors_error_t) * (e->count + 1));
	if (resized == NULL) return -1;

	e->errors = resized;
	e->errors[e->count] = error;
	e->count++;
	return e->count;
}

size_t errors_print(errors_error_t e, stream_t * s, char * cwd) {
	size_t ret = 0;
	if (e.context.filename) {
		char * rel = utils_relative(cwd, e.context.filename);
		ret += stream_printf(s, BOLD "%s:%ld:%d: " RESET, 
			rel,
			e.context.start.line + 1, 
			e.context.start.character + 1
		);
		free(rel);
	}

	switch (e.severity) {
		case HINT:
			break;
		case ERROR:
			ret += stream_printf(s, BOLDRED "error: " RESET);
			break;
		case WARNING:
			ret += stream_printf(s, BOLDMAGENTA "warning: " RESET);
			break;
		case INFO:
			ret += stream_printf(s, BOLDBLUE "note: " RESET);
			break;
	}

	ret += stream_printf(s, BOLD "%s\n" RESET, e.message);

	if (e.context.text == NULL) return 1;

	ret += stream_printf(s, "%s\n", e.context.text); // TODO: split this to allow multiple lines.

	char offset[255];
	int i;
	for (i = 0; i < e.context.start.character; i++) {
		switch (e.context.text[i]) {
			case '\t':
				offset[i] = '\t';
				break;
			default:
				offset[i] = ' ';
				break;
		}
	}
	offset[i] = 0;

	ret += stream_printf(s, GREEN "%s^\n" RESET, offset);
	return ret;
}

void errors_free(errors_error_t * e) {
	free(e->message);
	free(e->context.filename);
	free(e->context.text);
}

void errors_free_all(errors_t e) {
	size_t i;
	for (i = 0; i < e.count; i++) {
		errors_free(&(e.errors[i]));
	}
	free(e.errors);
}

