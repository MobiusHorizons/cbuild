package "makefile";

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/wait.h>
#include "utils/colors.h"

build depends "deps/hash/hash.c";
#include "deps/hash/hash.h"

import Package    from "package/package.module.c";
import pkg_export from "package/export.module.c";
import pkg_import from "package/import.module.c";
import atomic     from "package/atomic-stream.module.c";
import utils      from "utils/utils.module.c";
import fs         from "utils/fs.module.c";
import stream     from "deps/stream/stream.module.c";

static const char * ops[] = {
	":=",
	"?=",
	"+=",
};

typedef struct {
	char * makefile;
	char * makefile_dir;
	char * makefile_base;
	char * target;
	char * build_target;
	char * cwd;
} makevars;

export char * get_target(Package.t * pkg) {
	char * target = strdup(basename(pkg->generated));

	char * ext = strrchr(target, '.');
	if (strcmp(pkg->name, "main") == 0) {
		*ext = 0;
	} else {
		ext[1] = 'a';
	}

	return target;
}

export char * get_build_target(Package.t * pkg) {
	char * target = get_target(pkg);
	char * build_target = NULL;

	if (pkg->target) {
		char * base = index(pkg->target, '%');
		char * ext = strrchr(target, '.');
		if (base) {
			asprintf(
				&build_target,
				"%.*s%.*s%s", 
				(int)(base - pkg->target), pkg->target,
				(int)(ext - target), target,
				base + 1
			);
		} else {
			asprintf(
				&build_target,
				"%.*s__%s", 
				(int)(ext - target), target,
				pkg->target
			);
		}
		global.free(target);
	} else {
		build_target = target;
	}
	
	return build_target;
}

makevars get_makevars(Package.t * pkg, char * makefile) {
	char * buf = strdup(makefile);

	makevars v = {
		.makefile      = makefile,
		.makefile_base = strdup(basename(makefile)),
		.makefile_dir  = strdup(dirname(buf)),
		.target        = get_target(pkg),
		.build_target  = get_build_target(pkg),
		.cwd           = utils.getcwd(),
	};

	global.free(buf);

	chdir(v.makefile_dir);
	return v;
}


int clear_makevars(makevars v, int result, char * cmd) {
	chdir(v.cwd);

	free(v.cwd);
	free(v.target);
	free(v.build_target);
	free(v.makefile_dir);
	free(v.makefile_base);
	free(v.makefile);
	free(cmd);

	if (result == 0 || result == -1) return result;
	if (result == 127) return -1;
	return WEXITSTATUS(result);
}

int run_make(makevars v, char * cmd) {
	setenv("BUILD", v.makefile_dir, 1);
	return clear_makevars(v, system(cmd), cmd);
}

export int make(Package.t * pkg, char * makefile) {
	if (pkg == NULL) return -1;
	makevars v = get_makevars(pkg, makefile);

	char * cmd;
	asprintf(&cmd, "make -f %s %s", v.makefile_base, v.build_target);

	return run_make(v, cmd);
}

export int run(Package.t * pkg, char * makefile, const char * target) {
	if (pkg == NULL) return -1;
	makevars v = get_makevars(pkg, makefile);

	setenv("BUILD", v.makefile_dir, 1);

	char * pkg_name = NULL;
	if (strcmp(pkg->name, "main") == 0) {
		pkg_name = strdup(basename(pkg->generated));
		char * ext = strrchr(pkg_name, '.');
		*ext = 0;
	} else {
		pkg_name = strdup(pkg->name);
	}

	char * cmd;
	asprintf(&cmd, "make -f %s %s__%s", v.makefile_base, pkg_name, target);
 
	return clear_makevars(v, system(cmd), cmd);
}

export int clean(Package.t * pkg, char * makefile) {
	return run(pkg, makefile, "CLEAN");
}

char * write_deps(Package.t * pkg, Package.t * root, stream.t * out, char * deps) {
	if (pkg == NULL || pkg->status == PKG_EXPORTED) return deps;
	pkg->status = PKG_EXPORTED;

	char * source = utils.relative(root->generated, pkg->generated);
	char * object = strdup(source);
	object[strlen(source) - 1] = 'o';

	size_t len = deps == NULL ? 0 : strlen(deps);
	deps = realloc(deps, len + strlen(object) + 2);
	sprintf(deps + len, " %s", object);

	stream.printf(out, "#dependencies for package '%s'\n", source);

	int i;
	for (i = 0; i < pkg->n_variables; i++) {
		Package.var_t v = pkg->variables[i];
		stream.printf(out, "%s %s %s\n", v.name, ops[v.operation], v.value);
	}

	stream.printf(out, "%s: %s", object, source);
	global.free(source);
	global.free(object);

	if (pkg->deps == NULL) {
		stream.printf(out,"\n\n");
		return deps;
	}

	hash_each_val(pkg->deps, {
			pkg_import.t * dep = (pkg_import.t *) val;
			if (dep && dep->pkg && dep->pkg->header) {
				char * path = utils.relative(root->generated, dep->pkg->header);
				stream.printf(out, " %s", path);
				global.free(path);
			}
	});
	stream.printf(out,"\n\n");

	hash_each_val(pkg->deps, {
			pkg_import.t * dep = (pkg_import.t *)val;
			deps = write_deps(dep->pkg, root, out, deps);
	});

	return deps;
}

export char * get_makefile_name(const char * path) {
	size_t len = strlen(path);
	size_t name_l = len - strlen(".c") + strlen(".mk") + 1;
	char * name = malloc(name_l);
	snprintf(name, name_l, "%.*s.mk", (int)(len - strlen(".c")), path);
	return name;
}

void write_mkfile_deps(Package.t * p, stream.t * mkfile, char * filename, hash_t * seen) {
	if (p == NULL || p->deps == NULL) return;
	if (hash_has(seen, p->source_abs)) return;

	char * rel = utils.relative(filename, p->source_abs);
	stream.write(mkfile, " ", 1);
	stream.write(mkfile, rel, strlen(rel));
	global.free(rel);
	hash_set(seen, p->source_abs, p);

	hash_each_val(p->deps, {
			pkg_import.t * dep = (pkg_import.t *)val;
			write_mkfile_deps(dep->pkg, mkfile, filename, seen);
	})
}


export char * write(Package.t * pkg, const char * cbuild_exec) {
	char * target = get_target(pkg);
	char * mkfile_name = get_makefile_name(pkg->generated);
	stream.t * mkfile = atomic.open(mkfile_name);

	char * deps = write_deps(pkg, pkg, mkfile, NULL);

	if (strcmp(pkg->name, "main") == 0) {
		stream.printf(mkfile, "%s:%s\n", target, deps);
		stream.printf(mkfile, "\t$(CC) $(CFLAGS) $(LDFLAGS) %s -o %s $(LDLIBS)\n\n", deps, target);
	} else {
		char * buf = strdup(pkg->generated);
		char * base = basename(buf);
		asprintf(&target, "%.*s.a", (int)strlen(base) - 2, base);
		free(buf);

		stream.printf(mkfile, "%s:%s\n",target, deps);
		stream.printf(mkfile, "\tar rcs $@ $^\n\n");
	}

	int i;
	for(i = 0; i < pkg->n_rules; i++) {
		Package.rule_t r = pkg->rules[i];
		if (r.is_generic) {
			stream.printf(mkfile, "%s: %s\n",
					r.target,
					r.deps ?: "");
		} else {
			stream.printf(mkfile, "%s__%s: %s%s%s\n",
					target, r.target,
					target, r.deps ? " " : "", r.deps ?: "");
		}
		stream.printf(mkfile, "%s\n", r.recipe);
	}

	stream.printf(mkfile, "%s__CLEAN:\n", target);
	stream.printf(mkfile, "\trm -rf %s%s\n\n", target, deps);
	free(target);

	{ // Write the makefile's dependencies into the makefile so it can regenerate itself.
		stream.printf(mkfile, "#Recipe to rebuild %s:\n", basename(mkfile_name));
		stream.printf(mkfile, "%s:", basename(mkfile_name));
		hash_t * deps_cache = hash_new();
		write_mkfile_deps(pkg, mkfile, mkfile_name, deps_cache);
		hash_free(deps_cache);

		char * root_rel = utils.relative(mkfile_name, pkg->source_abs);
		stream.printf(mkfile, "\n\t%s generate -o . %s\n", cbuild_exec, root_rel);

		global.free(root_rel);
	}

	stream.close(mkfile);
	free(deps);

	return mkfile_name;
}
