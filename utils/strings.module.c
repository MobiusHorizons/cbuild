#include <string.h>
export #include <stdlib.h>

export char * dup(const char * value) {
	if (value == NULL) {
		return NULL;
	} else {
		return strdup(value);
	}
}

export int cmp(char * a, char * b) {
	if (a == b) return 0;
	if (a == NULL) return 1;
	if (b == NULL) return -1;
	return strcmp(a, b);
}

export size_t len(const char * value) {
	if (value == NULL) {
		return 0;
	} else {
		return strlen(value);
	}
}

